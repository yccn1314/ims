package edu.sias.ims.student.mapper;

import edu.sias.ims.student.domain.dto.ExportDto;

import java.util.List;

public interface ExportMapper {
    List<ExportDto> selectByTargat(ExportDto exportDto);
}
