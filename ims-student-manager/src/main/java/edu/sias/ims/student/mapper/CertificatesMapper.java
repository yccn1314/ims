package edu.sias.ims.student.mapper;

import edu.sias.ims.student.domain.Certificates;
import edu.sias.ims.student.domain.vo.CertificatesVo;

import java.util.List;


/**
 * 志愿者证书管理Mapper接口
 *
 * @author wenchao
 * @date 2024-07-17
 */
public interface CertificatesMapper
{
    /**
     * 查询志愿者证书管理
     *
     * @param certificateId 志愿者证书管理主键
     * @return 志愿者证书管理
     */
    public Certificates selectCertificatesByCertificateId(Long certificateId);

    /**
     * 查询志愿者证书管理列表
     *
     * @param certificates 志愿者证书管理
     * @return 志愿者证书管理集合
     */
    public List<Certificates> selectCertificatesList(Certificates certificates);

    /**
     * 新增志愿者证书管理
     *
     * @param certificates 志愿者证书管理
     * @return 结果
     */
    public int insertCertificates(Certificates certificates);

    /**
     * 修改志愿者证书管理
     *
     * @param certificates 志愿者证书管理
     * @return 结果
     */
    public int updateCertificates(Certificates certificates);

    /**
     * 删除志愿者证书管理
     *
     * @param certificateId 志愿者证书管理主键
     * @return 结果
     */
    public int deleteCertificatesByCertificateId(Long certificateId);

    /**
     * 批量删除志愿者证书管理
     *
     * @param certificateIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCertificatesByCertificateIds(Long[] certificateIds);

    public List<CertificatesVo> selectCertificatesListVo(Certificates certificates);
}
