package edu.sias.ims.student.mapper;

import java.util.List;
import edu.sias.ims.student.domain.Scholarship;
import edu.sias.ims.student.domain.vo.ScholarshipVo;

/**
 * 奖学金信息管理Mapper接口
 *
 * @author ruoyi
 * @date 2024-07-16
 */
public interface ScholarshipMapper
{
    /**
     * 查询奖学金信息管理
     *
     * @param id 奖学金信息管理主键
     * @return 奖学金信息管理
     */
    public ScholarshipVo selectScholarshipById(Long id);

    /**
     * 查询奖学金信息管理列表
     *
     * @param scholarship 奖学金信息管理
     * @return 奖学金信息管理集合
     */
    public List<ScholarshipVo> selectScholarshipList(ScholarshipVo scholarshipVo);

    /**
     * 新增奖学金信息管理
     *
     * @param scholarship 奖学金信息管理
     * @return 结果
     */
    public int insertScholarship(Scholarship scholarship);

    /**
     * 修改奖学金信息管理
     *
     * @param scholarship 奖学金信息管理
     * @return 结果
     */
    public int updateScholarship(Scholarship scholarship);

    /**
     * 删除奖学金信息管理
     *
     * @param id 奖学金信息管理主键
     * @return 结果
     */
    public int deleteScholarshipById(Long id);

    /**
     * 批量删除奖学金信息管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteScholarshipByIds(Long[] ids);
}
