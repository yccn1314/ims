package edu.sias.ims.student.controller;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.StudentsProjectExperience;
import edu.sias.ims.student.domain.vo.StudentsProjectExperienceVo;
import edu.sias.ims.student.service.IStudentsProjectExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学生实习项目经历Controller
 * 
 * @author 文超
 * @date 2024-07-31
 */
@RestController
@RequestMapping("/studentprojectexperience/studentprojectexperience")
public class StudentsProjectExperienceController extends BaseController
{
    @Autowired
    private IStudentsProjectExperienceService studentsProjectExperienceService;

    /**
     * 查询学生实习项目经历列表
     */
    @PreAuthorize("@ss.hasPermi('studentprojectexperience:studentprojectexperience:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentsProjectExperience studentsProjectExperience)
    {
        startPage();
//        List<StudentsProjectExperience> list = studentsProjectExperienceService.selectStudentsProjectExperienceList(studentsProjectExperience);
//        return getDataTable(list);
        List<StudentsProjectExperienceVo> listvo=studentsProjectExperienceService.selectStudentsProjectExperienceListVo(studentsProjectExperience);
        return getDataTable(listvo);
    }

    /**
     * 导出学生实习项目经历列表
     */
    @PreAuthorize("@ss.hasPermi('studentprojectexperience:studentprojectexperience:export')")
    @Log(title = "学生实习项目经历", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentsProjectExperience studentsProjectExperience)
    {
        List<StudentsProjectExperience> list = studentsProjectExperienceService.selectStudentsProjectExperienceList(studentsProjectExperience);
        ExcelUtil<StudentsProjectExperience> util = new ExcelUtil<StudentsProjectExperience>(StudentsProjectExperience.class);
        util.exportExcel(response, list, "学生实习项目经历数据");
    }

    /**
     * 获取学生实习项目经历详细信息
     */
    @PreAuthorize("@ss.hasPermi('studentprojectexperience:studentprojectexperience:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(studentsProjectExperienceService.selectStudentsProjectExperienceById(id));
    }

    /**
     * 新增学生实习项目经历
     */
    @PreAuthorize("@ss.hasPermi('studentprojectexperience:studentprojectexperience:add')")
    @Log(title = "学生实习项目经历", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentsProjectExperience studentsProjectExperience)
    {
        return toAjax(studentsProjectExperienceService.insertStudentsProjectExperience(studentsProjectExperience));
    }

    /**
     * 修改学生实习项目经历
     */
    @PreAuthorize("@ss.hasPermi('studentprojectexperience:studentprojectexperience:edit')")
    @Log(title = "学生实习项目经历", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentsProjectExperience studentsProjectExperience)
    {
        return toAjax(studentsProjectExperienceService.updateStudentsProjectExperience(studentsProjectExperience));
    }

    /**
     * 删除学生实习项目经历
     */
    @PreAuthorize("@ss.hasPermi('studentprojectexperience:studentprojectexperience:remove')")
    @Log(title = "学生实习项目经历", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(studentsProjectExperienceService.deleteStudentsProjectExperienceByIds(ids));
    }
    //    **************************************
    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "学生项目经历导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('studentprojectexperience:studentprojectexperience:import')")
    @PostMapping("/importstudentprojectexperienceData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<StudentsProjectExperience> util = new ExcelUtil<StudentsProjectExperience>(StudentsProjectExperience.class);
        List<StudentsProjectExperience> StudentsProjectExperienceList = util.importExcel(file.getInputStream());
        String message = studentsProjectExperienceService.importudentsProjectExperienceInfo(StudentsProjectExperienceList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importstudentsProjectExperienceTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<StudentsProjectExperience> util = new ExcelUtil<StudentsProjectExperience>(StudentsProjectExperience.class);
        util.importTemplateExcel(response, "用户数据");
    }
//    **************************************
}
