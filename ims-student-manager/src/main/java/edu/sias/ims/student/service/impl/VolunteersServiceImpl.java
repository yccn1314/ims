package edu.sias.ims.student.service.impl;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.Volunteers;
import edu.sias.ims.student.domain.vo.VolunteersVo;
import edu.sias.ims.student.mapper.VolunteersMapper;
import edu.sias.ims.student.service.IVolunteersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 志愿者管理Service业务层处理
 *
 * @author wenchao
 * @date 2024-07-17
 */
@Service
public class VolunteersServiceImpl implements IVolunteersService
{
    @Autowired
    private VolunteersMapper volunteersMapper;
    private static final Logger log = LoggerFactory.getLogger(VolunteersServiceImpl.class);
    /**
     * 导入志愿者列表
     *
     * @param volunteerList 志愿者列表
     * @return 导入状态信息
     */

    @Override
    public String importVolunteersInfo(List<Volunteers> volunteerList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(volunteerList) || volunteerList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
//        遍历志愿者列表
        for (Volunteers volunteerInfo : volunteerList) {

            try {
                Volunteers u = volunteersMapper.selectVolunteersByVolunteerId(volunteerInfo.getVolunteerId());
                if (StringUtils.isNull(u)) {
                    try {
                        volunteersMapper.insertVolunteers(volunteerInfo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + volunteerInfo.getStudentNumber() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + volunteerInfo.getStudentNumber() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                } else if (updateSupport) {
                    try {
                        volunteersMapper.updateVolunteers(volunteerInfo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + volunteerInfo.getStudentNumber() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + volunteerInfo.getStudentNumber() + " 导入失败：";

                    }

                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、志愿者 " + volunteerInfo.getStudentNumber() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、志愿者 " + volunteerInfo.getStudentNumber() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
            if (failureNum > 0) {
                failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                throw new ServiceException(failureMsg.toString());
            } else {
                successMsg.insert(0, "数据已全部导入成功！共 " + successNum + " 条，数据如下：");
            }
        return successMsg.toString();
    }

    /**
     * 查询志愿者管理
     *
     * @param volunteerId 志愿者管理主键
     * @return 志愿者管理
     */
    @Override
    public Volunteers selectVolunteersByVolunteerId(Long volunteerId)
    {
        return volunteersMapper.selectVolunteersByVolunteerId(volunteerId);
    }

    /**
     * 查询志愿者管理列表
     *
     * @param volunteers 志愿者管理
     * @return 志愿者管理
     */
    @Override
    public List<Volunteers> selectVolunteersList(Volunteers volunteers)
    {
        return volunteersMapper.selectVolunteersList(volunteers);
    }

    /**
     * 新增志愿者管理
     *
     * @param volunteers 志愿者管理
     * @return 结果
     */
    @Override
    public int insertVolunteers(Volunteers volunteers)
    {
        return volunteersMapper.insertVolunteers(volunteers);
    }

    /**
     * 修改志愿者管理
     *
     * @param volunteers 志愿者管理
     * @return 结果
     */
    @Override
    public int updateVolunteers(Volunteers volunteers)
    {
        return volunteersMapper.updateVolunteers(volunteers);
    }

    /**
     * 批量删除志愿者管理
     *
     * @param volunteerIds 需要删除的志愿者管理主键
     * @return 结果
     */
    @Override
    public int deleteVolunteersByVolunteerIds(Long[] volunteerIds)
    {
        return volunteersMapper.deleteVolunteersByVolunteerIds(volunteerIds);
    }

    /**
     * 删除志愿者管理信息
     *
     * @param volunteerId 志愿者管理主键
     * @return 结果
     */
    @Override
    public int deleteVolunteersByVolunteerId(Long volunteerId)
    {
        return volunteersMapper.deleteVolunteersByVolunteerId(volunteerId);
    }

    @Override
    public List<VolunteersVo> selectVolunteersListVo(Volunteers volunteers) {
        return volunteersMapper.selectVolunteersListVo(volunteers);
    }
}
