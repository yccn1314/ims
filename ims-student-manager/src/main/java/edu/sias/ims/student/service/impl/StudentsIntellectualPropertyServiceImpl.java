package edu.sias.ims.student.service.impl;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.StudentsIntellectualProperty;
import edu.sias.ims.student.domain.vo.StudentsIntellectualPropertyVo;
import edu.sias.ims.student.mapper.StudentsIntellectualPropertyMapper;
import edu.sias.ims.student.service.IStudentsIntellectualPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 知识产权Service业务层处理
 * 
 * @author wenchao
 * @date 2024-07-30
 */
@Service
public class StudentsIntellectualPropertyServiceImpl implements IStudentsIntellectualPropertyService 
{
    @Autowired
    private StudentsIntellectualPropertyMapper studentsIntellectualPropertyMapper;
    private static final Logger log = LoggerFactory.getLogger(VolunteersServiceImpl.class);
    /**
     * 查询知识产权
     * 
     * @param id 知识产权主键
     * @return 知识产权
     */
    @Override
    public StudentsIntellectualProperty selectStudentsIntellectualPropertyById(Long id)
    {
        return studentsIntellectualPropertyMapper.selectStudentsIntellectualPropertyById(id);
    }

    /**
     * 查询知识产权列表
     * 
     * @param studentsIntellectualProperty 知识产权
     * @return 知识产权
     */
    @Override
    public List<StudentsIntellectualProperty> selectStudentsIntellectualPropertyList(StudentsIntellectualProperty studentsIntellectualProperty)
    {
        return studentsIntellectualPropertyMapper.selectStudentsIntellectualPropertyList(studentsIntellectualProperty);
    }

    /**
     * 新增知识产权
     * 
     * @param studentsIntellectualProperty 知识产权
     * @return 结果
     */
    @Override
    public int insertStudentsIntellectualProperty(StudentsIntellectualProperty studentsIntellectualProperty)
    {
        studentsIntellectualProperty.setCreateTime(DateUtils.getNowDate());
        return studentsIntellectualPropertyMapper.insertStudentsIntellectualProperty(studentsIntellectualProperty);
    }

    /**
     * 修改知识产权
     * 
     * @param studentsIntellectualProperty 知识产权
     * @return 结果
     */
    @Override
    public int updateStudentsIntellectualProperty(StudentsIntellectualProperty studentsIntellectualProperty)
    {
        studentsIntellectualProperty.setUpdateTime(DateUtils.getNowDate());
        return studentsIntellectualPropertyMapper.updateStudentsIntellectualProperty(studentsIntellectualProperty);
    }

    /**
     * 批量删除知识产权
     * 
     * @param ids 需要删除的知识产权主键
     * @return 结果
     */
    @Override
    public int deleteStudentsIntellectualPropertyByIds(Long[] ids)
    {
        return studentsIntellectualPropertyMapper.deleteStudentsIntellectualPropertyByIds(ids);
    }

    /**
     * 删除知识产权信息
     * 
     * @param id 知识产权主键
     * @return 结果
     */
    @Override
    public int deleteStudentsIntellectualPropertyById(Long id)
    {
        return studentsIntellectualPropertyMapper.deleteStudentsIntellectualPropertyById(id);
    }

    @Override
    public List<StudentsIntellectualPropertyVo> selectStudentsIntellectualPropertyListVo(StudentsIntellectualProperty studentsIntellectualProperty) {
        return studentsIntellectualPropertyMapper.selectStudentsIntellectualPropertyListVo(studentsIntellectualProperty);
    }
//知识产权导入方法
    @Override
    public String importStudentIntellectualPropertyInfo(List<StudentsIntellectualProperty> studentsIntellectualPropertyList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(studentsIntellectualPropertyList) || studentsIntellectualPropertyList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
//        遍历志愿者列表
        for (StudentsIntellectualProperty studentsintellectualproperty : studentsIntellectualPropertyList) {

            try {
                StudentsIntellectualProperty u = studentsIntellectualPropertyMapper.selectStudentsIntellectualPropertyById(studentsintellectualproperty.getId());
                if (StringUtils.isNull(u)) {
                    try {
                        studentsIntellectualPropertyMapper.insertStudentsIntellectualProperty(studentsintellectualproperty);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + studentsintellectualproperty.getPrincipalStudentId() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + studentsintellectualproperty.getPrincipalStudentId() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                } else if (updateSupport) {
                    try {
                        studentsIntellectualPropertyMapper.updateStudentsIntellectualProperty(studentsintellectualproperty);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + studentsintellectualproperty.getPrincipalStudentId() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + studentsintellectualproperty.getPrincipalStudentId() + " 导入失败：";

                    }

                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、志愿者 " +studentsintellectualproperty.getPrincipalStudentId() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、志愿者 " + studentsintellectualproperty.getPrincipalStudentId() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
