package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 学生信息管理对象 students
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
public class Students extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNumber;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 专业 */
    @Excel(name = "专业")
    private String major;

    /** 班级 */
    @Excel(name = "班级")
    private String studentClass;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phoneNumber;

    /** 逻辑删除 0:未删除 1：已删除 */
    private Long isDeleted;

    /** 创建人ID */
    private Long creatorBy;

    /** 修改人ID */
    private Long updataBy;

    /** 修改时间 */
    private Date modifyTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentNumber(String studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public String getStudentNumber() 
    {
        return studentNumber;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMajor(String major) 
    {
        this.major = major;
    }

    public String getMajor() 
    {
        return major;
    }
    public void setStudentClass(String studentClass) 
    {
        this.studentClass = studentClass;
    }

    public String getStudentClass() 
    {
        return studentClass;
    }
    public void setPhoneNumber(String phoneNumber) 
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() 
    {
        return phoneNumber;
    }
    public void setIsDeleted(Long isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() 
    {
        return isDeleted;
    }
    public void setCreatorBy(Long creatorBy) 
    {
        this.creatorBy = creatorBy;
    }

    public Long getCreatorBy() 
    {
        return creatorBy;
    }
    public void setUpdataBy(Long updataBy) 
    {
        this.updataBy = updataBy;
    }

    public Long getUpdataBy() 
    {
        return updataBy;
    }
    public void setModifyTime(Date modifyTime) 
    {
        this.modifyTime = modifyTime;
    }

    public Date getModifyTime() 
    {
        return modifyTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentNumber", getStudentNumber())
            .append("name", getName())
            .append("major", getMajor())
            .append("studentClass", getStudentClass())
            .append("phoneNumber", getPhoneNumber())
            .append("isDeleted", getIsDeleted())
            .append("creatorBy", getCreatorBy())
            .append("createTime", getCreateTime())
            .append("updataBy", getUpdataBy())
            .append("modifyTime", getModifyTime())
            .append("remark", getRemark())
            .toString();
    }
}
