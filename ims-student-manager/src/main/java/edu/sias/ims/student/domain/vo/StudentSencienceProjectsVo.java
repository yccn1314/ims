package edu.sias.ims.student.domain.vo;

import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.student.domain.StudentSencienceProjects;

public class StudentSencienceProjectsVo extends StudentSencienceProjects {

    @Excel(name = "学生姓名")
    private String studentName;

    public StudentSencienceProjectsVo() {
    }

    public StudentSencienceProjectsVo(String studentName) {
        this.studentName = studentName;
    }

    /**
     * 获取
     * @return studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * 设置
     * @param studentName
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String toString() {
        return "StudentSencienceProjectsVo{studentName = " + studentName + "}";
    }
}
