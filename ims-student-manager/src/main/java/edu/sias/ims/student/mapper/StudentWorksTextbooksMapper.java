package edu.sias.ims.student.mapper;

import java.util.List;
import edu.sias.ims.student.domain.StudentWorksTextbooks;
import edu.sias.ims.student.domain.vo.StudentWorksTextbooksVo;

/**
 * 著作教材Mapper接口
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
public interface StudentWorksTextbooksMapper 
{
    /**
     * 查询著作教材
     * 
     * @param id 著作教材主键
     * @return 著作教材
     */
    public StudentWorksTextbooks selectStudentWorksTextbooksById(Long id);

    /**
     * 查询著作教材列表
     * 
     * @param studentWorksTextbooks 著作教材
     * @return 著作教材集合
     */
    public List<StudentWorksTextbooksVo> selectStudentWorksTextbooksList(StudentWorksTextbooks studentWorksTextbooks);

    /**
     * 新增著作教材
     * 
     * @param studentWorksTextbooks 著作教材
     * @return 结果
     */
    public int insertStudentWorksTextbooks(StudentWorksTextbooks studentWorksTextbooks);

    /**
     * 修改著作教材
     * 
     * @param studentWorksTextbooks 著作教材
     * @return 结果
     */
    public int updateStudentWorksTextbooks(StudentWorksTextbooks studentWorksTextbooks);

    /**
     * 删除著作教材
     * 
     * @param id 著作教材主键
     * @return 结果
     */
    public int deleteStudentWorksTextbooksById(Long id);

    /**
     * 批量删除著作教材
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentWorksTextbooksByIds(Long[] ids);
}
