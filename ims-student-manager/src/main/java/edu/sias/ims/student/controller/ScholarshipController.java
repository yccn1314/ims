package edu.sias.ims.student.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.Students;
import edu.sias.ims.student.domain.vo.ScholarshipVo;
import edu.sias.ims.student.domain.Scholarship;
import edu.sias.ims.student.service.IScholarshipService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * 奖学金信息管理Controller
 *
 * @author ruoyi
 * @date 2024-07-16
 */
@RestController
@RequestMapping("/scholarship/scholarship")
public class ScholarshipController extends BaseController {
    @Autowired
    private IScholarshipService scholarshipService;


    /**
     * 导入
     *
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "学生信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('scholarship:scholarship:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<Scholarship> util = new ExcelUtil<Scholarship>(Scholarship.class);
        List<Scholarship> scholarshipList = util.importExcel(file.getInputStream());
        String message = scholarshipService.importscholarship(scholarshipList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     *
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<Scholarship> util = new ExcelUtil<Scholarship>(Scholarship.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 查询奖学金信息管理列表
     */
    @PreAuthorize("@ss.hasPermi('scholarship:scholarship:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScholarshipVo scholarshipVo) {
        startPage();
        List<ScholarshipVo> list = scholarshipService.selectScholarshipList(scholarshipVo);
        return getDataTable(list);
    }

    /**
     * 导出奖学金信息管理列表
     */
    @PreAuthorize("@ss.hasPermi('scholarship:scholarship:export')")
    @Log(title = "奖学金信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScholarshipVo scholarshipVo) {
        List<ScholarshipVo> list = scholarshipService.selectScholarshipList(scholarshipVo);
        ExcelUtil<ScholarshipVo> util = new ExcelUtil<ScholarshipVo>(ScholarshipVo.class);
        util.exportExcel(response, list, "奖学金信息管理数据");
    }

    /**
     * 获取奖学金信息管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('scholarship:scholarship:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(scholarshipService.selectScholarshipById(id));
    }

    /**
     * 新增奖学金信息管理
     */
    @PreAuthorize("@ss.hasPermi('scholarship:scholarship:add')")
    @Log(title = "奖学金信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Scholarship scholarship) {
        return toAjax(scholarshipService.insertScholarship(scholarship));
    }

    /**
     * 修改奖学金信息管理
     */
    @PreAuthorize("@ss.hasPermi('scholarship:scholarship:edit')")
    @Log(title = "奖学金信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Scholarship scholarship) {
        return toAjax(scholarshipService.updateScholarship(scholarship));
    }

    /**
     * 删除奖学金信息管理
     */
    @PreAuthorize("@ss.hasPermi('scholarship:scholarship:remove')")
    @Log(title = "奖学金信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(scholarshipService.deleteScholarshipByIds(ids));
    }
}
