package edu.sias.ims.student.service;

import edu.sias.ims.student.domain.dto.ExportDto;

import java.util.List;

public interface ExportService {
    List<ExportDto> selectByTargat(ExportDto exportDto);
}
