package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import edu.sias.ims.common.core.domain.BaseEntity;

/**
 * 志愿者管理对象 volunteers
 * 
 * @author wenchao
 * @date 2024-07-17
 */
public class Volunteers extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 志愿者id */
    private Long volunteerId;

    /** 志愿者学号 */
    @Excel(name = "志愿者学号")
    private String studentNumber;

    /** 服务内容 */
    @Excel(name = "服务内容")
    private String serviceContent;

    /** 服务时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "服务时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date serviceTime;

    /** 公示文件附件* */
    @Excel(name = "公示文件附件*")
    private String documentAttachment;

    /** 创建时间 */
    private Date creationTime;

    /** 修改人 */
    private String updataBy;

    /** 修改时间 */
    private Date updataTime;

    /** 删除标记 */
    private Integer isDeleted;

    /** 部门id */
    @Excel(name = "部门id")
    private Long sectionId;

    public void setVolunteerId(Long volunteerId) 
    {
        this.volunteerId = volunteerId;
    }

    public Long getVolunteerId() 
    {
        return volunteerId;
    }
    public void setStudentNumber(String studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public String getStudentNumber() 
    {
        return studentNumber;
    }
    public void setServiceContent(String serviceContent) 
    {
        this.serviceContent = serviceContent;
    }

    public String getServiceContent() 
    {
        return serviceContent;
    }
    public void setServiceTime(Date serviceTime) 
    {
        this.serviceTime = serviceTime;
    }

    public Date getServiceTime() 
    {
        return serviceTime;
    }
    public void setDocumentAttachment(String documentAttachment) 
    {
        this.documentAttachment = documentAttachment;
    }

    public String getDocumentAttachment() 
    {
        return documentAttachment;
    }
    public void setCreationTime(Date creationTime) 
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime() 
    {
        return creationTime;
    }
    public void setUpdataBy(String updataBy) 
    {
        this.updataBy = updataBy;
    }

    public String getUpdataBy() 
    {
        return updataBy;
    }
    public void setUpdataTime(Date updataTime) 
    {
        this.updataTime = updataTime;
    }

    public Date getUpdataTime() 
    {
        return updataTime;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }
    public void setSectionId(Long sectionId) 
    {
        this.sectionId = sectionId;
    }

    public Long getSectionId() 
    {
        return sectionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("volunteerId", getVolunteerId())
            .append("studentNumber", getStudentNumber())
            .append("serviceContent", getServiceContent())
            .append("serviceTime", getServiceTime())
            .append("documentAttachment", getDocumentAttachment())
            .append("createBy", getCreateBy())
            .append("creationTime", getCreationTime())
            .append("updataBy", getUpdataBy())
            .append("updataTime", getUpdataTime())
            .append("isDeleted", getIsDeleted())
            .append("sectionId", getSectionId())
            .append("remark", getRemark())
            .toString();
    }
}
