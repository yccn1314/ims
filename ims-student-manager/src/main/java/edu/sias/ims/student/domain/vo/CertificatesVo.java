package edu.sias.ims.student.domain.vo;


import edu.sias.ims.student.domain.Certificates;

public class CertificatesVo extends Certificates {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
