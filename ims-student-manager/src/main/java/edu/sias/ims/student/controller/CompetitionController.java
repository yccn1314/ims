package edu.sias.ims.student.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.Competition;
import edu.sias.ims.student.domain.Scholarship;
import edu.sias.ims.student.domain.vo.CompetitionVo;
import edu.sias.ims.student.service.ICompetitionService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * 竞赛管理Controller
 *
 * @author ruoyi
 * @date 2024-07-16
 */
@RestController
@RequestMapping("/studentmanger/competition")
public class CompetitionController extends BaseController
{
    @Autowired
    private ICompetitionService competitionService;


    /**
     * 导入
     *
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "竞赛导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('competion:competition:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<Competition> util = new ExcelUtil<Competition>(Competition.class);
        List<Competition> competitionsList = util.importExcel(file.getInputStream());
        String message = competitionService.importscholarship(competitionsList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     *
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<Competition> util = new ExcelUtil<Competition>(Competition.class);
        util.importTemplateExcel(response, "用户数据");
    }


    /**
     * 查询竞赛管理列表
     */
    @PreAuthorize("@ss.hasPermi('competion:competition:list')")
    @GetMapping("/list")
    public TableDataInfo list(CompetitionVo competitionVo)
    {
        startPage();
        List<CompetitionVo> list = competitionService.selectCompetitionList(competitionVo);
        return getDataTable(list);
    }

    /**
     * 导出竞赛管理列表
     */
    @PreAuthorize("@ss.hasPermi('competion:competition:export')")
    @Log(title = "竞赛管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CompetitionVo competitionVo)
    {
        List<CompetitionVo> list = competitionService.selectCompetitionList(competitionVo);
        ExcelUtil<CompetitionVo> util = new ExcelUtil<CompetitionVo>(CompetitionVo.class);
        util.exportExcel(response, list, "竞赛管理数据");
    }

    /**
     * 获取竞赛管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('competion:competition:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(competitionService.selectCompetitionById(id));
    }

    /**
     * 新增竞赛管理
     */
    @PreAuthorize("@ss.hasPermi('competion:competition:add')")
    @Log(title = "竞赛管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CompetitionVo competitionVo)
    {
        return toAjax(competitionService.insertCompetition(competitionVo));
    }

    /**
     * 修改竞赛管理
     */
    @PreAuthorize("@ss.hasPermi('competion:competition:edit')")
    @Log(title = "竞赛管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CompetitionVo competitionVo)
    {
        return toAjax(competitionService.updateCompetition(competitionVo));
    }

    /**
     * 删除竞赛管理
     */
    @PreAuthorize("@ss.hasPermi('competion:competition:remove')")
    @Log(title = "竞赛管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(competitionService.deleteCompetitionByIds(ids));
    }
}
