package edu.sias.ims.student.service.impl;

import java.util.List;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.vo.StudentSencienceProjectsVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.sias.ims.student.mapper.StudentSencienceProjectsMapper;
import edu.sias.ims.student.domain.StudentSencienceProjects;
import edu.sias.ims.student.service.IStudentSencienceProjectsService;

/**
 * 学生科研立项Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
@Service
public class StudentSencienceProjectsServiceImpl implements IStudentSencienceProjectsService 
{
    @Autowired
    private StudentSencienceProjectsMapper studentSencienceProjectsMapper;

    private static final Logger log = LoggerFactory.getLogger(StudentsServiceImpl.class);

    /**
     * 查询学生科研立项
     * 
     * @param id 学生科研立项主键
     * @return 学生科研立项
     */
    @Override
    public StudentSencienceProjects selectStudentSencienceProjectsById(Long id)
    {
        return studentSencienceProjectsMapper.selectStudentSencienceProjectsById(id);
    }

    /**
     * 查询学生科研立项列表
     * 
     * @param studentSencienceProjects 学生科研立项
     * @return 学生科研立项
     */
    @Override
    public List<StudentSencienceProjectsVo> selectStudentSencienceProjectsList(StudentSencienceProjects studentSencienceProjects)
    {
        return studentSencienceProjectsMapper.selectStudentSencienceProjectsList(studentSencienceProjects);
    }

    /**
     * 新增学生科研立项
     * 
     * @param studentSencienceProjects 学生科研立项
     * @return 结果
     */
    @Override
    public int insertStudentSencienceProjects(StudentSencienceProjects studentSencienceProjects)
    {
        studentSencienceProjects.setCreateTime(DateUtils.getNowDate());
        return studentSencienceProjectsMapper.insertStudentSencienceProjects(studentSencienceProjects);
    }

    /**
     * 修改学生科研立项
     * 
     * @param studentSencienceProjects 学生科研立项
     * @return 结果
     */
    @Override
    public int updateStudentSencienceProjects(StudentSencienceProjects studentSencienceProjects)
    {
        studentSencienceProjects.setUpdateTime(DateUtils.getNowDate());
        return studentSencienceProjectsMapper.updateStudentSencienceProjects(studentSencienceProjects);
    }

    /**
     * 批量删除学生科研立项
     * 
     * @param ids 需要删除的学生科研立项主键
     * @return 结果
     */
    @Override
    public int deleteStudentSencienceProjectsByIds(Long[] ids)
    {
        return studentSencienceProjectsMapper.deleteStudentSencienceProjectsByIds(ids);
    }

    /**
     * 删除学生科研立项信息
     * 
     * @param id 学生科研立项主键
     * @return 结果
     */
    @Override
    public int deleteStudentSencienceProjectsById(Long id)
    {
        return studentSencienceProjectsMapper.deleteStudentSencienceProjectsById(id);
    }

    @Override
    public String importStudentSencienceProjectsVoService(List<StudentSencienceProjectsVo> studentSencienceProjectsVoList, boolean updateSupport) {

        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(studentSencienceProjectsVoList) || studentSencienceProjectsVoList.isEmpty()) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (StudentSencienceProjectsVo studentSencienceProjectsVo : studentSencienceProjectsVoList) {

            try{
                StudentSencienceProjects u = studentSencienceProjectsMapper.selectStudentSencienceProjectsById(studentSencienceProjectsVo.getId());
                if(StringUtils.isNull(u)){
                    try {
                        studentSencienceProjectsMapper.insertStudentSencienceProjects(studentSencienceProjectsVo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、学生科研立项 " + studentSencienceProjectsVo.getProjectName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、学生科研立项 " + studentSencienceProjectsVo.getProjectName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        studentSencienceProjectsMapper.updateStudentSencienceProjects(studentSencienceProjectsVo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、学生科研立项 " + studentSencienceProjectsVo.getProjectName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、学生科研立项 " + studentSencienceProjectsVo.getProjectName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、学生科研立项" + studentSencienceProjectsVo.getProjectName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、学生科研立项" + studentSencienceProjectsVo.getProjectName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();

    }
}
