package edu.sias.ims.student.mapper;

import edu.sias.ims.student.domain.vo.VolunteersVo;
import edu.sias.ims.student.domain.Volunteers;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 志愿者管理Mapper接口
 *
 * @author wenchao
 * @date 2024-07-17
 */
@Mapper
public interface VolunteersMapper
{
    /**
     * 查询志愿者管理
     *
     * @param volunteerId 志愿者管理主键
     * @return 志愿者管理
     */
    public Volunteers selectVolunteersByVolunteerId(Long volunteerId);

    /**
     * 查询志愿者管理列表
     *
     * @param volunteers 志愿者管理
     * @return 志愿者管理集合
     */
    public List<Volunteers> selectVolunteersList(Volunteers volunteers);

    /**
     * 新增志愿者管理
     *
     * @param volunteers 志愿者管理
     * @return 结果
     */
    public int insertVolunteers(Volunteers volunteers);

    /**
     * 修改志愿者管理
     *
     * @param volunteers 志愿者管理
     * @return 结果
     */
    public int updateVolunteers(Volunteers volunteers);

    /**
     * 删除志愿者管理
     *
     * @param volunteerId 志愿者管理主键
     * @return 结果
     */
    public int deleteVolunteersByVolunteerId(Long volunteerId);

    /**
     * 批量删除志愿者管理
     *
     * @param volunteerIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVolunteersByVolunteerIds(Long[] volunteerIds);
    /**
     * 查询志愿者管理列表
     *
     * @param volunteers 志愿者管理
     * @return 志愿者管理集合
     */
    public List<VolunteersVo> selectVolunteersListVo(Volunteers volunteers);
}
