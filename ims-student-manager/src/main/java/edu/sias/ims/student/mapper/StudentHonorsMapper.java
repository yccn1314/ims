package edu.sias.ims.student.mapper;

import java.util.List;
import edu.sias.ims.student.domain.StudentHonors;
import edu.sias.ims.student.domain.vo.StudentHonorsVo;

/**
 * 学生荣誉Mapper接口
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
public interface StudentHonorsMapper 
{
    /**
     * 查询学生荣誉
     * 
     * @param id 学生荣誉主键
     * @return 学生荣誉
     */
    public StudentHonors selectStudentHonorsById(Long id);

    /**
     * 查询学生荣誉列表
     * 
     * @param studentHonors 学生荣誉
     * @return 学生荣誉集合
     */
    public List<StudentHonorsVo> selectStudentHonorsList(StudentHonors studentHonors);

    /**
     * 新增学生荣誉
     * 
     * @param studentHonors 学生荣誉
     * @return 结果
     */
    public int insertStudentHonors(StudentHonors studentHonors);

    /**
     * 修改学生荣誉
     * 
     * @param studentHonors 学生荣誉
     * @return 结果
     */
    public int updateStudentHonors(StudentHonors studentHonors);

    /**
     * 删除学生荣誉
     * 
     * @param id 学生荣誉主键
     * @return 结果
     */
    public int deleteStudentHonorsById(Long id);

    /**
     * 批量删除学生荣誉
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentHonorsByIds(Long[] ids);
}
