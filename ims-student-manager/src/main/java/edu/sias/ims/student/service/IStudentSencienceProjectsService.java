package edu.sias.ims.student.service;

import java.util.List;
import edu.sias.ims.student.domain.StudentSencienceProjects;
import edu.sias.ims.student.domain.vo.StudentSencienceProjectsVo;

/**
 * 学生科研立项Service接口
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
public interface IStudentSencienceProjectsService 
{
    /**
     * 查询学生科研立项
     * 
     * @param id 学生科研立项主键
     * @return 学生科研立项
     */
    public StudentSencienceProjects selectStudentSencienceProjectsById(Long id);

    /**
     * 查询学生科研立项列表
     * 
     * @param studentSencienceProjects 学生科研立项
     * @return 学生科研立项集合
     */
    public List<StudentSencienceProjectsVo> selectStudentSencienceProjectsList(StudentSencienceProjects studentSencienceProjects);

    /**
     * 新增学生科研立项
     * 
     * @param studentSencienceProjects 学生科研立项
     * @return 结果
     */
    public int insertStudentSencienceProjects(StudentSencienceProjects studentSencienceProjects);

    /**
     * 修改学生科研立项
     * 
     * @param studentSencienceProjects 学生科研立项
     * @return 结果
     */
    public int updateStudentSencienceProjects(StudentSencienceProjects studentSencienceProjects);

    /**
     * 批量删除学生科研立项
     * 
     * @param ids 需要删除的学生科研立项主键集合
     * @return 结果
     */
    public int deleteStudentSencienceProjectsByIds(Long[] ids);

    /**
     * 删除学生科研立项信息
     * 
     * @param id 学生科研立项主键
     * @return 结果
     */
    public int deleteStudentSencienceProjectsById(Long id);

    String importStudentSencienceProjectsVoService(List<StudentSencienceProjectsVo> studentsList, boolean updateSupport);
}
