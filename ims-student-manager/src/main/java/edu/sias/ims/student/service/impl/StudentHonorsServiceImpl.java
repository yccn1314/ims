package edu.sias.ims.student.service.impl;

import java.util.List;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.vo.StudentHonorsVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.sias.ims.student.mapper.StudentHonorsMapper;
import edu.sias.ims.student.domain.StudentHonors;
import edu.sias.ims.student.service.IStudentHonorsService;

/**
 * 学生荣誉Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
@Service
public class StudentHonorsServiceImpl implements IStudentHonorsService 
{
    @Autowired
    private StudentHonorsMapper studentHonorsMapper;
    private static final Logger log = LoggerFactory.getLogger(StudentsServiceImpl.class);

    /**
     * 查询学生荣誉
     * 
     * @param id 学生荣誉主键
     * @return 学生荣誉
     */
    @Override
    public StudentHonors selectStudentHonorsById(Long id)
    {
        return studentHonorsMapper.selectStudentHonorsById(id);
    }

    /**
     * 查询学生荣誉列表
     * 
     * @param studentHonors 学生荣誉
     * @return 学生荣誉
     */
    @Override
    public List<StudentHonorsVo> selectStudentHonorsList(StudentHonors studentHonors)
    {
        return studentHonorsMapper.selectStudentHonorsList(studentHonors);
    }

    /**
     * 新增学生荣誉
     * 
     * @param studentHonors 学生荣誉
     * @return 结果
     */
    @Override
    public int insertStudentHonors(StudentHonors studentHonors)
    {
        studentHonors.setCreateTime(DateUtils.getNowDate());
        return studentHonorsMapper.insertStudentHonors(studentHonors);
    }

    /**
     * 修改学生荣誉
     * 
     * @param studentHonors 学生荣誉
     * @return 结果
     */
    @Override
    public int updateStudentHonors(StudentHonors studentHonors)
    {
        studentHonors.setUpdateTime(DateUtils.getNowDate());
        return studentHonorsMapper.updateStudentHonors(studentHonors);
    }

    /**
     * 批量删除学生荣誉
     * 
     * @param ids 需要删除的学生荣誉主键
     * @return 结果
     */
    @Override
    public int deleteStudentHonorsByIds(Long[] ids)
    {
        return studentHonorsMapper.deleteStudentHonorsByIds(ids);
    }

    /**
     * 删除学生荣誉信息
     * 
     * @param id 学生荣誉主键
     * @return 结果
     */
    @Override
    public int deleteStudentHonorsById(Long id)
    {
        return studentHonorsMapper.deleteStudentHonorsById(id);
    }

    @Override
    public String importStudentsHonorService(List<StudentHonorsVo> studentsList, boolean updateSupport) {

            int successNum = 0;
            int failureNum = 0;
            StringBuilder successMsg = new StringBuilder();
            StringBuilder failureMsg = new StringBuilder();
            if (StringUtils.isNull(studentsList) || studentsList.isEmpty()) {
                throw new ServiceException("导入用户数据不能为空！");
            }
            for (StudentHonorsVo studentHonorsVo : studentsList) {

                try{
                     StudentHonors u = studentHonorsMapper.selectStudentHonorsById(studentHonorsVo.getId());
                    if(StringUtils.isNull(u)){
                        try {
                            studentHonorsMapper.insertStudentHonors(studentHonorsVo);
                            successNum++;
                            successMsg.append("<br/>" + successNum + "、学生荣誉 " + studentHonorsVo.getHonorName() + " 导入成功");
                        } catch (Exception e) {
                            failureNum++;
                            String msg = "<br/>" + failureNum + "、学生荣誉 " + studentHonorsVo.getHonorName() + " 导入失败：";
                            failureMsg.append(msg + e.getMessage());
                        }
                    }
                    else if (updateSupport) {
                        try {
                            studentHonorsMapper.updateStudentHonors(studentHonorsVo);
                            successNum++;
                            successMsg.append("<br/>" + successNum + "、学生荣誉 " + studentHonorsVo.getHonorName() + " 导入成功");
                        } catch (Exception e) {
                            failureNum++;
                            String msg = "<br/>" + failureNum + "、学生荣誉 " + studentHonorsVo.getHonorName() + " 导入失败：";

                        }

                    }else{
                        failureNum++;
                        failureMsg.append("<br/>" + failureNum + "、学生荣誉 " + studentHonorsVo.getHonorName() + " 已存在");
                    }
                }catch (Exception e)
                {
                    failureNum++;
                    String msg = "<br/>" + failureNum + "、学生荣誉 " + studentHonorsVo.getHonorName() + " 导入失败：";
                    failureMsg.append(msg + e.getMessage());
                    log.error(msg, e);
                }
            }

            if (failureNum > 0) {
                failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                throw new ServiceException(failureMsg.toString());
            } else {
                successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
            }
            return successMsg.toString();
        }

    }

