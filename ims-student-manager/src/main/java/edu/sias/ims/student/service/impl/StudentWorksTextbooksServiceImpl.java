package edu.sias.ims.student.service.impl;

import java.util.List;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.vo.StudentWorksTextbooksVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.sias.ims.student.mapper.StudentWorksTextbooksMapper;
import edu.sias.ims.student.domain.StudentWorksTextbooks;
import edu.sias.ims.student.service.IStudentWorksTextbooksService;

/**
 * 著作教材Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
@Service
public class StudentWorksTextbooksServiceImpl implements IStudentWorksTextbooksService 
{
    @Autowired
    private StudentWorksTextbooksMapper studentWorksTextbooksMapper;

    private static final Logger log = LoggerFactory.getLogger(StudentsServiceImpl.class);


    /**
     * 查询著作教材
     * 
     * @param id 著作教材主键
     * @return 著作教材
     */
    @Override
    public StudentWorksTextbooks selectStudentWorksTextbooksById(Long id)
    {
        return studentWorksTextbooksMapper.selectStudentWorksTextbooksById(id);
    }

    /**
     * 查询著作教材列表
     * 
     * @param studentWorksTextbooks 著作教材
     * @return 著作教材
     */
    @Override
    public List<StudentWorksTextbooksVo> selectStudentWorksTextbooksList(StudentWorksTextbooks studentWorksTextbooks)
    {
        return studentWorksTextbooksMapper.selectStudentWorksTextbooksList(studentWorksTextbooks);
    }

    /**
     * 新增著作教材
     * 
     * @param studentWorksTextbooks 著作教材
     * @return 结果
     */
    @Override
    public int insertStudentWorksTextbooks(StudentWorksTextbooks studentWorksTextbooks)
    {
        studentWorksTextbooks.setCreateTime(DateUtils.getNowDate());
        return studentWorksTextbooksMapper.insertStudentWorksTextbooks(studentWorksTextbooks);
    }

    /**
     * 修改著作教材
     * 
     * @param studentWorksTextbooks 著作教材
     * @return 结果
     */
    @Override
    public int updateStudentWorksTextbooks(StudentWorksTextbooks studentWorksTextbooks)
    {
        studentWorksTextbooks.setUpdateTime(DateUtils.getNowDate());
        return studentWorksTextbooksMapper.updateStudentWorksTextbooks(studentWorksTextbooks);
    }

    /**
     * 批量删除著作教材
     * 
     * @param ids 需要删除的著作教材主键
     * @return 结果
     */
    @Override
    public int deleteStudentWorksTextbooksByIds(Long[] ids)
    {
        return studentWorksTextbooksMapper.deleteStudentWorksTextbooksByIds(ids);
    }

    /**
     * 删除著作教材信息
     * 
     * @param id 著作教材主键
     * @return 结果
     */
    @Override
    public int deleteStudentWorksTextbooksById(Long id)
    {
        return studentWorksTextbooksMapper.deleteStudentWorksTextbooksById(id);
    }

    @Override
    public String importStudentWorksTextbooksVoService(List<StudentWorksTextbooksVo> StudentWorksTextbooksVoList, boolean updateSupport) {

        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(StudentWorksTextbooksVoList) || StudentWorksTextbooksVoList.isEmpty()) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (StudentWorksTextbooksVo studentWorksTextbooksVo : StudentWorksTextbooksVoList) {

            try{
                StudentWorksTextbooks u = studentWorksTextbooksMapper.selectStudentWorksTextbooksById(studentWorksTextbooksVo.getId());
                if(StringUtils.isNull(u)){
                    try {
                        studentWorksTextbooksMapper.insertStudentWorksTextbooks(studentWorksTextbooksVo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、学生著作 " + studentWorksTextbooksVo.getTitle() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、学生著作 " + studentWorksTextbooksVo.getTitle() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        studentWorksTextbooksMapper.updateStudentWorksTextbooks(studentWorksTextbooksVo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、学生著作 " + studentWorksTextbooksVo.getTitle() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、学生著作 " + studentWorksTextbooksVo.getTitle() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、学生著作 " + studentWorksTextbooksVo.getTitle() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、学生著作 " + studentWorksTextbooksVo.getTitle() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();

    }
}
