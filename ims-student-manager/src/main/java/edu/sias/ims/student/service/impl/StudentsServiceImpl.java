package edu.sias.ims.student.service.impl;

import java.util.List;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.sias.ims.student.mapper.StudentsMapper;
import edu.sias.ims.student.domain.Students;
import edu.sias.ims.student.service.IStudentsService;

/**
 * 学生信息管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
@Service
public class StudentsServiceImpl implements IStudentsService 
{
    @Autowired
    private StudentsMapper studentsMapper;

    private static final Logger log = LoggerFactory.getLogger(StudentsServiceImpl.class);


    /**
     * 查询学生信息管理
     * 
     * @param id 学生信息管理主键
     * @return 学生信息管理
     */
    @Override
    public Students selectStudentsById(Long id)
    {
        return studentsMapper.selectStudentsById(id);
    }

    /**
     * 查询学生信息管理列表
     * 
     * @param students 学生信息管理
     * @return 学生信息管理
     */
    @Override
    public List<Students> selectStudentsList(Students students)
    {
        return studentsMapper.selectStudentsList(students);
    }

    /**
     * 新增学生信息管理
     * 
     * @param students 学生信息管理
     * @return 结果
     */
    @Override
    public int insertStudents(Students students)
    {
        students.setCreateTime(DateUtils.getNowDate());
        return studentsMapper.insertStudents(students);
    }

    /**
     * 修改学生信息管理
     * 
     * @param students 学生信息管理
     * @return 结果
     */
    @Override
    public int updateStudents(Students students)
    {
        return studentsMapper.updateStudents(students);
    }

    /**
     * 批量删除学生信息管理
     * 
     * @param ids 需要删除的学生信息管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentsByIds(Long[] ids)
    {
        return studentsMapper.deleteStudentsByIds(ids);
    }

    /**
     * 删除学生信息管理信息
     * 
     * @param id 学生信息管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentsById(Long id)
    {
        return studentsMapper.deleteStudentsById(id);
    }

    @Override
    public String importStudentsService(List<Students> studentsList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(studentsList) || studentsList.isEmpty()) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (Students students : studentsList) {

            try{
                Students u = studentsMapper.selectStudentsById(students.getId());
                if(StringUtils.isNull(u)){
                    try {
                        studentsMapper.insertStudents(students);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、学生信息 " + students.getName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、学生信息 " + students.getName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        studentsMapper.updateStudents(students);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、学生信息 " + students.getName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、学生信息 " + students.getName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、学生信息 " + students.getName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、学生信息 " + students.getName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
