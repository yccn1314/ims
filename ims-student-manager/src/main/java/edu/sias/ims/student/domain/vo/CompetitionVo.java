package edu.sias.ims.student.domain.vo;

import edu.sias.ims.student.domain.Competition;

public class CompetitionVo extends Competition {

    private String studentName;

    public CompetitionVo() {
    }

    public CompetitionVo(String studentName) {
        this.studentName = studentName;
    }

    /**
     * 获取
     * @return studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * 设置
     * @param studentName
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String toString() {
        return "CompetitionVo{studentName = " + studentName + "}";
    }
}
