package edu.sias.ims.student.service.impl;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.StudentsThesis;
import edu.sias.ims.student.domain.vo.StudentsThesisVo;
import edu.sias.ims.student.mapper.StudentsThesisMapper;
import edu.sias.ims.student.service.IStudentsThesisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生论文管理Service业务层处理
 * 
 * @author wenchao
 * @date 2024-07-31
 */
@Service
public class StudentsThesisServiceImpl implements IStudentsThesisService 
{
    @Autowired
    private StudentsThesisMapper studentsThesisMapper;
    private static final Logger log = LoggerFactory.getLogger(VolunteersServiceImpl.class);
    /**
     * 查询学生论文管理
     * 
     * @param id 学生论文管理主键
     * @return 学生论文管理
     */
    @Override
    public StudentsThesis selectStudentsThesisById(Long id)
    {
        return studentsThesisMapper.selectStudentsThesisById(id);
    }

    /**
     * 查询学生论文管理列表
     * 
     * @param studentsThesis 学生论文管理
     * @return 学生论文管理
     */
    @Override
    public List<StudentsThesis> selectStudentsThesisList(StudentsThesis studentsThesis)
    {
        return studentsThesisMapper.selectStudentsThesisList(studentsThesis);
    }

    /**
     * 新增学生论文管理
     * 
     * @param studentsThesis 学生论文管理
     * @return 结果
     */
    @Override
    public int insertStudentsThesis(StudentsThesis studentsThesis)
    {
        studentsThesis.setCreateTime(DateUtils.getNowDate());
        return studentsThesisMapper.insertStudentsThesis(studentsThesis);
    }

    /**
     * 修改学生论文管理
     * 
     * @param studentsThesis 学生论文管理
     * @return 结果
     */
    @Override
    public int updateStudentsThesis(StudentsThesis studentsThesis)
    {
        studentsThesis.setUpdateTime(DateUtils.getNowDate());
        return studentsThesisMapper.updateStudentsThesis(studentsThesis);
    }

    /**
     * 批量删除学生论文管理
     * 
     * @param ids 需要删除的学生论文管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentsThesisByIds(Long[] ids)
    {
        return studentsThesisMapper.deleteStudentsThesisByIds(ids);
    }

    /**
     * 删除学生论文管理信息
     * 
     * @param id 学生论文管理主键
     * @return 结果
     */
    @Override
    public int deleteStudentsThesisById(Long id)
    {
        return studentsThesisMapper.deleteStudentsThesisById(id);
    }

    @Override
    public List<StudentsThesisVo> selectStudentsThesisListVo(StudentsThesis studentsThesis) {
        return studentsThesisMapper.selectStudentsThesisListVo(studentsThesis);
    }
//导入学生论文信息
    @Override
    public String importstudentsThesisInfo(List<StudentsThesis> studentsThesisList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(studentsThesisList) || studentsThesisList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
//        遍历志愿者列表
        for (StudentsThesis studentsthesis : studentsThesisList) {

            try {
                StudentsThesis u = studentsThesisMapper.selectStudentsThesisById(studentsthesis.getId());
                if (StringUtils.isNull(u)) {
                    try {
                        studentsThesisMapper.insertStudentsThesis(studentsthesis);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + studentsthesis.getThesisName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + studentsthesis.getThesisName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                } else if (updateSupport) {
                    try {
                        studentsThesisMapper.updateStudentsThesis(studentsthesis);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + studentsthesis.getThesisName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + studentsthesis.getThesisName() + " 导入失败：";

                    }

                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、志愿者 " + studentsthesis.getThesisName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、志愿者 " + studentsthesis.getThesisName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
