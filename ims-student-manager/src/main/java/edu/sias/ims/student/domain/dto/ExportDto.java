package edu.sias.ims.student.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;

import java.time.LocalDate;
import java.util.Date;

public class ExportDto {
    private static final long serialVersionUID = 1L;

    /**
     * 查询开始日期
     */

    private LocalDate begin;

    /**
     * 查询截止日期
     */
    private LocalDate end;


    /**
     * 学号
     */
    @Excel(name = "学号")
    private String studentNumber;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 专业
     */
    @Excel(name = "专业")
    private String major;

    /**
     * 班级
     */
    @Excel(name = "班级")
    private String studentClass;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String phoneNumber;


    /**
     * 竞赛名称
     */
    @Excel(name = "竞赛名称")
    private String competitionName;

    /**
     * 赛道名称
     */
    @Excel(name = "赛道名称")
    private String trackName;

    /**
     * 获奖级别
     */
    @Excel(name = "获奖级别")
    private String awardLevel;

    /**
     * 名次
     */
    @Excel(name = "名次")
    private String position;

    /**
     * 颁发单位
     */
    @Excel(name = "颁发单位")
    private String issuingUnit;

    /**
     * 获奖时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "竞赛获奖时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date awardTime;


    /**
     * 奖学金名称
     */
    @Excel(name = "奖学金名称")
    private String scholarshipName;

    /**
     * 级别
     */
    @Excel(name = "级别")
    private String level;

    /**
     * 资助单位
     */
    @Excel(name = "资助单位")
    private String fundingUnit;

    /**
     * 获得时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "奖学金获得时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date acquisitionDate;


    /**
     * 证书名称
     */
    @Excel(name = "证书名称")
    private String certificateName;


    /**
     * 颁发时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "颁发时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date issuanceDate;


    @Excel(name = "服务内容")
    private String serviceContent;

    /**
     * 服务时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "服务时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date serviceTime;

    /**
     * 公示文件附件*
     */
    @Excel(name = "公示文件附件*")
    private String documentAttachment;


    public ExportDto() {
    }

    public ExportDto( LocalDate begin, LocalDate end, String studentNumber, String name, String major, String studentClass, String phoneNumber, String competitionName, String trackName, String awardLevel, String position, String issuingUnit, Date awardTime, String scholarshipName, String level, String fundingUnit, Date acquisitionDate, String certificateName, Date issuanceDate, String serviceContent, Date serviceTime, String documentAttachment) {

        this.begin = begin;
        this.end = end;
        this.studentNumber = studentNumber;
        this.name = name;
        this.major = major;
        this.studentClass = studentClass;
        this.phoneNumber = phoneNumber;
        this.competitionName = competitionName;
        this.trackName = trackName;
        this.awardLevel = awardLevel;
        this.position = position;
        this.issuingUnit = issuingUnit;
        this.awardTime = awardTime;
        this.scholarshipName = scholarshipName;
        this.level = level;
        this.fundingUnit = fundingUnit;
        this.acquisitionDate = acquisitionDate;
        this.certificateName = certificateName;
        this.issuanceDate = issuanceDate;
        this.serviceContent = serviceContent;
        this.serviceTime = serviceTime;
        this.documentAttachment = documentAttachment;
    }

    /**
     * 获取
     * @return begin
     */
    public LocalDate getBegin() {
        return begin;
    }

    /**
     * 设置
     * @param begin
     */
    public void setBegin(LocalDate begin) {
        this.begin = begin;
    }

    /**
     * 获取
     * @return end
     */
    public LocalDate getEnd() {
        return end;
    }

    /**
     * 设置
     * @param end
     */
    public void setEnd(LocalDate end) {
        this.end = end;
    }

    /**
     * 获取
     * @return studentNumber
     */
    public String getStudentNumber() {
        return studentNumber;
    }

    /**
     * 设置
     * @param studentNumber
     */
    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return major
     */
    public String getMajor() {
        return major;
    }

    /**
     * 设置
     * @param major
     */
    public void setMajor(String major) {
        this.major = major;
    }

    /**
     * 获取
     * @return studentClass
     */
    public String getStudentClass() {
        return studentClass;
    }

    /**
     * 设置
     * @param studentClass
     */
    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    /**
     * 获取
     * @return phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * 设置
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * 获取
     * @return competitionName
     */
    public String getCompetitionName() {
        return competitionName;
    }

    /**
     * 设置
     * @param competitionName
     */
    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    /**
     * 获取
     * @return trackName
     */
    public String getTrackName() {
        return trackName;
    }

    /**
     * 设置
     * @param trackName
     */
    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    /**
     * 获取
     * @return awardLevel
     */
    public String getAwardLevel() {
        return awardLevel;
    }

    /**
     * 设置
     * @param awardLevel
     */
    public void setAwardLevel(String awardLevel) {
        this.awardLevel = awardLevel;
    }

    /**
     * 获取
     * @return position
     */
    public String getPosition() {
        return position;
    }

    /**
     * 设置
     * @param position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 获取
     * @return issuingUnit
     */
    public String getIssuingUnit() {
        return issuingUnit;
    }

    /**
     * 设置
     * @param issuingUnit
     */
    public void setIssuingUnit(String issuingUnit) {
        this.issuingUnit = issuingUnit;
    }

    /**
     * 获取
     * @return awardTime
     */
    public Date getAwardTime() {
        return awardTime;
    }

    /**
     * 设置
     * @param awardTime
     */
    public void setAwardTime(Date awardTime) {
        this.awardTime = awardTime;
    }

    /**
     * 获取
     * @return scholarshipName
     */
    public String getScholarshipName() {
        return scholarshipName;
    }

    /**
     * 设置
     * @param scholarshipName
     */
    public void setScholarshipName(String scholarshipName) {
        this.scholarshipName = scholarshipName;
    }

    /**
     * 获取
     * @return level
     */
    public String getLevel() {
        return level;
    }

    /**
     * 设置
     * @param level
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * 获取
     * @return fundingUnit
     */
    public String getFundingUnit() {
        return fundingUnit;
    }

    /**
     * 设置
     * @param fundingUnit
     */
    public void setFundingUnit(String fundingUnit) {
        this.fundingUnit = fundingUnit;
    }

    /**
     * 获取
     * @return acquisitionDate
     */
    public Date getAcquisitionDate() {
        return acquisitionDate;
    }

    /**
     * 设置
     * @param acquisitionDate
     */
    public void setAcquisitionDate(Date acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    /**
     * 获取
     * @return certificateName
     */
    public String getCertificateName() {
        return certificateName;
    }

    /**
     * 设置
     * @param certificateName
     */
    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    /**
     * 获取
     * @return issuanceDate
     */
    public Date getIssuanceDate() {
        return issuanceDate;
    }

    /**
     * 设置
     * @param issuanceDate
     */
    public void setIssuanceDate(Date issuanceDate) {
        this.issuanceDate = issuanceDate;
    }

    /**
     * 获取
     * @return serviceContent
     */
    public String getServiceContent() {
        return serviceContent;
    }

    /**
     * 设置
     * @param serviceContent
     */
    public void setServiceContent(String serviceContent) {
        this.serviceContent = serviceContent;
    }

    /**
     * 获取
     * @return serviceTime
     */
    public Date getServiceTime() {
        return serviceTime;
    }

    /**
     * 设置
     * @param serviceTime
     */
    public void setServiceTime(Date serviceTime) {
        this.serviceTime = serviceTime;
    }

    /**
     * 获取
     * @return documentAttachment
     */
    public String getDocumentAttachment() {
        return documentAttachment;
    }

    /**
     * 设置
     * @param documentAttachment
     */
    public void setDocumentAttachment(String documentAttachment) {
        this.documentAttachment = documentAttachment;
    }

    public String toString() {
        return "ExportDto{serialVersionUID = " + serialVersionUID + ", begin = " + begin + ", end = " + end + ", studentNumber = " + studentNumber + ", name = " + name + ", major = " + major + ", studentClass = " + studentClass + ", phoneNumber = " + phoneNumber + ", competitionName = " + competitionName + ", trackName = " + trackName + ", awardLevel = " + awardLevel + ", position = " + position + ", issuingUnit = " + issuingUnit + ", awardTime = " + awardTime + ", scholarshipName = " + scholarshipName + ", level = " + level + ", fundingUnit = " + fundingUnit + ", acquisitionDate = " + acquisitionDate + ", certificateName = " + certificateName + ", issuanceDate = " + issuanceDate + ", serviceContent = " + serviceContent + ", serviceTime = " + serviceTime + ", documentAttachment = " + documentAttachment + "}";
    }
}
