package edu.sias.ims.student.controller;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.StudentsIntellectualProperty;
import edu.sias.ims.student.domain.vo.StudentsIntellectualPropertyVo;
import edu.sias.ims.student.service.IStudentsIntellectualPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 知识产权Controller
 * 
 * @author wenchao
 * @date 2024-07-30
 */
@RestController
@RequestMapping("/studentintellectualproperty/StudentIntellectualProperty")
public class StudentsIntellectualPropertyController extends BaseController
{
    @Autowired
    private IStudentsIntellectualPropertyService studentsIntellectualPropertyService;

    /**
     * 查询知识产权列表
     */
    @PreAuthorize("@ss.hasPermi('studentintellectualproperty:StudentIntellectualProperty:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentsIntellectualProperty studentsIntellectualProperty)
    {
        startPage();
        List<StudentsIntellectualPropertyVo> listvo = studentsIntellectualPropertyService.selectStudentsIntellectualPropertyListVo(studentsIntellectualProperty);
        return getDataTable(listvo);

    }

    /**
     * 导出知识产权列表
     */
    @PreAuthorize("@ss.hasPermi('studentintellectualproperty:StudentIntellectualProperty:export')")
    @Log(title = "知识产权", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentsIntellectualProperty studentsIntellectualProperty)
    {
        List<StudentsIntellectualProperty> list = studentsIntellectualPropertyService.selectStudentsIntellectualPropertyList(studentsIntellectualProperty);
        ExcelUtil<StudentsIntellectualProperty> util = new ExcelUtil<StudentsIntellectualProperty>(StudentsIntellectualProperty.class);
        util.exportExcel(response, list, "知识产权数据");
    }

    /**
     * 获取知识产权详细信息
     */
    @PreAuthorize("@ss.hasPermi('studentintellectualproperty:StudentIntellectualProperty:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(studentsIntellectualPropertyService.selectStudentsIntellectualPropertyById(id));
    }

    /**
     * 新增知识产权
     */
    @PreAuthorize("@ss.hasPermi('studentintellectualproperty:StudentIntellectualProperty:add')")
    @Log(title = "知识产权", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentsIntellectualProperty studentsIntellectualProperty)
    {
        return toAjax(studentsIntellectualPropertyService.insertStudentsIntellectualProperty(studentsIntellectualProperty));
    }

    /**
     * 修改知识产权
     */
    @PreAuthorize("@ss.hasPermi('studentintellectualproperty:StudentIntellectualProperty:edit')")
    @Log(title = "知识产权", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentsIntellectualProperty studentsIntellectualProperty)
    {
        return toAjax(studentsIntellectualPropertyService.updateStudentsIntellectualProperty(studentsIntellectualProperty));
    }

    /**
     * 删除知识产权
     */
    @PreAuthorize("@ss.hasPermi('studentintellectualproperty:StudentIntellectualProperty:remove')")
    @Log(title = "知识产权", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(studentsIntellectualPropertyService.deleteStudentsIntellectualPropertyByIds(ids));
    }
    //    **************************************
    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "知识产权导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('studentintellectualproperty:StudentIntellectualProperty:import')")
    @PostMapping("/importstudentintellectualpropertyData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<StudentsIntellectualProperty> util = new ExcelUtil<StudentsIntellectualProperty>(StudentsIntellectualProperty.class);
        List<StudentsIntellectualProperty> StudentsIntellectualPropertyList = util.importExcel(file.getInputStream());
        String message = studentsIntellectualPropertyService.importStudentIntellectualPropertyInfo(StudentsIntellectualPropertyList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/imporstudentsIntellectualPropertyTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<StudentsIntellectualProperty> util = new ExcelUtil<StudentsIntellectualProperty>(StudentsIntellectualProperty.class);
        util.importTemplateExcel(response, "用户数据");
    }
//    **************************************
}
