package edu.sias.ims.student.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import edu.sias.ims.student.domain.Students;
import edu.sias.ims.student.domain.vo.StudentSencienceProjectsVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.student.domain.StudentSencienceProjects;
import edu.sias.ims.student.service.IStudentSencienceProjectsService;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 学生科研立项Controller
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
@RestController
@RequestMapping("/studentScienceProject/projects")
public class StudentSencienceProjectsController extends BaseController
{
    @Autowired
    private IStudentSencienceProjectsService studentSencienceProjectsService;


    /**
     * 导入
     *
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "学生信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('studentScienceProject:projects::import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<StudentSencienceProjectsVo> util = new ExcelUtil<StudentSencienceProjectsVo>(StudentSencienceProjectsVo.class);
        List<StudentSencienceProjectsVo> studentsList = util.importExcel(file.getInputStream());
        String message = studentSencienceProjectsService.importStudentSencienceProjectsVoService(studentsList, updateSupport);
        return success(message);
    }

    /**
     * 模版
     *
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<StudentSencienceProjectsVo> util = new ExcelUtil<StudentSencienceProjectsVo>(StudentSencienceProjectsVo.class);
        util.importTemplateExcel(response, "用户数据");
    }


    /**
     * 查询学生科研立项列表
     */
    @PreAuthorize("@ss.hasPermi('studentScienceProject:projects:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentSencienceProjects studentSencienceProjects)
    {
        startPage();
        List<StudentSencienceProjectsVo> list = studentSencienceProjectsService.selectStudentSencienceProjectsList(studentSencienceProjects);
        return getDataTable(list);
    }

    /**
     * 导出学生科研立项列表
     */
    @PreAuthorize("@ss.hasPermi('studentScienceProject:projects:export')")
    @Log(title = "学生科研立项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentSencienceProjects studentSencienceProjects)
    {
        List<StudentSencienceProjectsVo> list = studentSencienceProjectsService.selectStudentSencienceProjectsList(studentSencienceProjects);
        ExcelUtil<StudentSencienceProjectsVo> util = new ExcelUtil<StudentSencienceProjectsVo>(StudentSencienceProjectsVo.class);
        util.exportExcel(response, list, "学生科研立项数据");
    }

    /**
     * 获取学生科研立项详细信息
     */
    @PreAuthorize("@ss.hasPermi('studentScienceProject:projects:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(studentSencienceProjectsService.selectStudentSencienceProjectsById(id));
    }

    /**
     * 新增学生科研立项
     */
    @PreAuthorize("@ss.hasPermi('studentScienceProject:projects:add')")
    @Log(title = "学生科研立项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentSencienceProjects studentSencienceProjects)
    {
        return toAjax(studentSencienceProjectsService.insertStudentSencienceProjects(studentSencienceProjects));
    }

    /**
     * 修改学生科研立项
     */
    @PreAuthorize("@ss.hasPermi('studentScienceProject:projects:edit')")
    @Log(title = "学生科研立项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentSencienceProjects studentSencienceProjects)
    {
        return toAjax(studentSencienceProjectsService.updateStudentSencienceProjects(studentSencienceProjects));
    }

    /**
     * 删除学生科研立项
     */
    @PreAuthorize("@ss.hasPermi('studentScienceProject:projects:remove')")
    @Log(title = "学生科研立项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(studentSencienceProjectsService.deleteStudentSencienceProjectsByIds(ids));
    }
}
