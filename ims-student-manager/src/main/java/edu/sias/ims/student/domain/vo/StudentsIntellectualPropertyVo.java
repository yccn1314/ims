package edu.sias.ims.student.domain.vo;

import edu.sias.ims.student.domain.StudentsIntellectualProperty;

public class StudentsIntellectualPropertyVo extends StudentsIntellectualProperty {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
