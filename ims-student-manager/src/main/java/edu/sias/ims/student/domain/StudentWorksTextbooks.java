package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;

/**
 * 著作教材对象 student_works_textbooks
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
public class StudentWorksTextbooks extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String title;

    /** 类型（指著作或教材） */
    @Excel(name = "类型", readConverterExp = "指=著作或教材")
    private String type;

    /** 作者学号 */
    @Excel(name = "作者学号")
    private String studentNumber;

    /** 工作内容 */
    @Excel(name = "工作内容")
    private String bookWorkContent;

    /** 出版社 */
    @Excel(name = "出版社")
    private String publisher;

    /** 出版日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出版日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publicationDate;

    /** ISBN号 */
    @Excel(name = "ISBN号")
    private String isbn;

    /** 全书总字数 */
    @Excel(name = "全书总字数")
    private String totalWordCount;

    /** 附件*（封面、CIP页、前言、目录） */
    @Excel(name = "附件*", readConverterExp = "封=面、CIP页、前言、目录")
    private String attachment;

    /** 创建人 */
    private String createName;

    /** 修改人 */
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setStudentNumber(String studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public String getStudentNumber() 
    {
        return studentNumber;
    }
    public void setBookWorkContent(String bookWorkContent) 
    {
        this.bookWorkContent = bookWorkContent;
    }

    public String getBookWorkContent() 
    {
        return bookWorkContent;
    }
    public void setPublisher(String publisher) 
    {
        this.publisher = publisher;
    }

    public String getPublisher() 
    {
        return publisher;
    }
    public void setPublicationDate(Date publicationDate) 
    {
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() 
    {
        return publicationDate;
    }
    public void setIsbn(String isbn) 
    {
        this.isbn = isbn;
    }

    public String getIsbn() 
    {
        return isbn;
    }
    public void setTotalWordCount(String totalWordCount) 
    {
        this.totalWordCount = totalWordCount;
    }

    public String getTotalWordCount() 
    {
        return totalWordCount;
    }
    public void setAttachment(String attachment) 
    {
        this.attachment = attachment;
    }

    public String getAttachment() 
    {
        return attachment;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("type", getType())
            .append("studentNumber", getStudentNumber())
            .append("bookWorkContent", getBookWorkContent())
            .append("publisher", getPublisher())
            .append("publicationDate", getPublicationDate())
            .append("isbn", getIsbn())
            .append("totalWordCount", getTotalWordCount())
            .append("attachment", getAttachment())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("remarks", getRemarks())
            .toString();
    }
}
