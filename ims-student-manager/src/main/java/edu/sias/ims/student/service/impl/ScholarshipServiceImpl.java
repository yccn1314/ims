package edu.sias.ims.student.service.impl;

import java.util.List;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.vo.ScholarshipVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.sias.ims.student.mapper.ScholarshipMapper;
import edu.sias.ims.student.domain.Scholarship;
import edu.sias.ims.student.service.IScholarshipService;

/**
 * 奖学金信息管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-16
 */
@Service
public class ScholarshipServiceImpl implements IScholarshipService {
    @Autowired
    private ScholarshipMapper scholarshipMapper;

    /**
     * 查询奖学金信息管理
     *
     * @param id 奖学金信息管理主键
     * @return 奖学金信息管理
     */
    @Override
    public ScholarshipVo selectScholarshipById(Long id) {
        return scholarshipMapper.selectScholarshipById(id);
    }

    /**
     * 查询奖学金信息管理列表
     *
     * @param scholarship 奖学金信息管理
     * @return 奖学金信息管理
     */
    @Override
    public List<ScholarshipVo> selectScholarshipList(ScholarshipVo scholarshipVo) {
        return scholarshipMapper.selectScholarshipList(scholarshipVo);
    }

    /**
     * 新增奖学金信息管理
     *
     * @param scholarship 奖学金信息管理
     * @return 结果
     */
    @Override
    public int insertScholarship(Scholarship scholarship) {
        scholarship.setCreateTime(DateUtils.getNowDate());
        return scholarshipMapper.insertScholarship(scholarship);
    }

    /**
     * 修改奖学金信息管理
     *
     * @param scholarship 奖学金信息管理
     * @return 结果
     */
    @Override
    public int updateScholarship(Scholarship scholarship) {
        scholarship.setUpdateTime(DateUtils.getNowDate());
        return scholarshipMapper.updateScholarship(scholarship);
    }

    /**
     * 批量删除奖学金信息管理
     *
     * @param ids 需要删除的奖学金信息管理主键
     * @return 结果
     */
    @Override
    public int deleteScholarshipByIds(Long[] ids) {
        return scholarshipMapper.deleteScholarshipByIds(ids);
    }

    /**
     * 删除奖学金信息管理信息
     *
     * @param id 奖学金信息管理主键
     * @return 结果
     */
    @Override
    public int deleteScholarshipById(Long id) {
        return scholarshipMapper.deleteScholarshipById(id);
    }

    private static final Logger log = LoggerFactory.getLogger(StudentsServiceImpl.class);

    @Override
    public String importscholarship(List<Scholarship> scholarshipList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(scholarshipList) || scholarshipList.isEmpty()) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (Scholarship scholarship : scholarshipList) {

            try {
                Scholarship u = scholarshipMapper.selectScholarshipById(scholarship.getId());
                if (StringUtils.isNull(u)) {
                    try {
                        scholarshipMapper.insertScholarship(scholarship);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、奖学金 " + scholarship.getScholarshipName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、奖学金 " + scholarship.getScholarshipName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                } else if (updateSupport) {
                    try {
                        scholarshipMapper.updateScholarship(scholarship);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、奖学金 " + scholarship.getScholarshipName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、奖学金 " + scholarship.getScholarshipName() + " 导入失败：";

                    }

                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、奖学金 " + scholarship.getScholarshipName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、奖学金 " + scholarship.getScholarshipName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();

    }
}
