package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 竞赛管理对象 competition
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
public class Competition extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private String studentId;

    /** 竞赛名称 */
    @Excel(name = "竞赛名称")
    private String competitionName;

    /** 赛道名称 */
    @Excel(name = "赛道名称")
    private String trackName;

    /** 获奖级别 */
    @Excel(name = "获奖级别")
    private String awardLevel;

    /** 名次 */
    @Excel(name = "名次")
    private String position;

    /** 颁发单位 */
    @Excel(name = "颁发单位")
    private String issuingUnit;

    /** 获奖时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "获奖时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date awardTime;

    /** 证书 */
    @Excel(name = "证书")
    private String certificate;

    /** 逻辑删除 0:未删除 1：已删除 */
    private Long isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentId(String studentId) 
    {
        this.studentId = studentId;
    }

    public String getStudentId() 
    {
        return studentId;
    }
    public void setCompetitionName(String competitionName) 
    {
        this.competitionName = competitionName;
    }

    public String getCompetitionName() 
    {
        return competitionName;
    }
    public void setTrackName(String trackName) 
    {
        this.trackName = trackName;
    }

    public String getTrackName() 
    {
        return trackName;
    }
    public void setAwardLevel(String awardLevel) 
    {
        this.awardLevel = awardLevel;
    }

    public String getAwardLevel() 
    {
        return awardLevel;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setIssuingUnit(String issuingUnit) 
    {
        this.issuingUnit = issuingUnit;
    }

    public String getIssuingUnit() 
    {
        return issuingUnit;
    }
    public void setAwardTime(Date awardTime) 
    {
        this.awardTime = awardTime;
    }

    public Date getAwardTime() 
    {
        return awardTime;
    }
    public void setCertificate(String certificate) 
    {
        this.certificate = certificate;
    }

    public String getCertificate() 
    {
        return certificate;
    }
    public void setIsDeleted(Long isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentId", getStudentId())
            .append("competitionName", getCompetitionName())
            .append("trackName", getTrackName())
            .append("awardLevel", getAwardLevel())
            .append("position", getPosition())
            .append("issuingUnit", getIssuingUnit())
            .append("awardTime", getAwardTime())
            .append("certificate", getCertificate())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("isDeleted", getIsDeleted())
            .append("remark", getRemark())
            .toString();
    }
}
