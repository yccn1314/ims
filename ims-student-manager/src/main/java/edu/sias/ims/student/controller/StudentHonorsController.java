package edu.sias.ims.student.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import edu.sias.ims.student.domain.Students;
import edu.sias.ims.student.domain.vo.StudentHonorsVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.student.domain.StudentHonors;
import edu.sias.ims.student.service.IStudentHonorsService;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 学生荣誉Controller
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
@RestController
@RequestMapping("/studentHonors/honors")
public class  StudentHonorsController extends BaseController
{
    @Autowired
    private IStudentHonorsService studentHonorsService;

    /**
     * 导入
     *
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "学生信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<StudentHonorsVo> util = new ExcelUtil<StudentHonorsVo>(StudentHonorsVo.class);
        List<StudentHonorsVo> studentsList = util.importExcel(file.getInputStream());
        String message = studentHonorsService.importStudentsHonorService(studentsList, updateSupport);
        return success(message);
    }

    /**
     * 模版
     *
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<StudentHonorsVo> util = new ExcelUtil<StudentHonorsVo>(StudentHonorsVo.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 查询学生荣誉列表
     */
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentHonors studentHonors)
    {
        startPage();
        List<StudentHonorsVo> list = studentHonorsService.selectStudentHonorsList(studentHonors);
        return getDataTable(list);
    }

    /**
     * 导出学生荣誉列表
     */
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:export')")
    @Log(title = "学生荣誉", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentHonors studentHonors)
    {
        List<StudentHonorsVo> list = studentHonorsService.selectStudentHonorsList(studentHonors);
        ExcelUtil<StudentHonorsVo> util = new ExcelUtil<StudentHonorsVo>(StudentHonorsVo.class);
        util.exportExcel(response, list, "学生荣誉数据");
    }

    /**
     * 获取学生荣誉详细信息
     */
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(studentHonorsService.selectStudentHonorsById(id));
    }

    /**
     * 新增学生荣誉
     */
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:add')")
    @Log(title = "学生荣誉", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentHonors studentHonors)
    {
        return toAjax(studentHonorsService.insertStudentHonors(studentHonors));
    }

    /**
     * 修改学生荣誉
     */
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:edit')")
    @Log(title = "学生荣誉", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentHonors studentHonors)
    {
        return toAjax(studentHonorsService.updateStudentHonors(studentHonors));
    }

    /**
     * 删除学生荣誉
     */
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:remove')")
    @Log(title = "学生荣誉", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(studentHonorsService.deleteStudentHonorsByIds(ids));
    }
}
