package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 志愿者证书管理对象 professional_certificates
 * 
 * @author wenchao
 * @date 2024-07-17
 */
public class Certificates extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 职业技能证书id */
    private Long certificateId;

    /** 学号 */
    @Excel(name = "学号")
    private String studentId;

    /** 证书名称 */
    @Excel(name = "证书名称")
    private String certificateName;

    /** 颁发单位 */
    @Excel(name = "颁发单位")
    private String issuingUnit;

    /** 颁发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "颁发时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date issuanceDate;

    /** 证书附件* */
    @Excel(name = "证书附件*")
    private String certificateAttachment;

    /** 修改人 */
    private String updataBy;

    /** 修改时间 */
    private Date updataTime;

    /** 删除标记 */
    private String isDeleted;

    /** 部门id */
    @Excel(name = "部门id")
    private Long sectionId;

    public void setCertificateId(Long certificateId) 
    {
        this.certificateId = certificateId;
    }

    public Long getCertificateId() 
    {
        return certificateId;
    }
    public void setStudentId(String studentId) 
    {
        this.studentId = studentId;
    }

    public String getStudentId() 
    {
        return studentId;
    }
    public void setCertificateName(String certificateName) 
    {
        this.certificateName = certificateName;
    }

    public String getCertificateName() 
    {
        return certificateName;
    }
    public void setIssuingUnit(String issuingUnit) 
    {
        this.issuingUnit = issuingUnit;
    }

    public String getIssuingUnit() 
    {
        return issuingUnit;
    }
    public void setIssuanceDate(Date issuanceDate) 
    {
        this.issuanceDate = issuanceDate;
    }

    public Date getIssuanceDate() 
    {
        return issuanceDate;
    }
    public void setCertificateAttachment(String certificateAttachment) 
    {
        this.certificateAttachment = certificateAttachment;
    }

    public String getCertificateAttachment() 
    {
        return certificateAttachment;
    }
    public void setUpdataBy(String updataBy) 
    {
        this.updataBy = updataBy;
    }

    public String getUpdataBy() 
    {
        return updataBy;
    }
    public void setUpdataTime(Date updataTime) 
    {
        this.updataTime = updataTime;
    }

    public Date getUpdataTime() 
    {
        return updataTime;
    }
    public void setIsDeleted(String isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public String getIsDeleted() 
    {
        return isDeleted;
    }
    public void setSectionId(Long sectionId) 
    {
        this.sectionId = sectionId;
    }

    public Long getSectionId() 
    {
        return sectionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("certificateId", getCertificateId())
            .append("studentId", getStudentId())
            .append("certificateName", getCertificateName())
            .append("issuingUnit", getIssuingUnit())
            .append("issuanceDate", getIssuanceDate())
            .append("certificateAttachment", getCertificateAttachment())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updataBy", getUpdataBy())
            .append("updataTime", getUpdataTime())
            .append("isDeleted", getIsDeleted())
            .append("sectionId", getSectionId())
            .append("remark", getRemark())
            .toString();
    }
}
