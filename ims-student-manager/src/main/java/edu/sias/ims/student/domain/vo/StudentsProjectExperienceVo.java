package edu.sias.ims.student.domain.vo;

import edu.sias.ims.student.domain.StudentsProjectExperience;

public class StudentsProjectExperienceVo extends StudentsProjectExperience {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
