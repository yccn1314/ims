package edu.sias.ims.student.service;

import edu.sias.ims.student.domain.StudentsIntellectualProperty;
import edu.sias.ims.student.domain.vo.StudentsIntellectualPropertyVo;

import java.util.List;

/**
 * 知识产权Service接口
 * 
 * @author wenchao
 * @date 2024-07-30
 */
public interface IStudentsIntellectualPropertyService 
{
    /**
     * 查询知识产权
     * 
     * @param id 知识产权主键
     * @return 知识产权
     */
    public StudentsIntellectualProperty selectStudentsIntellectualPropertyById(Long id);

    /**
     * 查询知识产权列表
     * 
     * @param studentsIntellectualProperty 知识产权
     * @return 知识产权集合
     */
    public List<StudentsIntellectualProperty> selectStudentsIntellectualPropertyList(StudentsIntellectualProperty studentsIntellectualProperty);

    /**
     * 新增知识产权
     * 
     * @param studentsIntellectualProperty 知识产权
     * @return 结果
     */
    public int insertStudentsIntellectualProperty(StudentsIntellectualProperty studentsIntellectualProperty);

    /**
     * 修改知识产权
     * 
     * @param studentsIntellectualProperty 知识产权
     * @return 结果
     */
    public int updateStudentsIntellectualProperty(StudentsIntellectualProperty studentsIntellectualProperty);

    /**
     * 批量删除知识产权
     * 
     * @param ids 需要删除的知识产权主键集合
     * @return 结果
     */
    public int deleteStudentsIntellectualPropertyByIds(Long[] ids);

    /**
     * 删除知识产权信息
     * 
     * @param id 知识产权主键
     * @return 结果
     */
    public int deleteStudentsIntellectualPropertyById(Long id);
    /**
     * 查询知识产权列表
     *
     * @param studentsIntellectualProperty 知识产权
     * @return 知识产权集合
     */
    public List<StudentsIntellectualPropertyVo> selectStudentsIntellectualPropertyListVo(StudentsIntellectualProperty studentsIntellectualProperty);

    public String importStudentIntellectualPropertyInfo(List<StudentsIntellectualProperty> studentsIntellectualPropertyList, boolean updateSupport);
}
