package edu.sias.ims.student.controller;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.Volunteers;
import edu.sias.ims.student.domain.vo.VolunteersVo;
import edu.sias.ims.student.service.IVolunteersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 志愿者管理Controller
 *
 * @author wenchao
 * @date 2024-07-17
 */
@RestController
@RequestMapping("/manage/volunteers")
public class VolunteersController extends BaseController
{
    @Autowired
    private IVolunteersService volunteersService;

    /**
     * 查询志愿者管理列表
     */
    @PreAuthorize("@ss.hasPermi('manage:volunteers:list')")
    @GetMapping("/list")
    public TableDataInfo list(Volunteers volunteers)
    {
        startPage();
//        List<Volunteers> list = volunteersService.selectVolunteersList(volunteers);
        List<VolunteersVo> list = volunteersService.selectVolunteersListVo(volunteers);
        return getDataTable(list);
    }

    /**
     * 导出志愿者管理列表
     */
    @PreAuthorize("@ss.hasPermi('manage:volunteers:export')")
    @Log(title = "志愿者管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Volunteers volunteers)
    {
        List<Volunteers> list = volunteersService.selectVolunteersList(volunteers);
        ExcelUtil<Volunteers> util = new ExcelUtil<Volunteers>(Volunteers.class);
        util.exportExcel(response, list, "志愿者管理数据");
    }
//    **************************************
    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "志愿者导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('volunteer:volunteer:import')")
    @PostMapping("/importvolunteerData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Volunteers> util = new ExcelUtil<Volunteers>(Volunteers.class);
        List<Volunteers> volunteerList = util.importExcel(file.getInputStream());
        String message = volunteersService.importVolunteersInfo(volunteerList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importvolunteerTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<VolunteersVo> util = new ExcelUtil<VolunteersVo>(VolunteersVo.class);
        util.importTemplateExcel(response, "用户数据");
    }
//    **************************************

    /**
     * 获取志愿者管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('manage:volunteers:query')")
    @GetMapping(value = "/{volunteerId}")
    public AjaxResult getInfo(@PathVariable("volunteerId") Long volunteerId)
    {
        return success(volunteersService.selectVolunteersByVolunteerId(volunteerId));
    }

    /**
     * 新增志愿者管理
     */
    @PreAuthorize("@ss.hasPermi('manage:volunteers:add')")
    @Log(title = "志愿者管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Volunteers volunteers)
    {
        return toAjax(volunteersService.insertVolunteers(volunteers));
    }

    /**
     * 修改志愿者管理
     */
    @PreAuthorize("@ss.hasPermi('manage:volunteers:edit')")
    @Log(title = "志愿者管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Volunteers volunteers)
    {
        return toAjax(volunteersService.updateVolunteers(volunteers));
    }

    /**
     * 删除志愿者管理
     */
    @PreAuthorize("@ss.hasPermi('manage:volunteers:remove')")
    @Log(title = "志愿者管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{volunteerIds}")
    public AjaxResult remove(@PathVariable Long[] volunteerIds)
    {
        return toAjax(volunteersService.deleteVolunteersByVolunteerIds(volunteerIds));
    }
}
