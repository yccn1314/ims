package edu.sias.ims.student.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 知识产权对象 students_intellectual_property
 * 
 * @author wenchao
 * @date 2024-07-30
 */
public class StudentsIntellectualProperty extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 成果名称 */
    @Excel(name = "成果名称")
    private String achievementName;

    /** 类别 */
    @Excel(name = "类别")
    private String category;

    /** 负责人学号 */
    @Excel(name = "负责人学号")
    private String principalStudentId;

    /** 成果编号 */
    @Excel(name = "成果编号")
    private String achievementCode;

    /** 完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completionDate;

    /** 成员名字 */
    @Excel(name = "成员名字")
    private String members;

    /** 是否转化金额 */
    private Integer isConverted;

    /** 转化金额（元） */
    @Excel(name = "转化金额", readConverterExp = "元=")
    private String conversionAmount;

    /** 相关附件* */
    @Excel(name = "相关附件*")
    private String attachment;

    /** 删除标记 */
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAchievementName(String achievementName) 
    {
        this.achievementName = achievementName;
    }

    public String getAchievementName() 
    {
        return achievementName;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setPrincipalStudentId(String principalStudentId) 
    {
        this.principalStudentId = principalStudentId;
    }

    public String getPrincipalStudentId() 
    {
        return principalStudentId;
    }
    public void setAchievementCode(String achievementCode) 
    {
        this.achievementCode = achievementCode;
    }

    public String getAchievementCode() 
    {
        return achievementCode;
    }
    public void setCompletionDate(Date completionDate) 
    {
        this.completionDate = completionDate;
    }

    public Date getCompletionDate() 
    {
        return completionDate;
    }
    public void setMembers(String members) 
    {
        this.members = members;
    }

    public String getMembers() 
    {
        return members;
    }
    public void setIsConverted(Integer isConverted) 
    {
        this.isConverted = isConverted;
    }

    public Integer getIsConverted() 
    {
        return isConverted;
    }
    public void setConversionAmount(String conversionAmount) 
    {
        this.conversionAmount = conversionAmount;
    }

    public String getConversionAmount() 
    {
        return conversionAmount;
    }
    public void setAttachment(String attachment) 
    {
        this.attachment = attachment;
    }

    public String getAttachment() 
    {
        return attachment;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("achievementName", getAchievementName())
            .append("category", getCategory())
            .append("principalStudentId", getPrincipalStudentId())
            .append("achievementCode", getAchievementCode())
            .append("completionDate", getCompletionDate())
            .append("members", getMembers())
            .append("isConverted", getIsConverted())
            .append("conversionAmount", getConversionAmount())
            .append("attachment", getAttachment())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
