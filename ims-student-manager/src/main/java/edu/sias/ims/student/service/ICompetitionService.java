package edu.sias.ims.student.service;

import java.util.List;
import edu.sias.ims.student.domain.Competition;
import edu.sias.ims.student.domain.vo.CompetitionVo;

/**
 * 竞赛管理Service接口
 *
 * @author ruoyi
 * @date 2024-07-16
 */
public interface ICompetitionService
{
    /**
     * 查询竞赛管理
     *
     * @param id 竞赛管理主键
     * @return 竞赛管理
     */
    public Competition selectCompetitionById(Long id);

    /**
     * 查询竞赛管理列表
     *
     * @param competition 竞赛管理
     * @return 竞赛管理集合
     */
    public List<CompetitionVo> selectCompetitionList(CompetitionVo competitionVo);

    /**
     * 新增竞赛管理
     *
     * @param competition 竞赛管理
     * @return 结果
     */
    public int insertCompetition(CompetitionVo competitionVo);

    /**
     * 修改竞赛管理
     *
     * @param competition 竞赛管理
     * @return 结果
     */
    public int updateCompetition(CompetitionVo competitionVo);

    /**
     * 批量删除竞赛管理
     *
     * @param ids 需要删除的竞赛管理主键集合
     * @return 结果
     */
    public int deleteCompetitionByIds(Long[] ids);

    /**
     * 删除竞赛管理信息
     *
     * @param id 竞赛管理主键
     * @return 结果
     */
    public int deleteCompetitionById(Long id);

    String importscholarship(List<Competition> competitionsList, boolean updateSupport);
}
