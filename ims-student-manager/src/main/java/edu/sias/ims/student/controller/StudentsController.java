package edu.sias.ims.student.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.Students;
import edu.sias.ims.student.service.IStudentsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * 学生信息管理Controller
 *
 * @author ruoyi
 * @date 2024-07-16
 */
@RestController
@RequestMapping("/StudentManger/students")
public class StudentsController extends BaseController {

    /**
     * 导入
     *
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "学生信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('StudentManger:students:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<Students> util = new ExcelUtil<Students>(Students.class);
        List<Students> studentsList = util.importExcel(file.getInputStream());
        String message = studentsService.importStudentsService(studentsList, updateSupport);
        return success(message);
    }

    /**
     * 模版
     *
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<Students> util = new ExcelUtil<Students>(Students.class);
        util.importTemplateExcel(response, "用户数据");
    }


    @Autowired
    private IStudentsService studentsService;

    /**
     * 查询学生信息管理列表
     */
    @PreAuthorize("@ss.hasPermi('StudentManger:students:list')")
    @GetMapping("/list")
    public TableDataInfo list(Students students) {
        startPage();
        List<Students> list = studentsService.selectStudentsList(students);
        return getDataTable(list);
    }

    /**
     * 导出学生信息管理列表
     */
    @PreAuthorize("@ss.hasPermi('StudentManger:students:export')")
    @Log(title = "学生信息管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Students students) {
        List<Students> list = studentsService.selectStudentsList(students);
        ExcelUtil<Students> util = new ExcelUtil<Students>(Students.class);
        util.exportExcel(response, list, "学生信息管理数据");
    }

    /**
     * 获取学生信息管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('StudentManger:students:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(studentsService.selectStudentsById(id));
    }

    /**
     * 新增学生信息管理
     */
    @PreAuthorize("@ss.hasPermi('StudentManger:students:add')")
    @Log(title = "学生信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Students students) {
        return toAjax(studentsService.insertStudents(students));
    }

    /**
     * 修改学生信息管理
     */
    @PreAuthorize("@ss.hasPermi('StudentManger:students:edit')")
    @Log(title = "学生信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Students students) {
        return toAjax(studentsService.updateStudents(students));
    }

    /**
     * 删除学生信息管理
     */
    @PreAuthorize("@ss.hasPermi('StudentManger:students:remove')")
    @Log(title = "学生信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(studentsService.deleteStudentsByIds(ids));
    }
}
