package edu.sias.ims.student.service.impl;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.StudentsProjectExperience;
import edu.sias.ims.student.domain.vo.StudentsProjectExperienceVo;
import edu.sias.ims.student.mapper.StudentsProjectExperienceMapper;
import edu.sias.ims.student.service.IStudentsProjectExperienceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生实习项目经历Service业务层处理
 * 
 * @author 文超
 * @date 2024-07-31
 */
@Service
public class StudentsProjectExperienceServiceImpl implements IStudentsProjectExperienceService 
{
    @Autowired
    private StudentsProjectExperienceMapper studentsProjectExperienceMapper;
    private static final Logger log = LoggerFactory.getLogger(VolunteersServiceImpl.class);
    /**
     * 查询学生实习项目经历
     * 
     * @param id 学生实习项目经历主键
     * @return 学生实习项目经历
     */
    @Override
    public StudentsProjectExperience selectStudentsProjectExperienceById(Long id)
    {
        return studentsProjectExperienceMapper.selectStudentsProjectExperienceById(id);
    }

    /**
     * 查询学生实习项目经历列表
     * 
     * @param studentsProjectExperience 学生实习项目经历
     * @return 学生实习项目经历
     */
    @Override
    public List<StudentsProjectExperience> selectStudentsProjectExperienceList(StudentsProjectExperience studentsProjectExperience)
    {
        return studentsProjectExperienceMapper.selectStudentsProjectExperienceList(studentsProjectExperience);
    }

    /**
     * 新增学生实习项目经历
     * 
     * @param studentsProjectExperience 学生实习项目经历
     * @return 结果
     */
    @Override
    public int insertStudentsProjectExperience(StudentsProjectExperience studentsProjectExperience)
    {
        studentsProjectExperience.setCreateTime(DateUtils.getNowDate());
        return studentsProjectExperienceMapper.insertStudentsProjectExperience(studentsProjectExperience);
    }

    /**
     * 修改学生实习项目经历
     * 
     * @param studentsProjectExperience 学生实习项目经历
     * @return 结果
     */
    @Override
    public int updateStudentsProjectExperience(StudentsProjectExperience studentsProjectExperience)
    {
        studentsProjectExperience.setUpdateTime(DateUtils.getNowDate());
        return studentsProjectExperienceMapper.updateStudentsProjectExperience(studentsProjectExperience);
    }

    /**
     * 批量删除学生实习项目经历
     * 
     * @param ids 需要删除的学生实习项目经历主键
     * @return 结果
     */
    @Override
    public int deleteStudentsProjectExperienceByIds(Long[] ids)
    {
        return studentsProjectExperienceMapper.deleteStudentsProjectExperienceByIds(ids);
    }

    /**
     * 删除学生实习项目经历信息
     * 
     * @param id 学生实习项目经历主键
     * @return 结果
     */
    @Override
    public int deleteStudentsProjectExperienceById(Long id)
    {
        return studentsProjectExperienceMapper.deleteStudentsProjectExperienceById(id);
    }
    /**
     * 联合查询学生实习项目经历列表
     *
     * @param studentsProjectExperience 学生实习项目经历
     * @return 学生实习项目经历
     */
    @Override
    public List<StudentsProjectExperienceVo> selectStudentsProjectExperienceListVo(StudentsProjectExperience studentsProjectExperience) {
        return studentsProjectExperienceMapper.selectStudentsProjectExperienceListVo(studentsProjectExperience);
    }
//导入学生实习项目经历信息
    @Override
    public String importudentsProjectExperienceInfo(List<StudentsProjectExperience> studentsProjectExperienceList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(studentsProjectExperienceList) || studentsProjectExperienceList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
//        遍历志愿者列表
        for (StudentsProjectExperience studentssrojectexperience : studentsProjectExperienceList) {

            try {
                StudentsProjectExperience u = studentsProjectExperienceMapper.selectStudentsProjectExperienceById(studentssrojectexperience.getId());
                if (StringUtils.isNull(u)) {
                    try {
                        studentsProjectExperienceMapper.insertStudentsProjectExperience(studentssrojectexperience);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + studentssrojectexperience.getStudentId() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + studentssrojectexperience.getStudentId() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                } else if (updateSupport) {
                    try {
                        studentsProjectExperienceMapper.updateStudentsProjectExperience(studentssrojectexperience);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + studentssrojectexperience.getStudentId() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " + studentssrojectexperience.getStudentId() + " 导入失败：";

                    }

                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、志愿者 " + studentssrojectexperience.getStudentId() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、志愿者 " + studentssrojectexperience.getStudentId() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
