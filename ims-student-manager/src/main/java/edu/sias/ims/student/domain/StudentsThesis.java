package edu.sias.ims.student.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 学生论文管理对象 students_thesis
 * 
 * @author wenchao
 * @date 2024-07-31
 */
public class StudentsThesis extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 论文ID */
    private Long id;

    /** 论文名称 */
    @Excel(name = "论文名称")
    private String thesisName;

    /** 支持基金或项目 */
    @Excel(name = "支持基金或项目")
    private String fundingProject;

    /** 作者学号 */
    @Excel(name = "作者学号")
    private String studentId;

    /** 作者名次 */
    @Excel(name = "作者名次")
    private Long authorRank;

    /** 刊物名称 */
    @Excel(name = "刊物名称")
    private String publicationName;

    /** 论文级别 */
    @Excel(name = "论文级别")
    private String thesisLevel;

    /** 统一刊号（CN） */
    @Excel(name = "统一刊号", readConverterExp = "C=N")
    private String cnNumber;

    /** 论文字数 */
    @Excel(name = "论文字数")
    private Long wordCount;

    /** 出版时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出版时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishDate;

    /** 论文检索网站 */
    @Excel(name = "论文检索网站")
    private String searchWebsite;

    /** 论文附件* */
    @Excel(name = "论文附件*")
    private String attachmentThesis;

    /** 删除标记 */
    private Integer isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setThesisName(String thesisName) 
    {
        this.thesisName = thesisName;
    }

    public String getThesisName() 
    {
        return thesisName;
    }
    public void setFundingProject(String fundingProject) 
    {
        this.fundingProject = fundingProject;
    }

    public String getFundingProject() 
    {
        return fundingProject;
    }
    public void setStudentId(String studentId) 
    {
        this.studentId = studentId;
    }

    public String getStudentId() 
    {
        return studentId;
    }
    public void setAuthorRank(Long authorRank) 
    {
        this.authorRank = authorRank;
    }

    public Long getAuthorRank() 
    {
        return authorRank;
    }
    public void setPublicationName(String publicationName) 
    {
        this.publicationName = publicationName;
    }

    public String getPublicationName() 
    {
        return publicationName;
    }
    public void setThesisLevel(String thesisLevel) 
    {
        this.thesisLevel = thesisLevel;
    }

    public String getThesisLevel() 
    {
        return thesisLevel;
    }
    public void setCnNumber(String cnNumber) 
    {
        this.cnNumber = cnNumber;
    }

    public String getCnNumber() 
    {
        return cnNumber;
    }
    public void setWordCount(Long wordCount) 
    {
        this.wordCount = wordCount;
    }

    public Long getWordCount() 
    {
        return wordCount;
    }
    public void setPublishDate(Date publishDate) 
    {
        this.publishDate = publishDate;
    }

    public Date getPublishDate() 
    {
        return publishDate;
    }
    public void setSearchWebsite(String searchWebsite) 
    {
        this.searchWebsite = searchWebsite;
    }

    public String getSearchWebsite() 
    {
        return searchWebsite;
    }
    public void setAttachmentThesis(String attachmentThesis) 
    {
        this.attachmentThesis = attachmentThesis;
    }

    public String getAttachmentThesis() 
    {
        return attachmentThesis;
    }
    public void setIsDeleted(Integer isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Integer getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("thesisName", getThesisName())
            .append("fundingProject", getFundingProject())
            .append("studentId", getStudentId())
            .append("authorRank", getAuthorRank())
            .append("publicationName", getPublicationName())
            .append("thesisLevel", getThesisLevel())
            .append("cnNumber", getCnNumber())
            .append("wordCount", getWordCount())
            .append("publishDate", getPublishDate())
            .append("searchWebsite", getSearchWebsite())
            .append("attachmentThesis", getAttachmentThesis())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("isDeleted", getIsDeleted())
            .toString();
    }
}
