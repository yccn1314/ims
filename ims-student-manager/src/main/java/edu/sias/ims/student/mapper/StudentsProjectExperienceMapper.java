package edu.sias.ims.student.mapper;

import edu.sias.ims.student.domain.StudentsProjectExperience;
import edu.sias.ims.student.domain.vo.StudentsProjectExperienceVo;

import java.util.List;

/**
 * 学生实习项目经历Mapper接口
 * 
 * @author 文超
 * @date 2024-07-31
 */
public interface StudentsProjectExperienceMapper 
{
    /**
     * 查询学生实习项目经历
     * 
     * @param id 学生实习项目经历主键
     * @return 学生实习项目经历
     */
    public StudentsProjectExperience selectStudentsProjectExperienceById(Long id);

    /**
     * 查询学生实习项目经历列表
     * 
     * @param studentsProjectExperience 学生实习项目经历
     * @return 学生实习项目经历集合
     */
    public List<StudentsProjectExperience> selectStudentsProjectExperienceList(StudentsProjectExperience studentsProjectExperience);

    /**
     * 新增学生实习项目经历
     * 
     * @param studentsProjectExperience 学生实习项目经历
     * @return 结果
     */
    public int insertStudentsProjectExperience(StudentsProjectExperience studentsProjectExperience);

    /**
     * 修改学生实习项目经历
     * 
     * @param studentsProjectExperience 学生实习项目经历
     * @return 结果
     */
    public int updateStudentsProjectExperience(StudentsProjectExperience studentsProjectExperience);

    /**
     * 删除学生实习项目经历
     * 
     * @param id 学生实习项目经历主键
     * @return 结果
     */
    public int deleteStudentsProjectExperienceById(Long id);

    /**
     * 批量删除学生实习项目经历
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentsProjectExperienceByIds(Long[] ids);
    /**
     * 查询学生实习项目经历列表
     *
     * @param studentsProjectExperience 学生实习项目经历
     * @return 学生实习项目经历集合
     */
    public List<StudentsProjectExperienceVo> selectStudentsProjectExperienceListVo(StudentsProjectExperience studentsProjectExperience);

}
