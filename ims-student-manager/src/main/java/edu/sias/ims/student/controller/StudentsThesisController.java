package edu.sias.ims.student.controller;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.StudentsThesis;
import edu.sias.ims.student.domain.vo.StudentsThesisVo;
import edu.sias.ims.student.service.IStudentsThesisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学生论文管理Controller
 * 
 * @author wenchao
 * @date 2024-07-31
 */
@RestController
@RequestMapping("/studentshesis/studentshesis")
public class StudentsThesisController extends BaseController
{
    @Autowired
    private IStudentsThesisService studentsThesisService;

    /**
     * 查询学生论文管理列表
     */
    @PreAuthorize("@ss.hasPermi('studentshesis:studentshesis:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentsThesis studentsThesis)
    {
        startPage();
        List<StudentsThesisVo> list = studentsThesisService.selectStudentsThesisListVo(studentsThesis);
        return getDataTable(list);
    }
//    **************************************
    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "学生论文导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('studentshesis:studentshesis:import')")
    @PostMapping("/importstudentshesisData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<StudentsThesis> util = new ExcelUtil<StudentsThesis>(StudentsThesis.class);
        List<StudentsThesis> StudentsThesisList = util.importExcel(file.getInputStream());
        String message = studentsThesisService.importstudentsThesisInfo(StudentsThesisList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importstudentshesisTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<StudentsThesis> util = new ExcelUtil<StudentsThesis>(StudentsThesis.class);
        util.importTemplateExcel(response, "用户数据");
    }
//    **************************************
    /**
     * 导出学生论文管理列表
     */
    @PreAuthorize("@ss.hasPermi('studentshesis:studentshesis:export')")
    @Log(title = "学生论文管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentsThesis studentsThesis)
    {
        List<StudentsThesis> list = studentsThesisService.selectStudentsThesisList(studentsThesis);
        ExcelUtil<StudentsThesis> util = new ExcelUtil<StudentsThesis>(StudentsThesis.class);
        util.exportExcel(response, list, "学生论文管理数据");
    }

    /**
     * 获取学生论文管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('studentshesis:studentshesis:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(studentsThesisService.selectStudentsThesisById(id));
    }

    /**
     * 新增学生论文管理
     */
    @PreAuthorize("@ss.hasPermi('studentshesis:studentshesis:add')")
    @Log(title = "学生论文管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentsThesis studentsThesis)
    {
        return toAjax(studentsThesisService.insertStudentsThesis(studentsThesis));
    }

    /**
     * 修改学生论文管理
     */
    @PreAuthorize("@ss.hasPermi('studentshesis:studentshesis:edit')")
    @Log(title = "学生论文管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentsThesis studentsThesis)
    {
        return toAjax(studentsThesisService.updateStudentsThesis(studentsThesis));
    }

    /**
     * 删除学生论文管理
     */
    @PreAuthorize("@ss.hasPermi('studentshesis:studentshesis:remove')")
    @Log(title = "学生论文管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(studentsThesisService.deleteStudentsThesisByIds(ids));
    }
}
