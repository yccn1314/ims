package edu.sias.ims.student.service.impl;

import java.util.List;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.Scholarship;
import edu.sias.ims.student.domain.vo.CompetitionVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.sias.ims.student.mapper.CompetitionMapper;
import edu.sias.ims.student.domain.Competition;
import edu.sias.ims.student.service.ICompetitionService;

/**
 * 竞赛管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-07-16
 */
@Service
public class CompetitionServiceImpl implements ICompetitionService
{
    @Autowired
    private CompetitionMapper competitionMapper;

    private static final Logger log = LoggerFactory.getLogger(StudentsServiceImpl.class);

    /**
     * 查询竞赛管理
     *
     * @param id 竞赛管理主键
     * @return 竞赛管理
     */
    @Override
    public Competition selectCompetitionById(Long id)
    {
        return competitionMapper.selectCompetitionById(id);
    }

    /**
     * 查询竞赛管理列表
     *
     * @param competition 竞赛管理
     * @return 竞赛管理
     */
    @Override
    public List<CompetitionVo> selectCompetitionList(CompetitionVo competitionVo)
    {
        return competitionMapper.selectCompetitionList(competitionVo);
    }

    /**
     * 新增竞赛管理
     *
     * @param competition 竞赛管理
     * @return 结果
     */
    @Override
    public int insertCompetition(CompetitionVo competitionVo)
    {
        competitionVo.setCreateTime(DateUtils.getNowDate());
        return competitionMapper.insertCompetition(competitionVo);
    }

    /**
     * 修改竞赛管理
     *
     * @param competition 竞赛管理
     * @return 结果
     */
    @Override
    public int updateCompetition(CompetitionVo competitionVo)
    {
        return competitionMapper.updateCompetition(competitionVo);
    }

    /**
     * 批量删除竞赛管理
     *
     * @param ids 需要删除的竞赛管理主键
     * @return 结果
     */
    @Override
    public int deleteCompetitionByIds(Long[] ids)
    {
        return competitionMapper.deleteCompetitionByIds(ids);
    }

    /**
     * 删除竞赛管理信息
     *
     * @param id 竞赛管理主键
     * @return 结果
     */
    @Override
    public int deleteCompetitionById(Long id)
    {
        return competitionMapper.deleteCompetitionById(id);
    }

    @Override
    public String importscholarship(List<Competition> competitionsList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(competitionsList) || competitionsList.isEmpty()) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (Competition competition : competitionsList) {

            try {
                Competition u = competitionMapper.selectCompetitionById(competition.getId());
                if (StringUtils.isNull(u)) {
                    try {
                        competitionMapper.insertCompetition(competition);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "竞赛信息 " + competition.getCompetitionName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "竞赛信息 " + competition.getCompetitionName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                } else if (updateSupport) {
                    try {
                        competitionMapper.updateCompetition(competition);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "竞赛信息 " + competition.getCompetitionName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "竞赛信息 " + competition.getCompetitionName() + " 导入失败：";

                    }

                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "竞赛信息 " + competition.getCompetitionName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "竞赛信息 " + competition.getCompetitionName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
