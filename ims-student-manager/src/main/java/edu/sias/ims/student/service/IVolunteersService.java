package edu.sias.ims.student.service;

import edu.sias.ims.student.domain.Volunteers;
import edu.sias.ims.student.domain.vo.VolunteersVo;

import java.util.List;


/**
 * 志愿者管理Service接口
 *
 * @author wenchao
 * @date 2024-07-17
 */
public interface IVolunteersService {

    public String importVolunteersInfo(List<Volunteers> volunteerList, boolean updateSupport) ;

    /**
     * 查询志愿者管理
     *
     * @param volunteerId 志愿者管理主键
     * @return 志愿者管理
     */
    public Volunteers selectVolunteersByVolunteerId(Long volunteerId);

    /**
     * 查询志愿者管理列表
     *
     * @param volunteers 志愿者管理
     * @return 志愿者管理集合
     */
    public List<Volunteers> selectVolunteersList(Volunteers volunteers);

    /**
     * 新增志愿者管理
     *
     * @param volunteers 志愿者管理
     * @return 结果
     */
    public int insertVolunteers(Volunteers volunteers);

    /**
     * 修改志愿者管理
     *
     * @param volunteers 志愿者管理
     * @return 结果
     */
    public int updateVolunteers(Volunteers volunteers);

    /**
     * 批量删除志愿者管理
     *
     * @param volunteerIds 需要删除的志愿者管理主键集合
     * @return 结果
     */
    public int deleteVolunteersByVolunteerIds(Long[] volunteerIds);

    /**
     * 删除志愿者管理信息
     *
     * @param volunteerId 志愿者管理主键
     * @return 结果
     */
    public int deleteVolunteersByVolunteerId(Long volunteerId);
    /**
     * 查询志愿者管理列表
     *
     * @param volunteers 志愿者管理
     * @return 志愿者管理集合
     */
    public List<VolunteersVo> selectVolunteersListVo(Volunteers volunteers);

}
