package edu.sias.ims.student.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import edu.sias.ims.student.domain.vo.StudentHonorsVo;
import edu.sias.ims.student.domain.vo.StudentWorksTextbooksVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.student.domain.StudentWorksTextbooks;
import edu.sias.ims.student.service.IStudentWorksTextbooksService;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 著作教材Controller
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
@RestController
@RequestMapping("/StudentTextBook/textbooks")
public class StudentWorksTextbooksController extends BaseController
{
    @Autowired
    private IStudentWorksTextbooksService studentWorksTextbooksService;

    /**
     * 导入
     *
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "学生信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('studentHonors:honors:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<StudentWorksTextbooksVo> util = new ExcelUtil<StudentWorksTextbooksVo>(StudentWorksTextbooksVo.class);
        List<StudentWorksTextbooksVo> studentsList = util.importExcel(file.getInputStream());
        String message = studentWorksTextbooksService.importStudentWorksTextbooksVoService(studentsList, updateSupport);
        return success(message);
    }

    /**
     * 模版
     *
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        ExcelUtil<StudentWorksTextbooksVo> util = new ExcelUtil<StudentWorksTextbooksVo>(StudentWorksTextbooksVo.class);
        util.importTemplateExcel(response, "用户数据");
    }



    /**
     * 查询著作教材列表
     */
    @PreAuthorize("@ss.hasPermi('StudentTextBook:textbooks:list')")
    @GetMapping("/list")
    public TableDataInfo list(StudentWorksTextbooks studentWorksTextbooks)
    {
        startPage();
        List<StudentWorksTextbooksVo> list = studentWorksTextbooksService.selectStudentWorksTextbooksList(studentWorksTextbooks);
        return getDataTable(list);
    }

    /**
     * 导出著作教材列表
     */
    @PreAuthorize("@ss.hasPermi('StudentTextBook:textbooks:export')")
    @Log(title = "著作教材", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StudentWorksTextbooks studentWorksTextbooks)
    {
        List<StudentWorksTextbooksVo> list = studentWorksTextbooksService.selectStudentWorksTextbooksList(studentWorksTextbooks);
        ExcelUtil<StudentWorksTextbooksVo> util = new ExcelUtil<StudentWorksTextbooksVo>(StudentWorksTextbooksVo.class);
        util.exportExcel(response, list, "著作教材数据");
    }

    /**
     * 获取著作教材详细信息
     */
    @PreAuthorize("@ss.hasPermi('StudentTextBook:textbooks:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(studentWorksTextbooksService.selectStudentWorksTextbooksById(id));
    }

    /**
     * 新增著作教材
     */
    @PreAuthorize("@ss.hasPermi('StudentTextBook:textbooks:add')")
    @Log(title = "著作教材", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StudentWorksTextbooks studentWorksTextbooks)
    {
        return toAjax(studentWorksTextbooksService.insertStudentWorksTextbooks(studentWorksTextbooks));
    }

    /**
     * 修改著作教材
     */
    @PreAuthorize("@ss.hasPermi('StudentTextBook:textbooks:edit')")
    @Log(title = "著作教材", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StudentWorksTextbooks studentWorksTextbooks)
    {
        return toAjax(studentWorksTextbooksService.updateStudentWorksTextbooks(studentWorksTextbooks));
    }

    /**
     * 删除著作教材
     */
    @PreAuthorize("@ss.hasPermi('StudentTextBook:textbooks:remove')")
    @Log(title = "著作教材", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(studentWorksTextbooksService.deleteStudentWorksTextbooksByIds(ids));
    }
}
