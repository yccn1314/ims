package edu.sias.ims.student.mapper;

import java.util.List;
import edu.sias.ims.student.domain.Students;

/**
 * 学生信息管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
public interface StudentsMapper 
{
    /**
     * 查询学生信息管理
     * 
     * @param id 学生信息管理主键
     * @return 学生信息管理
     */
    public Students selectStudentsById(Long id);

    /**
     * 查询学生信息管理列表
     * 
     * @param students 学生信息管理
     * @return 学生信息管理集合
     */
    public List<Students> selectStudentsList(Students students);

    /**
     * 新增学生信息管理
     * 
     * @param students 学生信息管理
     * @return 结果
     */
    public int insertStudents(Students students);

    /**
     * 修改学生信息管理
     * 
     * @param students 学生信息管理
     * @return 结果
     */
    public int updateStudents(Students students);

    /**
     * 删除学生信息管理
     * 
     * @param id 学生信息管理主键
     * @return 结果
     */
    public int deleteStudentsById(Long id);

    /**
     * 批量删除学生信息管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentsByIds(Long[] ids);
}
