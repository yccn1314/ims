package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;

/**
 * 学生科研立项对象 student_sencience_projects
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
public class StudentSencienceProjects extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 立项编号 */
    @Excel(name = "立项编号")
    private String projectCode;

    /** 项目名称 */
    @Excel(name = "项目名称")
    private String projectName;

    /** 项目级别（分国家级、省级、厅级、校级、企业5类） */
    @Excel(name = "项目级别", readConverterExp = "分=国家级、省级、厅级、校级、企业5类")
    private String projectLevel;

    /** 项目来源（指项目发布单位） */
    @Excel(name = "项目来源", readConverterExp = "指=项目发布单位")
    private String projectSource;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNumber;

    /** 项目名次 */
    @Excel(name = "项目名次")
    private String projectPosition;

    /** 立项时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "立项时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date initiationDate;

    /** 结项时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结项时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completionDate;

    /** 工作内容 */
    @Excel(name = "工作内容")
    private String workContent;

    /** 附件*（申报材料、立项证书、结项材料、结项证书等） */
    @Excel(name = "附件*", readConverterExp = "申=报材料、立项证书、结项材料、结项证书等")
    private String attachments;

    /** 创建人 */
    private String createName;

    /** 修改人 */
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectCode(String projectCode) 
    {
        this.projectCode = projectCode;
    }

    public String getProjectCode() 
    {
        return projectCode;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setProjectLevel(String projectLevel) 
    {
        this.projectLevel = projectLevel;
    }

    public String getProjectLevel() 
    {
        return projectLevel;
    }
    public void setProjectSource(String projectSource) 
    {
        this.projectSource = projectSource;
    }

    public String getProjectSource() 
    {
        return projectSource;
    }
    public void setStudentNumber(String studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public String getStudentNumber() 
    {
        return studentNumber;
    }
    public void setProjectPosition(String projectPosition) 
    {
        this.projectPosition = projectPosition;
    }

    public String getProjectPosition() 
    {
        return projectPosition;
    }
    public void setInitiationDate(Date initiationDate) 
    {
        this.initiationDate = initiationDate;
    }

    public Date getInitiationDate() 
    {
        return initiationDate;
    }
    public void setCompletionDate(Date completionDate) 
    {
        this.completionDate = completionDate;
    }

    public Date getCompletionDate() 
    {
        return completionDate;
    }
    public void setWorkContent(String workContent) 
    {
        this.workContent = workContent;
    }

    public String getWorkContent() 
    {
        return workContent;
    }
    public void setAttachments(String attachments) 
    {
        this.attachments = attachments;
    }

    public String getAttachments() 
    {
        return attachments;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectCode", getProjectCode())
            .append("projectName", getProjectName())
            .append("projectLevel", getProjectLevel())
            .append("projectSource", getProjectSource())
            .append("studentNumber", getStudentNumber())
            .append("projectPosition", getProjectPosition())
            .append("initiationDate", getInitiationDate())
            .append("completionDate", getCompletionDate())
            .append("workContent", getWorkContent())
            .append("attachments", getAttachments())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("remarks", getRemarks())
            .toString();
    }
}
