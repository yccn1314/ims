package edu.sias.ims.student.service.impl;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.student.domain.Certificates;
import edu.sias.ims.student.domain.vo.CertificatesVo;
import edu.sias.ims.student.mapper.CertificatesMapper;
import edu.sias.ims.student.service.ICertificatesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 志愿者证书管理Service业务层处理
 *
 * @author wenchao
 * @date 2024-07-17
 */
@Service
public class CertificatesServiceImpl implements ICertificatesService
{
    @Autowired
    private CertificatesMapper certificatesMapper;

    private static final Logger log = LoggerFactory.getLogger(VolunteersServiceImpl.class);
    @Override
    public String importCertificatesInfo(List<Certificates> certificatesList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(certificatesList) || certificatesList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
//        遍历志愿者列表
        for (Certificates certificate : certificatesList) {

            try {
                Certificates u = certificatesMapper.selectCertificatesByCertificateId(certificate.getCertificateId());
                if (StringUtils.isNull(u)) {
                    try {
                        certificatesMapper.insertCertificates(certificate);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " + certificate.getCertificateId() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " +  certificate.getCertificateId() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                } else if (updateSupport) {
                    try {
                        certificatesMapper.updateCertificates(certificate);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、志愿者 " +  certificate.getCertificateId() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、志愿者 " +  certificate.getCertificateId() + " 导入失败：";

                    }

                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、志愿者 " +  certificate.getCertificateId() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、志愿者 " +  certificate.getCertificateId() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    /**
     * 查询志愿者证书管理
     *
     * @param certificateId 志愿者证书管理主键
     * @return 志愿者证书管理
     */
    @Override
    public Certificates selectCertificatesByCertificateId(Long certificateId)
    {
        return certificatesMapper.selectCertificatesByCertificateId(certificateId);
    }

    /**
     * 查询志愿者证书管理列表
     *
     * @param certificates 志愿者证书管理
     * @return 志愿者证书管理
     */
    @Override
    public List<Certificates> selectCertificatesList(Certificates certificates)
    {
        return certificatesMapper.selectCertificatesList(certificates);
    }

    /**
     * 新增志愿者证书管理
     *
     * @param certificates 志愿者证书管理
     * @return 结果
     */
    @Override
    public int insertCertificates(Certificates certificates)
    {
        certificates.setCreateTime(DateUtils.getNowDate());
        return certificatesMapper.insertCertificates(certificates);
    }

    /**
     * 修改志愿者证书管理
     *
     * @param certificates 志愿者证书管理
     * @return 结果
     */
    @Override
    public int updateCertificates(Certificates certificates)
    {
        return certificatesMapper.updateCertificates(certificates);
    }

    /**
     * 批量删除志愿者证书管理
     *
     * @param certificateIds 需要删除的志愿者证书管理主键
     * @return 结果
     */
    @Override
    public int deleteCertificatesByCertificateIds(Long[] certificateIds)
    {
        return certificatesMapper.deleteCertificatesByCertificateIds(certificateIds);
    }

    /**
     * 删除志愿者证书管理信息
     *
     * @param certificateId 志愿者证书管理主键
     * @return 结果
     */
    @Override
    public int deleteCertificatesByCertificateId(Long certificateId)
    {
        return certificatesMapper.deleteCertificatesByCertificateId(certificateId);
    }

    @Override
    public List<CertificatesVo> selectCertificatesListVo(Certificates certificates) {
        return certificatesMapper.selectCertificatesListVo(certificates);
    }
}
