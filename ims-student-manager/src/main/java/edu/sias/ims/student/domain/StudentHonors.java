package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;

/**
 * 学生荣誉对象 student_honors
 * 
 * @author ruoyi
 * @date 2024-07-31
 */
public class StudentHonors extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private String studentNumber;

    /** 荣誉名称 */
    @Excel(name = "荣誉名称")
    private String honorName;

    /** 颁发单位 */
    @Excel(name = "颁发单位")
    private String grantingOrganization;

    /** 颁发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "颁发时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date awardTime;

    /** 证书附件* */
    @Excel(name = "证书附件*")
    private String certificateAttachment;

    /** 创建人 */
    private String createName;

    /** 修改人 */
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 备注 */
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentNumber(String studentNumber) 
    {
        this.studentNumber = studentNumber;
    }

    public String getStudentNumber() 
    {
        return studentNumber;
    }
    public void setHonorName(String honorName) 
    {
        this.honorName = honorName;
    }

    public String getHonorName() 
    {
        return honorName;
    }
    public void setGrantingOrganization(String grantingOrganization) 
    {
        this.grantingOrganization = grantingOrganization;
    }

    public String getGrantingOrganization() 
    {
        return grantingOrganization;
    }
    public void setAwardTime(Date awardTime) 
    {
        this.awardTime = awardTime;
    }

    public Date getAwardTime() 
    {
        return awardTime;
    }
    public void setCertificateAttachment(String certificateAttachment) 
    {
        this.certificateAttachment = certificateAttachment;
    }

    public String getCertificateAttachment() 
    {
        return certificateAttachment;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentNumber", getStudentNumber())
            .append("honorName", getHonorName())
            .append("grantingOrganization", getGrantingOrganization())
            .append("awardTime", getAwardTime())
            .append("certificateAttachment", getCertificateAttachment())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("remarks", getRemarks())
            .toString();
    }
}
