package edu.sias.ims.student.mapper;

import edu.sias.ims.student.domain.StudentsThesis;
import edu.sias.ims.student.domain.vo.StudentsThesisVo;

import java.util.List;

/**
 * 学生论文管理Mapper接口
 * 
 * @author wenchao
 * @date 2024-07-31
 */
public interface StudentsThesisMapper 
{
    /**
     * 查询学生论文管理
     * 
     * @param id 学生论文管理主键
     * @return 学生论文管理
     */
    public StudentsThesis selectStudentsThesisById(Long id);

    /**
     * 查询学生论文管理列表
     * 
     * @param studentsThesis 学生论文管理
     * @return 学生论文管理集合
     */
    public List<StudentsThesis> selectStudentsThesisList(StudentsThesis studentsThesis);
    /**
     * 查询学生论文管理列表
     *
     * @param studentsThesis 学生论文管理
     * @return 学生论文管理集合
     */
    public List<StudentsThesisVo> selectStudentsThesisListVo(StudentsThesis studentsThesis);

    /**
     * 新增学生论文管理
     * 
     * @param studentsThesis 学生论文管理
     * @return 结果
     */
    public int insertStudentsThesis(StudentsThesis studentsThesis);

    /**
     * 修改学生论文管理
     * 
     * @param studentsThesis 学生论文管理
     * @return 结果
     */
    public int updateStudentsThesis(StudentsThesis studentsThesis);

    /**
     * 删除学生论文管理
     * 
     * @param id 学生论文管理主键
     * @return 结果
     */
    public int deleteStudentsThesisById(Long id);

    /**
     * 批量删除学生论文管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStudentsThesisByIds(Long[] ids);
}
