package edu.sias.ims.student.service.impl;

import edu.sias.ims.student.domain.dto.ExportDto;
import edu.sias.ims.student.mapper.ExportMapper;
import edu.sias.ims.student.service.ExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExportServiceImpl implements ExportService {

    @Autowired
    private ExportMapper exportMapper;

    @Override
    public List<ExportDto> selectByTargat(ExportDto exportDto) {
        return exportMapper.selectByTargat(exportDto);
    }
}
