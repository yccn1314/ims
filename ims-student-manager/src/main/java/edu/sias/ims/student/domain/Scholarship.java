package edu.sias.ims.student.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 奖学金信息管理对象 scholarship
 * 
 * @author ruoyi
 * @date 2024-07-16
 */
public class Scholarship extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 学号 */
    @Excel(name = "学号")
    private Long studentId;

    /** 奖学金名称 */
    @Excel(name = "奖学金名称")
    private String scholarshipName;

    /** 级别 */
    @Excel(name = "级别")
    private String level;

    /** 资助单位 */
    @Excel(name = "资助单位")
    private String fundingUnit;

    /** 获得时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "获得时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date acquisitionDate;

    /** 公示或公告文件附件 */
    @Excel(name = "公示或公告文件附件")
    private String announcementAttachment;

    /** 逻辑删除 0:未删除 1：删除 */
    private Long isDeleted;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStudentId(Long studentId) 
    {
        this.studentId = studentId;
    }

    public Long getStudentId() 
    {
        return studentId;
    }
    public void setScholarshipName(String scholarshipName) 
    {
        this.scholarshipName = scholarshipName;
    }

    public String getScholarshipName() 
    {
        return scholarshipName;
    }
    public void setLevel(String level) 
    {
        this.level = level;
    }

    public String getLevel() 
    {
        return level;
    }
    public void setFundingUnit(String fundingUnit) 
    {
        this.fundingUnit = fundingUnit;
    }

    public String getFundingUnit() 
    {
        return fundingUnit;
    }
    public void setAcquisitionDate(Date acquisitionDate) 
    {
        this.acquisitionDate = acquisitionDate;
    }

    public Date getAcquisitionDate() 
    {
        return acquisitionDate;
    }
    public void setAnnouncementAttachment(String announcementAttachment) 
    {
        this.announcementAttachment = announcementAttachment;
    }

    public String getAnnouncementAttachment() 
    {
        return announcementAttachment;
    }
    public void setIsDeleted(Long isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() 
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentId", getStudentId())
            .append("scholarshipName", getScholarshipName())
            .append("level", getLevel())
            .append("fundingUnit", getFundingUnit())
            .append("acquisitionDate", getAcquisitionDate())
            .append("announcementAttachment", getAnnouncementAttachment())
            .append("isDeleted", getIsDeleted())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
