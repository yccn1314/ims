package edu.sias.ims.student.mapper;

import java.util.List;

import edu.sias.ims.student.domain.Competition;
import edu.sias.ims.student.domain.vo.CompetitionVo;

/**
 * 竞赛管理Mapper接口
 *
 * @author ruoyi
 * @date 2024-07-16
 */
public interface CompetitionMapper
{
    /**
     * 查询竞赛管理
     *
     * @param id 竞赛管理主键
     * @return 竞赛管理
     */
    public CompetitionVo selectCompetitionById(Long id);

    /**
     * 查询竞赛管理列表
     *
     * @param competition 竞赛管理
     * @return 竞赛管理集合
     */
    public List<CompetitionVo> selectCompetitionList(CompetitionVo competitionVo);

    /**
     * 新增竞赛管理
     *
     * @param competition 竞赛管理
     * @return 结果
     */
    public int insertCompetition(Competition competitionVo);

    /**
     * 修改竞赛管理
     *
     * @param competition 竞赛管理
     * @return 结果
     */
    public int updateCompetition(Competition competition);

    /**
     * 删除竞赛管理
     *
     * @param id 竞赛管理主键
     * @return 结果
     */
    public int deleteCompetitionById(Long id);

    /**
     * 批量删除竞赛管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCompetitionByIds(Long[] ids);
}
