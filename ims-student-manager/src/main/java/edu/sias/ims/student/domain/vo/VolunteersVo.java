package edu.sias.ims.student.domain.vo;


import edu.sias.ims.student.domain.Volunteers;

public class VolunteersVo extends Volunteers {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
