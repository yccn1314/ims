package edu.sias.ims.student.domain.vo;

import edu.sias.ims.student.domain.StudentsThesis;

public class StudentsThesisVo  extends StudentsThesis {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
