package edu.sias.ims.student.controller;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.student.domain.Certificates;
import edu.sias.ims.student.domain.vo.CertificatesVo;
import edu.sias.ims.student.service.ICertificatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * 志愿者证书管理Controller
 *
 * @author wenchao
 * @date 2024-07-17
 */
@RestController
@RequestMapping("/certificate/certificates")
public class CertificatesController extends BaseController
{
    @Autowired
    private ICertificatesService certificatesService;

    /**
     * 查询志愿者证书管理列表
     */
    @PreAuthorize("@ss.hasPermi('certificate:certificates:list')")
    @GetMapping("/list")
    public TableDataInfo list(Certificates certificates) {
        startPage();
//        List<Certificates> list = certificatesService.selectCertificatesList(certificates);
        List<CertificatesVo> list = certificatesService.selectCertificatesListVo(certificates);
        return getDataTable(list);
    }

    /**
     * 导出志愿者证书管理列表
     */
    @PreAuthorize("@ss.hasPermi('certificate:certificates:export')")
    @Log(title = "志愿者证书管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Certificates certificates)
    {
        List<Certificates> list = certificatesService.selectCertificatesList(certificates);
        ExcelUtil<Certificates> util = new ExcelUtil<Certificates>(Certificates.class);
        util.exportExcel(response, list, "志愿者证书管理数据");
    }
//    **************************************
    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "志愿者证书导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('certificate:certificates:import')")
    @PostMapping("/importcertificatesData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Certificates> util = new ExcelUtil<Certificates>(Certificates.class);
        List<Certificates> certificatesList = util.importExcel(file.getInputStream());
        String message = certificatesService.importCertificatesInfo(certificatesList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importvolunteerTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<Certificates> util = new ExcelUtil<Certificates>(Certificates.class);
        util.importTemplateExcel(response, "用户数据");
    }
//    **************************************
    /**
     * 获取志愿者证书管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('certificate:certificates:query')")
    @GetMapping(value = "/{certificateId}")
    public AjaxResult getInfo(@PathVariable("certificateId") Long certificateId)
    {
        return success(certificatesService.selectCertificatesByCertificateId(certificateId));
    }

    /**
     * 新增志愿者证书管理
     */
    @PreAuthorize("@ss.hasPermi('certificate:certificates:add')")
    @Log(title = "志愿者证书管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Certificates certificates)
    {
        return toAjax(certificatesService.insertCertificates(certificates));
    }

    /**
     * 修改志愿者证书管理
     */
    @PreAuthorize("@ss.hasPermi('certificate:certificates:edit')")
    @Log(title = "志愿者证书管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Certificates certificates)
    {
        return toAjax(certificatesService.updateCertificates(certificates));
    }

    /**
     * 删除志愿者证书管理
     */
    @PreAuthorize("@ss.hasPermi('certificate:certificates:remove')")
    @Log(title = "志愿者证书管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{certificateIds}")
    public AjaxResult remove(@PathVariable Long[] certificateIds)
    {
        return toAjax(certificatesService.deleteCertificatesByCertificateIds(certificateIds));
    }
}
