package edu.sias.ims.teacher.service.impl;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import edu.sias.ims.teacher.domain.TeacherInfo;
import edu.sias.ims.teacher.mapper.TeacherInfoMapper;
import edu.sias.ims.teacher.service.ITeacherInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师信息Service业务层处理
 * 
 * @author summer
 * @date 2024-07-15
 */
@Service
public class TeacherInfoServiceImpl implements ITeacherInfoService 
{
    @Autowired
    private TeacherInfoMapper teacherInfoMapper;
    private static final Logger log = LoggerFactory.getLogger(TeacherInfoServiceImpl.class);

    /**
     * 查询教师信息
     * 
     * @param id 教师信息主键
     * @return 教师信息
     */
    @Override
    public TeacherInfo selectTeacherInfoById(Long id)
    {
        return teacherInfoMapper.selectTeacherInfoById(id);
    }

    /**
     * 查询教师信息列表
     * 
     * @param teacherInfo 教师信息
     * @return 教师信息
     */
    @Override
    public List<TeacherInfo> selectTeacherInfoList(TeacherInfo teacherInfo)
    {
        return teacherInfoMapper.selectTeacherInfoList(teacherInfo);
    }

    /**
     * 新增教师信息
     * 
     * @param teacherInfo 教师信息
     * @return 结果
     */
    @Override
    public int insertTeacherInfo(TeacherInfo teacherInfo)
    {
        teacherInfo.setCreateTime(DateUtils.getNowDate());
        return teacherInfoMapper.insertTeacherInfo(teacherInfo);
    }

    /**
     * 修改教师信息
     * 
     * @param teacherInfo 教师信息
     * @return 结果
     */
    @Override
    public int updateTeacherInfo(TeacherInfo teacherInfo)
    {
        teacherInfo.setUpdateTime(DateUtils.getNowDate());
        return teacherInfoMapper.updateTeacherInfo(teacherInfo);
    }

    /**
     * 批量删除教师信息
     * 
     * @param ids 需要删除的教师信息主键
     * @return 结果
     */
    @Override
    public int deleteTeacherInfoByIds(Long[] ids)
    {
        return teacherInfoMapper.deleteTeacherInfoByIds(ids);
    }

    /**
     * 删除教师信息信息
     * 
     * @param id 教师信息主键
     * @return 结果
     */
    @Override
    public int deleteTeacherInfoById(Long id)
    {
        return teacherInfoMapper.deleteTeacherInfoById(id);
    }

    @Override
    public String importTeacherInfo(List<TeacherInfo> userList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (TeacherInfo teacherInfo : userList) {

            try{
                TeacherInfo u = teacherInfoMapper.selectTeacherInfoById(teacherInfo.getId());
                if(StringUtils.isNull(u)){
                    try {
                        teacherInfoMapper.insertTeacherInfo(teacherInfo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、教师 " + teacherInfo.getName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、教师 " +  teacherInfo.getName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        teacherInfoMapper.updateTeacherInfo(teacherInfo);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、教师 " +  teacherInfo.getName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、教师 " + teacherInfo.getName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、教师 " +  teacherInfo.getName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、教师 " +  teacherInfo.getName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
