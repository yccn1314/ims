package edu.sias.ims.teacher.domain;

import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 教师信息对象 teacher_info
 *
 * @author summer
 * @date 2024-07-15
 */
public class TeacherInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 姓名 */
    @Excel(name = "*姓名")
    private String name;

    /** 工号 */
    @Excel(name = "*工号")
    private String employeeNumber;

    /** 民族 */
    @Excel(name = "*民族")
    private String nationality;

    /** 0女1男2未知 */
    @Excel(name = "*性别 0女1男2未知")
    private Long gender;

    /** 身份证号 */

    @Excel(name="身份证号")
    private String nationalId;

    /** 手机号 */

    private String phoneNumber;

    /** 所属系别 */
    @Excel(name = "所属系别")
    private String department;

    /** 岗位 */
    @Excel(name = "岗位")
    private String position;

    /** 职称 */
    @Excel(name = "职称")
    private String title;


    /** 职称获取时间 */
    @Excel(name="职称获取时间 yyyy-MM-dd")
    private Date titleAcquisitionDate;

    /** 第一学历 */
    @Excel(name = "第一学历")
    private String firstDegree;

    /** 第一学历专业 */
    @Excel(name = "第一学历专业")
    private String firstDegreeObject;

    /** 第一学历获取时间 */
    private Date firstDegreeDate;

    /** 最高学历 */
//    @Excel(name = "最高学历")
    private String highestDegree;

    public String getHighestDegreeObject() {
        return highestDegreeObject;
    }

    public void setHighestDegreeObject(String highestDegreeObject) {
        this.highestDegreeObject = highestDegreeObject;
    }

    /** 最高学历专业 */
    @Excel(name = "最高学历专业")
    private String highestDegreeObject;

    /** 最高学历获取时间 */
    private Date highestDegreeDate;

    /** 0专职1兼职 */
    @Excel(name = "就职状态 0专职1兼职")
    private Long employmentType;

    /** 政治面貌 */
    @Excel(name = "政治面貌 0~2代表群众,团员,党员")
    private String politicalAffiliation;

    /** 入职时间 */
    @Excel(name="入职时间")
    private Date hireDate;

    /** 创建人 */
    private String createName;

    /** 修改人 */
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long departmentId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setEmployeeNumber(String employeeNumber)
    {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeNumber()
    {
        return employeeNumber;
    }
    public void setNationality(String nationality)
    {
        this.nationality = nationality;
    }

    public String getNationality()
    {
        return nationality;
    }
    public void setGender(Long gender)
    {
        this.gender = gender;
    }

    public Long getGender()
    {
        return gender;
    }
    public void setNationalId(String nationalId)
    {
        this.nationalId = nationalId;
    }

    public String getNationalId()
    {
        return nationalId;
    }
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    public void setDepartment(String department)
    {
        this.department = department;
    }

    public String getDepartment()
    {
        return department;
    }
    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getPosition()
    {
        return position;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setTitleAcquisitionDate(Date titleAcquisitionDate)
    {
        this.titleAcquisitionDate = titleAcquisitionDate;
    }

    public Date getTitleAcquisitionDate()
    {
        return titleAcquisitionDate;
    }
    public void setFirstDegree(String firstDegree)
    {
        this.firstDegree = firstDegree;
    }

    public String getFirstDegree()
    {
        return firstDegree;
    }
    public void setFirstDegreeObject(String firstDegreeObject)
    {
        this.firstDegreeObject = firstDegreeObject;
    }

    public String getFirstDegreeObject()
    {
        return firstDegreeObject;
    }
    public void setFirstDegreeDate(Date firstDegreeDate)
    {
        this.firstDegreeDate = firstDegreeDate;
    }

    public Date getFirstDegreeDate()
    {
        return firstDegreeDate;
    }
    public void setHighestDegree(String highestDegree)
    {
        this.highestDegree = highestDegree;
    }

    public String getHighestDegree()
    {
        return highestDegree;
    }
    public void setHighestDegreeDate(Date highestDegreeDate)
    {
        this.highestDegreeDate = highestDegreeDate;
    }

    public Date getHighestDegreeDate()
    {
        return highestDegreeDate;
    }
    public void setEmploymentType(Long employmentType)
    {
        this.employmentType = employmentType;
    }

    public Long getEmploymentType()
    {
        return employmentType;
    }
    public void setPoliticalAffiliation(String politicalAffiliation)
    {
        this.politicalAffiliation = politicalAffiliation;
    }

    public String getPoliticalAffiliation()
    {
        return politicalAffiliation;
    }
    public void setHireDate(Date hireDate)
    {
        this.hireDate = hireDate;
    }

    public Date getHireDate()
    {
        return hireDate;
    }
    public void setCreateName(String createName)
    {
        this.createName = createName;
    }

    public String getCreateName()
    {
        return createName;
    }
    public void setUpdateName(String updateName)
    {
        this.updateName = updateName;
    }

    public String getUpdateName()
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag)
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag()
    {
        return deleteFlag;
    }
    public void setDepartmentId(Long departmentId)
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId()
    {
        return departmentId;
    }
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }

    public String getRemarks()
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("name", getName())
                .append("employeeNumber", getEmployeeNumber())
                .append("nationality", getNationality())
                .append("gender", getGender())
                .append("nationalId", getNationalId())
                .append("phoneNumber", getPhoneNumber())
                .append("department", getDepartment())
                .append("position", getPosition())
                .append("title", getTitle())
                .append("titleAcquisitionDate", getTitleAcquisitionDate())
                .append("firstDegree", getFirstDegree())
                .append("firstDegreeObject", getFirstDegreeObject())
                .append("firstDegreeDate", getFirstDegreeDate())
                .append("highestDegree", getHighestDegree())
                .append("highestDegreeObject", getHighestDegreeObject())
                .append("highestDegreeDate", getHighestDegreeDate())
                .append("employmentType", getEmploymentType())
                .append("politicalAffiliation", getPoliticalAffiliation())
                .append("hireDate", getHireDate())
                .append("createName", getCreateName())
                .append("createTime", getCreateTime())
                .append("updateName", getUpdateName())
                .append("updateTime", getUpdateTime())
                .append("deleteFlag", getDeleteFlag())
                .append("departmentId", getDepartmentId())
                .append("remarks", getRemarks())
                .toString();
    }
}
