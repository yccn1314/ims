package edu.sias.ims.teacher.controller;


import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.teacher.domain.TeacherInfo;
import edu.sias.ims.teacher.service.ITeacherInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师信息Controller
 *
 * @author summer
 * @date 2024-07-15
 */
@RestController
@RequestMapping("/teacher/teacher")
public class TeacherInfoController extends BaseController
{
    @Autowired
    private ITeacherInfoService teacherInfoService;

    /**
     * 查询教师信息列表
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacher:list')")
    @GetMapping("/list")
    public TableDataInfo list(TeacherInfo teacherInfo)
    {
        startPage();
        List<TeacherInfo> list = teacherInfoService.selectTeacherInfoList(teacherInfo);
        return getDataTable(list);
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "论文导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('teacher:teacher:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<TeacherInfo> util = new ExcelUtil<TeacherInfo>(TeacherInfo.class);
        List<TeacherInfo> userList = util.importExcel(file.getInputStream());
        String message = teacherInfoService.importTeacherInfo(userList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<TeacherInfo> util = new ExcelUtil<TeacherInfo>(TeacherInfo.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 导出教师信息列表
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacher:export')")
    @Log(title = "教师信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TeacherInfo teacherInfo)
    {
        List<TeacherInfo> list = teacherInfoService.selectTeacherInfoList(teacherInfo);
        ExcelUtil<TeacherInfo> util = new ExcelUtil<TeacherInfo>(TeacherInfo.class);
        util.exportExcel(response, list, "教师信息数据");
    }

    /**
     * 获取教师信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacher:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(teacherInfoService.selectTeacherInfoById(id));
    }

    /**
     * 新增教师信息
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacher:add')")
    @Log(title = "教师信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TeacherInfo teacherInfo)
    {
        teacherInfo.setDepartmentId(getDeptId());
        return toAjax(teacherInfoService.insertTeacherInfo(teacherInfo));
    }

    /**
     * 修改教师信息
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacher:edit')")
    @Log(title = "教师信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TeacherInfo teacherInfo)
    {
        return toAjax(teacherInfoService.updateTeacherInfo(teacherInfo));
    }

    /**
     * 删除教师信息
     */
    @PreAuthorize("@ss.hasPermi('teacher:teacher:remove')")
    @Log(title = "教师信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(teacherInfoService.deleteTeacherInfoByIds(ids));
    }
}
