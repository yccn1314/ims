package edu.sias.ims.teacher.service;

import edu.sias.ims.teacher.domain.TeacherInfo;

import java.util.List;

/**
 * 教师信息Service接口
 * 
 * @author summer
 * @date 2024-07-15
 */
public interface ITeacherInfoService 
{
    /**
     * 查询教师信息
     * 
     * @param id 教师信息主键
     * @return 教师信息
     */
    public TeacherInfo selectTeacherInfoById(Long id);

    /**
     * 查询教师信息列表
     * 
     * @param teacherInfo 教师信息
     * @return 教师信息集合
     */
    public List<TeacherInfo> selectTeacherInfoList(TeacherInfo teacherInfo);

    /**
     * 新增教师信息
     * 
     * @param teacherInfo 教师信息
     * @return 结果
     */
    public int insertTeacherInfo(TeacherInfo teacherInfo);

    /**
     * 修改教师信息
     * 
     * @param teacherInfo 教师信息
     * @return 结果
     */
    public int updateTeacherInfo(TeacherInfo teacherInfo);

    /**
     * 批量删除教师信息
     * 
     * @param ids 需要删除的教师信息主键集合
     * @return 结果
     */
    public int deleteTeacherInfoByIds(Long[] ids);

    /**
     * 删除教师信息信息
     * 
     * @param id 教师信息主键
     * @return 结果
     */
    public int deleteTeacherInfoById(Long id);

    String importTeacherInfo(List<TeacherInfo> userList, boolean updateSupport);
}
