/*
 Navicat Premium Data Transfer

 Source Server         : hadoop102
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : 192.168.10.102:3306
 Source Schema         : ims-db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 12/08/2024 15:22:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for QRTZ_BLOB_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_BLOB_TRIGGERS`;
CREATE TABLE `QRTZ_BLOB_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_BLOB_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CALENDARS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CALENDARS`;
CREATE TABLE `QRTZ_CALENDARS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CALENDARS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_CRON_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_CRON_TRIGGERS`;
CREATE TABLE `QRTZ_CRON_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_CRON_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_FIRED_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_FIRED_TRIGGERS`;
CREATE TABLE `QRTZ_FIRED_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(0) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(0) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(0) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_FIRED_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_JOB_DETAILS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_JOB_DETAILS`;
CREATE TABLE `QRTZ_JOB_DETAILS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_JOB_DETAILS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_LOCKS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_LOCKS`;
CREATE TABLE `QRTZ_LOCKS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_LOCKS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_PAUSED_TRIGGER_GRPS`;
CREATE TABLE `QRTZ_PAUSED_TRIGGER_GRPS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_PAUSED_TRIGGER_GRPS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SCHEDULER_STATE
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SCHEDULER_STATE`;
CREATE TABLE `QRTZ_SCHEDULER_STATE`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(0) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(0) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SCHEDULER_STATE
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SIMPLE_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPLE_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPLE_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(0) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(0) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(0) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPLE_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_SIMPROP_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_SIMPROP_TRIGGERS`;
CREATE TABLE `QRTZ_SIMPROP_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(0) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(0) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(0) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(0) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `QRTZ_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `QRTZ_TRIGGERS` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_SIMPROP_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for QRTZ_TRIGGERS
-- ----------------------------
DROP TABLE IF EXISTS `QRTZ_TRIGGERS`;
CREATE TABLE `QRTZ_TRIGGERS`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(0) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(0) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(0) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(0) NOT NULL COMMENT '开始时间',
  `end_time` bigint(0) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(0) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `QRTZ_TRIGGERS_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `QRTZ_JOB_DETAILS` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of QRTZ_TRIGGERS
-- ----------------------------

-- ----------------------------
-- Table structure for adopted_outcomes
-- ----------------------------
DROP TABLE IF EXISTS `adopted_outcomes`;
CREATE TABLE `adopted_outcomes`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `outcome_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '成果名称',
  `principal_investigator_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人工号',
  `adopted_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '采纳单位',
  `adoption_date` date NULL DEFAULT NULL COMMENT '采纳日期',
  `proof_attachment` mediumblob NULL COMMENT '采纳证明*',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `department_id` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `teacher_id` int(0) NULL DEFAULT NULL COMMENT '教师工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '成果采纳' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of adopted_outcomes
-- ----------------------------
INSERT INTO `adopted_outcomes` VALUES (3, '去黑头', '2224124', '中国国家美容院', '2018-07-07', 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F68656C6C6F5F3230323430373137303935323137413030352E747874, NULL, '2024-07-17 09:52:25', NULL, '2024-08-01 18:55:36', NULL, 103, '测试', 2224124);

-- ----------------------------
-- Table structure for check_projects
-- ----------------------------
DROP TABLE IF EXISTS `check_projects`;
CREATE TABLE `check_projects`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `project_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '立项编号',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `project_level` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目级别（分国家级、省级、厅级、校级、企业5类）',
  `project_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目来源（指项目发布单位）',
  `principal_investigator_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主持人工号',
  `check_grade` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '鉴定级别',
  `check_time` datetime(0) NULL DEFAULT NULL COMMENT '鉴定时间',
  `attachments` mediumblob NULL COMMENT '附件*（含申请材料、鉴定结果证书等）',
  `creater` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `partment` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `teacher_id` int(0) NULL DEFAULT NULL COMMENT '教师工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '鉴定项目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of check_projects
-- ----------------------------
INSERT INTO `check_projects` VALUES (1, '1241241', '软件杯', '4', '大学生创业基地', '2224124', '2', '2023-08-01 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F68656C6C6F5F3230323430373137313030353338413031332E747874, NULL, '2024-07-16 20:48:46', NULL, '2024-08-01 18:56:15', NULL, 103, '测试', 2224124);
INSERT INTO `check_projects` VALUES (2, '213121412', '测试', '2', '教育局', '12412412', '3', '2024-07-04 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F32352F68656C6C6F5F3230323430373235303030343539413030312E747874, NULL, '2024-07-25 00:05:22', NULL, '2024-08-01 18:56:24', NULL, 103, '唔', 12412412);
INSERT INTO `check_projects` VALUES (5, '2142', '额外的', '2', '打我的2', '21421', '4', '2024-07-08 00:00:00', NULL, NULL, '2024-07-25 14:49:37', NULL, NULL, NULL, 103, '我', 2);

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `tpl_web_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '前端模板类型（element-ui模版 element-plus模版）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (3, 'teacher_info', '教师信息', NULL, NULL, 'TeacherInfo', 'crud', 'element-plus', 'com.ruoyi.teacher', 'teacher', 'teacher', '教师信息', 'summer', '0', '/', '{\"parentMenuId\":\"\"}', 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47', NULL);
INSERT INTO `gen_table` VALUES (7, 'papers', '论文', NULL, NULL, 'Papers', 'crud', 'element-plus', 'com.ruoyi.papers', 'papers', 'papers', '论文', 'summer', '0', '/', '{\"parentMenuId\":\"\"}', 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21', '论文修改');
INSERT INTO `gen_table` VALUES (8, 'works_textbooks', '著作教材', NULL, NULL, 'WorksTextbooks', 'crud', 'element-plus', 'com.ruoyi.textbook', 'textbook', 'textbooks', '著作教材', 'summer', '0', '/', '{\"parentMenuId\":1}', 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30', '著作教材表');
INSERT INTO `gen_table` VALUES (9, 'check_projects', '鉴定项目', NULL, NULL, 'CheckProjects', 'crud', 'element-plus', 'com.ruoyi.CheckProjects', 'CheckProjects', 'CheckProjects', '鉴定项目', 'sb', '0', '/', '{\"parentMenuId\":2067}', 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint(0) NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 193 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (55, 3, 'id', '唯一标识', 'int', 'Long', 'id', '1', '1', '0', '0', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (56, 3, 'name', '姓名', 'varchar(50)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (57, 3, 'employee_number', '工号', 'varchar(20)', 'String', 'employeeNumber', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 3, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (58, 3, 'nationality', '民族', 'varchar(50)', 'String', 'nationality', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'select', 'nation_choose', 4, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (59, 3, 'gender', '0女1男2未知', 'int', 'Long', 'gender', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'select', 'sys_user_sex', 5, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (60, 3, 'national_id', '身份证号', 'char(18)', 'String', 'nationalId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (61, 3, 'phone_number', '手机号', 'varchar(15)', 'String', 'phoneNumber', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (62, 3, 'department', '所属系别', 'varchar(100)', 'String', 'department', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (63, 3, 'position', '岗位', 'varchar(50)', 'String', 'position', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 9, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (64, 3, 'title', '职称', 'varchar(50)', 'String', 'title', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 10, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (65, 3, 'title_acquisition_date', '职称获取时间', 'date', 'Date', 'titleAcquisitionDate', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'datetime', '', 11, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (66, 3, 'first_degree', '第一学历', 'varchar(100)', 'String', 'firstDegree', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 12, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (67, 3, 'first_degree_object', '第一学历专业', 'varchar(100)', 'String', 'firstDegreeObject', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 13, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (68, 3, 'first_degree_date', '第一学历获取时间', 'date', 'Date', 'firstDegreeDate', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'datetime', '', 14, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (69, 3, 'highest_degree', '最高学历', 'varchar(100)', 'String', 'highestDegree', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 15, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (70, 3, 'highestDegree_object', '最高学历专业', 'varchar(100)', 'String', 'highestdegreeObject', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 16, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (71, 3, 'highest_degree_date', '最高学历获取时间', 'date', 'Date', 'highestDegreeDate', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'datetime', '', 17, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (72, 3, 'employment_type', '0专职1兼职', 'int', 'Long', 'employmentType', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'select', 'teacher_first', 18, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (73, 3, 'political_affiliation', '政治面貌', 'varchar(50)', 'String', 'politicalAffiliation', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'select', 'teacher_status', 19, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (74, 3, 'hire_date', '入职时间', 'date', 'Date', 'hireDate', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'datetime', '', 20, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (75, 3, 'create_name', '创建人', 'varchar(50)', 'String', 'createName', '0', '0', '0', '0', '0', '0', '0', 'LIKE', 'input', '', 21, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (76, 3, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '0', NULL, NULL, NULL, 'EQ', 'datetime', '', 22, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (77, 3, 'update_name', '修改人', 'varchar(50)', 'String', 'updateName', '0', '0', '0', '0', '0', '0', '0', 'LIKE', 'input', '', 23, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (78, 3, 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '0', '0', '0', NULL, NULL, 'EQ', 'datetime', '', 24, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (79, 3, 'delete_flag', '删除标记0未删除1已删除', 'int', 'Long', 'deleteFlag', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 25, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (80, 3, 'department_id', '部门id', 'int', 'Long', 'departmentId', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 26, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (81, 3, 'remarks', '备注', 'text', 'String', 'remarks', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'textarea', '', 27, 'admin', '2024-07-15 16:02:21', '', '2024-07-15 17:02:47');
INSERT INTO `gen_table_column` VALUES (138, 7, 'id', '唯一标识', 'int', 'Long', 'id', '1', '1', '0', '0', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (139, 7, 'paper_title', '论文名称', 'varchar(255)', 'String', 'paperTitle', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (140, 7, 'funding_or_project', '支持基金或项目', 'varchar(255)', 'String', 'fundingOrProject', '0', '0', '0', '0', '1', '0', '0', 'EQ', 'input', '', 3, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (141, 7, 'author_employee_id', '作者工号', 'varchar(20)', 'String', 'authorEmployeeId', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (142, 7, 'author_rank', '作者名次', 'smallint unsigned', 'String', 'authorRank', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'select', 'placing', 5, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (143, 7, 'journal_name', '刊物名称', 'varchar(255)', 'String', 'journalName', '0', '0', '0', '1', '1', '1', '1', 'LIKE', 'input', '', 6, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (144, 7, 'paper_level', '论文级别(0-4对应A到E)', 'char(10)', 'String', 'paperLevel', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'select', 'level', 7, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (145, 7, 'cn_number', '统一刊号（CN）', 'varchar(20)', 'String', 'cnNumber', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 8, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (146, 7, 'word_count', '论文字数', 'int', 'String', 'wordCount', '0', '0', '0', '1', '1', '0', '0', 'EQ', 'input', '', 9, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (147, 7, 'publication_date', '出版时间', 'date', 'Date', 'publicationDate', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'datetime', '', 10, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (148, 7, 'year', '年', 'smallint unsigned', 'String', 'year', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 11, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (149, 7, 'issue', '期', 'smallint unsigned', 'String', 'issue', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 12, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (150, 7, 'search_website', '论文检索网站', 'varchar(255)', 'String', 'searchWebsite', '0', '0', '0', '1', '1', '0', '0', 'EQ', 'input', '', 13, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (151, 7, 'attachment', '论文附件*（刊物封面、CN页、目录、正文、检索页）', 'blob', 'String', 'attachment', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'fileUpload', '', 14, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (152, 7, 'create_name', '创建人', 'varchar(50)', 'String', 'createName', '0', '0', '0', '0', '0', '0', '0', 'LIKE', 'input', '', 15, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (153, 7, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '0', NULL, '1', NULL, 'EQ', 'datetime', '', 16, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (154, 7, 'update_name', '修改人', 'varchar(50)', 'String', 'updateName', '0', '0', '0', '0', '0', '0', '0', 'LIKE', 'input', '', 17, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (155, 7, 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '0', '0', '0', NULL, NULL, 'EQ', 'datetime', '', 18, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (156, 7, 'delete_flag', '删除标记0未删除1已删除', 'int', 'Long', 'deleteFlag', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 19, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (157, 7, 'department_id', '部门id', 'int', 'Long', 'departmentId', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 20, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (158, 7, 'remarks', '备注', 'text', 'String', 'remarks', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'textarea', '', 21, 'admin', '2024-07-15 20:01:55', '', '2024-07-15 20:06:21');
INSERT INTO `gen_table_column` VALUES (159, 8, 'id', '唯一标识', 'int', 'Long', 'id', '1', '1', '0', '0', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (160, 8, 'title', '名称', 'varchar(255)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (161, 8, 'type', '类型（指著作或教材）', 'varchar(100)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (162, 8, 'author_employee_id', '作者工号', 'varchar(20)', 'String', 'authorEmployeeId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (163, 8, 'author_role', '作者名次（分主编、副主编、参编三类，或第一、第二等）', 'varchar(100)', 'String', 'authorRole', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'select', 'placing', 5, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (164, 8, 'publisher', '出版社', 'varchar(100)', 'String', 'publisher', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', '', 6, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (165, 8, 'publication_date', '出版日期', 'date', 'Date', 'publicationDate', '0', '0', '1', '0', '0', '0', '0', 'EQ', 'datetime', '', 7, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (166, 8, 'isbn', 'ISBN号', 'varchar(20)', 'String', 'isbn', '0', '0', '0', '1', '1', '0', '0', 'EQ', 'input', '', 8, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (167, 8, 'total_word_count', '全书总字数', 'int unsigned', 'String', 'totalWordCount', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 9, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (168, 8, 'author_word_count', '本作者撰写字数', 'int unsigned', 'String', 'authorWordCount', '0', '0', '0', '1', '1', '0', '0', 'EQ', 'input', '', 10, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (169, 8, 'attachment', '附件*（封面、CIP页、前言、目录）', 'mediumblob', 'String', 'attachment', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'fileUpload', '', 11, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (170, 8, 'create_name', '创建人', 'varchar(50)', 'String', 'createName', '0', '0', '0', '0', '0', '0', '0', 'LIKE', 'input', '', 12, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (171, 8, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '0', NULL, '1', NULL, 'EQ', 'datetime', '', 13, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (172, 8, 'update_name', '修改人', 'varchar(50)', 'String', 'updateName', '0', '0', '0', '0', '0', '0', '0', 'LIKE', 'input', '', 14, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (173, 8, 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '0', '0', '0', NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (174, 8, 'delete_flag', '删除标记0未删除1已删除', 'int', 'Long', 'deleteFlag', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 16, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (175, 8, 'department_id', '部门id', 'int', 'Long', 'departmentId', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 17, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (176, 8, 'remarks', '备注', 'text', 'String', 'remarks', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'textarea', '', 18, 'admin', '2024-07-15 20:14:12', '', '2024-07-15 20:22:30');
INSERT INTO `gen_table_column` VALUES (177, 9, 'id', '唯一标识', 'int', 'Long', 'id', '1', '1', '0', '0', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (178, 9, 'project_code', '立项编号', 'varchar(50)', 'String', 'projectCode', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (179, 9, 'project_name', '项目名称', 'varchar(255)', 'String', 'projectName', '0', '0', '0', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (180, 9, 'project_level', '项目级别（分国家级、省级、厅级、校级、企业5类）', 'varchar(100)', 'String', 'projectLevel', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'select', 'level', 4, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (181, 9, 'project_source', '项目来源（指项目发布单位）', 'varchar(255)', 'String', 'projectSource', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 5, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (182, 9, 'principal_investigator_id', '主持人工号', 'varchar(20)', 'String', 'principalInvestigatorId', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (183, 9, 'check_grade', '鉴定级别', 'varchar(50)', 'String', 'checkGrade', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'input', '', 7, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (184, 9, 'check_time', '鉴定时间', 'datetime', 'Date', 'checkTime', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'datetime', '', 8, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (185, 9, 'attachments', '附件*（含申请材料、鉴定结果证书等）', 'mediumblob', 'String', 'attachments', '0', '0', '0', '1', '1', '0', '0', 'EQ', NULL, '', 9, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (186, 9, 'creater', '创建人', 'varchar(50)', 'String', 'creater', '0', '0', '0', '1', '0', '0', '0', 'EQ', 'input', '', 10, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (187, 9, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '0', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (188, 9, 'updater', '修改人', 'varchar(50)', 'String', 'updater', '0', '0', '0', '0', '1', '0', '0', 'EQ', 'input', '', 12, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (189, 9, 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '0', '0', '0', NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (190, 9, 'delete_flag', '删除标记0未删除1已删除', 'int', 'Long', 'deleteFlag', '0', '0', '0', '1', '1', '0', '0', 'EQ', 'input', '', 14, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (191, 9, 'partment', '部门id', 'int', 'Long', 'partment', '0', '0', '0', '1', '1', '0', '0', 'EQ', 'input', '', 15, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');
INSERT INTO `gen_table_column` VALUES (192, 9, 'remarks', '备注', 'text', 'String', 'remarks', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'textarea', '', 16, 'admin', '2024-07-16 01:35:12', '', '2024-07-16 01:39:45');

-- ----------------------------
-- Table structure for intellectual_property
-- ----------------------------
DROP TABLE IF EXISTS `intellectual_property`;
CREATE TABLE `intellectual_property`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `achievement_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '成果名称',
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别（软件著作、发明专利、实用新型专利、其他）',
  `principal_investigator_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人工号',
  `achievement_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '成果编号',
  `completion_date` date NULL DEFAULT NULL COMMENT '完成时间',
  `members` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '成员',
  `is_transferred` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否转化(0否1是)',
  `transfer_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '转化金额（元）',
  `attachment` mediumblob NULL COMMENT '附件*（证书等）',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `department_id` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `teacher_id` int(0) NULL DEFAULT NULL COMMENT '教师工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '知识产权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of intellectual_property
-- ----------------------------
INSERT INTO `intellectual_property` VALUES (3, '冯诺依曼架构', '0', '2224124', '1', '2024-07-01', '小李', '0', 0.00, 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F68656C6C6F5F3230323430373137313030313432413031302E747874, NULL, '2024-07-16 20:48:29', NULL, '2024-08-02 11:37:29', NULL, 103, '测试', 2224124);

-- ----------------------------
-- Table structure for papers
-- ----------------------------
DROP TABLE IF EXISTS `papers`;
CREATE TABLE `papers`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `paper_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '论文名称',
  `funding_or_project` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支持基金或项目',
  `author_employee_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者工号',
  `author_rank` smallint(0) UNSIGNED NULL DEFAULT NULL COMMENT '作者名次',
  `journal_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '刊物名称',
  `paper_level` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '论文级别(0-4对应A到E)',
  `cn_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '统一刊号（CN）',
  `word_count` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '论文字数',
  `publication_date` date NULL DEFAULT NULL COMMENT '出版时间',
  `year` smallint(0) UNSIGNED NULL DEFAULT NULL COMMENT '年',
  `issue` smallint(0) UNSIGNED NULL DEFAULT NULL COMMENT '期',
  `search_website` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '论文检索网站',
  `attachment` blob NULL COMMENT '论文附件*（刊物封面、CN页、目录、正文、检索页）',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `department_id` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `teacher_id` int(0) NULL DEFAULT NULL COMMENT '教师工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '论文' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of papers
-- ----------------------------
INSERT INTO `papers` VALUES (2, '量子力学', NULL, '2224124', 1, '物理学的突破', '2', 'CN-35325235-4634', 12214, '2015-07-09', 2015, 34, 'www.123.com', 0x2F70726F66696C652F75706C6F61642F323032342F30382F30322FE7AD94E8BEA9E997AEE9A2985F3230323430383032313730383035413030312E786C7378, NULL, '2024-07-17 09:36:32', NULL, '2024-08-02 17:08:21', NULL, 103, '测试', 2224124);

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `project_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '立项编号',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目名称',
  `project_level` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目级别（分国家级、省级、厅级、校级、企业5类）',
  `project_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目来源（指项目发布单位）',
  `principal_investigator_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '主持人工号',
  `team_members` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '项目成员（限6名，含主持人）',
  `initiation_date` date NULL DEFAULT NULL COMMENT '立项时间',
  `completion_date` date NULL DEFAULT NULL COMMENT '结项时间',
  `attachments` mediumblob NULL COMMENT '附件*（申报材料、立项证书、结项材料、结项证书等）',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `department_id` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `teacher_id` int(0) NULL DEFAULT NULL COMMENT '教师工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '项目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES (1, '12312312', '软件杯', '3', '全国大学生培养基地', '2224124', '小李', '2022-07-07', '2023-07-04', 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F68656C6C6F5F3230323430373137313030353133413031322E747874, NULL, '2024-07-17 10:04:14', NULL, '2024-08-01 18:55:51', NULL, 103, '测试', 2224124);

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(0) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(0) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(0) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(0) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(0) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for student_honors
-- ----------------------------
DROP TABLE IF EXISTS `student_honors`;
CREATE TABLE `student_honors`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `student_number` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学号',
  `honor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '荣誉名称名称',
  `award_time` datetime(0) NULL DEFAULT NULL COMMENT '颁发时间',
  `certificate_attachment` mediumblob NULL COMMENT '证书附件*',
  `granting_organization` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '颁发单位',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT 0 COMMENT '删除标记0未删除1已删除',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student_honors
-- ----------------------------
INSERT INTO `student_honors` VALUES (1, '2022105110001', '三好学生', '2024-07-09 00:00:00', NULL, '学校', NULL, '2024-07-31 17:15:02', NULL, NULL, 0, NULL);
INSERT INTO `student_honors` VALUES (2, '2022105110001', '全国好学生', NULL, '', '学校', NULL, NULL, NULL, NULL, 0, NULL);

-- ----------------------------
-- Table structure for student_sencience_projects
-- ----------------------------
DROP TABLE IF EXISTS `student_sencience_projects`;
CREATE TABLE `student_sencience_projects`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `project_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '立项编号',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '项目名称',
  `project_level` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '项目级别（分国家级、省级、厅级、校级、企业5类）',
  `project_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '项目来源（指项目发布单位）',
  `student_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学号',
  `project_position` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '项目名次',
  `initiation_date` date NULL DEFAULT NULL COMMENT '立项时间',
  `completion_date` date NULL DEFAULT NULL COMMENT '结项时间',
  `work_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工作内容',
  `attachments` mediumblob NULL COMMENT '附件*（申报材料、立项证书、结项材料、结项证书等）',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT 0 COMMENT '删除标记0未删除1已删除',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student_sencience_projects
-- ----------------------------
INSERT INTO `student_sencience_projects` VALUES (1, '1', '1', '2', '1', '1', '1', '2024-07-09', '2024-07-09', NULL, NULL, NULL, '2024-07-31 17:52:18', NULL, NULL, 0, NULL);
INSERT INTO `student_sencience_projects` VALUES (2, '1', '是', '2', '是', '2022105110001', '是', '2024-07-09', '2024-07-26', NULL, NULL, NULL, '2024-07-31 18:02:05', NULL, NULL, 0, NULL);
INSERT INTO `student_sencience_projects` VALUES (3, '1456789', '巨大', '1', ' 大苏打', '2022105110001', '过客', '2024-07-09', '2024-07-17', NULL, NULL, NULL, '2024-07-31 19:21:41', NULL, NULL, 0, NULL);
INSERT INTO `student_sencience_projects` VALUES (4, '11111', '111111', '', '', '2022105110071', '1', NULL, NULL, '2', '', NULL, NULL, NULL, NULL, 1, '111');

-- ----------------------------
-- Table structure for student_works_textbooks
-- ----------------------------
DROP TABLE IF EXISTS `student_works_textbooks`;
CREATE TABLE `student_works_textbooks`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类型（指著作或教材）',
  `student_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作者学号',
  `book_work_content` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工作内容',
  `publisher` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出版社',
  `publication_date` date NULL DEFAULT NULL COMMENT '出版日期',
  `isbn` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'ISBN号',
  `total_word_count` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '全书总字数',
  `attachment` mediumblob NULL COMMENT '附件*（封面、CIP页、前言、目录）',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT 0 COMMENT '删除标记0未删除1已删除',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student_works_textbooks
-- ----------------------------
INSERT INTO `student_works_textbooks` VALUES (1, 'spring教程', '文学类', '2022105110001', NULL, '郑州西亚斯学院出版社', '2024-07-30', '11102148898878', NULL, NULL, NULL, '2024-07-31 19:04:36', NULL, '2024-07-31 19:14:13', 0, NULL);
INSERT INTO `student_works_textbooks` VALUES (2, '1', NULL, '2022105110071', '2', '33', NULL, '123456', 1000000000, '', NULL, NULL, NULL, NULL, 1, '');

-- ----------------------------
-- Table structure for students_competition
-- ----------------------------
DROP TABLE IF EXISTS `students_competition`;
CREATE TABLE `students_competition`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学号',
  `competition_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '竞赛名称',
  `track_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '赛道名称',
  `award_level` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '获奖级别',
  `position` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名次',
  `issuing_unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '颁发单位',
  `award_time` datetime(0) NULL DEFAULT NULL COMMENT '获奖时间',
  `certificate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '证书',
  `create_by` int(0) NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `is_deleted` tinyint(0) NULL DEFAULT 0 COMMENT '逻辑删除 0:未删除 1：已删除',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_competition
-- ----------------------------
INSERT INTO `students_competition` VALUES (1, '2022105110001', '蓝桥杯', 'Java', '省级', NULL, '蓝桥杯组委会', '2024-07-17 00:00:00', NULL, NULL, '2024-07-19 09:37:12', 0, NULL);
INSERT INTO `students_competition` VALUES (2, '2022105110002', 'ACM', '本科', '国家', NULL, '组委会', '2024-07-16 00:00:00', NULL, NULL, '2024-07-19 09:39:54', 0, NULL);

-- ----------------------------
-- Table structure for students_development_experience
-- ----------------------------
DROP TABLE IF EXISTS `students_development_experience`;
CREATE TABLE `students_development_experience`  (
  `development_experience_id` int(0) NOT NULL COMMENT 'id主键',
  `student_id` int(0) NULL DEFAULT NULL COMMENT '学号',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '开发项目名称',
  `work_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '工作内容',
  `delivery_time` datetime(0) NULL DEFAULT NULL COMMENT '交付时间',
  `certificate_attachment` blob NULL COMMENT '证明材料',
  PRIMARY KEY (`development_experience_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_development_experience
-- ----------------------------
INSERT INTO `students_development_experience` VALUES (102, 2022, '学生管理系统', '后端开发', '2024-07-23 16:18:23', '');

-- ----------------------------
-- Table structure for students_info
-- ----------------------------
DROP TABLE IF EXISTS `students_info`;
CREATE TABLE `students_info`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `student_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学号',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姓名',
  `major` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '专业',
  `student_class` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '班级',
  `phone_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `is_deleted` int(0) NULL DEFAULT 0,
  `creator_by` int(0) NULL DEFAULT NULL COMMENT '创建人ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updata_by` int(0) NULL DEFAULT NULL COMMENT '修改人ID',
  `modify_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2032 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_info
-- ----------------------------
INSERT INTO `students_info` VALUES (1, '2022105110001', '张三', '计算机科学与技术', '一班', '13777777777', 0, NULL, '2024-07-19 09:33:30', NULL, NULL, '测试数据');
INSERT INTO `students_info` VALUES (2, '2022105110002', '李四', '软件工程', '1班', '13888888888', 0, NULL, '2024-07-19 09:34:19', NULL, NULL, '测试数据');
INSERT INTO `students_info` VALUES (3, '2024', '于心心', '大数据', '1班', '12578906657', 1, NULL, '2024-07-23 17:01:12', NULL, NULL, '测试数据');
INSERT INTO `students_info` VALUES (4, '2022', '菲菲', '算法', '阿飞', '2321322', 0, 432, '2024-07-17 14:40:03', 24324, '2024-07-25 14:40:09', '测试数据');
INSERT INTO `students_info` VALUES (5, '2021', '猪猪', '发生', '12', '23124', 0, 2431, '2024-07-09 17:13:34', 213, '2024-07-18 17:13:41', '方式');
INSERT INTO `students_info` VALUES (6, '321', '超超', '计科', '2班', '12888888888', 1, NULL, '2024-07-23 17:32:16', NULL, NULL, '数据');
INSERT INTO `students_info` VALUES (2031, '2312', '于洋', '政治', '6班', '19000000000', 0, NULL, '2024-07-23 17:36:29', NULL, NULL, '备注');

-- ----------------------------
-- Table structure for students_intellectual_property
-- ----------------------------
DROP TABLE IF EXISTS `students_intellectual_property`;
CREATE TABLE `students_intellectual_property`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `achievement_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '成果名称',
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '类别',
  `principal_student_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '负责人学号',
  `achievement_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '成果编号',
  `completion_date` date NULL DEFAULT NULL COMMENT '完成时间',
  `members` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '成员',
  `is_converted` tinyint(1) NULL DEFAULT 0 COMMENT '是否转化，0表示未转化，1表示已转化',
  `conversion_amount` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '转化金额（元）',
  `attachment` blob NULL COMMENT '附件（证书等）',
  `create_time` time(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` time(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '删除标记，0表示未删除，1表示已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 103 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_intellectual_property
-- ----------------------------
INSERT INTO `students_intellectual_property` VALUES (102, '物理学突破', '国家级', '2022', '123241243134', '2024-07-30', '鱼鱼，花花', 0, '0', '', '19:00:27', '19:00:31', '大大', '负担', '测试数据', 0);

-- ----------------------------
-- Table structure for students_project_experience
-- ----------------------------
DROP TABLE IF EXISTS `students_project_experience`;
CREATE TABLE `students_project_experience`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `student_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '学号',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '项目名称',
  `work_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '工作内容',
  `delivery_date` date NULL DEFAULT NULL COMMENT '交付时间',
  `certificate_attachment` blob NULL COMMENT '采纳证明附件',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '删除标记，0表示未删除，1表示已删除',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_project_experience
-- ----------------------------
INSERT INTO `students_project_experience` VALUES (1, '2022', '学生管理系统', '<p>开发学生管理模块</p>', '2024-07-30', 0x2F70726F66696C652F75706C6F61642F323032342F30372F33312F746578745F3230323430373331313132373430413030312E747874, '2024-07-30 18:26:14', '2024-07-31 11:27:43', '雯雯', '鱼鱼', 0, '测试数据');
INSERT INTO `students_project_experience` VALUES (2, '2021', '商城管理', '<p>开发购物车模块</p>', '2024-07-30', 0x2F70726F66696C652F75706C6F61642F323032342F30372F33312F746578745F3230323430373331323230363533413030312E747874, '2024-07-31 22:07:03', '2024-08-01 00:26:13', NULL, NULL, 0, '测试数据2');

-- ----------------------------
-- Table structure for students_scholarship
-- ----------------------------
DROP TABLE IF EXISTS `students_scholarship`;
CREATE TABLE `students_scholarship`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '学号',
  `scholarship_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '奖学金名称',
  `level` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '级别',
  `funding_unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '资助单位',
  `acquisition_date` date NULL DEFAULT NULL COMMENT '获得时间',
  `announcement_attachment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公示或公告文件附件',
  `is_deleted` tinyint(0) NULL DEFAULT 0 COMMENT '逻辑删除 0:未删除 1：删除',
  `create_by` int(0) NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(0) NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_scholarship
-- ----------------------------
INSERT INTO `students_scholarship` VALUES (1, '2022105110001', '励志奖学金', '国家', '教育厅', '2024-07-09', '测试文件路径', 0, NULL, '2024-07-19 09:36:19', NULL, NULL, '文件公示暂时不确定存储方式所以展示为文本框');
INSERT INTO `students_scholarship` VALUES (2, '2022105110002', '贫困助学金', '国家', '郑州西亚斯学院', '2024-07-17', '文件路径', 0, NULL, '2024-07-19 09:38:06', NULL, '2024-07-19 09:38:24', '文件公示暂时不确定存储方式所以展示为文本框');

-- ----------------------------
-- Table structure for students_thesis
-- ----------------------------
DROP TABLE IF EXISTS `students_thesis`;
CREATE TABLE `students_thesis`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `thesis_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '论文名称',
  `funding_project` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '支持基金或项目',
  `student_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '作者学号',
  `author_rank` int(0) NOT NULL COMMENT '作者名次',
  `publication_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '刊物名称',
  `thesis_level` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '论文级别',
  `cn_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '统一刊号（CN）',
  `word_count` int(0) NOT NULL COMMENT '论文字数',
  `publish_date` date NOT NULL COMMENT '出版时间',
  `search_website` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '论文检索网站',
  `attachment_thesis` blob NULL COMMENT '论文附件*',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_thesis
-- ----------------------------
INSERT INTO `students_thesis` VALUES (108, '葡萄酒研究', '郑州西亚斯学院', '2022', 2, 'SCI', '1', '32948294', 38970, '2024-07-30', 'www.baidu.com', '', '2024-07-30 20:46:59', '2024-07-30 20:46:59', '夸夸', '苏轼', '测试数据', 0);
INSERT INTO `students_thesis` VALUES (109, '内存回收研究', '加米迪大学', '2021', 0, '北大中文核心', '4', '6538764', 99999, '2024-07-31', 'www.beida.com', NULL, '2024-07-31 11:18:56', '2024-08-01 00:26:28', NULL, NULL, '测试数据', 0);

-- ----------------------------
-- Table structure for students_volunteers
-- ----------------------------
DROP TABLE IF EXISTS `students_volunteers`;
CREATE TABLE `students_volunteers`  (
  `volunteer_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '志愿者id',
  `student_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '志愿者学号',
  `service_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '服务内容',
  `service_time` datetime(0) NOT NULL COMMENT '服务时间',
  `document_attachment` blob NULL COMMENT '公示文件附件*',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `creation_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updata_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `updata_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` int(0) NULL DEFAULT 0,
  `section_id` int(0) NOT NULL COMMENT '部门id',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`volunteer_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students_volunteers
-- ----------------------------
INSERT INTO `students_volunteers` VALUES (103, '2022', '演讲', '2024-07-25 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F746578745F3230323430373137313431373438413030322E747874, NULL, NULL, NULL, NULL, 0, 101, '测试数据1');
INSERT INTO `students_volunteers` VALUES (106, '2021', '<p>学习</p>', '2024-07-31 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F32332F746578745F3230323430373233313632363334413030312E747874, NULL, NULL, NULL, NULL, 0, 109, '测试数据3');
INSERT INTO `students_volunteers` VALUES (107, '2021', '<p>发额</p>', '2024-08-01 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F32332F746578745F3230323430373233313632383131413030322E747874, NULL, NULL, NULL, NULL, 1, 123, '栓发');

-- ----------------------------
-- Table structure for studnts_professional_certificates
-- ----------------------------
DROP TABLE IF EXISTS `studnts_professional_certificates`;
CREATE TABLE `studnts_professional_certificates`  (
  `certificate_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '职业技能证书id',
  `student_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '学号',
  `certificate_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '证书名称',
  `issuing_unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '颁发单位',
  `issuance_date` datetime(0) NOT NULL COMMENT '颁发时间',
  `certificate_attachment` blob NOT NULL COMMENT '证书附件*',
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updata_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '修改人',
  `updata_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` int(0) NULL DEFAULT 0,
  `section_id` int(0) NOT NULL COMMENT '部门id',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`certificate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10212327 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of studnts_professional_certificates
-- ----------------------------
INSERT INTO `studnts_professional_certificates` VALUES (10212324, '2022', '发生过', '郑州西亚斯学院', '2024-07-10 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F746578745F3230323430373137313431373034413030312E747874, NULL, '2024-07-17 14:17:06', NULL, NULL, 0, 102, '测试数据');
INSERT INTO `studnts_professional_certificates` VALUES (10212325, '2021', '蓝桥杯WEB', 'fsafa', '2024-07-03 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F31392F746578745F3230323430373139313531303539413030322E747874, 'chaocaho', '2024-07-17 11:59:39', 'chaochao', '2024-07-25 11:59:47', 0, 105, '测试数据');
INSERT INTO `studnts_professional_certificates` VALUES (10212326, '2023', '蓝桥杯单片机', '郑州西亚斯学院', '2024-07-03 00:00:00', 0x2F70726F66696C652F75706C6F61642F323032342F30372F32332F746578745F3230323430373233313634353135413030312E747874, NULL, '2024-07-23 16:45:30', NULL, NULL, 1, 108, '测试数据3');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2024-07-15 14:20:16', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2024-07-15 14:20:16', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2024-07-15 14:20:16', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'false', 'Y', 'admin', '2024-07-15 14:20:16', 'admin', '2024-07-16 01:22:44', '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2024-07-15 14:20:16', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (6, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2024-07-15 14:20:16', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(0) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(0) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 0, '汉族', '0', 'nation_choose', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:39:07', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (101, 1, '回族', '1', 'nation_choose', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:39:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (102, 2, '傣族', '2', 'nation_choose', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:39:27', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 3, '蒙古族', '3', 'nation_choose', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:39:45', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 0, '群众', '0', 'teacher_status', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:41:18', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (105, 1, '团员', '1', 'teacher_status', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:41:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (106, 2, '党员', '2', 'teacher_status', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:41:41', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 0, '专职', '0', 'teacher_first', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:45:00', '', NULL, '专职');
INSERT INTO `sys_dict_data` VALUES (108, 1, '兼职', '1', 'teacher_first', NULL, 'default', 'N', '0', 'admin', '2024-07-15 14:45:23', '', NULL, '兼职');
INSERT INTO `sys_dict_data` VALUES (109, 0, '企业', '0', 'level', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:12:53', '', NULL, '企业');
INSERT INTO `sys_dict_data` VALUES (110, 1, '校级', '1', 'level', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:13:10', '', NULL, '企业');
INSERT INTO `sys_dict_data` VALUES (111, 2, '厅级', '2', 'level', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:13:22', '', NULL, '厅级');
INSERT INTO `sys_dict_data` VALUES (112, 3, '省级', '3', 'level', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:13:32', '', NULL, '省级');
INSERT INTO `sys_dict_data` VALUES (113, 4, '国家级', '4', 'level', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:13:47', '', NULL, '国家级');
INSERT INTO `sys_dict_data` VALUES (114, 0, '主编', '0', 'placing', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:27:15', '', NULL, '教材主编');
INSERT INTO `sys_dict_data` VALUES (115, 1, '副主编', '1', 'placing', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:30:58', '', NULL, '副主编');
INSERT INTO `sys_dict_data` VALUES (116, 2, '参编', '2', 'placing', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:31:18', '', NULL, '参编');
INSERT INTO `sys_dict_data` VALUES (117, 1, '主持人', '1', 'grade', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:34:03', '', NULL, '主持人');
INSERT INTO `sys_dict_data` VALUES (118, 0, '成员', '0', 'grade', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:34:22', '', NULL, '项目成员');
INSERT INTO `sys_dict_data` VALUES (119, 0, '软件著作', '0', 'category', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:36:59', '', NULL, '软件著作');
INSERT INTO `sys_dict_data` VALUES (120, 1, '发明专利', '1', 'category', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:37:15', '', NULL, '发明专利');
INSERT INTO `sys_dict_data` VALUES (121, 2, '实用新型专利', '2', 'category', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:37:49', '', NULL, '实用新型专利');
INSERT INTO `sys_dict_data` VALUES (122, 3, '其它', '3', 'category', NULL, 'default', 'N', '0', 'admin', '2024-07-15 19:37:59', '', NULL, '其它');
INSERT INTO `sys_dict_data` VALUES (123, 0, '普通刊物', '0', 'paper_level', NULL, 'default', 'N', '0', 'admin', '2024-08-01 18:01:25', '', NULL, '普通刊物');
INSERT INTO `sys_dict_data` VALUES (124, 1, '北大核心刊物', '1', 'paper_level', NULL, 'default', 'N', '0', 'admin', '2024-08-01 18:01:35', '', NULL, '北大核心刊物');
INSERT INTO `sys_dict_data` VALUES (125, 2, '南大核心刊物', '2', 'paper_level', NULL, 'default', 'N', '0', 'admin', '2024-08-01 18:01:47', '', NULL, '南大核心刊物');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2024-07-15 14:20:16', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, '民族选择', 'nation_choose', '0', 'admin', '2024-07-15 14:38:33', '', NULL, '所属民族');
INSERT INTO `sys_dict_type` VALUES (101, '政治面貌', 'teacher_status', '0', 'admin', '2024-07-15 14:41:02', '', NULL, '教师的政治面貌');
INSERT INTO `sys_dict_type` VALUES (102, '专职与兼职', 'teacher_first', '0', 'admin', '2024-07-15 14:44:41', '', NULL, '教师');
INSERT INTO `sys_dict_type` VALUES (103, '级别', 'level', '0', 'admin', '2024-07-15 19:12:11', 'admin', '2024-07-15 19:36:07', '论文,项目,鉴定项目,教师荣誉都可以用');
INSERT INTO `sys_dict_type` VALUES (104, '名次', 'placing', '0', 'admin', '2024-07-15 19:24:41', 'admin', '2024-07-15 19:36:18', '著作教材中的名次');
INSERT INTO `sys_dict_type` VALUES (105, '等级', 'grade', '0', 'admin', '2024-07-15 19:33:36', 'admin', '2024-07-15 19:36:40', '规定项目成员中谁是主持人');
INSERT INTO `sys_dict_type` VALUES (106, '类别', 'category', '0', 'admin', '2024-07-15 19:35:40', '', NULL, '用于知识产权');
INSERT INTO `sys_dict_type` VALUES (107, '论文级别', 'paper_level', '0', 'admin', '2024-08-01 18:01:07', '', NULL, '论文级别');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2024-07-15 14:20:16', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `idx_sys_logininfor_s`(`status`) USING BTREE,
  INDEX `idx_sys_logininfor_lt`(`login_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 155 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-15 14:35:43');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-15 16:39:28');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '1', '验证码错误', '2024-07-15 17:00:31');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-15 17:00:34');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-15 18:56:06');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 01:21:28');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 01:23:29');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 01:57:52');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 03:18:10');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 08:41:57');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 09:04:56');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 11:08:32');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 11:34:48');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 12:24:31');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2024-07-16 12:48:02');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-16 21:11:45');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-17 09:05:22');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-17 10:54:49');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-17 10:55:29');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-17 11:48:26');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-17 11:51:02');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-17 11:52:19');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-17 14:30:25');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-18 14:50:25');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-18 15:09:46');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-19 16:02:54');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-22 09:59:53');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-22 10:29:59');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-24 22:42:18');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-25 09:49:55');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-25 10:39:18');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-25 14:33:29');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-29 12:10:11');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-07-29 15:43:52');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-08-01 12:35:35');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-01 17:58:40');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-01 18:04:18');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-02 11:31:20');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-02 14:34:16');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-02 15:47:07');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-07 09:17:54');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 14:42:23');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 14:46:38');
INSERT INTO `sys_logininfor` VALUES (143, 'laoshi', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 14:46:42');
INSERT INTO `sys_logininfor` VALUES (144, 'laoshi', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 14:47:08');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 14:47:13');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 14:52:23');
INSERT INTO `sys_logininfor` VALUES (147, 'laoshi', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 14:52:28');
INSERT INTO `sys_logininfor` VALUES (148, 'laoshi', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 14:52:42');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 14:52:48');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 14:53:54');
INSERT INTO `sys_logininfor` VALUES (151, 'laoshi', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 14:53:59');
INSERT INTO `sys_logininfor` VALUES (152, 'laoshi', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 14:54:11');
INSERT INTO `sys_logininfor` VALUES (153, 'a', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '1', '用户不存在/密码错误', '2024-08-12 14:54:16');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 14:54:18');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:01:32');
INSERT INTO `sys_logininfor` VALUES (156, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '注册成功', '2024-08-12 15:03:18');
INSERT INTO `sys_logininfor` VALUES (157, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:03:25');
INSERT INTO `sys_logininfor` VALUES (158, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:03:35');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:03:38');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:03:52');
INSERT INTO `sys_logininfor` VALUES (161, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:03:55');
INSERT INTO `sys_logininfor` VALUES (162, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:05:50');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:05:55');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:07:16');
INSERT INTO `sys_logininfor` VALUES (165, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:07:20');
INSERT INTO `sys_logininfor` VALUES (166, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:07:26');
INSERT INTO `sys_logininfor` VALUES (167, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:07:33');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:08:08');
INSERT INTO `sys_logininfor` VALUES (169, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:08:12');
INSERT INTO `sys_logininfor` VALUES (170, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:08:21');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:08:26');
INSERT INTO `sys_logininfor` VALUES (172, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:13:45');
INSERT INTO `sys_logininfor` VALUES (173, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:13:49');
INSERT INTO `sys_logininfor` VALUES (174, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:14:09');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:14:13');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:15:26');
INSERT INTO `sys_logininfor` VALUES (177, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:15:31');
INSERT INTO `sys_logininfor` VALUES (178, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:15:43');
INSERT INTO `sys_logininfor` VALUES (179, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:15:44');
INSERT INTO `sys_logininfor` VALUES (180, 'test', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '退出成功', '2024-08-12 15:15:47');
INSERT INTO `sys_logininfor` VALUES (181, 'admin', '127.0.0.1', '内网IP', 'Firefox 12', 'Windows 10', '0', '登录成功', '2024-08-12 15:15:51');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(0) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '路由参数',
  `route_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '路由名称',
  `is_frame` int(0) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(0) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4078 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 2, 'system', NULL, '', '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2024-07-15 14:20:16', 'admin', '2024-08-07 09:24:05', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 4, 'monitor', NULL, '', '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2024-07-15 14:20:16', 'admin', '2024-08-02 11:56:58', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2024-07-15 14:20:16', 'admin', '2024-08-07 09:23:59', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, '', '', 0, 0, 'M', '1', '0', '', 'guide', 'admin', '2024-07-15 14:20:16', 'admin', '2024-07-19 15:55:01', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2024-07-15 14:20:16', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2024-07-15 14:20:16', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2024-07-15 14:20:16', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2024-07-15 14:20:16', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2024-07-15 14:20:16', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2024-07-15 14:20:16', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2024-07-15 14:20:16', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2024-07-15 14:20:16', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2024-07-15 14:20:16', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2024-07-15 14:20:16', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2024-07-15 14:20:16', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2024-07-15 14:20:16', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2024-07-15 14:20:16', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2024-07-15 14:20:16', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2024-07-15 14:20:16', '', NULL, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2024-07-15 14:20:16', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2024-07-15 14:20:16', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2024-07-15 14:20:16', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2024-07-15 14:20:16', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2024-07-15 14:20:16', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2024-07-15 14:20:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '教师信息', 0, 1, 'teacher', 'teacher/teacher/index', NULL, '', 1, 0, 'C', '0', '0', 'teacher:teacher:list', 'cascader', 'admin', '2024-07-15 16:06:07', 'admin', '2024-07-15 21:37:25', '教师信息菜单');
INSERT INTO `sys_menu` VALUES (2019, '教师信息查询', 2018, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacher:teacher:query', '#', 'admin', '2024-07-15 16:06:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '教师信息新增', 2018, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacher:teacher:add', '#', 'admin', '2024-07-15 16:06:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2021, '教师信息修改', 2018, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacher:teacher:edit', '#', 'admin', '2024-07-15 16:06:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '教师信息删除', 2018, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacher:teacher:remove', '#', 'admin', '2024-07-15 16:06:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '教师信息导出', 2018, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacher:teacher:export', '#', 'admin', '2024-07-15 16:06:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '论文', 2067, 1, 'papers', 'papers/papers/index', NULL, '', 1, 0, 'C', '0', '0', 'papers:papers:list', '#', 'admin', '2024-07-15 20:07:45', 'admin', '2024-07-15 21:35:10', '论文菜单');
INSERT INTO `sys_menu` VALUES (2031, '论文查询', 2030, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'papers:papers:query', '#', 'admin', '2024-07-15 20:07:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '论文新增', 2030, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'papers:papers:add', '#', 'admin', '2024-07-15 20:07:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '论文修改', 2030, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'papers:papers:edit', '#', 'admin', '2024-07-15 20:07:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '论文删除', 2030, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'papers:papers:remove', '#', 'admin', '2024-07-15 20:07:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '论文导出', 2030, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'papers:papers:export', '#', 'admin', '2024-07-15 20:07:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '著作教材', 2067, 1, 'textbook', 'textbook/textbooks/index', NULL, '', 1, 0, 'C', '0', '0', 'textbook:textbooks:list', '#', 'admin', '2024-07-15 20:26:30', 'admin', '2024-07-15 21:35:59', '著作教材菜单');
INSERT INTO `sys_menu` VALUES (2037, '著作教材查询', 2036, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'textbook:textbooks:query', '#', 'admin', '2024-07-15 20:26:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '著作教材新增', 2036, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'textbook:textbooks:add', '#', 'admin', '2024-07-15 20:26:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '著作教材修改', 2036, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'textbook:textbooks:edit', '#', 'admin', '2024-07-15 20:26:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '著作教材删除', 2036, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'textbook:textbooks:remove', '#', 'admin', '2024-07-15 20:26:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '著作教材导出', 2036, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'textbook:textbooks:export', '#', 'admin', '2024-07-15 20:26:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2042, '成果采纳', 2067, 1, 'adoptedOutcomes', 'adoptedOutcomes/adoptedOutcomes/index', NULL, '', 1, 0, 'C', '0', '0', 'adoptedOutcomes:adoptedOutcomes:list', '#', 'admin', '2024-07-15 20:35:28', 'admin', '2024-08-12 14:45:12', '成果采纳菜单');
INSERT INTO `sys_menu` VALUES (2043, '成果采纳查询', 2042, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'adoptedOutcomes:adoptedOutcomes:query', '#', 'admin', '2024-07-15 20:35:28', 'admin', '2024-08-12 14:47:27', '');
INSERT INTO `sys_menu` VALUES (2044, '成果采纳新增', 2042, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'adoptedOutcomes:adoptedOutcomes:add', '#', 'admin', '2024-07-15 20:35:28', 'admin', '2024-08-12 14:47:32', '');
INSERT INTO `sys_menu` VALUES (2045, '成果采纳修改', 2042, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'adoptedOutcomes:adoptedOutcomes:edit', '#', 'admin', '2024-07-15 20:35:28', 'admin', '2024-08-12 14:47:37', '');
INSERT INTO `sys_menu` VALUES (2046, '成果采纳删除', 2042, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'adoptedOutcomes:adoptedOutcomes:remove', '#', 'admin', '2024-07-15 20:35:28', 'admin', '2024-08-12 14:47:42', '');
INSERT INTO `sys_menu` VALUES (2047, '成果采纳导出', 2042, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'adoptedOutcomes:adoptedOutcomes:export', '#', 'admin', '2024-07-15 20:35:28', 'admin', '2024-08-12 14:47:50', '');
INSERT INTO `sys_menu` VALUES (2048, '教师荣誉', 2067, 1, 'teacherHonors', 'teacherHonors/teacherHonors/index', NULL, '', 1, 0, 'C', '0', '0', 'teacherHonors:teacherHonors:list', '#', 'admin', '2024-07-15 20:35:35', 'admin', '2024-08-12 14:48:20', '教师荣誉菜单');
INSERT INTO `sys_menu` VALUES (2049, '教师荣誉查询', 2048, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacherHonors:teacherHonors:query', '#', 'admin', '2024-07-15 20:35:35', 'admin', '2024-08-12 14:48:31', '');
INSERT INTO `sys_menu` VALUES (2050, '教师荣誉新增', 2048, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacherHonors:teacherHonors:add', '#', 'admin', '2024-07-15 20:35:35', 'admin', '2024-08-12 14:48:37', '');
INSERT INTO `sys_menu` VALUES (2051, '教师荣誉修改', 2048, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacherHonors:teacherHonors:edit', '#', 'admin', '2024-07-15 20:35:35', 'admin', '2024-08-12 14:48:43', '');
INSERT INTO `sys_menu` VALUES (2052, '教师荣誉删除', 2048, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacherHonors:teacherHonors:remove', '#', 'admin', '2024-07-15 20:35:35', 'admin', '2024-08-12 14:48:50', '');
INSERT INTO `sys_menu` VALUES (2053, '教师荣誉导出', 2048, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'teacherHonors:teacherHonors:export', '#', 'admin', '2024-07-15 20:35:35', 'admin', '2024-08-12 14:48:57', '');
INSERT INTO `sys_menu` VALUES (2054, '知识产权', 2067, 1, 'intellectualProperty', 'intellectualProperty/intellectualProperty/index', NULL, '', 1, 0, 'C', '0', '0', 'intellectualProperty:intellectualProperty:list', '#', 'admin', '2024-07-15 20:35:42', 'admin', '2024-08-12 14:49:43', '知识产权菜单');
INSERT INTO `sys_menu` VALUES (2055, '知识产权查询', 2054, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'intellectualProperty:intellectualProperty:query', '#', 'admin', '2024-07-15 20:35:42', 'admin', '2024-08-12 14:49:56', '');
INSERT INTO `sys_menu` VALUES (2056, '知识产权新增', 2054, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'intellectualProperty:intellectualProperty:add', '#', 'admin', '2024-07-15 20:35:42', 'admin', '2024-08-12 14:50:11', '');
INSERT INTO `sys_menu` VALUES (2057, '知识产权修改', 2054, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'intellectualProperty:intellectualProperty:edit', '#', 'admin', '2024-07-15 20:35:42', 'admin', '2024-08-12 14:50:21', '');
INSERT INTO `sys_menu` VALUES (2058, '知识产权删除', 2054, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'intellectualProperty:intellectualProperty:remove', '#', 'admin', '2024-07-15 20:35:42', 'admin', '2024-08-12 14:50:28', '');
INSERT INTO `sys_menu` VALUES (2059, '知识产权导出', 2054, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'intellectualProperty:intellectualProperty:export', '#', 'admin', '2024-07-15 20:35:42', 'admin', '2024-08-12 14:50:34', '');
INSERT INTO `sys_menu` VALUES (2060, '项目', 2067, 1, 'project', 'project/projects/index', NULL, '', 1, 0, 'C', '0', '0', 'project:projects:list', '#', 'admin', '2024-07-15 20:50:20', 'admin', '2024-07-15 21:36:25', '项目菜单');
INSERT INTO `sys_menu` VALUES (2061, '项目查询', 2060, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'project:projects:query', '#', 'admin', '2024-07-15 20:50:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2062, '项目新增', 2060, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'project:projects:add', '#', 'admin', '2024-07-15 20:50:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2063, '项目修改', 2060, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'project:projects:edit', '#', 'admin', '2024-07-15 20:50:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2064, '项目删除', 2060, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'project:projects:remove', '#', 'admin', '2024-07-15 20:50:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2065, '项目导出', 2060, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'project:projects:export', '#', 'admin', '2024-07-15 20:50:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, '教师成果', 0, 2, '/', NULL, NULL, '', 1, 0, 'M', '0', '0', '', 'example', 'admin', '2024-07-15 21:34:57', 'admin', '2024-08-02 11:56:05', '');
INSERT INTO `sys_menu` VALUES (2068, '鉴定项目', 2067, 1, 'checkProjects', 'checkProjects/checkProjects/index', NULL, '', 1, 0, 'C', '0', '0', 'checkProjects:checkProjects:list', '#', 'admin', '2024-07-16 01:42:36', 'admin', '2024-08-12 14:51:23', '鉴定项目菜单');
INSERT INTO `sys_menu` VALUES (2069, '鉴定项目查询', 2068, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'checkProjects:checkProjects:query', '#', 'admin', '2024-07-16 01:42:36', 'admin', '2024-08-12 14:51:30', '');
INSERT INTO `sys_menu` VALUES (2070, '鉴定项目新增', 2068, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'checkProjects:checkProjects:add', '#', 'admin', '2024-07-16 01:42:36', 'admin', '2024-08-12 14:51:35', '');
INSERT INTO `sys_menu` VALUES (2071, '鉴定项目修改', 2068, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'checkProjects:checkProjects:edit', '#', 'admin', '2024-07-16 01:42:36', 'admin', '2024-08-12 14:51:40', '');
INSERT INTO `sys_menu` VALUES (2072, '鉴定项目删除', 2068, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'checkProjects:checkProjects:remove', '#', 'admin', '2024-07-16 01:42:36', 'admin', '2024-08-12 14:51:45', '');
INSERT INTO `sys_menu` VALUES (2073, '鉴定项目导出', 2068, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'checkProjects:checkProjects:export', '#', 'admin', '2024-07-16 01:42:36', 'admin', '2024-08-12 14:51:50', '');
INSERT INTO `sys_menu` VALUES (3028, '学生成果管理', 0, 3, 'student/student', NULL, NULL, '', 1, 0, 'M', '0', '0', '', 'people', 'admin', '2024-07-16 10:44:38', 'admin', '2024-08-02 11:55:23', '');
INSERT INTO `sys_menu` VALUES (3029, '学生信息管理', 0, 1, 'students', 'student/student/index', NULL, '', 1, 0, 'C', '0', '0', 'StudentManger:students:list', 'tool', 'admin', '2024-07-16 11:18:16', 'admin', '2024-07-16 11:24:57', '学生信息管理菜单');
INSERT INTO `sys_menu` VALUES (3030, '学生信息管理查询', 3029, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentManger:students:query', '#', 'admin', '2024-07-16 11:18:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3031, '学生信息管理新增', 3029, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentManger:students:add', '#', 'admin', '2024-07-16 11:18:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3032, '学生信息管理修改', 3029, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentManger:students:edit', '#', 'admin', '2024-07-16 11:18:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3033, '学生信息管理删除', 3029, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentManger:students:remove', '#', 'admin', '2024-07-16 11:18:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3034, '学生信息管理导出', 3029, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentManger:students:export', '#', 'admin', '2024-07-16 11:18:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3035, '奖学金信息管理', 3028, 1, 'scholarship', 'scholarship/scholarship/index', NULL, '', 1, 0, 'C', '0', '0', 'scholarship:scholarship:list', '#', 'admin', '2024-07-16 16:54:10', '', NULL, '奖学金信息管理菜单');
INSERT INTO `sys_menu` VALUES (3036, '奖学金信息管理查询', 3035, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'scholarship/scholarship:scholarship:query', '#', 'admin', '2024-07-16 16:54:10', 'admin', '2024-08-12 15:14:40', '');
INSERT INTO `sys_menu` VALUES (3037, '奖学金信息管理新增', 3035, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'scholarship/scholarship:scholarship:add', '#', 'admin', '2024-07-16 16:54:10', 'admin', '2024-08-12 15:14:44', '');
INSERT INTO `sys_menu` VALUES (3038, '奖学金信息管理修改', 3035, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'scholarship/scholarship:scholarship:edit', '#', 'admin', '2024-07-16 16:54:10', 'admin', '2024-08-12 15:14:49', '');
INSERT INTO `sys_menu` VALUES (3039, '奖学金信息管理删除', 3035, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'scholarship/scholarship:scholarship:remove', '#', 'admin', '2024-07-16 16:54:10', 'admin', '2024-08-12 15:14:53', '');
INSERT INTO `sys_menu` VALUES (3040, '奖学金信息管理导出', 3035, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'scholarship/scholarship:scholarship:export', '#', 'admin', '2024-07-16 16:54:10', 'admin', '2024-08-12 15:15:00', '');
INSERT INTO `sys_menu` VALUES (3041, '竞赛管理', 3028, 1, 'competition', 'competion/competition/index', NULL, '', 1, 0, 'C', '0', '0', 'competion:competition:list', '#', 'admin', '2024-07-16 19:56:41', '', NULL, '竞赛管理菜单');
INSERT INTO `sys_menu` VALUES (3042, '竞赛管理查询', 3041, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'competion:competition:query', '#', 'admin', '2024-07-16 19:56:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3043, '竞赛管理新增', 3041, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'competion:competition:add', '#', 'admin', '2024-07-16 19:56:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3044, '竞赛管理修改', 3041, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'competion:competition:edit', '#', 'admin', '2024-07-16 19:56:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3045, '竞赛管理删除', 3041, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'competion:competition:remove', '#', 'admin', '2024-07-16 19:56:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3046, '竞赛管理导出', 3041, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'competion:competition:export', '#', 'admin', '2024-07-16 19:56:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3048, '志愿者管理', 3028, 1, 'volunteers', 'manage/volunteers/index', NULL, '', 1, 0, 'C', '0', '0', 'manage:volunteers:list', '#', 'admin', '2024-07-19 14:57:08', 'admin', '2024-07-19 14:58:44', '志愿者管理菜单');
INSERT INTO `sys_menu` VALUES (3049, '志愿者管理查询', 3048, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'manage:volunteers:query', '#', 'admin', '2024-07-19 14:57:08', 'admin', '2024-07-19 14:59:42', '');
INSERT INTO `sys_menu` VALUES (3050, '志愿者管理新增', 3048, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'manage:volunteers:add', '#', 'admin', '2024-07-19 14:57:08', 'admin', '2024-07-19 14:59:53', '');
INSERT INTO `sys_menu` VALUES (3051, '志愿者管理修改', 3048, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'manage:volunteers:edit', '#', 'admin', '2024-07-19 14:57:08', 'admin', '2024-07-19 14:59:59', '');
INSERT INTO `sys_menu` VALUES (3052, '志愿者管理删除', 3048, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'manage:volunteers:remove', '#', 'admin', '2024-07-19 14:57:08', 'admin', '2024-07-19 15:00:06', '');
INSERT INTO `sys_menu` VALUES (3053, '志愿者管理导出', 3048, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'manage:volunteers:export', '#', 'admin', '2024-07-19 14:57:08', 'admin', '2024-07-19 15:01:34', '');
INSERT INTO `sys_menu` VALUES (3054, '志愿者证书管理', 3028, 1, 'volunteerscertificate', 'certificate/certificates/index', NULL, '', 1, 0, 'C', '0', '0', 'certificate:certificates:list', '#', 'admin', '2024-07-19 15:05:09', 'admin', '2024-07-19 15:08:28', '志愿者管理菜单');
INSERT INTO `sys_menu` VALUES (3055, '志愿者证书查询', 3054, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'certificate:certificates:query', '#', 'admin', '2024-07-19 15:05:09', 'admin', '2024-07-19 15:09:01', '');
INSERT INTO `sys_menu` VALUES (3056, '志愿者证书新增', 3054, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'certificate:certificates:add', '#', 'admin', '2024-07-19 15:05:09', 'admin', '2024-07-19 15:09:27', '');
INSERT INTO `sys_menu` VALUES (3057, '志愿者证书修改', 3054, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'certificate:certificates:edit', '#', 'admin', '2024-07-19 15:05:09', 'admin', '2024-07-19 15:09:42', '');
INSERT INTO `sys_menu` VALUES (3058, '志愿者证书删除', 3054, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'certificate:certificates:remove', '#', 'admin', '2024-07-19 15:05:09', 'admin', '2024-07-19 15:09:59', '');
INSERT INTO `sys_menu` VALUES (3059, '志愿者证书管理导出', 3054, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'certificate:certificates:export', '#', 'admin', '2024-07-19 15:05:09', 'admin', '2024-07-19 15:10:21', '');
INSERT INTO `sys_menu` VALUES (3060, '学生论文管理', 3028, 1, 'studentshesis', 'studentshesis/studentshesis/index', NULL, '', 1, 0, 'C', '0', '0', 'studentshesis:studentshesis:list', '#', 'admin', '2024-08-01 12:34:41', '', NULL, '学生论文管理菜单');
INSERT INTO `sys_menu` VALUES (3061, '学生论文管理查询', 3060, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentshesis:studentshesis:query', '#', 'admin', '2024-08-01 12:34:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3062, '学生论文管理新增', 3060, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentshesis:studentshesis:add', '#', 'admin', '2024-08-01 12:34:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3063, '学生论文管理修改', 3060, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentshesis:studentshesis:edit', '#', 'admin', '2024-08-01 12:34:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3064, '学生论文管理删除', 3060, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentshesis:studentshesis:remove', '#', 'admin', '2024-08-01 12:34:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3065, '学生论文管理导出', 3060, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentshesis:studentshesis:export', '#', 'admin', '2024-08-01 12:34:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3066, '学生实习项目经历', 3028, 1, 'studentprojectexperience', 'studentprojectexperience/studentprojectexperience/index', NULL, '', 1, 0, 'C', '0', '0', 'studentprojectexperience:studentprojectexperience:list', '#', 'admin', '2024-08-01 12:34:56', '', NULL, '学生实习项目经历菜单');
INSERT INTO `sys_menu` VALUES (3067, '学生实习项目经历查询', 3066, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentprojectexperience:studentprojectexperience:query', '#', 'admin', '2024-08-01 12:34:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3068, '学生实习项目经历新增', 3066, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentprojectexperience:studentprojectexperience:add', '#', 'admin', '2024-08-01 12:34:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3069, '学生实习项目经历修改', 3066, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentprojectexperience:studentprojectexperience:edit', '#', 'admin', '2024-08-01 12:34:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3070, '学生实习项目经历删除', 3066, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentprojectexperience:studentprojectexperience:remove', '#', 'admin', '2024-08-01 12:34:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3071, '学生实习项目经历导出', 3066, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentprojectexperience:studentprojectexperience:export', '#', 'admin', '2024-08-01 12:34:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3072, '知识产权', 3028, 1, 'StudentIntellectualProperty', 'studentintellectualproperty/StudentIntellectualProperty/index', NULL, '', 1, 0, 'C', '0', '0', 'studentintellectualproperty:StudentIntellectualProperty:list', '#', 'admin', '2024-08-01 12:35:15', '', NULL, '知识产权菜单');
INSERT INTO `sys_menu` VALUES (3073, '知识产权查询', 3072, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentintellectualproperty:StudentIntellectualProperty:query', '#', 'admin', '2024-08-01 12:35:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3074, '知识产权新增', 3072, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentintellectualproperty:StudentIntellectualProperty:add', '#', 'admin', '2024-08-01 12:35:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3075, '知识产权修改', 3072, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentintellectualproperty:StudentIntellectualProperty:edit', '#', 'admin', '2024-08-01 12:35:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3076, '知识产权删除', 3072, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentintellectualproperty:StudentIntellectualProperty:remove', '#', 'admin', '2024-08-01 12:35:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (3077, '知识产权导出', 3072, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentintellectualproperty:StudentIntellectualProperty:export', '#', 'admin', '2024-08-01 12:35:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4060, '学生荣誉', 3028, 1, 'honors', 'studentHonors/honors/index', NULL, '', 1, 0, 'C', '0', '0', 'studentHonors:honors:list', '#', 'admin', '2024-07-31 00:15:40', '', NULL, '学生荣誉菜单');
INSERT INTO `sys_menu` VALUES (4061, '学生荣誉查询', 3060, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentHonors:honors:query', '#', 'admin', '2024-07-31 00:15:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4062, '学生荣誉新增', 3060, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentHonors:honors:add', '#', 'admin', '2024-07-31 00:15:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4063, '学生荣誉修改', 3060, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentHonors:honors:edit', '#', 'admin', '2024-07-31 00:15:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4064, '学生荣誉删除', 3060, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentHonors:honors:remove', '#', 'admin', '2024-07-31 00:15:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4065, '学生荣誉导出', 3060, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentHonors:honors:export', '#', 'admin', '2024-07-31 00:15:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4066, '学生科研立项', 3028, 1, 'projects', 'studentScienceProject/projects/index', NULL, '', 1, 0, 'C', '0', '0', 'studentScienceProject:projects:list', '#', 'admin', '2024-07-31 00:35:03', '', NULL, '学生科研立项菜单');
INSERT INTO `sys_menu` VALUES (4067, '学生科研立项查询', 3066, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentScienceProject:projects:query', '#', 'admin', '2024-07-31 00:35:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4068, '学生科研立项新增', 3066, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentScienceProject:projects:add', '#', 'admin', '2024-07-31 00:35:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4069, '学生科研立项修改', 3066, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentScienceProject:projects:edit', '#', 'admin', '2024-07-31 00:35:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4070, '学生科研立项删除', 3066, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentScienceProject:projects:remove', '#', 'admin', '2024-07-31 00:35:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4071, '学生科研立项导出', 3066, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'studentScienceProject:projects:export', '#', 'admin', '2024-07-31 00:35:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4072, '著作教材', 3028, 1, 'textbooks', 'StudentTextBook/textbooks/index', NULL, '', 1, 0, 'C', '0', '0', 'StudentTextBook:textbooks:list', '#', 'admin', '2024-07-31 00:59:28', '', NULL, '著作教材菜单');
INSERT INTO `sys_menu` VALUES (4073, '著作教材查询', 3072, 1, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentTextBook:textbooks:query', '#', 'admin', '2024-07-31 00:59:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4074, '著作教材新增', 3072, 2, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentTextBook:textbooks:add', '#', 'admin', '2024-07-31 00:59:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4075, '著作教材修改', 3072, 3, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentTextBook:textbooks:edit', '#', 'admin', '2024-07-31 00:59:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4076, '著作教材删除', 3072, 4, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentTextBook:textbooks:remove', '#', 'admin', '2024-07-31 00:59:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4077, '著作教材导出', 3072, 5, '#', '', NULL, '', 1, 0, 'F', '0', '0', 'StudentTextBook:textbooks:export', '#', 'admin', '2024-07-31 00:59:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4078, '教师信息导入', 2018, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'teacher:teacher:import', '#', 'admin', '2024-08-12 14:44:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4079, '成果采纳导入', 2042, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'adoptedOutcomes:adoptedOutcomes:import', '#', 'admin', '2024-08-12 14:48:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4080, '教师荣誉导入', 2048, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'teacherHonors:teacherHonors:import', '#', 'admin', '2024-08-12 14:49:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4081, '知识产权导入', 2054, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'intellectualProperty:intellectualProperty:import', '#', 'admin', '2024-08-12 14:50:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4082, '项目导入', 2060, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'project:projects:import', '#', 'admin', '2024-08-12 14:51:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4083, '鉴定项目导入', 2068, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', '	 checkProjects:checkProjects:import', '#', 'admin', '2024-08-12 14:52:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4084, '著作教材导入', 2036, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'textbook:textbooks:import', '#', 'admin', '2024-08-12 14:53:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4085, '论文导入', 2030, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'papers:papers:import', '#', 'admin', '2024-08-12 14:53:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4086, '学生信息管理导入', 3029, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'StudentManger:students:import', '#', 'admin', '2024-08-12 15:07:00', 'admin', '2024-08-12 15:07:09', '');
INSERT INTO `sys_menu` VALUES (4087, '志愿者管理导入', 3048, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'volunteer:volunteer:import', '#', 'admin', '2024-08-12 15:09:38', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4088, '志愿者证书管理导入', 3054, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'certificate:certificates:import', '#', 'admin', '2024-08-12 15:10:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4089, '学生荣誉导入', 3060, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'studentHonors:honors:import', '#', 'admin', '2024-08-12 15:10:58', 'admin', '2024-08-12 15:11:04', '');
INSERT INTO `sys_menu` VALUES (4090, '学生论文管理导入', 3060, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', '	 studentshesis:studentshesis:import', '#', 'admin', '2024-08-12 15:11:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4091, '学生科研立项导入', 3066, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', '	 studentScienceProject:projects:import', '#', 'admin', '2024-08-12 15:11:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4092, '学生实习项目经历导入', 3066, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'studentprojectexperience:studentprojectexperience:import', '#', 'admin', '2024-08-12 15:12:18', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4093, '知识产权导入', 3072, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'studentintellectualproperty:StudentIntellectualProperty:import', '#', 'admin', '2024-08-12 15:13:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4094, '著作教材导入', 3072, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'StudentTextBook:textbooks:import', '#', 'admin', '2024-08-12 15:13:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (4095, '奖学金信息管理导入', 3035, 6, '', NULL, NULL, '', 1, 0, 'F', '0', '0', 'scholarship/scholarship:scholarship:import', '#', 'admin', '2024-08-12 15:15:18', '', NULL, '');
INSERT INTO  `sys_menu`  (menu_id, menu_name, parent_id, order_num, path, component, query, route_name, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) VALUES (4096, '竞赛管理导入', 3041, 6, '', null, null, '', 1, 0, 'F', '0', '0', 'competion:competition:import', '#', 'admin', '2024-08-12 16:14:34', '', null, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2024-07-15 14:20:16', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2024-07-15 14:20:16', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(0) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(0) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '返回参数',
  `status` int(0) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(0) NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type`) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status`) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 350 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:35:56', 157);
INSERT INTO `sys_oper_log` VALUES (101, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"民族选择\",\"dictType\":\"nation_choose\",\"params\":{},\"remark\":\"所属民族\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:38:34', 11);
INSERT INTO `sys_oper_log` VALUES (102, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"汉族\",\"dictSort\":0,\"dictType\":\"nation_choose\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:39:07', 15);
INSERT INTO `sys_oper_log` VALUES (103, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"回族\",\"dictSort\":1,\"dictType\":\"nation_choose\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:39:16', 6);
INSERT INTO `sys_oper_log` VALUES (104, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"傣族\",\"dictSort\":2,\"dictType\":\"nation_choose\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:39:27', 9);
INSERT INTO `sys_oper_log` VALUES (105, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"蒙古族\",\"dictSort\":3,\"dictType\":\"nation_choose\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:39:45', 8);
INSERT INTO `sys_oper_log` VALUES (106, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"政治面貌\",\"dictType\":\"teacher_status\",\"params\":{},\"remark\":\"教师的政治面貌\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:41:02', 6);
INSERT INTO `sys_oper_log` VALUES (107, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"群众\",\"dictSort\":0,\"dictType\":\"teacher_status\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:41:18', 8);
INSERT INTO `sys_oper_log` VALUES (108, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"团员\",\"dictSort\":1,\"dictType\":\"teacher_status\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:41:30', 8);
INSERT INTO `sys_oper_log` VALUES (109, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"党员\",\"dictSort\":2,\"dictType\":\"teacher_status\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:41:41', 8);
INSERT INTO `sys_oper_log` VALUES (110, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"专职与兼职\",\"dictType\":\"teacher_first\",\"params\":{},\"remark\":\"教师\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:44:41', 5);
INSERT INTO `sys_oper_log` VALUES (111, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"专职\",\"dictSort\":0,\"dictType\":\"teacher_first\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"专职\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:45:00', 12);
INSERT INTO `sys_oper_log` VALUES (112, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"兼职\",\"dictSort\":1,\"dictType\":\"teacher_first\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"兼职\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 14:45:23', 9);
INSERT INTO `sys_oper_log` VALUES (113, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"TeacherInfo\",\"columns\":[{\"capJavaField\":\"ID\",\"columnComment\":\"唯一标识\",\"columnId\":1,\"columnName\":\"ID\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"ID\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":2,\"columnName\":\"Name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"Name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"EmployeeNumber\",\"columnComment\":\"工号\",\"columnId\":3,\"columnName\":\"EmployeeNumber\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"EmployeeNumber\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Nationality\",\"columnComment\":\"民族\",\"columnId\":4,\"columnName\":\"Nationality\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"nation_choose\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 15:16:19', 100);
INSERT INTO `sys_oper_log` VALUES (114, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"TeacherInfo\",\"columns\":[{\"capJavaField\":\"ID\",\"columnComment\":\"唯一标识\",\"columnId\":1,\"columnName\":\"ID\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"ID\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:16:18\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":2,\"columnName\":\"Name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"Name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:16:18\",\"usableColumn\":false},{\"capJavaField\":\"EmployeeNumber\",\"columnComment\":\"工号\",\"columnId\":3,\"columnName\":\"EmployeeNumber\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"EmployeeNumber\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:16:18\",\"usableColumn\":false},{\"capJavaField\":\"Nationality\",\"columnComment\":\"民族\",\"columnId\":4,\"columnName\":\"Nationality\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"nation_choose\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"inse', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 15:16:43', 74);
INSERT INTO `sys_oper_log` VALUES (115, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', NULL, 0, NULL, '2024-07-15 15:16:53', 217);
INSERT INTO `sys_oper_log` VALUES (116, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"teacher\",\"className\":\"TeacherInfo\",\"columns\":[{\"capJavaField\":\"ID\",\"columnComment\":\"唯一标识\",\"columnId\":1,\"columnName\":\"ID\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"ID\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:16:43\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":2,\"columnName\":\"Name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"Name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:16:43\",\"usableColumn\":false},{\"capJavaField\":\"EmployeeNumber\",\"columnComment\":\"工号\",\"columnId\":3,\"columnName\":\"EmployeeNumber\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"EmployeeNumber\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:16:43\",\"usableColumn\":false},{\"capJavaField\":\"Nationality\",\"columnComment\":\"民族\",\"columnId\":4,\"columnName\":\"Nationality\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:35:56\",\"dictType\":\"nation_choose\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"i', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 15:19:53', 110);
INSERT INTO `sys_oper_log` VALUES (117, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', NULL, 0, NULL, '2024-07-15 15:19:57', 51);
INSERT INTO `sys_oper_log` VALUES (118, '教师信息', 1, 'com.ruoyi.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 15:25:45\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\r\n### The error may exist in file [D:\\桌面\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\teacher\\TeacherInfoMapper.xml]\r\n### The error may involve com.ruoyi.teacher.mapper.TeacherInfoMapper.insertTeacherInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into teacher_info          ( createTime )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\n; Field \'Name\' doesn\'t have a default value;\n\r\n\nnnested\n\r\n\nexception is java.sql.SQLException: Field\n\r\n\n\n\'Name\' doesn\n\r\n\n\n\'t have a default value', '2024-07-15 15:25:45', 66);
INSERT INTO `sys_oper_log` VALUES (119, '教师信息', 1, 'com.ruoyi.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 15:26:20\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\r\n### The error may exist in file [D:\\桌面\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\teacher\\TeacherInfoMapper.xml]\r\n### The error may involve com.ruoyi.teacher.mapper.TeacherInfoMapper.insertTeacherInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into teacher_info          ( createTime )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\n; Field \'Name\' doesn\'t have a default value;\n\r\n\nnnested\n\r\n\nexception is java.sql.SQLException: Field\n\r\n\n\n\'Name\' doesn\n\r\n\n\n\'t have a default value', '2024-07-15 15:26:20', 4);
INSERT INTO `sys_oper_log` VALUES (120, '教师信息', 1, 'com.ruoyi.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 15:28:29\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\r\n### The error may exist in file [D:\\桌面\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\teacher\\TeacherInfoMapper.xml]\r\n### The error may involve com.ruoyi.teacher.mapper.TeacherInfoMapper.insertTeacherInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into teacher_info          ( createTime )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\n; Field \'Name\' doesn\'t have a default value;\n\r\n\nnnested\n\r\n\nexception is java.sql.SQLException: Field\n\r\n\n\n\'Name\' doesn\n\r\n\n\n\'t have a default value', '2024-07-15 15:28:29', 7);
INSERT INTO `sys_oper_log` VALUES (121, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '研发部门', '/tool/gen/1', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 15:32:29', 39);
INSERT INTO `sys_oper_log` VALUES (122, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 15:32:41', 124);
INSERT INTO `sys_oper_log` VALUES (123, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"info\",\"className\":\"TeacherInfo\",\"columns\":[{\"capJavaField\":\"ID\",\"columnComment\":\"唯一标识\",\"columnId\":28,\"columnName\":\"ID\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"ID\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":29,\"columnName\":\"Name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"Name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"EmployeeNumber\",\"columnComment\":\"工号\",\"columnId\":30,\"columnName\":\"EmployeeNumber\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"EmployeeNumber\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Nationality\",\"columnComment\":\"民族\",\"columnId\":31,\"columnName\":\"Nationality\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"nation_choose\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequi', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 15:35:44', 77);
INSERT INTO `sys_oper_log` VALUES (124, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"teacher\",\"className\":\"TeacherInfo\",\"columns\":[{\"capJavaField\":\"ID\",\"columnComment\":\"唯一标识\",\"columnId\":28,\"columnName\":\"ID\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"ID\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:35:44\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":29,\"columnName\":\"Name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"Name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:35:44\",\"usableColumn\":false},{\"capJavaField\":\"EmployeeNumber\",\"columnComment\":\"工号\",\"columnId\":30,\"columnName\":\"EmployeeNumber\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",\"javaField\":\"EmployeeNumber\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 15:35:44\",\"usableColumn\":false},{\"capJavaField\":\"Nationality\",\"columnComment\":\"民族\",\"columnId\":31,\"columnName\":\"Nationality\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 15:32:41\",\"dictType\":\"nation_choose\",\"edit\":true,\"htmlType\":\"select\",\"increment\":fals', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 15:36:07', 60);
INSERT INTO `sys_oper_log` VALUES (125, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', NULL, 0, NULL, '2024-07-15 15:36:10', 195);
INSERT INTO `sys_oper_log` VALUES (126, '教师信息', 1, 'com.ruoyi.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 15:40:33\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\r\n### The error may exist in file [D:\\桌面\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\teacher\\TeacherInfoMapper.xml]\r\n### The error may involve com.ruoyi.teacher.mapper.TeacherInfoMapper.insertTeacherInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into teacher_info          ( createTime )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\n; Field \'Name\' doesn\'t have a default value;\n\r\n\nnnested\n\r\n\nexception is java.sql.SQLException: Field\n\r\n\n\n\'Name\' doesn\n\r\n\n\n\'t have a default value', '2024-07-15 15:40:33', 81);
INSERT INTO `sys_oper_log` VALUES (127, '教师信息', 1, 'com.ruoyi.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 15:49:51\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\r\n### The error may exist in file [D:\\桌面\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\teacher\\TeacherInfoMapper.xml]\r\n### The error may involve com.ruoyi.teacher.mapper.TeacherInfoMapper.insertTeacherInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into teacher_info          ( createTime )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'Name\' doesn\'t have a default value\n; Field \'Name\' doesn\'t have a default value;\n\r\n\nnnested\n\r\n\nexception is java.sql.SQLException: Field\n\r\n\n\n\'Name\' doesn\n\r\n\n\n\'t have a default value', '2024-07-15 15:49:51', 74);
INSERT INTO `sys_oper_log` VALUES (128, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '研发部门', '/tool/gen/2', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 16:01:57', 34);
INSERT INTO `sys_oper_log` VALUES (129, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 16:02:21', 147);
INSERT INTO `sys_oper_log` VALUES (130, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"teacher\",\"className\":\"TeacherInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"唯一标识\",\"columnId\":55,\"columnName\":\"id\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":56,\"columnName\":\"name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"EmployeeNumber\",\"columnComment\":\"工号\",\"columnId\":57,\"columnName\":\"employee_number\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"0\",\"javaField\":\"employeeNumber\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Nationality\",\"columnComment\":\"民族\",\"columnId\":58,\"columnName\":\"nationality\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"1\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 16:05:08', 110);
INSERT INTO `sys_oper_log` VALUES (131, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', NULL, 0, NULL, '2024-07-15 16:05:13', 192);
INSERT INTO `sys_oper_log` VALUES (132, '教师信息', 1, 'com.ruoyi.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:07:58\",\"gender\":0,\"id\":1,\"name\":\"1\",\"nationalId\":\"111\",\"nationality\":\"1\",\"params\":{},\"phoneNumber\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 16:07:58', 38);
INSERT INTO `sys_oper_log` VALUES (133, '教师信息', 2, 'com.ruoyi.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:07:58\",\"gender\":0,\"id\":1,\"name\":\"1\",\"nationalId\":\"111\",\"nationality\":\"1\",\"params\":{},\"phoneNumber\":\"2\",\"updateTime\":\"2024-07-15 16:08:18\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 16:08:18', 16);
INSERT INTO `sys_oper_log` VALUES (134, '教师信息', 1, 'com.ruoyi.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:50:21\",\"department\":\"2\",\"employeeNumber\":\"22\",\"employmentType\":0,\"firstDegree\":\"2\",\"firstDegreeObject\":\"2\",\"gender\":0,\"highestDegree\":\"2\",\"highestdegreeObject\":\"2\",\"id\":2,\"name\":\"2\",\"nationalId\":\"222\",\"nationality\":\"2\",\"params\":{},\"phoneNumber\":\"2\",\"politicalAffiliation\":\"1\",\"position\":\"2\",\"remarks\":\"2\",\"title\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 16:50:21', 28);
INSERT INTO `sys_oper_log` VALUES (135, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"teacher\",\"className\":\"TeacherInfo\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"唯一标识\",\"columnId\":55,\"columnName\":\"id\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 16:05:08\",\"usableColumn\":false},{\"capJavaField\":\"Name\",\"columnComment\":\"姓名\",\"columnId\":56,\"columnName\":\"name\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"name\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 16:05:08\",\"usableColumn\":false},{\"capJavaField\":\"EmployeeNumber\",\"columnComment\":\"工号\",\"columnId\":57,\"columnName\":\"employee_number\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"0\",\"javaField\":\"employeeNumber\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 16:05:08\",\"usableColumn\":false},{\"capJavaField\":\"Nationality\",\"columnComment\":\"民族\",\"columnId\":58,\"columnName\":\"nationality\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 16:02:21\",\"dictType\":\"nation_choose\",\"edit\":true,\"htmlType\":\"select\",\"increment\":fa', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 17:02:47', 112);
INSERT INTO `sys_oper_log` VALUES (136, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"teacher_info\"}', NULL, 0, NULL, '2024-07-15 17:02:53', 253);
INSERT INTO `sys_oper_log` VALUES (137, '教师信息', 3, 'com.ruoyi.teacher.controller.TeacherInfoController.remove()', 'DELETE', 1, 'admin', '研发部门', '/teacher/teacher/1', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 17:07:10', 11);
INSERT INTO `sys_oper_log` VALUES (138, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teacher/teacher/index\",\"createTime\":\"2024-07-15 16:06:07\",\"icon\":\"dashboard\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"教师信息\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":3,\"path\":\"teacher\",\"perms\":\"teacher:teacher:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:03:05', 21);
INSERT INTO `sys_oper_log` VALUES (139, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teacher/teacher/index\",\"createTime\":\"2024-07-15 16:06:07\",\"icon\":\"dashboard\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"教师信息\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"teacher:teacher:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:03:30', 11);
INSERT INTO `sys_oper_log` VALUES (140, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teacher/teacher/index\",\"createTime\":\"2024-07-15 16:06:07\",\"icon\":\"dashboard\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"教师信息\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"teacher:teacher:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:03:59', 10);
INSERT INTO `sys_oper_log` VALUES (141, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"projects,papers\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:04:20', 169);
INSERT INTO `sys_oper_log` VALUES (142, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '研发部门', '/tool/gen/5', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:04:35', 10);
INSERT INTO `sys_oper_log` VALUES (143, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"works_textbooks\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:04:39', 67);
INSERT INTO `sys_oper_log` VALUES (144, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"级别\",\"dictType\":\"category\",\"params\":{},\"remark\":\"论文,项目级别都可以用\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:12:11', 29);
INSERT INTO `sys_oper_log` VALUES (145, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"企业\",\"dictSort\":0,\"dictType\":\"category\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"企业\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:12:53', 10);
INSERT INTO `sys_oper_log` VALUES (146, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"校级\",\"dictSort\":1,\"dictType\":\"category\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"企业\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:13:10', 9);
INSERT INTO `sys_oper_log` VALUES (147, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"厅级\",\"dictSort\":2,\"dictType\":\"category\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"remark\":\"厅级\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:13:22', 8);
INSERT INTO `sys_oper_log` VALUES (148, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"省级\",\"dictSort\":3,\"dictType\":\"category\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"remark\":\"省级\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:13:32', 8);
INSERT INTO `sys_oper_log` VALUES (149, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"国家级\",\"dictSort\":4,\"dictType\":\"category\",\"dictValue\":\"4\",\"listClass\":\"default\",\"params\":{},\"remark\":\"国家级\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:13:47', 8);
INSERT INTO `sys_oper_log` VALUES (150, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"名次\",\"dictType\":\"placing\",\"params\":{},\"remark\":\"名次\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:24:41', 8);
INSERT INTO `sys_oper_log` VALUES (151, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"主编\",\"dictSort\":0,\"dictType\":\"placing\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"教材主编\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:27:15', 16);
INSERT INTO `sys_oper_log` VALUES (152, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"cssClass\":\"\",\"default\":false,\"dictLabel\":\"副主编\",\"dictSort\":1,\"dictType\":\"placing\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"副主编\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:30:58', 20);
INSERT INTO `sys_oper_log` VALUES (153, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"参编\",\"dictSort\":2,\"dictType\":\"placing\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"remark\":\"参编\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:31:18', 13);
INSERT INTO `sys_oper_log` VALUES (154, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"等级\",\"dictType\":\"grade\",\"params\":{},\"remark\":\"等级\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:33:36', 9);
INSERT INTO `sys_oper_log` VALUES (155, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"主持人\",\"dictSort\":1,\"dictType\":\"grade\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"主持人\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:34:03', 8);
INSERT INTO `sys_oper_log` VALUES (156, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"成员\",\"dictSort\":0,\"dictType\":\"grade\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"项目成员\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:34:22', 7);
INSERT INTO `sys_oper_log` VALUES (157, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:12:11\",\"dictId\":103,\"dictName\":\"级别\",\"dictType\":\"level\",\"params\":{},\"remark\":\"论文,项目级别都可以用\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:35:15', 23);
INSERT INTO `sys_oper_log` VALUES (158, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"类别\",\"dictType\":\"category\",\"params\":{},\"remark\":\"用于知识产权\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:35:40', 13);
INSERT INTO `sys_oper_log` VALUES (159, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:12:11\",\"dictId\":103,\"dictName\":\"级别\",\"dictType\":\"level\",\"params\":{},\"remark\":\"论文,项目,鉴定项目,教师荣誉都可以用\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:36:07', 16);
INSERT INTO `sys_oper_log` VALUES (160, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:24:41\",\"dictId\":104,\"dictName\":\"名次\",\"dictType\":\"placing\",\"params\":{},\"remark\":\"著作教材中的名次\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:36:18', 15);
INSERT INTO `sys_oper_log` VALUES (161, '字典类型', 2, 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:33:36\",\"dictId\":105,\"dictName\":\"等级\",\"dictType\":\"grade\",\"params\":{},\"remark\":\"规定项目成员中谁是主持人\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:36:40', 23);
INSERT INTO `sys_oper_log` VALUES (162, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"软件著作\",\"dictSort\":0,\"dictType\":\"category\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"软件著作\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:36:59', 6);
INSERT INTO `sys_oper_log` VALUES (163, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"发明专利\",\"dictSort\":1,\"dictType\":\"category\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"发明专利\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:37:15', 7);
INSERT INTO `sys_oper_log` VALUES (164, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"实用新型专利\",\"dictSort\":2,\"dictType\":\"category\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"remark\":\"实用新型专利\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:37:49', 10);
INSERT INTO `sys_oper_log` VALUES (165, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"其它\",\"dictSort\":3,\"dictType\":\"category\",\"dictValue\":\"3\",\"listClass\":\"default\",\"params\":{},\"remark\":\"其它\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:37:59', 9);
INSERT INTO `sys_oper_log` VALUES (166, '字典数据', 5, 'com.ruoyi.web.controller.system.SysDictDataController.export()', 'POST', 1, 'admin', '研发部门', '/system/dict/data/export', '127.0.0.1', '内网IP', '{\"pageSize\":\"10\",\"pageNum\":\"1\",\"dictType\":\"category\"}', NULL, 0, NULL, '2024-07-15 19:38:04', 597);
INSERT INTO `sys_oper_log` VALUES (167, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"paper\",\"className\":\"Papers\",\"columns\":[{\"capJavaField\":\"ID\",\"columnComment\":\"唯一标识\",\"columnId\":82,\"columnName\":\"ID\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"ID\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"PaperTitle\",\"columnComment\":\"论文名称\",\"columnId\":83,\"columnName\":\"PaperTitle\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"PaperTitle\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"FundingOrProject\",\"columnComment\":\"支持基金或项目\",\"columnId\":84,\"columnName\":\"FundingOrProject\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"0\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"0\",\"javaField\":\"FundingOrProject\",\"javaType\":\"String\",\"list\":false,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AuthorEmployeeID\",\"columnComment\":\"作者工号\",\"columnId\":85,\"columnName\":\"AuthorEmployeeID\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:49:09', 60);
INSERT INTO `sys_oper_log` VALUES (168, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"papers\",\"className\":\"Papers\",\"columns\":[{\"capJavaField\":\"ID\",\"columnComment\":\"唯一标识\",\"columnId\":82,\"columnName\":\"ID\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"ID\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 19:49:09\",\"usableColumn\":false},{\"capJavaField\":\"PaperTitle\",\"columnComment\":\"论文名称\",\"columnId\":83,\"columnName\":\"PaperTitle\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"PaperTitle\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 19:49:09\",\"usableColumn\":false},{\"capJavaField\":\"FundingOrProject\",\"columnComment\":\"支持基金或项目\",\"columnId\":84,\"columnName\":\"FundingOrProject\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"0\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"0\",\"javaField\":\"FundingOrProject\",\"javaType\":\"String\",\"list\":false,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":4,\"updateBy\":\"\",\"updateTime\":\"2024-07-15 19:49:09\",\"usableColumn\":false},{\"capJavaField\":\"AuthorEmployeeID\",\"columnComment\":\"作者工号\",\"columnId\":85,\"columnName\":\"AuthorEmployeeID\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 19:04:20\",\"dictType\":\"\",\"edit\":true,\"htmlType\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 19:50:46', 45);
INSERT INTO `sys_oper_log` VALUES (169, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"papers\"}', NULL, 0, NULL, '2024-07-15 19:50:49', 222);
INSERT INTO `sys_oper_log` VALUES (170, '论文', 1, 'com.ruoyi.papers.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 19:54:08\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'PaperTitle\' doesn\'t have a default value\r\n### The error may exist in file [D:\\桌面\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\papers\\PapersMapper.xml]\r\n### The error may involve com.ruoyi.papers.mapper.PapersMapper.insertPapers-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into papers          ( createTime )           values ( ? )\r\n### Cause: java.sql.SQLException: Field \'PaperTitle\' doesn\'t have a default value\n; Field \'PaperTitle\' doesn\'t have a default value;\n\r\n\nnnested\n\r\n\nexception is java.sql.SQLException: Field\n\r\n\n\n\'PaperTitle\' doesn\n\r\n\n\n\'t have a default value', '2024-07-15 19:54:09', 77);
INSERT INTO `sys_oper_log` VALUES (171, '论文', 1, 'com.ruoyi.papers.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 19:57:54\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'createTime\' in \'field list\'\r\n### The error may exist in file [D:\\桌面\\RuoYi-Vue\\ruoyi-admin\\target\\classes\\mapper\\papers\\PapersMapper.xml]\r\n### The error may involve com.ruoyi.papers.mapper.PapersMapper.insertPapers-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into papers          ( createTime )           values ( ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'createTime\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'createTime\' in \'field list\'', '2024-07-15 19:57:54', 10);
INSERT INTO `sys_oper_log` VALUES (172, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '研发部门', '/tool/gen/4', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:01:48', 56);
INSERT INTO `sys_oper_log` VALUES (173, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', '研发部门', '/tool/gen/6', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:01:50', 25);
INSERT INTO `sys_oper_log` VALUES (174, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"papers\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:01:55', 145);
INSERT INTO `sys_oper_log` VALUES (175, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"papers\",\"className\":\"Papers\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"唯一标识\",\"columnId\":138,\"columnName\":\"id\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:01:55\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"PaperTitle\",\"columnComment\":\"论文名称\",\"columnId\":139,\"columnName\":\"paper_title\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:01:55\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"0\",\"javaField\":\"paperTitle\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"FundingOrProject\",\"columnComment\":\"支持基金或项目\",\"columnId\":140,\"columnName\":\"funding_or_project\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:01:55\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":false,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"0\",\"isList\":\"0\",\"isPk\":\"0\",\"isQuery\":\"0\",\"isRequired\":\"0\",\"javaField\":\"fundingOrProject\",\"javaType\":\"String\",\"list\":false,\"params\":{},\"pk\":false,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":7,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AuthorEmployeeId\",\"columnComment\":\"作者工号\",\"columnId\":141,\"columnName\":\"author_employee_id\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:01:55\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:06:21', 94);
INSERT INTO `sys_oper_log` VALUES (176, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"papers\"}', NULL, 0, NULL, '2024-07-15 20:06:25', 208);
INSERT INTO `sys_oper_log` VALUES (177, '论文', 1, 'com.ruoyi.papers.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/15/hello_20240715200922A001.txt\",\"authorEmployeeId\":\"1\",\"authorRank\":\"0\",\"cnNumber\":\"1\",\"createTime\":\"2024-07-15 20:09:26\",\"id\":1,\"issue\":\"1\",\"journalName\":\"1\",\"paperLevel\":\"2\",\"paperTitle\":\"1\",\"params\":{},\"publicationDate\":\"2024-07-10\",\"searchWebsite\":\"1\",\"wordCount\":\"1\",\"year\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:09:26', 33);
INSERT INTO `sys_oper_log` VALUES (178, '论文', 2, 'com.ruoyi.papers.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"\",\"authorEmployeeId\":\"1\",\"authorRank\":\"0\",\"cnNumber\":\"2\",\"createTime\":\"2024-07-15 20:09:26\",\"id\":1,\"issue\":\"1\",\"journalName\":\"1\",\"paperLevel\":\"2\",\"paperTitle\":\"1\",\"params\":{},\"publicationDate\":\"2024-07-10\",\"searchWebsite\":\"1\",\"updateTime\":\"2024-07-15 20:09:41\",\"wordCount\":\"1\",\"year\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:09:41', 14);
INSERT INTO `sys_oper_log` VALUES (179, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"works_textbooks\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:14:12', 143);
INSERT INTO `sys_oper_log` VALUES (180, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"textbooks\",\"className\":\"WorksTextbooks\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"唯一标识\",\"columnId\":159,\"columnName\":\"id\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:14:12\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Title\",\"columnComment\":\"名称\",\"columnId\":160,\"columnName\":\"title\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:14:12\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"title\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"Type\",\"columnComment\":\"类型（指著作或教材）\",\"columnId\":161,\"columnName\":\"type\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:14:12\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"type\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":8,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"AuthorEmployeeId\",\"columnComment\":\"作者工号\",\"columnId\":162,\"columnName\":\"author_employee_id\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 20:14:12\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:22:30', 69);
INSERT INTO `sys_oper_log` VALUES (181, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"works_textbooks\"}', NULL, 0, NULL, '2024-07-15 20:22:37', 243);
INSERT INTO `sys_oper_log` VALUES (182, '著作教材', 1, 'com.ruoyi.textbook.controller.WorksTextbooksController.add()', 'POST', 1, 'admin', '研发部门', '/textbook/textbooks', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/15/hello_20240715202728A001.txt\",\"authorEmployeeId\":\"1\",\"authorRole\":\"1\",\"authorWordCount\":\"1\",\"createTime\":\"2024-07-15 20:27:29\",\"id\":1,\"isbn\":\"1\",\"params\":{},\"publisher\":\"1\",\"remarks\":\"1\",\"title\":\"1\",\"totalWordCount\":\"1\",\"type\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:27:29', 24);
INSERT INTO `sys_oper_log` VALUES (183, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teacher/teacher/index\",\"createTime\":\"2024-07-15 16:06:07\",\"icon\":\"dashboard\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"教师信息\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"teacher\",\"perms\":\"teacher:teacher:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 20:55:12', 28);
INSERT INTO `sys_oper_log` VALUES (184, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"教师成果\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:03:15', 52);
INSERT INTO `sys_oper_log` VALUES (185, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teacher/teacher/index\",\"createTime\":\"2024-07-15 16:06:07\",\"icon\":\"dashboard\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"教师信息\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"teacher:teacher:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:03:27', 15);
INSERT INTO `sys_oper_log` VALUES (186, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"textbook/textbooks/index\",\"createTime\":\"2024-07-15 20:26:30\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2036,\"menuName\":\"著作教材\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"textbooks\",\"perms\":\"textbook:textbooks:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:04:01', 12);
INSERT INTO `sys_oper_log` VALUES (187, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"AdoptedOutcomes/AdoptedOutcomes/index\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2042,\"menuName\":\"成果采纳\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"AdoptedOutcomes\",\"perms\":\"AdoptedOutcomes:AdoptedOutcomes:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:04:26', 12);
INSERT INTO `sys_oper_log` VALUES (188, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"TeacherHonors/TeacherHonors/index\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2048,\"menuName\":\"教师荣誉\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"TeacherHonors\",\"perms\":\"TeacherHonors:TeacherHonors:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:04:35', 16);
INSERT INTO `sys_oper_log` VALUES (189, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"IntellectualProperty/IntellectualProperty/index\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2054,\"menuName\":\"知识产权\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"IntellectualProperty\",\"perms\":\"IntellectualProperty:IntellectualProperty:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:04:44', 9);
INSERT INTO `sys_oper_log` VALUES (190, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"papers/papers/index\",\"createTime\":\"2024-07-15 20:07:45\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2030,\"menuName\":\"论文\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"papers\",\"perms\":\"papers:papers:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:05:06', 15);
INSERT INTO `sys_oper_log` VALUES (191, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"project/projects/index\",\"createTime\":\"2024-07-15 20:50:20\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2060,\"menuName\":\"项目\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"projects\",\"perms\":\"project:projects:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:05:14', 8);
INSERT INTO `sys_oper_log` VALUES (192, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"papers/papers/index\",\"createTime\":\"2024-07-15 20:07:45\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2030,\"menuName\":\"论文\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"papers\",\"perms\":\"papers:papers:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:12:46', 8);
INSERT INTO `sys_oper_log` VALUES (193, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"textbook/textbooks/index\",\"createTime\":\"2024-07-15 20:26:30\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2036,\"menuName\":\"著作教材\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"textbooks\",\"perms\":\"textbook:textbooks:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:14:00', 13);
INSERT INTO `sys_oper_log` VALUES (194, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"textbook/textbooks/index\",\"createTime\":\"2024-07-15 20:26:30\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2036,\"menuName\":\"著作教材\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2066,\"path\":\"textbooks\",\"perms\":\"textbook:textbooks:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:14:43', 9);
INSERT INTO `sys_oper_log` VALUES (195, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"textbook/textbooks/index\",\"createTime\":\"2024-07-15 20:26:30\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2036,\"menuName\":\"著作教材\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"textbooks\",\"perms\":\"textbook:textbooks:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:32:55', 32);
INSERT INTO `sys_oper_log` VALUES (196, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"AdoptedOutcomes/AdoptedOutcomes/index\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2042,\"menuName\":\"成果采纳\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"AdoptedOutcomes\",\"perms\":\"AdoptedOutcomes:AdoptedOutcomes:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:33:02', 25);
INSERT INTO `sys_oper_log` VALUES (197, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"TeacherHonors/TeacherHonors/index\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2048,\"menuName\":\"教师荣誉\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"TeacherHonors\",\"perms\":\"TeacherHonors:TeacherHonors:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:33:11', 14);
INSERT INTO `sys_oper_log` VALUES (198, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"IntellectualProperty/IntellectualProperty/index\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2054,\"menuName\":\"知识产权\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"IntellectualProperty\",\"perms\":\"IntellectualProperty:IntellectualProperty:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:33:19', 9);
INSERT INTO `sys_oper_log` VALUES (199, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"project/projects/index\",\"createTime\":\"2024-07-15 20:50:20\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2060,\"menuName\":\"项目\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"projects\",\"perms\":\"project:projects:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:33:24', 9);
INSERT INTO `sys_oper_log` VALUES (200, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', '研发部门', '/system/menu/2066', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:33:27', 23);
INSERT INTO `sys_oper_log` VALUES (201, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"教师成果\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"/#\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:34:57', 13);
INSERT INTO `sys_oper_log` VALUES (202, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"papers/papers/index\",\"createTime\":\"2024-07-15 20:07:45\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2030,\"menuName\":\"论文\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"papers\",\"perms\":\"papers:papers:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:35:10', 14);
INSERT INTO `sys_oper_log` VALUES (203, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 21:34:57\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2067,\"menuName\":\"教师成果\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"/\",\"perms\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:35:38', 12);
INSERT INTO `sys_oper_log` VALUES (204, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"textbook/textbooks/index\",\"createTime\":\"2024-07-15 20:26:30\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2036,\"menuName\":\"著作教材\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"textbooks\",\"perms\":\"textbook:textbooks:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:35:59', 9);
INSERT INTO `sys_oper_log` VALUES (205, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"AdoptedOutcomes/AdoptedOutcomes/index\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2042,\"menuName\":\"成果采纳\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"AdoptedOutcomes\",\"perms\":\"AdoptedOutcomes:AdoptedOutcomes:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:36:04', 21);
INSERT INTO `sys_oper_log` VALUES (206, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"TeacherHonors/TeacherHonors/index\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2048,\"menuName\":\"教师荣誉\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"TeacherHonors\",\"perms\":\"TeacherHonors:TeacherHonors:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:36:10', 12);
INSERT INTO `sys_oper_log` VALUES (207, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"IntellectualProperty/IntellectualProperty/index\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2054,\"menuName\":\"知识产权\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"IntellectualProperty\",\"perms\":\"IntellectualProperty:IntellectualProperty:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:36:17', 23);
INSERT INTO `sys_oper_log` VALUES (208, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"project/projects/index\",\"createTime\":\"2024-07-15 20:50:20\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2060,\"menuName\":\"项目\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"projects\",\"perms\":\"project:projects:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:36:25', 13);
INSERT INTO `sys_oper_log` VALUES (209, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 21:34:57\",\"icon\":\"example\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2067,\"menuName\":\"教师成果\",\"menuType\":\"M\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"/\",\"perms\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:37:05', 7);
INSERT INTO `sys_oper_log` VALUES (210, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teacher/teacher/index\",\"createTime\":\"2024-07-15 16:06:07\",\"icon\":\"cascader\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2018,\"menuName\":\"教师信息\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":0,\"path\":\"teacher\",\"perms\":\"teacher:teacher:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-15 21:37:25', 27);
INSERT INTO `sys_oper_log` VALUES (211, '参数管理', 2, 'com.ruoyi.web.controller.system.SysConfigController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/config', '127.0.0.1', '内网IP', '{\"configId\":4,\"configKey\":\"sys.account.captchaEnabled\",\"configName\":\"账号自助-验证码开关\",\"configType\":\"Y\",\"configValue\":\"false\",\"createBy\":\"admin\",\"createTime\":\"2024-07-15 14:20:16\",\"params\":{},\"remark\":\"是否开启验证码功能（true开启，false关闭）\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 01:22:44', 19);
INSERT INTO `sys_oper_log` VALUES (212, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"check_projects\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 01:35:12', 101);
INSERT INTO `sys_oper_log` VALUES (213, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"CheckProjects\",\"className\":\"CheckProjects\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"唯一标识\",\"columnId\":177,\"columnName\":\"id\",\"columnType\":\"int\",\"createBy\":\"admin\",\"createTime\":\"2024-07-16 01:35:12\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":false,\"isIncrement\":\"1\",\"isInsert\":\"0\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProjectCode\",\"columnComment\":\"立项编号\",\"columnId\":178,\"columnName\":\"project_code\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-16 01:35:12\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"0\",\"javaField\":\"projectCode\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProjectName\",\"columnComment\":\"项目名称\",\"columnId\":179,\"columnName\":\"project_name\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-16 01:35:12\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"0\",\"javaField\":\"projectName\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"LIKE\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":9,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ProjectLevel\",\"columnComment\":\"项目级别（分国家级、省级、厅级、校级、企业5类）\",\"columnId\":180,\"columnName\":\"project_level\",\"columnType\":\"varchar(100)\",\"createBy\":\"admin\",\"createTime\":\"2024-07-16 01:35:12\",\"dictType\":\"level\",\"edit\":true,\"htmlType\":\"select\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInser', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 01:39:45', 62);
INSERT INTO `sys_oper_log` VALUES (214, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"check_projects\"}', NULL, 0, NULL, '2024-07-16 01:39:48', 162);
INSERT INTO `sys_oper_log` VALUES (215, '论文', 2, 'com.ruoyi.papers.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/16/暑假计划_20240716111849A001.txt\",\"authorEmployeeId\":\"1\",\"authorRank\":\"0\",\"cnNumber\":\"2\",\"createTime\":\"2024-07-15 20:09:26\",\"id\":1,\"issue\":\"1\",\"journalName\":\"1\",\"paperLevel\":\"2\",\"paperTitle\":\"1\",\"params\":{},\"publicationDate\":\"2024-07-10\",\"searchWebsite\":\"1\",\"updateTime\":\"2024-07-16 11:18:50\",\"wordCount\":\"1\",\"year\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 03:18:49', 17);
INSERT INTO `sys_oper_log` VALUES (216, '知识产权', 1, 'edu.sias.ims.intellectualProperty.controller.IntellectualPropertyController.add()', 'POST', 1, 'admin', '研发部门', '/IntellectualProperty/IntellectualProperty', '127.0.0.1', '内网IP', '{\"achievementName\":\"1\",\"achievementNumber\":\"2\",\"category\":\"0\",\"completionDate\":\"2024-07-26\",\"createTime\":\"2024-07-16 19:35:08\",\"departmentId\":103,\"id\":1,\"isTransferred\":\"1\",\"members\":\"2\",\"params\":{},\"principalInvestigatorId\":\"2\",\"remarks\":\"11\",\"transferAmount\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 11:35:07', 30);
INSERT INTO `sys_oper_log` VALUES (217, '知识产权', 2, 'edu.sias.ims.intellectualProperty.controller.IntellectualPropertyController.edit()', 'PUT', 1, 'admin', '研发部门', '/IntellectualProperty/IntellectualProperty', '127.0.0.1', '内网IP', '{\"achievementName\":\"1\",\"achievementNumber\":\"2\",\"category\":\"0\",\"completionDate\":\"2024-07-26\",\"createTime\":\"2024-07-16 19:35:08\",\"departmentId\":103,\"id\":1,\"isTransferred\":\"1\",\"members\":\"2\",\"params\":{},\"principalInvestigatorId\":\"2\",\"remarks\":\"12\",\"transferAmount\":11,\"updateTime\":\"2024-07-16 19:35:36\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 11:35:35', 12);
INSERT INTO `sys_oper_log` VALUES (218, '知识产权', 1, 'edu.sias.ims.intellectualProperty.controller.IntellectualPropertyController.add()', 'POST', 1, 'admin', '研发部门', '/IntellectualProperty/IntellectualProperty', '127.0.0.1', '内网IP', '{\"achievementName\":\"2\",\"achievementNumber\":\"2\",\"category\":\"1\",\"completionDate\":\"2024-07-23\",\"createTime\":\"2024-07-16 19:37:07\",\"departmentId\":103,\"id\":2,\"isTransferred\":\"0\",\"members\":\"2\",\"params\":{},\"principalInvestigatorId\":\"2\",\"remarks\":\"2\",\"transferAmount\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 11:37:06', 13);
INSERT INTO `sys_oper_log` VALUES (219, '成果采纳', 1, 'edu.sias.ims.adoptedOutcomes.controller.AdoptedOutcomesController.add()', 'POST', 1, 'admin', '研发部门', '/adoptedOutcomes/adoptedOutcomes', '127.0.0.1', '内网IP', '{\"adoptedBy\":\"1\",\"adoptionDate\":\"2024-07-03\",\"createTime\":\"2024-07-16 20:33:05\",\"departmentId\":103,\"id\":1,\"outcomeName\":\"1\",\"params\":{},\"principalInvestigatorId\":\"1\",\"remarks\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 12:33:05', 44);
INSERT INTO `sys_oper_log` VALUES (220, '教师荣誉', 1, 'edu.sias.ims.teacherHonors.controller.TeacherHonorsController.add()', 'POST', 1, 'admin', '研发部门', '/teacherHonors/teacherHonors', '127.0.0.1', '内网IP', '{\"achievementForm\":\"1\",\"achievementName\":\"1\",\"awardCategory\":\"0\",\"createTime\":\"2024-07-16 20:48:18\",\"departmentId\":103,\"grantingOrganization\":\"1\",\"id\":1,\"params\":{},\"recipients\":\"1\",\"remarks\":\"1\",\"rewardLevel\":\"1\",\"rewardName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 12:48:18', 18);
INSERT INTO `sys_oper_log` VALUES (221, '知识产权', 1, 'edu.sias.ims.intellectualProperty.controller.IntellectualPropertyController.add()', 'POST', 1, 'admin', '研发部门', '/intellectualProperty/intellectualProperty', '127.0.0.1', '内网IP', '{\"achievementName\":\"1\",\"achievementNumber\":\"1\",\"category\":\"1\",\"completionDate\":\"2024-07-01\",\"createTime\":\"2024-07-16 20:48:28\",\"departmentId\":103,\"id\":3,\"members\":\"1\",\"params\":{},\"principalInvestigatorId\":\"1\",\"remarks\":\"1\",\"transferAmount\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 12:48:28', 13);
INSERT INTO `sys_oper_log` VALUES (222, '成果采纳', 1, 'edu.sias.ims.adoptedOutcomes.controller.AdoptedOutcomesController.add()', 'POST', 1, 'admin', '研发部门', '/adoptedOutcomes/adoptedOutcomes', '127.0.0.1', '内网IP', '{\"adoptedBy\":\"2\",\"adoptionDate\":\"2024-07-04\",\"createTime\":\"2024-07-16 20:48:36\",\"departmentId\":103,\"id\":2,\"outcomeName\":\"2\",\"params\":{},\"remarks\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 12:48:36', 11);
INSERT INTO `sys_oper_log` VALUES (223, '鉴定项目', 1, 'edu.sias.ims.checkProjects.controller.CheckProjectsController.add()', 'POST', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"checkGrade\":\"2\",\"checkTime\":\"2024-08-09\",\"createTime\":\"2024-07-16 20:48:46\",\"id\":1,\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"2\",\"projectCode\":\"1\",\"projectLevel\":\"1\",\"projectName\":\"2\",\"projectSource\":\"2\",\"remarks\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-16 12:48:46', 12);
INSERT INTO `sys_oper_log` VALUES (224, '论文', 3, 'edu.sias.ims.papers.controller.PapersController.remove()', 'DELETE', 1, 'admin', '研发部门', '/papers/papers/1', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:35:23', 24);
INSERT INTO `sys_oper_log` VALUES (225, '论文', 1, 'edu.sias.ims.papers.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717093544A001.txt\",\"authorEmployeeId\":\"21424325141\",\"authorRank\":\"1\",\"cnNumber\":\"35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"wordCount\":\"1241214\",\"year\":\"2015\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:36:32', 25);
INSERT INTO `sys_oper_log` VALUES (226, '著作教材', 3, 'edu.sias.ims.textbook.controller.WorksTextbooksController.remove()', 'DELETE', 1, 'admin', '研发部门', '/textbook/textbooks/1', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:37:44', 6);
INSERT INTO `sys_oper_log` VALUES (227, '著作教材', 1, 'edu.sias.ims.textbook.controller.WorksTextbooksController.add()', 'POST', 1, 'admin', '研发部门', '/textbook/textbooks', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717095029A004.txt\",\"authorEmployeeId\":\"24124121\",\"authorRole\":\"0\",\"authorWordCount\":\"123123\",\"createTime\":\"2024-07-17 09:50:33\",\"departmentId\":103,\"id\":2,\"isbn\":\"2141241\",\"params\":{},\"publisher\":\"新华出版社的\",\"remarks\":\"测试\",\"title\":\"相对论\",\"totalWordCount\":\"212332\",\"type\":\"科学\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:50:34', 9);
INSERT INTO `sys_oper_log` VALUES (228, '成果采纳', 3, 'edu.sias.ims.adoptedOutcomes.controller.AdoptedOutcomesController.remove()', 'DELETE', 1, 'admin', '研发部门', '/adoptedOutcomes/adoptedOutcomes/1', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:50:39', 5);
INSERT INTO `sys_oper_log` VALUES (229, '成果采纳', 3, 'edu.sias.ims.adoptedOutcomes.controller.AdoptedOutcomesController.remove()', 'DELETE', 1, 'admin', '研发部门', '/adoptedOutcomes/adoptedOutcomes/2', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:50:40', 12);
INSERT INTO `sys_oper_log` VALUES (230, '成果采纳', 1, 'edu.sias.ims.adoptedOutcomes.controller.AdoptedOutcomesController.add()', 'POST', 1, 'admin', '研发部门', '/adoptedOutcomes/adoptedOutcomes', '127.0.0.1', '内网IP', '{\"adoptedBy\":\"中国国家美容院\",\"adoptionDate\":\"2018-07-07\",\"createTime\":\"2024-07-17 09:52:24\",\"departmentId\":103,\"id\":3,\"outcomeName\":\"去黑头\",\"params\":{},\"principalInvestigatorId\":\"12412312\",\"proofAttachment\":\"/profile/upload/2024/07/17/hello_20240717095217A005.txt\",\"remarks\":\"测试\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:52:24', 18);
INSERT INTO `sys_oper_log` VALUES (231, '教师荣誉', 2, 'edu.sias.ims.teacherHonors.controller.TeacherHonorsController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacherHonors/teacherHonors', '127.0.0.1', '内网IP', '{\"achievementForm\":\"纸质\",\"achievementName\":\"软件杯全国一等奖\",\"awardCategory\":\"0\",\"createTime\":\"2024-07-16 20:48:19\",\"departmentId\":103,\"grantingOrganization\":\"教育局\",\"id\":1,\"params\":{},\"primaryRecipientId\":\"4212124124\",\"recipients\":\"小王,小李\",\"remarks\":\"测试\",\"rewardLevel\":\"1\",\"rewardName\":\"一等奖\",\"updateTime\":\"2024-07-17 09:53:47\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:53:47', 10);
INSERT INTO `sys_oper_log` VALUES (232, '教师荣誉', 2, 'edu.sias.ims.teacherHonors.controller.TeacherHonorsController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacherHonors/teacherHonors', '127.0.0.1', '内网IP', '{\"achievementForm\":\"纸质\",\"achievementName\":\"软件杯全国一等奖\",\"awardCategory\":\"0\",\"createTime\":\"2024-07-16 20:48:19\",\"departmentId\":103,\"grantingOrganization\":\"教育局\",\"id\":1,\"params\":{},\"primaryRecipientId\":\"4212124124\",\"recipients\":\"小王,小李\",\"remarks\":\"测试\",\"rewardLevel\":\"1\",\"rewardName\":\"一等奖\",\"updateTime\":\"2024-07-17 09:54:01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:54:01', 3);
INSERT INTO `sys_oper_log` VALUES (233, '教师荣誉', 2, 'edu.sias.ims.teacherHonors.controller.TeacherHonorsController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacherHonors/teacherHonors', '127.0.0.1', '内网IP', '{\"achievementForm\":\"纸质\",\"achievementName\":\"软件杯全国一等奖\",\"awardCategory\":\"0\",\"createTime\":\"2024-07-16 20:48:19\",\"departmentId\":103,\"grantingOrganization\":\"教育局\",\"id\":1,\"params\":{},\"primaryRecipientId\":\"4212124124\",\"recipients\":\"小王,小李\",\"remarks\":\"测试\",\"rewardLevel\":\"1\",\"rewardName\":\"一等奖\",\"updateTime\":\"2024-07-17 09:57:16\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:57:16', 6);
INSERT INTO `sys_oper_log` VALUES (234, '教师荣誉', 2, 'edu.sias.ims.teacherHonors.controller.TeacherHonorsController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacherHonors/teacherHonors', '127.0.0.1', '内网IP', '{\"achievementForm\":\"纸质\",\"achievementName\":\"软件杯全国一等奖\",\"awardCategory\":\"0\",\"certificateAttachment\":\"/profile/upload/2024/07/17/hello_20240717095951A009.txt\",\"createTime\":\"2024-07-16 20:48:19\",\"departmentId\":103,\"grantingOrganization\":\"教育局\",\"id\":1,\"params\":{},\"primaryRecipientId\":\"4212124124\",\"recipients\":\"小王,小李\",\"remarks\":\"测试\",\"rewardLevel\":\"1\",\"rewardName\":\"一等奖\",\"updateTime\":\"2024-07-17 09:59:51\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 09:59:52', 10);
INSERT INTO `sys_oper_log` VALUES (235, '知识产权', 3, 'edu.sias.ims.intellectualProperty.controller.IntellectualPropertyController.remove()', 'DELETE', 1, 'admin', '研发部门', '/intellectualProperty/intellectualProperty/1', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 10:00:22', 10);
INSERT INTO `sys_oper_log` VALUES (236, '知识产权', 3, 'edu.sias.ims.intellectualProperty.controller.IntellectualPropertyController.remove()', 'DELETE', 1, 'admin', '研发部门', '/intellectualProperty/intellectualProperty/2', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 10:00:24', 5);
INSERT INTO `sys_oper_log` VALUES (237, '知识产权', 2, 'edu.sias.ims.intellectualProperty.controller.IntellectualPropertyController.edit()', 'PUT', 1, 'admin', '研发部门', '/intellectualProperty/intellectualProperty', '127.0.0.1', '内网IP', '{\"achievementName\":\"冯诺依曼架构\",\"achievementNumber\":\"1\",\"attachment\":\"/profile/upload/2024/07/17/hello_20240717100142A010.txt\",\"category\":\"0\",\"completionDate\":\"2024-07-01\",\"createTime\":\"2024-07-16 20:48:29\",\"departmentId\":103,\"id\":3,\"isTransferred\":\"0\",\"members\":\"小李\",\"params\":{},\"principalInvestigatorId\":\"13423412\",\"remarks\":\"测试\",\"transferAmount\":2141241,\"updateTime\":\"2024-07-17 10:01:52\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 10:01:53', 26);
INSERT INTO `sys_oper_log` VALUES (238, '项目', 1, 'edu.sias.ims.project.controller.ProjectsController.add()', 'POST', 1, 'admin', '研发部门', '/project/projects', '127.0.0.1', '内网IP', '{\"completionDate\":\"2023-07-04\",\"createTime\":\"2024-07-17 10:04:13\",\"departmentId\":103,\"id\":1,\"initiationDate\":\"2022-07-07\",\"params\":{},\"principalInvestigatorId\":\"221412\",\"projectCode\":\"12312312\",\"projectLevel\":\"3\",\"projectName\":\"软件杯\",\"projectSource\":\"全国大学生培养基地\",\"remarks\":\"测试\",\"teamMembers\":\"小李\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 10:04:14', 9);
INSERT INTO `sys_oper_log` VALUES (239, '项目', 2, 'edu.sias.ims.project.controller.ProjectsController.edit()', 'PUT', 1, 'admin', '研发部门', '/project/projects', '127.0.0.1', '内网IP', '{\"attachments\":\"/profile/upload/2024/07/17/hello_20240717100513A012.txt\",\"completionDate\":\"2023-07-04\",\"createTime\":\"2024-07-17 10:04:14\",\"departmentId\":103,\"id\":1,\"initiationDate\":\"2022-07-07\",\"params\":{},\"principalInvestigatorId\":\"221412\",\"projectCode\":\"12312312\",\"projectLevel\":\"3\",\"projectName\":\"软件杯\",\"projectSource\":\"全国大学生培养基地\",\"remarks\":\"测试\",\"teamMembers\":\"小李\",\"updateTime\":\"2024-07-17 10:05:14\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 10:05:14', 17);
INSERT INTO `sys_oper_log` VALUES (240, '鉴定项目', 2, 'edu.sias.ims.checkProjects.controller.CheckProjectsController.edit()', 'PUT', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"attachments\":\"/profile/upload/2024/07/17/hello_20240717100538A013.txt\",\"checkGrade\":\"2\",\"checkTime\":\"2023-08-01\",\"createTime\":\"2024-07-16 20:48:46\",\"id\":1,\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"2123123\",\"projectCode\":\"1241241\",\"projectLevel\":\"4\",\"projectName\":\"软件杯\",\"projectSource\":\"大学生创业基地\",\"remarks\":\"测试\",\"updateTime\":\"2024-07-17 10:06:55\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 10:06:55', 11);
INSERT INTO `sys_oper_log` VALUES (241, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:50:21\",\"department\":\"计算机\",\"employeeNumber\":\"2224124\",\"employmentType\":1,\"firstDegree\":\"研究生\",\"firstDegreeObject\":\"软件工程\",\"gender\":0,\"highestDegree\":\"研究生\",\"highestdegreeObject\":\"软件工程\",\"id\":2,\"name\":\"老王\",\"nationalId\":\"401424500350242051\",\"nationality\":\"2\",\"params\":{},\"phoneNumber\":\"24214124121\",\"politicalAffiliation\":\"0\",\"position\":\"教育\",\"remarks\":\"测试数据\",\"title\":\"教授\",\"updateTime\":\"2024-07-17 10:57:36\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 10:57:36', 25);
INSERT INTO `sys_oper_log` VALUES (242, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:50:21\",\"department\":\"计算机\",\"employeeNumber\":\"2224124\",\"employmentType\":1,\"firstDegree\":\"研究生\",\"firstDegreeObject\":\"软件工程\",\"gender\":0,\"highestDegree\":\"研究生\",\"highestdegreeObject\":\"软件工程\",\"id\":2,\"name\":\"老王\",\"nationalId\":\"401424500350242051\",\"nationality\":\"2\",\"params\":{},\"phoneNumber\":\"24214124121\",\"politicalAffiliation\":\"0\",\"position\":\"教育\",\"remarks\":\"测试数据\",\"title\":\"教授\",\"updateTime\":\"2024-07-17 11:22:06\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 11:22:06', 28);
INSERT INTO `sys_oper_log` VALUES (243, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:50:21\",\"department\":\"计算机\",\"employeeNumber\":\"2224124\",\"employmentType\":1,\"firstDegree\":\"研究生\",\"firstDegreeObject\":\"软件工程\",\"gender\":0,\"highestDegree\":\"研究生\",\"highestdegreeObject\":\"软件工程\",\"id\":2,\"name\":\"老王\",\"nationalId\":\"401124500350242251\",\"nationality\":\"2\",\"params\":{},\"phoneNumber\":\"24214124412\",\"politicalAffiliation\":\"0\",\"position\":\"教育\",\"remarks\":\"测试数据\",\"title\":\"教授\",\"updateTime\":\"2024-07-17 11:22:32\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 11:22:33', 7);
INSERT INTO `sys_oper_log` VALUES (244, '教师信息', 1, 'edu.sias.ims.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-17 11:23:44\",\"department\":\"商学院\",\"departmentId\":103,\"employeeNumber\":\"12412412\",\"employmentType\":0,\"firstDegree\":\"硕士\",\"firstDegreeObject\":\"心理学\",\"gender\":0,\"highestDegree\":\"硕士\",\"highestdegreeObject\":\"心理学\",\"id\":3,\"name\":\"小王\",\"nationalId\":\"241241241241251235\",\"nationality\":\"1\",\"params\":{},\"phoneNumber\":\"43252352622\",\"politicalAffiliation\":\"0\",\"position\":\"教师\",\"remarks\":\"测试\",\"title\":\"教授\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 11:23:45', 10);
INSERT INTO `sys_oper_log` VALUES (245, '教师信息', 1, 'edu.sias.ims.teacher.controller.TeacherInfoController.add()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-17 11:34:50\",\"department\":\"第五\",\"departmentId\":103,\"employeeNumber\":\"744555\",\"employmentType\":0,\"firstDegree\":\"的\",\"firstDegreeObject\":\"得到\",\"gender\":0,\"highestDegree\":\"得到\",\"highestdegreeObject\":\"的\",\"id\":4,\"name\":\"达到\",\"nationalId\":\"145542156448965895\",\"nationality\":\"1\",\"params\":{},\"phoneNumber\":\"15498562145\",\"politicalAffiliation\":\"0\",\"position\":\"打\",\"remarks\":\"的\",\"title\":\"第五\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 11:34:51', 118);
INSERT INTO `sys_oper_log` VALUES (246, '教师信息', 3, 'edu.sias.ims.teacher.controller.TeacherInfoController.remove()', 'DELETE', 1, 'admin', '研发部门', '/teacher/teacher/4', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-17 11:34:57', 7);
INSERT INTO `sys_oper_log` VALUES (247, '教师信息', 5, 'edu.sias.ims.teacher.controller.TeacherInfoController.export()', 'POST', 1, 'admin', '研发部门', '/teacher/teacher/export', '127.0.0.1', '内网IP', '{\"pageSize\":\"10\",\"pageNum\":\"1\"}', NULL, 0, NULL, '2024-07-17 15:05:18', 899);
INSERT INTO `sys_oper_log` VALUES (248, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"guide\",\"isCache\":\"0\",\"isFrame\":\"0\",\"menuId\":4,\"menuName\":\"若依官网\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"http://ruoyi.vip\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-19 15:55:01', 9);
INSERT INTO `sys_oper_log` VALUES (249, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717093544A001.txt\",\"authorEmployeeId\":\"21424325141\",\"authorRank\":\"1\",\"cnNumber\":\"35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"3\",\"updateTime\":\"2024-07-24 22:58:38\",\"wordCount\":\"12214\",\"year\":\"2015\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-24 22:58:39', 57);
INSERT INTO `sys_oper_log` VALUES (250, '知识产权', 3, 'edu.sias.ims.adults.controller.IntellectualPropertyController.remove()', 'DELETE', 1, 'admin', '研发部门', '/intellectualProperty/intellectualProperty/1', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-24 23:00:32', 11);
INSERT INTO `sys_oper_log` VALUES (251, '鉴定项目', 1, 'edu.sias.ims.adults.controller.CheckProjectsController.add()', 'POST', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"attachments\":\"/profile/upload/2024/07/25/hello_20240725000459A001.txt\",\"checkGrade\":\"3\",\"checkTime\":\"2024-07-04\",\"createTime\":\"2024-07-25 00:05:21\",\"id\":2,\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"24121\",\"projectCode\":\"213121412\",\"projectLevel\":\"2\",\"projectName\":\"测试\",\"projectSource\":\"教育局\",\"remarks\":\"唔\",\"teacherId\":\"2224124\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-25 00:05:21', 136);
INSERT INTO `sys_oper_log` VALUES (252, '鉴定项目', 1, 'edu.sias.ims.adults.controller.CheckProjectsController.add()', 'POST', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"checkGrade\":\"4\",\"checkTime\":\"2024-07-08\",\"createTime\":\"2024-07-25 14:39:16\",\"id\":3,\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"23123\",\"projectCode\":\"2124124\",\"projectLevel\":\"1\",\"projectName\":\"物理\",\"projectSource\":\"低洼地\",\"remarks\":\"无\",\"teacherId\":\"4\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-25 14:39:16', 20);
INSERT INTO `sys_oper_log` VALUES (253, '鉴定项目', 1, 'edu.sias.ims.adults.controller.CheckProjectsController.add()', 'POST', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"checkGrade\":\"问答\",\"checkTime\":\"2024-07-02\",\"createTime\":\"2024-07-25 14:42:52\",\"id\":4,\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"2412\",\"projectCode\":\"2424\",\"projectLevel\":\"0\",\"projectName\":\"dawd\",\"projectSource\":\"大王\",\"remarks\":\"d爱我的\",\"teacherId\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-25 14:42:52', 141);
INSERT INTO `sys_oper_log` VALUES (254, '鉴定项目', 1, 'edu.sias.ims.adults.controller.CheckProjectsController.add()', 'POST', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"checkGrade\":\"4\",\"checkTime\":\"2024-07-08\",\"createTime\":\"2024-07-25 14:49:37\",\"id\":5,\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"21421\",\"projectCode\":\"2142\",\"projectLevel\":\"2\",\"projectName\":\"额外的\",\"projectSource\":\"打我的2\",\"remarks\":\"我\",\"teacherId\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-25 14:49:37', 137);
INSERT INTO `sys_oper_log` VALUES (255, '奖学金信息管理', 5, 'edu.sias.ims.student.controller.ScholarshipController.export()', 'POST', 1, 'admin', '研发部门', '/scholarship/scholarship/export', '127.0.0.1', '内网IP', '{\"pageSize\":\"10\",\"pageNum\":\"1\"}', NULL, 0, NULL, '2024-07-25 15:12:44', 873);
INSERT INTO `sys_oper_log` VALUES (256, '论文', 1, 'edu.sias.ims.adults.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"authorEmployeeId\":\"23\",\"authorRank\":\"0\",\"cnNumber\":\"21341231\",\"createTime\":\"2024-07-29 12:18:56\",\"departmentId\":103,\"id\":3,\"issue\":\"2\",\"journalName\":\"21312\",\"paperLevel\":\"1\",\"paperTitle\":\"dawd\",\"params\":{},\"publicationDate\":\"2024-07-10\",\"remarks\":\"213\",\"searchWebsite\":\"www.baidu.com\",\"teacherId\":\"2224124\",\"wordCount\":\"21321\",\"year\":\"2132\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-29 12:18:57', 36);
INSERT INTO `sys_oper_log` VALUES (257, '论文', 1, 'edu.sias.ims.adults.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"authorEmployeeId\":\"213\",\"authorRank\":\"1\",\"cnNumber\":\"3额13\",\"createTime\":\"2024-07-29 12:27:48\",\"departmentId\":103,\"issue\":\"321\",\"journalName\":\"12312\",\"paperLevel\":\"1\",\"paperTitle\":\"打我的2\",\"params\":{},\"publicationDate\":\"2024-07-09\",\"remarks\":\"2\",\"searchWebsite\":\"www.baidu.com\",\"teacherId\":\"2224124\",\"wordCount\":\"312312\",\"year\":\"2023\"}', NULL, 1, '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Out of range value for column \'word_count\' at row 1\r\n### The error may exist in file [D:\\Java\\ruoyi\\暑期实训\\ims\\ims-achievement-manage\\target\\classes\\mapper\\papers\\PapersMapper.xml]\r\n### The error may involve edu.sias.ims.adults.mapper.PapersMapper.insertPapers-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into papers          ( paper_title,                          author_employee_id,             author_rank,             journal_name,             paper_level,             cn_number,             word_count,             publication_date,             year,             issue,             search_website,                                       create_time,                                                    department_id,             remarks,             teacher_id )           values ( ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                       ?,                                                    ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Out of range value for column \'word_count\' at row 1\n; Data truncation: Out of range value for column \'word_count\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Out of range value for column \'word_count\' at row 1', '2024-07-29 12:27:48', 146);
INSERT INTO `sys_oper_log` VALUES (258, '论文', 1, 'edu.sias.ims.adults.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"authorEmployeeId\":\"213\",\"authorRank\":\"1\",\"cnNumber\":\"3额13\",\"createTime\":\"2024-07-29 12:30:24\",\"departmentId\":103,\"id\":4,\"issue\":\"321\",\"journalName\":\"12312\",\"paperLevel\":\"1\",\"paperTitle\":\"打我的2\",\"params\":{},\"publicationDate\":\"2024-07-09\",\"remarks\":\"2\",\"searchWebsite\":\"www.baidu.com\",\"teacherId\":\"2224124\",\"wordCount\":\"312\",\"year\":\"2023\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-29 12:30:25', 8);
INSERT INTO `sys_oper_log` VALUES (259, '论文', 1, 'edu.sias.ims.adults.controller.PapersController.add()', 'POST', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"authorEmployeeId\":\"2313412\",\"authorRank\":\"0\",\"cnNumber\":\"23112\",\"createTime\":\"2024-07-29 12:34:15\",\"departmentId\":103,\"id\":5,\"issue\":\"12\",\"journalName\":\"213\",\"paperLevel\":\"1\",\"paperTitle\":\"打我的\",\"params\":{},\"publicationDate\":\"2024-07-01\",\"remarks\":\"多企鹅群\",\"searchWebsite\":\"www.baidu.com\",\"teacherId\":\"2224124\",\"wordCount\":\"23121412\",\"year\":\"213\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-29 12:34:15', 110);
INSERT INTO `sys_oper_log` VALUES (260, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"authorEmployeeId\":\"2313412\",\"authorRank\":\"0\",\"cnNumber\":\"23112\",\"createTime\":\"2024-07-29 12:34:15\",\"departmentId\":103,\"id\":5,\"issue\":\"12\",\"journalName\":\"213\",\"name\":\"小李\",\"paperLevel\":\"1\",\"paperTitle\":\"打的钱\",\"params\":{},\"publicationDate\":\"2024-07-01\",\"remarks\":\"多企鹅群\",\"searchWebsite\":\"www.baidu.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-07-29 12:34:21\",\"wordCount\":\"23121412\",\"year\":\"213\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: Unknown column \'teacherId\' in \'field list\'\r\n### The error may exist in file [D:\\Java\\ruoyi\\暑期实训\\ims\\ims-achievement-manage\\target\\classes\\mapper\\papers\\PapersMapper.xml]\r\n### The error may involve edu.sias.ims.adults.mapper.PapersMapper.updatePapers-Inline\r\n### The error occurred while setting parameters\r\n### SQL: update papers          SET paper_title = ?,                          author_employee_id = ?,             author_rank = ?,             journal_name = ?,             paper_level = ?,             cn_number = ?,             word_count = ?,             publication_date = ?,             year = ?,             issue = ?,             search_website = ?,                                       create_time = ?,                          update_time = ?,                          department_id = ?,             remarks = ?,             teacherId = ?          where id = ?\r\n### Cause: java.sql.SQLSyntaxErrorException: Unknown column \'teacherId\' in \'field list\'\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: Unknown column \'teacherId\' in \'field list\'', '2024-07-29 12:34:21', 55);
INSERT INTO `sys_oper_log` VALUES (261, '论文', 3, 'edu.sias.ims.adults.controller.PapersController.remove()', 'DELETE', 1, 'admin', '研发部门', '/papers/papers/5', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-07-29 12:38:51', 12);
INSERT INTO `sys_oper_log` VALUES (262, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:50:21\",\"department\":\"计算机\",\"employeeNumber\":\"2224124\",\"employmentType\":1,\"firstDegree\":\"研究生\",\"firstDegreeObject\":\"软件工程\",\"gender\":0,\"highestDegree\":\"\",\"highestDegreeObject\":\"软件工程\",\"hireDate\":\"2024-07-27 00:00:00\",\"id\":2,\"name\":\"小李\",\"nationalId\":\"401124500350242251\",\"nationality\":\"2\",\"params\":{},\"phoneNumber\":\"24214124412\",\"politicalAffiliation\":\"0\",\"position\":\"教育\",\"remarks\":\"测试数据\",\"title\":\"教授\",\"titleAcquisitionDate\":\"2024-07-28 00:00:00\",\"updateTime\":\"2024-08-01 17:59:22\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 17:59:22', 18);
INSERT INTO `sys_oper_log` VALUES (263, '字典类型', 1, 'edu.sias.ims.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"论文级别\",\"dictType\":\"paper_level\",\"params\":{},\"remark\":\"论文级别\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:01:07', 14);
INSERT INTO `sys_oper_log` VALUES (264, '字典数据', 1, 'edu.sias.ims.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"普通刊物\",\"dictSort\":0,\"dictType\":\"paper_level\",\"dictValue\":\"0\",\"listClass\":\"default\",\"params\":{},\"remark\":\"普通刊物\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:01:25', 11);
INSERT INTO `sys_oper_log` VALUES (265, '字典数据', 1, 'edu.sias.ims.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"北大核心刊物\",\"dictSort\":1,\"dictType\":\"paper_level\",\"dictValue\":\"1\",\"listClass\":\"default\",\"params\":{},\"remark\":\"北大核心刊物\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:01:35', 9);
INSERT INTO `sys_oper_log` VALUES (266, '字典数据', 1, 'edu.sias.ims.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', '研发部门', '/system/dict/data', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"default\":false,\"dictLabel\":\"南大核心刊物\",\"dictSort\":2,\"dictType\":\"paper_level\",\"dictValue\":\"2\",\"listClass\":\"default\",\"params\":{},\"remark\":\"南大核心刊物\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:01:47', 9);
INSERT INTO `sys_oper_log` VALUES (267, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-15 16:50:21\",\"department\":\"计算机\",\"employeeNumber\":\"2224124\",\"employmentType\":1,\"firstDegree\":\"研究生\",\"firstDegreeObject\":\"软件工程\",\"gender\":0,\"highestDegree\":\"\",\"highestDegreeObject\":\"软件工程\",\"hireDate\":\"2024-07-27 00:00:00\",\"id\":2,\"name\":\"小李\",\"nationalId\":\"401124200550242251\",\"nationality\":\"2\",\"params\":{},\"phoneNumber\":\"24214124412\",\"politicalAffiliation\":\"0\",\"position\":\"教育\",\"remarks\":\"测试数据\",\"title\":\"教授\",\"titleAcquisitionDate\":\"2024-07-28 00:00:00\",\"updateTime\":\"2024-08-01 18:52:44\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:52:44', 31);
INSERT INTO `sys_oper_log` VALUES (268, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-17 11:23:45\",\"department\":\"商学院\",\"departmentId\":103,\"employeeNumber\":\"12412412\",\"employmentType\":0,\"firstDegree\":\"硕士\",\"firstDegreeObject\":\"心理学\",\"gender\":0,\"highestDegree\":\"硕士\",\"highestDegreeObject\":\"心理学\",\"id\":3,\"name\":\"小王\",\"nationalId\":\"441242014241251235\",\"nationality\":\"1\",\"params\":{},\"phoneNumber\":\"43252352622\",\"politicalAffiliation\":\"0\",\"position\":\"教师\",\"remarks\":\"测试\",\"title\":\"教授\",\"updateTime\":\"2024-08-01 18:53:06\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:53:06', 5);
INSERT INTO `sys_oper_log` VALUES (269, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-17 11:23:45\",\"department\":\"商学院\",\"departmentId\":103,\"employeeNumber\":\"12412412\",\"employmentType\":0,\"firstDegree\":\"硕士\",\"firstDegreeObject\":\"心理学\",\"gender\":0,\"highestDegree\":\"硕士\",\"highestDegreeObject\":\"心理学\",\"id\":3,\"name\":\"小王\",\"nationalId\":\"441242200241251235\",\"nationality\":\"1\",\"params\":{},\"phoneNumber\":\"43252352622\",\"politicalAffiliation\":\"0\",\"position\":\"教师\",\"remarks\":\"测试\",\"title\":\"教授\",\"updateTime\":\"2024-08-01 18:53:34\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:53:34', 6);
INSERT INTO `sys_oper_log` VALUES (270, '教师信息', 2, 'edu.sias.ims.teacher.controller.TeacherInfoController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacher/teacher', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-07-17 11:23:45\",\"department\":\"商学院\",\"departmentId\":103,\"employeeNumber\":\"12412412\",\"employmentType\":0,\"firstDegree\":\"硕士\",\"firstDegreeObject\":\"心理学\",\"gender\":0,\"highestDegree\":\"硕士\",\"highestDegreeObject\":\"心理学\",\"hireDate\":\"2023-08-23 00:00:00\",\"id\":3,\"name\":\"小王\",\"nationalId\":\"441242200241251235\",\"nationality\":\"1\",\"params\":{},\"phoneNumber\":\"43252352622\",\"politicalAffiliation\":\"0\",\"position\":\"教师\",\"remarks\":\"测试\",\"title\":\"教授\",\"titleAcquisitionDate\":\"2024-07-27 00:00:00\",\"updateTime\":\"2024-08-01 18:53:44\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:53:44', 4);
INSERT INTO `sys_oper_log` VALUES (271, '教师荣誉', 2, 'edu.sias.ims.adults.controller.TeacherHonorsController.edit()', 'PUT', 1, 'admin', '研发部门', '/teacherHonors/teacherHonors', '127.0.0.1', '内网IP', '{\"achievementForm\":\"纸质\",\"achievementName\":\"软件杯全国一等奖\",\"awardCategory\":\"0\",\"certificateAttachment\":\"/profile/upload/2024/07/17/hello_20240717095951A009.txt\",\"createTime\":\"2024-07-16 20:48:19\",\"departmentId\":103,\"grantingOrganization\":\"教育局\",\"id\":1,\"name\":\"小李\",\"params\":{},\"primaryRecipientId\":\"2224124\",\"recipients\":\"小王,小李\",\"remarks\":\"测试\",\"rewardLevel\":\"1\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-01 18:55:25\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:55:26', 25);
INSERT INTO `sys_oper_log` VALUES (272, '成果采纳', 2, 'edu.sias.ims.adults.controller.AdoptedOutcomesController.edit()', 'PUT', 1, 'admin', '研发部门', '/adoptedOutcomes/adoptedOutcomes', '127.0.0.1', '内网IP', '{\"adoptedBy\":\"中国国家美容院\",\"adoptionDate\":\"2018-07-07\",\"createTime\":\"2024-07-17 09:52:25\",\"departmentId\":103,\"id\":3,\"name\":\"小李\",\"outcomeName\":\"去黑头\",\"params\":{},\"principalInvestigatorId\":\"2224124\",\"proofAttachment\":\"/profile/upload/2024/07/17/hello_20240717095217A005.txt\",\"remarks\":\"测试\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-01 18:55:35\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:55:35', 7);
INSERT INTO `sys_oper_log` VALUES (273, '知识产权', 2, 'edu.sias.ims.adults.controller.IntellectualPropertyController.edit()', 'PUT', 1, 'admin', '研发部门', '/intellectualProperty/intellectualProperty', '127.0.0.1', '内网IP', '{\"achievementName\":\"冯诺依曼架构\",\"achievementNumber\":\"1\",\"attachment\":\"/profile/upload/2024/07/17/hello_20240717100142A010.txt\",\"category\":\"0\",\"completionDate\":\"2024-07-01\",\"createTime\":\"2024-07-16 20:48:29\",\"departmentId\":103,\"id\":3,\"isTransferred\":\"0\",\"members\":\"小李\",\"name\":\"小李\",\"params\":{},\"principalInvestigatorId\":\"2224124\",\"remarks\":\"测试\",\"teacherId\":\"2224124\",\"transferAmount\":2141241,\"updateTime\":\"2024-08-01 18:55:44\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:55:44', 10);
INSERT INTO `sys_oper_log` VALUES (274, '项目', 2, 'edu.sias.ims.adults.controller.ProjectsController.edit()', 'PUT', 1, 'admin', '研发部门', '/project/projects', '127.0.0.1', '内网IP', '{\"attachments\":\"/profile/upload/2024/07/17/hello_20240717100513A012.txt\",\"completionDate\":\"2023-07-04\",\"createTime\":\"2024-07-17 10:04:14\",\"departmentId\":103,\"id\":1,\"initiationDate\":\"2022-07-07\",\"name\":\"小李\",\"params\":{},\"principalInvestigatorId\":\"2224124\",\"projectCode\":\"12312312\",\"projectLevel\":\"3\",\"projectName\":\"软件杯\",\"projectSource\":\"全国大学生培养基地\",\"remarks\":\"测试\",\"teacherId\":\"2224124\",\"teamMembers\":\"小李\",\"updateTime\":\"2024-08-01 18:55:51\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:55:51', 10);
INSERT INTO `sys_oper_log` VALUES (275, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717093544A001.txt\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-01 18:56:06\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:56:06', 10);
INSERT INTO `sys_oper_log` VALUES (276, '鉴定项目', 2, 'edu.sias.ims.adults.controller.CheckProjectsController.edit()', 'PUT', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"attachments\":\"/profile/upload/2024/07/17/hello_20240717100538A013.txt\",\"checkGrade\":\"2\",\"checkTime\":\"2023-08-01\",\"createTime\":\"2024-07-16 20:48:46\",\"id\":1,\"name\":\"小李\",\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"2224124\",\"projectCode\":\"1241241\",\"projectLevel\":\"4\",\"projectName\":\"软件杯\",\"projectSource\":\"大学生创业基地\",\"remarks\":\"测试\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-01 18:56:14\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:56:14', 9);
INSERT INTO `sys_oper_log` VALUES (277, '鉴定项目', 2, 'edu.sias.ims.adults.controller.CheckProjectsController.edit()', 'PUT', 1, 'admin', '研发部门', '/checkProjects/checkProjects', '127.0.0.1', '内网IP', '{\"attachments\":\"/profile/upload/2024/07/25/hello_20240725000459A001.txt\",\"checkGrade\":\"3\",\"checkTime\":\"2024-07-04\",\"createTime\":\"2024-07-25 00:05:22\",\"id\":2,\"name\":\"小李\",\"params\":{},\"partment\":103,\"principalInvestigatorId\":\"12412412\",\"projectCode\":\"213121412\",\"projectLevel\":\"2\",\"projectName\":\"测试\",\"projectSource\":\"教育局\",\"remarks\":\"唔\",\"teacherId\":\"12412412\",\"updateTime\":\"2024-08-01 18:56:24\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:56:24', 12);
INSERT INTO `sys_oper_log` VALUES (278, '著作教材', 2, 'edu.sias.ims.adults.controller.WorksTextbooksController.edit()', 'PUT', 1, 'admin', '研发部门', '/textbook/textbooks', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717095029A004.txt\",\"authorEmployeeId\":\"12412412\",\"authorRole\":\"0\",\"authorWordCount\":\"123123\",\"createTime\":\"2024-07-17 09:50:34\",\"departmentId\":103,\"id\":2,\"isbn\":\"2141241\",\"name\":\"小李\",\"params\":{},\"publisher\":\"新华出版社的\",\"remarks\":\"测试\",\"teacherId\":\"12412412\",\"title\":\"相对论\",\"totalWordCount\":\"212332\",\"type\":\"科学\",\"updateTime\":\"2024-08-01 18:56:30\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-01 18:56:30', 11);
INSERT INTO `sys_oper_log` VALUES (279, '论文导入', 6, 'edu.sias.ims.adults.controller.TeacherHonorsController.importData()', 'POST', 1, 'admin', '研发部门', '/teacherHonors/teacherHonors/importData', '127.0.0.1', '内网IP', '{\"updateSupport\":\"0\"}', '{\"msg\":\"恭喜您，数据已全部导入成功！共 1 条，数据如下：<br/>1、教师荣誉 测试 导入成功\",\"code\":200}', 0, NULL, '2024-08-01 18:58:44', 126);
INSERT INTO `sys_oper_log` VALUES (280, '知识产权', 2, 'edu.sias.ims.adults.controller.IntellectualPropertyController.edit()', 'PUT', 1, 'admin', '研发部门', '/intellectualProperty/intellectualProperty', '127.0.0.1', '内网IP', '{\"achievementName\":\"冯诺依曼架构\",\"achievementNumber\":\"1\",\"attachment\":\"/profile/upload/2024/07/17/hello_20240717100142A010.txt\",\"category\":\"0\",\"completionDate\":\"2024-07-01\",\"createTime\":\"2024-07-16 20:48:29\",\"departmentId\":103,\"id\":3,\"isTransferred\":\"0\",\"members\":\"小李\",\"name\":\"小李\",\"params\":{},\"principalInvestigatorId\":\"2224124\",\"remarks\":\"测试\",\"teacherId\":\"2224124\",\"transferAmount\":0,\"updateTime\":\"2024-08-02 11:37:29\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:37:28', 21);
INSERT INTO `sys_oper_log` VALUES (281, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 21:34:57\",\"icon\":\"example\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2067,\"menuName\":\"教师成果\",\"menuType\":\"M\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"/\",\"perms\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:55:12', 35);
INSERT INTO `sys_oper_log` VALUES (282, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-16 10:44:38\",\"icon\":\"people\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3028,\"menuName\":\"学生成果管理\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"student/student\",\"perms\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:55:23', 7);
INSERT INTO `sys_oper_log` VALUES (283, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2,\"menuName\":\"系统监控\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"monitor\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:55:50', 12);
INSERT INTO `sys_oper_log` VALUES (284, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 21:34:57\",\"icon\":\"example\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2067,\"menuName\":\"教师成果\",\"menuType\":\"M\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"/\",\"perms\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:56:05', 9);
INSERT INTO `sys_oper_log` VALUES (285, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"system\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1,\"menuName\":\"系统管理\",\"menuType\":\"M\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"system\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:56:18', 7);
INSERT INTO `sys_oper_log` VALUES (286, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2,\"menuName\":\"系统监控\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"monitor\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:56:28', 6);
INSERT INTO `sys_oper_log` VALUES (287, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"monitor\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2,\"menuName\":\"系统监控\",\"menuType\":\"M\",\"orderNum\":4,\"params\":{},\"parentId\":0,\"path\":\"monitor\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:56:58', 9);
INSERT INTO `sys_oper_log` VALUES (288, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 11:57:08', 9);
INSERT INTO `sys_oper_log` VALUES (289, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 14:37:38', 20);
INSERT INTO `sys_oper_log` VALUES (290, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717093544A001.txt,/profile/upload/答辩问题.xlsx,/profile/upload/答辩问题.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 15:55:57\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 15:55:57', 38);
INSERT INTO `sys_oper_log` VALUES (291, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717093544A001.txt\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 15:56:05\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 15:56:05', 7);
INSERT INTO `sys_oper_log` VALUES (292, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717093544A001.txt,/profile/upload/答辩问题.xlsx,/profile/upload/答辩问题.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 15:57:06\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 15:57:06', 40);
INSERT INTO `sys_oper_log` VALUES (293, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/07/17/hello_20240717093544A001.txt,/profile/upload/答辩问题.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 15:57:10\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 15:57:10', 6);
INSERT INTO `sys_oper_log` VALUES (294, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/答辩问题.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 15:57:14\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 15:57:14', 5);
INSERT INTO `sys_oper_log` VALUES (295, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:04:07\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:04:07', 27);
INSERT INTO `sys_oper_log` VALUES (296, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02/答辩问题_20240802160411A003.xlsx,/profile/upload/2024/08/02/答辩问题_20240802160414A004.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:04:15\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:04:15', 6);
INSERT INTO `sys_oper_log` VALUES (297, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题.xlsx,/profile/upload/2024/08/02_答辩问题.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:11:43\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:11:43', 27);
INSERT INTO `sys_oper_log` VALUES (298, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx,/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:17:56\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:17:56', 26);
INSERT INTO `sys_oper_log` VALUES (299, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx,/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:24:56\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:24:56', 29);
INSERT INTO `sys_oper_log` VALUES (300, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:25:03\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:25:04', 6);
INSERT INTO `sys_oper_log` VALUES (301, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx,/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:31:18\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:31:18', 29);
INSERT INTO `sys_oper_log` VALUES (302, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx,/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:31:30\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:31:30', 6);
INSERT INTO `sys_oper_log` VALUES (303, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx,/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:51:07\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:51:07', 28);
INSERT INTO `sys_oper_log` VALUES (304, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx,/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:51:16\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:51:16', 5);
INSERT INTO `sys_oper_log` VALUES (305, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:51:44\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:51:44', 30);
INSERT INTO `sys_oper_log` VALUES (306, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx,/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:51:50\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:51:50', 6);
INSERT INTO `sys_oper_log` VALUES (307, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02_答辩问题_1.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 16:51:53\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 16:51:53', 6);
INSERT INTO `sys_oper_log` VALUES (308, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02/答辩问题_20240802170805A001.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 17:08:07\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 17:08:08', 26);
INSERT INTO `sys_oper_log` VALUES (309, '论文', 2, 'edu.sias.ims.adults.controller.PapersController.edit()', 'PUT', 1, 'admin', '研发部门', '/papers/papers', '127.0.0.1', '内网IP', '{\"attachment\":\"/profile/upload/2024/08/02/答辩问题_20240802170805A001.xlsx\",\"authorEmployeeId\":\"2224124\",\"authorRank\":\"1\",\"cnNumber\":\"CN-35325235-4634\",\"createTime\":\"2024-07-17 09:36:32\",\"departmentId\":103,\"id\":2,\"issue\":\"34\",\"journalName\":\"物理学的突破\",\"name\":\"小李\",\"paperLevel\":\"2\",\"paperTitle\":\"量子力学\",\"params\":{},\"publicationDate\":\"2015-07-09\",\"remarks\":\"测试\",\"searchWebsite\":\"www.123.com\",\"teacherId\":\"2224124\",\"updateTime\":\"2024-08-02 17:08:20\",\"wordCount\":\"12214\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-02 17:08:20', 3);
INSERT INTO `sys_oper_log` VALUES (310, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-07 09:21:20', 33);
INSERT INTO `sys_oper_log` VALUES (311, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-07 09:23:30', 17);
INSERT INTO `sys_oper_log` VALUES (312, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"tool\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3,\"menuName\":\"系统工具\",\"menuType\":\"M\",\"orderNum\":3,\"params\":{},\"parentId\":0,\"path\":\"tool\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-07 09:23:59', 16);
INSERT INTO `sys_oper_log` VALUES (313, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-07-15 14:20:16\",\"icon\":\"system\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":1,\"menuName\":\"系统管理\",\"menuType\":\"M\",\"orderNum\":2,\"params\":{},\"parentId\":0,\"path\":\"system\",\"perms\":\"\",\"query\":\"\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-07 09:24:05', 13);
INSERT INTO `sys_oper_log` VALUES (314, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"教师信息导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2018,\"perms\":\"teacher:teacher:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:44:07', 20);
INSERT INTO `sys_oper_log` VALUES (315, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"adoptedOutcomes/adoptedOutcomes/index\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2042,\"menuName\":\"成果采纳\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"adoptedOutcomes\",\"perms\":\"adoptedOutcomes:adoptedOutcomes:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:45:12', 16);
INSERT INTO `sys_oper_log` VALUES (316, '角色管理', 2, 'edu.sias.ims.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-07-15 14:20:15\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2018,2019,2020,2021,2022,2023,4078,2067,2030,2031,2032,2033,2034,2035,2036,2037,2038,2039,2040,2041,2042,2043,2044,2045,2046,2047,2048,2049,2050,2051,2052,2053,2054,2055,2056,2057,2058,2059,2060,2061,2062,2063,2064,2065,2068,2069,2070,2071,2072,2073],\"params\":{},\"remark\":\"普通角色\",\"roleId\":2,\"roleKey\":\"teacher\",\"roleName\":\"老师\",\"roleSort\":2,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:45:50', 39);
INSERT INTO `sys_oper_log` VALUES (317, '用户管理', 1, 'edu.sias.ims.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', '研发部门', '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"email\":\"\",\"nickName\":\"laoshi\",\"params\":{},\"postIds\":[],\"roleIds\":[2],\"status\":\"0\",\"userId\":100,\"userName\":\"laoshi\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:46:24', 97);
INSERT INTO `sys_oper_log` VALUES (318, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2043,\"menuName\":\"成果采纳查询\",\"menuType\":\"F\",\"orderNum\":1,\"params\":{},\"parentId\":2042,\"path\":\"#\",\"perms\":\"adoptedOutcomes:adoptedOutcomes:query\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:47:27', 10);
INSERT INTO `sys_oper_log` VALUES (319, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2044,\"menuName\":\"成果采纳新增\",\"menuType\":\"F\",\"orderNum\":2,\"params\":{},\"parentId\":2042,\"path\":\"#\",\"perms\":\"adoptedOutcomes:adoptedOutcomes:add\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:47:32', 9);
INSERT INTO `sys_oper_log` VALUES (320, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2045,\"menuName\":\"成果采纳修改\",\"menuType\":\"F\",\"orderNum\":3,\"params\":{},\"parentId\":2042,\"path\":\"#\",\"perms\":\"adoptedOutcomes:adoptedOutcomes:edit\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:47:37', 7);
INSERT INTO `sys_oper_log` VALUES (321, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2046,\"menuName\":\"成果采纳删除\",\"menuType\":\"F\",\"orderNum\":4,\"params\":{},\"parentId\":2042,\"path\":\"#\",\"perms\":\"adoptedOutcomes:adoptedOutcomes:remove\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:47:42', 10);
INSERT INTO `sys_oper_log` VALUES (322, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:28\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2047,\"menuName\":\"成果采纳导出\",\"menuType\":\"F\",\"orderNum\":5,\"params\":{},\"parentId\":2042,\"path\":\"#\",\"perms\":\"adoptedOutcomes:adoptedOutcomes:export\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:47:50', 9);
INSERT INTO `sys_oper_log` VALUES (323, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"成果采纳导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2042,\"perms\":\"adoptedOutcomes:adoptedOutcomes:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:48:13', 7);
INSERT INTO `sys_oper_log` VALUES (324, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"teacherHonors/teacherHonors/index\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2048,\"menuName\":\"教师荣誉\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"teacherHonors\",\"perms\":\"teacherHonors:teacherHonors:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:48:20', 8);
INSERT INTO `sys_oper_log` VALUES (325, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2049,\"menuName\":\"教师荣誉查询\",\"menuType\":\"F\",\"orderNum\":1,\"params\":{},\"parentId\":2048,\"path\":\"#\",\"perms\":\"teacherHonors:teacherHonors:query\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:48:31', 7);
INSERT INTO `sys_oper_log` VALUES (326, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2050,\"menuName\":\"教师荣誉新增\",\"menuType\":\"F\",\"orderNum\":2,\"params\":{},\"parentId\":2048,\"path\":\"#\",\"perms\":\"teacherHonors:teacherHonors:add\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:48:38', 7);
INSERT INTO `sys_oper_log` VALUES (327, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2051,\"menuName\":\"教师荣誉修改\",\"menuType\":\"F\",\"orderNum\":3,\"params\":{},\"parentId\":2048,\"path\":\"#\",\"perms\":\"teacherHonors:teacherHonors:edit\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:48:43', 8);
INSERT INTO `sys_oper_log` VALUES (328, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2052,\"menuName\":\"教师荣誉删除\",\"menuType\":\"F\",\"orderNum\":4,\"params\":{},\"parentId\":2048,\"path\":\"#\",\"perms\":\"teacherHonors:teacherHonors:remove\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:48:50', 9);
INSERT INTO `sys_oper_log` VALUES (329, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:35\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2053,\"menuName\":\"教师荣誉导出\",\"menuType\":\"F\",\"orderNum\":5,\"params\":{},\"parentId\":2048,\"path\":\"#\",\"perms\":\"teacherHonors:teacherHonors:export\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:48:57', 8);
INSERT INTO `sys_oper_log` VALUES (330, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"教师荣誉导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2048,\"perms\":\"teacherHonors:teacherHonors:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:49:27', 9);
INSERT INTO `sys_oper_log` VALUES (331, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"intellectualProperty/intellectualProperty/index\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2054,\"menuName\":\"知识产权\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"intellectualProperty\",\"perms\":\"intellectualProperty:intellectualProperty:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:49:43', 12);
INSERT INTO `sys_oper_log` VALUES (332, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2055,\"menuName\":\"知识产权查询\",\"menuType\":\"F\",\"orderNum\":1,\"params\":{},\"parentId\":2054,\"path\":\"#\",\"perms\":\"intellectualProperty:intellectualProperty:query\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:49:56', 7);
INSERT INTO `sys_oper_log` VALUES (333, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2056,\"menuName\":\"知识产权新增\",\"menuType\":\"F\",\"orderNum\":2,\"params\":{},\"parentId\":2054,\"path\":\"#\",\"perms\":\"intellectualProperty:intellectualProperty:add\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:50:11', 7);
INSERT INTO `sys_oper_log` VALUES (334, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2057,\"menuName\":\"知识产权修改\",\"menuType\":\"F\",\"orderNum\":3,\"params\":{},\"parentId\":2054,\"path\":\"#\",\"perms\":\"intellectualProperty:intellectualProperty:edit\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:50:21', 7);
INSERT INTO `sys_oper_log` VALUES (335, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2058,\"menuName\":\"知识产权删除\",\"menuType\":\"F\",\"orderNum\":4,\"params\":{},\"parentId\":2054,\"path\":\"#\",\"perms\":\"intellectualProperty:intellectualProperty:remove\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:50:28', 8);
INSERT INTO `sys_oper_log` VALUES (336, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-15 20:35:42\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2059,\"menuName\":\"知识产权导出\",\"menuType\":\"F\",\"orderNum\":5,\"params\":{},\"parentId\":2054,\"path\":\"#\",\"perms\":\"intellectualProperty:intellectualProperty:export\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:50:34', 7);
INSERT INTO `sys_oper_log` VALUES (337, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"知识产权导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2054,\"perms\":\"intellectualProperty:intellectualProperty:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:50:53', 6);
INSERT INTO `sys_oper_log` VALUES (338, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"项目导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2060,\"perms\":\"project:projects:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:51:13', 5);
INSERT INTO `sys_oper_log` VALUES (339, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"checkProjects/checkProjects/index\",\"createTime\":\"2024-07-16 01:42:36\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2068,\"menuName\":\"鉴定项目\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":2067,\"path\":\"checkProjects\",\"perms\":\"checkProjects:checkProjects:list\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:51:23', 5);
INSERT INTO `sys_oper_log` VALUES (340, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 01:42:36\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2069,\"menuName\":\"鉴定项目查询\",\"menuType\":\"F\",\"orderNum\":1,\"params\":{},\"parentId\":2068,\"path\":\"#\",\"perms\":\"checkProjects:checkProjects:query\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:51:30', 6);
INSERT INTO `sys_oper_log` VALUES (341, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 01:42:36\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2070,\"menuName\":\"鉴定项目新增\",\"menuType\":\"F\",\"orderNum\":2,\"params\":{},\"parentId\":2068,\"path\":\"#\",\"perms\":\"checkProjects:checkProjects:add\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:51:35', 7);
INSERT INTO `sys_oper_log` VALUES (342, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 01:42:36\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2071,\"menuName\":\"鉴定项目修改\",\"menuType\":\"F\",\"orderNum\":3,\"params\":{},\"parentId\":2068,\"path\":\"#\",\"perms\":\"checkProjects:checkProjects:edit\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:51:40', 8);
INSERT INTO `sys_oper_log` VALUES (343, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 01:42:36\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2072,\"menuName\":\"鉴定项目删除\",\"menuType\":\"F\",\"orderNum\":4,\"params\":{},\"parentId\":2068,\"path\":\"#\",\"perms\":\"checkProjects:checkProjects:remove\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:51:45', 6);
INSERT INTO `sys_oper_log` VALUES (344, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 01:42:36\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2073,\"menuName\":\"鉴定项目导出\",\"menuType\":\"F\",\"orderNum\":5,\"params\":{},\"parentId\":2068,\"path\":\"#\",\"perms\":\"checkProjects:checkProjects:export\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:51:50', 7);
INSERT INTO `sys_oper_log` VALUES (345, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"鉴定项目导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2068,\"perms\":\"\\t checkProjects:checkProjects:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:52:07', 7);
INSERT INTO `sys_oper_log` VALUES (346, '角色管理', 2, 'edu.sias.ims.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-07-15 14:20:15\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2018,2019,2020,2021,2022,2023,4078,2067,2030,2031,2032,2033,2034,2035,2036,2037,2038,2039,2040,2041,2042,2043,2044,2045,2046,2047,4079,2048,2049,2050,2051,2052,2053,4080,2054,2055,2056,2057,2058,2059,4081,2060,2061,2062,2063,2064,2065,4082,2068,2069,2070,2071,2072,2073,4083],\"params\":{},\"remark\":\"普通角色\",\"roleId\":2,\"roleKey\":\"teacher\",\"roleName\":\"老师\",\"roleSort\":2,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:52:17', 18);
INSERT INTO `sys_oper_log` VALUES (347, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"著作教材导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2036,\"perms\":\"textbook:textbooks:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:53:20', 6);
INSERT INTO `sys_oper_log` VALUES (348, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"论文导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":2030,\"perms\":\"papers:papers:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:53:42', 6);
INSERT INTO `sys_oper_log` VALUES (349, '角色管理', 2, 'edu.sias.ims.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-07-15 14:20:15\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[2018,2019,2020,2021,2022,2023,4078,2067,2030,2031,2032,2033,2034,2035,4085,2036,2037,2038,2039,2040,2041,4084,2042,2043,2044,2045,2046,2047,4079,2048,2049,2050,2051,2052,2053,4080,2054,2055,2056,2057,2058,2059,4081,2060,2061,2062,2063,2064,2065,4082,2068,2069,2070,2071,2072,2073,4083],\"params\":{},\"remark\":\"普通角色\",\"roleId\":2,\"roleKey\":\"teacher\",\"roleName\":\"老师\",\"roleSort\":2,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:53:51', 20);
INSERT INTO `sys_oper_log` VALUES (350, '角色管理', 1, 'edu.sias.ims.web.controller.system.SysRoleController.add()', 'POST', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"deptCheckStrictly\":true,\"deptIds\":[],\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[3028,3029,3030,3031,3032,3033,3034,3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3048,3049,3050,3051,3052,3053,3054,3055,3056,3057,3058,3059,3060,3061,4061,3062,4062,3063,4063,3064,4064,3065,4065,3066,3067,4067,3068,4068,3069,4069,3070,4070,3071,4071,3072,3073,4073,3074,4074,3075,4075,3076,4076,3077,4077,4060,4066,4072],\"params\":{},\"remark\":\"学生\",\"roleId\":100,\"roleKey\":\"student\",\"roleName\":\"学生\",\"roleSort\":3,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 14:57:34', 16);
INSERT INTO `sys_oper_log` VALUES (351, '用户管理', 2, 'edu.sias.ims.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"avatar\":\"\",\"createBy\":\"\",\"createTime\":\"2024-08-12 15:03:17\",\"delFlag\":\"0\",\"email\":\"\",\"loginDate\":\"2024-08-12 00:00:00\",\"loginIp\":\"127.0.0.1\",\"nickName\":\"test\",\"params\":{},\"phonenumber\":\"\",\"postIds\":[],\"roleIds\":[100],\"roles\":[{\"admin\":false,\"dataScope\":\"2\",\"deptCheckStrictly\":false,\"flag\":false,\"menuCheckStrictly\":false,\"params\":{},\"roleId\":2,\"roleKey\":\"teacher\",\"roleName\":\"老师\",\"roleSort\":2,\"status\":\"0\"}],\"sex\":\"0\",\"status\":\"0\",\"updateBy\":\"admin\",\"userId\":101,\"userName\":\"test\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:03:46', 34);
INSERT INTO `sys_oper_log` VALUES (352, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"学生信息导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3029,\"perms\":\"StudentManger:students:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:07:00', 19);
INSERT INTO `sys_oper_log` VALUES (353, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-08-12 15:07:00\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":4086,\"menuName\":\"学生信息管理导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3029,\"path\":\"\",\"perms\":\"StudentManger:students:import\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:07:09', 8);
INSERT INTO `sys_oper_log` VALUES (354, '角色管理', 2, 'edu.sias.ims.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-08-12 14:57:34\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[3028,3029,3030,3031,3032,3033,3034,4086,3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3048,3049,3050,3051,3052,3053,3054,3055,3056,3057,3058,3059,3060,3061,4061,3062,4062,3063,4063,3064,4064,3065,4065,3066,3067,4067,3068,4068,3069,4069,3070,4070,3071,4071,3072,3073,4073,3074,4074,3075,4075,3076,4076,3077,4077,4060,4066,4072],\"params\":{},\"remark\":\"学生\",\"roleId\":100,\"roleKey\":\"student\",\"roleName\":\"学生\",\"roleSort\":3,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:08:06', 23);
INSERT INTO `sys_oper_log` VALUES (355, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"志愿者管理导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3048,\"perms\":\"volunteer:volunteer:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:09:38', 9);
INSERT INTO `sys_oper_log` VALUES (356, '角色管理', 2, 'edu.sias.ims.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-08-12 14:57:34\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[3028,3029,3030,3031,3032,3033,3034,4086,3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3048,3049,3050,3051,3052,3053,4087,3054,3055,3056,3057,3058,3059,3060,3061,4061,3062,4062,3063,4063,3064,4064,3065,4065,3066,3067,4067,3068,4068,3069,4069,3070,4070,3071,4071,3072,3073,4073,3074,4074,3075,4075,3076,4076,3077,4077,4060,4066,4072],\"params\":{},\"remark\":\"学生\",\"roleId\":100,\"roleKey\":\"student\",\"roleName\":\"学生\",\"roleSort\":3,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:09:53', 24);
INSERT INTO `sys_oper_log` VALUES (357, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"志愿者证书管理导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3054,\"perms\":\"certificate:certificates:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:10:27', 7);
INSERT INTO `sys_oper_log` VALUES (358, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"学生荣誉管理导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3060,\"perms\":\"studentHonors:honors:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:10:58', 8);
INSERT INTO `sys_oper_log` VALUES (359, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createTime\":\"2024-08-12 15:10:58\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":4089,\"menuName\":\"学生荣誉导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3060,\"path\":\"\",\"perms\":\"studentHonors:honors:import\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:11:04', 7);
INSERT INTO `sys_oper_log` VALUES (360, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"学生论文管理导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3060,\"perms\":\"\\t studentshesis:studentshesis:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:11:23', 7);
INSERT INTO `sys_oper_log` VALUES (361, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"学生科研立项导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3066,\"perms\":\"\\t studentScienceProject:projects:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:11:51', 4);
INSERT INTO `sys_oper_log` VALUES (362, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"学生实习项目经历导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3066,\"perms\":\"studentprojectexperience:studentprojectexperience:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:12:18', 7);
INSERT INTO `sys_oper_log` VALUES (363, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"知识产权导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3072,\"perms\":\"studentintellectualproperty:StudentIntellectualProperty:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:13:03', 7);
INSERT INTO `sys_oper_log` VALUES (364, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"著作教材导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3072,\"perms\":\"StudentTextBook:textbooks:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:13:28', 6);
INSERT INTO `sys_oper_log` VALUES (365, '角色管理', 2, 'edu.sias.ims.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-08-12 14:57:34\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[3028,3029,3030,3031,3032,3033,3034,4086,3035,3036,3037,3038,3039,3040,3041,3042,3043,3044,3045,3046,3048,3049,3050,3051,3052,3053,4087,3054,3055,3056,3057,3058,3059,4088,3060,3061,4061,3062,4062,3063,4063,3064,4064,3065,4065,4089,4090,3066,3067,4067,3068,4068,3069,4069,3070,4070,3071,4071,4091,4092,3072,3073,4073,3074,4074,3075,4075,3076,4076,3077,4077,4093,4094,4060,4066,4072],\"params\":{},\"remark\":\"学生\",\"roleId\":100,\"roleKey\":\"student\",\"roleName\":\"学生\",\"roleSort\":3,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:13:37', 23);
INSERT INTO `sys_oper_log` VALUES (366, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 16:54:10\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3036,\"menuName\":\"奖学金信息管理查询\",\"menuType\":\"F\",\"orderNum\":1,\"params\":{},\"parentId\":3035,\"path\":\"#\",\"perms\":\"scholarship/scholarship:scholarship:query\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:14:40', 7);
INSERT INTO `sys_oper_log` VALUES (367, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 16:54:10\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3037,\"menuName\":\"奖学金信息管理新增\",\"menuType\":\"F\",\"orderNum\":2,\"params\":{},\"parentId\":3035,\"path\":\"#\",\"perms\":\"scholarship/scholarship:scholarship:add\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:14:44', 8);
INSERT INTO `sys_oper_log` VALUES (368, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 16:54:10\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3038,\"menuName\":\"奖学金信息管理修改\",\"menuType\":\"F\",\"orderNum\":3,\"params\":{},\"parentId\":3035,\"path\":\"#\",\"perms\":\"scholarship/scholarship:scholarship:edit\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:14:49', 8);
INSERT INTO `sys_oper_log` VALUES (369, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 16:54:10\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3039,\"menuName\":\"奖学金信息管理删除\",\"menuType\":\"F\",\"orderNum\":4,\"params\":{},\"parentId\":3035,\"path\":\"#\",\"perms\":\"scholarship/scholarship:scholarship:remove\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:14:53', 7);
INSERT INTO `sys_oper_log` VALUES (370, '菜单管理', 2, 'edu.sias.ims.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"\",\"createTime\":\"2024-07-16 16:54:10\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":3040,\"menuName\":\"奖学金信息管理导出\",\"menuType\":\"F\",\"orderNum\":5,\"params\":{},\"parentId\":3035,\"path\":\"#\",\"perms\":\"scholarship/scholarship:scholarship:export\",\"routeName\":\"\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:15:00', 10);
INSERT INTO `sys_oper_log` VALUES (371, '菜单管理', 1, 'edu.sias.ims.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"createBy\":\"admin\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuName\":\"奖学金信息管理导入\",\"menuType\":\"F\",\"orderNum\":6,\"params\":{},\"parentId\":3035,\"perms\":\"scholarship/scholarship:scholarship:import\",\"status\":\"0\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:15:18', 7);
INSERT INTO `sys_oper_log` VALUES (372, '角色管理', 2, 'edu.sias.ims.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/role', '127.0.0.1', '内网IP', '{\"admin\":false,\"createTime\":\"2024-08-12 14:57:34\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"deptCheckStrictly\":true,\"flag\":false,\"menuCheckStrictly\":true,\"menuIds\":[3028,3029,3030,3031,3032,3033,3034,4086,3035,3036,3037,3038,3039,3040,4095,3041,3042,3043,3044,3045,3046,3048,3049,3050,3051,3052,3053,4087,3054,3055,3056,3057,3058,3059,4088,3060,3061,4061,3062,4062,3063,4063,3064,4064,3065,4065,4089,4090,3066,3067,4067,3068,4068,3069,4069,3070,4070,3071,4071,4091,4092,3072,3073,4073,3074,4074,3075,4075,3076,4076,3077,4077,4093,4094,4060,4066,4072],\"params\":{},\"remark\":\"学生\",\"roleId\":100,\"roleKey\":\"student\",\"roleName\":\"学生\",\"roleSort\":3,\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-08-12 15:15:23', 16);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位名称',
  `post_sort` int(0) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2024-07-15 14:20:15', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2024-07-15 14:20:15', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2024-07-15 14:20:15', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2024-07-15 14:20:15', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(0) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2024-07-15 14:20:15', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '老师', 'teacher', 2, '2', 1, 1, '0', '0', 'admin', '2024-07-15 14:20:15', 'admin', '2024-08-12 14:53:51', '普通角色');
INSERT INTO `sys_role` VALUES (100, '学生', 'student', 3, '1', 1, 1, '0', '0', 'admin', '2024-08-12 14:57:34', 'admin', '2024-08-12 15:15:23', '学生');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(0) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(0) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 2018);
INSERT INTO `sys_role_menu` VALUES (2, 2019);
INSERT INTO `sys_role_menu` VALUES (2, 2020);
INSERT INTO `sys_role_menu` VALUES (2, 2021);
INSERT INTO `sys_role_menu` VALUES (2, 2022);
INSERT INTO `sys_role_menu` VALUES (2, 2023);
INSERT INTO `sys_role_menu` VALUES (2, 2030);
INSERT INTO `sys_role_menu` VALUES (2, 2031);
INSERT INTO `sys_role_menu` VALUES (2, 2032);
INSERT INTO `sys_role_menu` VALUES (2, 2033);
INSERT INTO `sys_role_menu` VALUES (2, 2034);
INSERT INTO `sys_role_menu` VALUES (2, 2035);
INSERT INTO `sys_role_menu` VALUES (2, 2036);
INSERT INTO `sys_role_menu` VALUES (2, 2037);
INSERT INTO `sys_role_menu` VALUES (2, 2038);
INSERT INTO `sys_role_menu` VALUES (2, 2039);
INSERT INTO `sys_role_menu` VALUES (2, 2040);
INSERT INTO `sys_role_menu` VALUES (2, 2041);
INSERT INTO `sys_role_menu` VALUES (2, 2042);
INSERT INTO `sys_role_menu` VALUES (2, 2043);
INSERT INTO `sys_role_menu` VALUES (2, 2044);
INSERT INTO `sys_role_menu` VALUES (2, 2045);
INSERT INTO `sys_role_menu` VALUES (2, 2046);
INSERT INTO `sys_role_menu` VALUES (2, 2047);
INSERT INTO `sys_role_menu` VALUES (2, 2048);
INSERT INTO `sys_role_menu` VALUES (2, 2049);
INSERT INTO `sys_role_menu` VALUES (2, 2050);
INSERT INTO `sys_role_menu` VALUES (2, 2051);
INSERT INTO `sys_role_menu` VALUES (2, 2052);
INSERT INTO `sys_role_menu` VALUES (2, 2053);
INSERT INTO `sys_role_menu` VALUES (2, 2054);
INSERT INTO `sys_role_menu` VALUES (2, 2055);
INSERT INTO `sys_role_menu` VALUES (2, 2056);
INSERT INTO `sys_role_menu` VALUES (2, 2057);
INSERT INTO `sys_role_menu` VALUES (2, 2058);
INSERT INTO `sys_role_menu` VALUES (2, 2059);
INSERT INTO `sys_role_menu` VALUES (2, 2060);
INSERT INTO `sys_role_menu` VALUES (2, 2061);
INSERT INTO `sys_role_menu` VALUES (2, 2062);
INSERT INTO `sys_role_menu` VALUES (2, 2063);
INSERT INTO `sys_role_menu` VALUES (2, 2064);
INSERT INTO `sys_role_menu` VALUES (2, 2065);
INSERT INTO `sys_role_menu` VALUES (2, 2067);
INSERT INTO `sys_role_menu` VALUES (2, 2068);
INSERT INTO `sys_role_menu` VALUES (2, 2069);
INSERT INTO `sys_role_menu` VALUES (2, 2070);
INSERT INTO `sys_role_menu` VALUES (2, 2071);
INSERT INTO `sys_role_menu` VALUES (2, 2072);
INSERT INTO `sys_role_menu` VALUES (2, 2073);
INSERT INTO `sys_role_menu` VALUES (2, 4078);
INSERT INTO `sys_role_menu` VALUES (2, 4079);
INSERT INTO `sys_role_menu` VALUES (2, 4080);
INSERT INTO `sys_role_menu` VALUES (2, 4081);
INSERT INTO `sys_role_menu` VALUES (2, 4082);
INSERT INTO `sys_role_menu` VALUES (2, 4083);
INSERT INTO `sys_role_menu` VALUES (2, 4084);
INSERT INTO `sys_role_menu` VALUES (2, 4085);
INSERT INTO `sys_role_menu` VALUES (100, 3028);
INSERT INTO `sys_role_menu` VALUES (100, 3029);
INSERT INTO `sys_role_menu` VALUES (100, 3030);
INSERT INTO `sys_role_menu` VALUES (100, 3031);
INSERT INTO `sys_role_menu` VALUES (100, 3032);
INSERT INTO `sys_role_menu` VALUES (100, 3033);
INSERT INTO `sys_role_menu` VALUES (100, 3034);
INSERT INTO `sys_role_menu` VALUES (100, 3035);
INSERT INTO `sys_role_menu` VALUES (100, 3036);
INSERT INTO `sys_role_menu` VALUES (100, 3037);
INSERT INTO `sys_role_menu` VALUES (100, 3038);
INSERT INTO `sys_role_menu` VALUES (100, 3039);
INSERT INTO `sys_role_menu` VALUES (100, 3040);
INSERT INTO `sys_role_menu` VALUES (100, 3041);
INSERT INTO `sys_role_menu` VALUES (100, 3042);
INSERT INTO `sys_role_menu` VALUES (100, 3043);
INSERT INTO `sys_role_menu` VALUES (100, 3044);
INSERT INTO `sys_role_menu` VALUES (100, 3045);
INSERT INTO `sys_role_menu` VALUES (100, 3046);
INSERT INTO `sys_role_menu` VALUES (100, 3048);
INSERT INTO `sys_role_menu` VALUES (100, 3049);
INSERT INTO `sys_role_menu` VALUES (100, 3050);
INSERT INTO `sys_role_menu` VALUES (100, 3051);
INSERT INTO `sys_role_menu` VALUES (100, 3052);
INSERT INTO `sys_role_menu` VALUES (100, 3053);
INSERT INTO `sys_role_menu` VALUES (100, 3054);
INSERT INTO `sys_role_menu` VALUES (100, 3055);
INSERT INTO `sys_role_menu` VALUES (100, 3056);
INSERT INTO `sys_role_menu` VALUES (100, 3057);
INSERT INTO `sys_role_menu` VALUES (100, 3058);
INSERT INTO `sys_role_menu` VALUES (100, 3059);
INSERT INTO `sys_role_menu` VALUES (100, 3060);
INSERT INTO `sys_role_menu` VALUES (100, 3061);
INSERT INTO `sys_role_menu` VALUES (100, 3062);
INSERT INTO `sys_role_menu` VALUES (100, 3063);
INSERT INTO `sys_role_menu` VALUES (100, 3064);
INSERT INTO `sys_role_menu` VALUES (100, 3065);
INSERT INTO `sys_role_menu` VALUES (100, 3066);
INSERT INTO `sys_role_menu` VALUES (100, 3067);
INSERT INTO `sys_role_menu` VALUES (100, 3068);
INSERT INTO `sys_role_menu` VALUES (100, 3069);
INSERT INTO `sys_role_menu` VALUES (100, 3070);
INSERT INTO `sys_role_menu` VALUES (100, 3071);
INSERT INTO `sys_role_menu` VALUES (100, 3072);
INSERT INTO `sys_role_menu` VALUES (100, 3073);
INSERT INTO `sys_role_menu` VALUES (100, 3074);
INSERT INTO `sys_role_menu` VALUES (100, 3075);
INSERT INTO `sys_role_menu` VALUES (100, 3076);
INSERT INTO `sys_role_menu` VALUES (100, 3077);
INSERT INTO `sys_role_menu` VALUES (100, 4060);
INSERT INTO `sys_role_menu` VALUES (100, 4061);
INSERT INTO `sys_role_menu` VALUES (100, 4062);
INSERT INTO `sys_role_menu` VALUES (100, 4063);
INSERT INTO `sys_role_menu` VALUES (100, 4064);
INSERT INTO `sys_role_menu` VALUES (100, 4065);
INSERT INTO `sys_role_menu` VALUES (100, 4066);
INSERT INTO `sys_role_menu` VALUES (100, 4067);
INSERT INTO `sys_role_menu` VALUES (100, 4068);
INSERT INTO `sys_role_menu` VALUES (100, 4069);
INSERT INTO `sys_role_menu` VALUES (100, 4070);
INSERT INTO `sys_role_menu` VALUES (100, 4071);
INSERT INTO `sys_role_menu` VALUES (100, 4072);
INSERT INTO `sys_role_menu` VALUES (100, 4073);
INSERT INTO `sys_role_menu` VALUES (100, 4074);
INSERT INTO `sys_role_menu` VALUES (100, 4075);
INSERT INTO `sys_role_menu` VALUES (100, 4076);
INSERT INTO `sys_role_menu` VALUES (100, 4077);
INSERT INTO `sys_role_menu` VALUES (100, 4086);
INSERT INTO `sys_role_menu` VALUES (100, 4087);
INSERT INTO `sys_role_menu` VALUES (100, 4088);
INSERT INTO `sys_role_menu` VALUES (100, 4089);
INSERT INTO `sys_role_menu` VALUES (100, 4090);
INSERT INTO `sys_role_menu` VALUES (100, 4091);
INSERT INTO `sys_role_menu` VALUES (100, 4092);
INSERT INTO `sys_role_menu` VALUES (100, 4093);
INSERT INTO `sys_role_menu` VALUES (100, 4094);
INSERT INTO `sys_role_menu` VALUES (100, 4095);
INSERT INTO `sys_role_menu` VALUES (100, 4096);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(0) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2024-08-12 15:15:51', 'admin', '2024-07-15 14:20:15', '', '2024-08-12 15:15:51', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2024-07-15 14:20:15', 'admin', '2024-07-15 14:20:15', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (100, NULL, 'laoshi', 'laoshi', '00', '', '', '0', '', '$2a$10$8RG0fxXzFAJjsXmdlPxBk.Ddb2AI1EQTMhWirai6dRIaiUaY3xpx.', '0', '0', '127.0.0.1', '2024-08-12 14:54:00', 'admin', '2024-08-12 14:46:24', '', '2024-08-12 14:53:59', NULL);
INSERT INTO `sys_user` VALUES (101, NULL, 'test', 'test', '00', '', '', '0', '', '$2a$10$YvHnjeGAJi61rKqymle8JOD9sgSJO0X4MQsM5tbMzTYx0nj7ZcNVe', '0', '0', '127.0.0.1', '2024-08-12 15:15:45', '', '2024-08-12 15:03:17', 'admin', '2024-08-12 15:15:44', NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `post_id` bigint(0) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (100, 2);
INSERT INTO `sys_user_role` VALUES (101, 100);

-- ----------------------------
-- Table structure for teacher_honors
-- ----------------------------
DROP TABLE IF EXISTS `teacher_honors`;
CREATE TABLE `teacher_honors`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `achievement_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '成果名称',
  `primary_recipient_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第一获奖人工号',
  `recipients` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '获奖成员',
  `award_category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '获奖类别（国家级、省级、厅级、校级、其他）',
  `achievement_form` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '成果形式',
  `reward_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '奖励名称',
  `reward_level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '奖励等级',
  `granting_organization` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授奖单位',
  `certificate_attachment` mediumblob NULL COMMENT '证书附件*',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `department_id` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `teacher_id` int(0) NULL DEFAULT NULL COMMENT '教师工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教师荣誉' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teacher_honors
-- ----------------------------
INSERT INTO `teacher_honors` VALUES (1, '软件杯全国一等奖', '2224124', '小王,小李', '0', '纸质', '一等奖', '1', '教育局', 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F68656C6C6F5F3230323430373137303935393531413030392E747874, NULL, '2024-07-16 20:48:19', NULL, '2024-08-01 18:55:26', NULL, 103, '测试', 2224124);
INSERT INTO `teacher_honors` VALUES (2, '测试', '12412412', '小王', '2', '纸质', NULL, '1', '模拟', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '唔', 12412412);

-- ----------------------------
-- Table structure for teacher_info
-- ----------------------------
DROP TABLE IF EXISTS `teacher_info`;
CREATE TABLE `teacher_info`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '姓名',
  `employee_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '工号',
  `nationality` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '民族',
  `gender` int(0) NULL DEFAULT NULL COMMENT '0女1男2未知',
  `national_id` char(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '身份证号',
  `phone_number` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '手机号',
  `department` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '所属系别',
  `position` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '岗位',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '职称',
  `title_acquisition_date` date NULL DEFAULT NULL COMMENT '职称获取时间',
  `first_degree` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '第一学历',
  `first_degree_object` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '第一学历专业',
  `first_degree_date` date NULL DEFAULT NULL COMMENT '第一学历获取时间',
  `highest_degree` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '最高学历',
  `highest_degree_object` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '最高学历专业',
  `highest_degree_date` date NULL DEFAULT NULL COMMENT '最高学历获取时间',
  `employment_type` int(0) NULL DEFAULT NULL COMMENT '0专职1兼职',
  `political_affiliation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '政治面貌',
  `hire_date` date NULL DEFAULT NULL COMMENT '入职时间',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `department_id` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `employee_number`(`employee_number`) USING BTREE,
  UNIQUE INDEX `national_id`(`national_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '教师信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teacher_info
-- ----------------------------
INSERT INTO `teacher_info` VALUES (2, '小李', '2224124', '2', 0, '401124200550242251', '24214124412', '计算机', '教育', '教授', '2024-07-28', '研究生', '软件工程', NULL, '', '软件工程', NULL, 1, '0', '2024-07-27', NULL, '2024-07-15 16:50:21', NULL, '2024-08-01 18:52:45', NULL, NULL, '测试数据');
INSERT INTO `teacher_info` VALUES (3, '小王', '12412412', '1', 0, '441242200241251235', '43252352622', '商学院', '教师', '教授', '2024-07-27', '硕士', '心理学', NULL, '硕士', '心理学', NULL, 0, '0', '2023-08-23', NULL, '2024-07-17 11:23:45', NULL, '2024-08-01 18:53:45', NULL, 103, '测试');

-- ----------------------------
-- Table structure for works_textbooks
-- ----------------------------
DROP TABLE IF EXISTS `works_textbooks`;
CREATE TABLE `works_textbooks`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型（指著作或教材）',
  `author_employee_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者工号',
  `author_role` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者名次（分主编、副主编、参编三类，或第一、第二等）',
  `publisher` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '出版社',
  `publication_date` date NULL DEFAULT NULL COMMENT '出版日期',
  `isbn` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ISBN号',
  `total_word_count` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '全书总字数',
  `author_word_count` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '本作者撰写字数',
  `attachment` mediumblob NULL COMMENT '附件*（封面、CIP页、前言、目录）',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `delete_flag` int(0) NULL DEFAULT NULL COMMENT '删除标记0未删除1已删除',
  `department_id` int(0) NULL DEFAULT NULL COMMENT '部门id',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `teacher_id` int(0) NULL DEFAULT NULL COMMENT '教师工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '著作教材' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of works_textbooks
-- ----------------------------
INSERT INTO `works_textbooks` VALUES (2, '相对论', '科学', '12412412', '0', '新华出版社的', NULL, '2141241', 212332, 123123, 0x2F70726F66696C652F75706C6F61642F323032342F30372F31372F68656C6C6F5F3230323430373137303935303239413030342E747874, NULL, '2024-07-17 09:50:34', NULL, '2024-08-01 18:56:30', NULL, 103, '测试', 12412412);

SET FOREIGN_KEY_CHECKS = 1;
