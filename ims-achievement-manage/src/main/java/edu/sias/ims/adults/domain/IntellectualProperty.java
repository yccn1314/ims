package edu.sias.ims.adults.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 知识产权对象 intellectual_property
 * 
 * @author sb
 * @date 2024-07-15
 */
public class IntellectualProperty extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 成果名称 */
    @Excel(name = "*成果名称")
    private String achievementName;

    /** 类别 */
    @Excel(name = "*类别 0~3代表软件著作,发明专利,实用新型专利,其它")
    private String category;

    /** 负责人工号 */
    @Excel(name = "*教师工号")
    private String principalInvestigatorId;

    /** 成果编号 */
    @Excel(name = "成果编号")
    private String achievementNumber;

    /** 完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "完成时间 yyyy-MM-dd", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completionDate;

    /** 成员 */
    @Excel(name = "成员")
    private String members;

    /** 是否转化(0否1是) */
//    @Excel(name = "是否转化(0否1是)")
    private String isTransferred;

    /** 转化金额（元） */
    @Excel(name="*转化金额 0代表未转化")
    private BigDecimal transferAmount;

    /** 附件* */
    private String attachment;

    /** 创建人 */
//    @Excel(name = "创建人")
    private String createName;

    /** 修改人 */
//    @Excel(name = "修改人")
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long departmentId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;
    @Excel(name = "*教师Id 与教师工号一致")
    private String teacherId;
    @Excel(name = "*教师姓名")
    @TableField(exist = false)
    private String name;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAchievementName(String achievementName) 
    {
        this.achievementName = achievementName;
    }

    public String getAchievementName() 
    {
        return achievementName;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setPrincipalInvestigatorId(String principalInvestigatorId) 
    {
        this.principalInvestigatorId = principalInvestigatorId;
    }

    public String getPrincipalInvestigatorId() 
    {
        return principalInvestigatorId;
    }
    public void setAchievementNumber(String achievementNumber) 
    {
        this.achievementNumber = achievementNumber;
    }

    public String getAchievementNumber() 
    {
        return achievementNumber;
    }
    public void setCompletionDate(Date completionDate) 
    {
        this.completionDate = completionDate;
    }

    public Date getCompletionDate() 
    {
        return completionDate;
    }
    public void setMembers(String members) 
    {
        this.members = members;
    }

    public String getMembers() 
    {
        return members;
    }
    public void setIsTransferred(String isTransferred) 
    {
        this.isTransferred = isTransferred;
    }

    public String getIsTransferred() 
    {
        return isTransferred;
    }
    public void setTransferAmount(BigDecimal transferAmount) 
    {
        this.transferAmount = transferAmount;
    }

    public BigDecimal getTransferAmount() 
    {
        return transferAmount;
    }
    public void setAttachment(String attachment) 
    {
        this.attachment = attachment;
    }

    public String getAttachment() 
    {
        return attachment;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setDepartmentId(Long departmentId) 
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() 
    {
        return departmentId;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("achievementName", getAchievementName())
            .append("category", getCategory())
            .append("principalInvestigatorId", getPrincipalInvestigatorId())
            .append("achievementNumber", getAchievementNumber())
            .append("completionDate", getCompletionDate())
            .append("members", getMembers())
            .append("isTransferred", getIsTransferred())
            .append("transferAmount", getTransferAmount())
            .append("attachment", getAttachment())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("departmentId", getDepartmentId())
            .append("remarks", getRemarks())
            .toString();
    }
}
