package edu.sias.ims.adults.controller;

import edu.sias.ims.adults.domain.AdoptedOutcomes;
import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.adults.service.IAdoptedOutcomesService;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 成果采纳Controller
 * 
 * @author sb
 * @date 2024-07-15
 */
@RestController
@RequestMapping("/adoptedOutcomes/adoptedOutcomes")
public class AdoptedOutcomesController extends BaseController
{
    @Autowired
    private IAdoptedOutcomesService adoptedOutcomesService;

    /**
     * 查询成果采纳列表
     */
    @PreAuthorize("@ss.hasPermi('adoptedOutcomes:adoptedOutcomes:list')")
    @GetMapping("/list")
    public TableDataInfo list(AdoptedOutcomes adoptedOutcomes)
    {
        startPage();
        List<AdoptedOutcomes> list = adoptedOutcomesService.selectAdoptedOutcomesList(adoptedOutcomes);
        return getDataTable(list);
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "成果采纳导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('adoptedOutcomes:adoptedOutcomes:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<AdoptedOutcomes> util = new ExcelUtil<AdoptedOutcomes>(AdoptedOutcomes.class);
        List<AdoptedOutcomes> userList = util.importExcel(file.getInputStream());
        String message = adoptedOutcomesService.importAdoptedOutcomes(userList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<AdoptedOutcomes> util = new ExcelUtil<AdoptedOutcomes>(AdoptedOutcomes.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 导出成果采纳列表
     */
    @PreAuthorize("@ss.hasPermi('adoptedOutcomes:adoptedOutcomes:export')")
    @Log(title = "成果采纳", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AdoptedOutcomes adoptedOutcomes)
    {
        List<AdoptedOutcomes> list = adoptedOutcomesService.selectAdoptedOutcomesList(adoptedOutcomes);
        ExcelUtil<AdoptedOutcomes> util = new ExcelUtil<AdoptedOutcomes>(AdoptedOutcomes.class);
        util.exportExcel(response, list, "成果采纳数据");
    }

    /**
     * 获取成果采纳详细信息
     */
    @PreAuthorize("@ss.hasPermi('adoptedOutcomes:adoptedOutcomes:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(adoptedOutcomesService.selectAdoptedOutcomesById(id));
    }

    /**
     * 新增成果采纳
     */
    @PreAuthorize("@ss.hasPermi('adoptedOutcomes:adoptedOutcomes:add')")
    @Log(title = "成果采纳", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AdoptedOutcomes adoptedOutcomes)
    {
        adoptedOutcomes.setPrincipalInvestigatorId(adoptedOutcomes.getTeacherId());
        adoptedOutcomes.setDepartmentId(getDeptId());
        return toAjax(adoptedOutcomesService.insertAdoptedOutcomes(adoptedOutcomes));
    }

    /**
     * 修改成果采纳
     */
    @PreAuthorize("@ss.hasPermi('adoptedOutcomes:adoptedOutcomes:edit')")
    @Log(title = "成果采纳", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AdoptedOutcomes adoptedOutcomes)
    {
        adoptedOutcomes.setPrincipalInvestigatorId(adoptedOutcomes.getTeacherId());
        return toAjax(adoptedOutcomesService.updateAdoptedOutcomes(adoptedOutcomes));
    }

    /**
     * 删除成果采纳
     */
    @PreAuthorize("@ss.hasPermi('adoptedOutcomes:adoptedOutcomes:remove')")
    @Log(title = "成果采纳", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(adoptedOutcomesService.deleteAdoptedOutcomesByIds(ids));
    }
}
