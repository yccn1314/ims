package edu.sias.ims.adults.service;

import edu.sias.ims.adults.domain.Papers;

import java.util.List;

/**
 * 论文Service接口
 * 
 * @author summer
 * @date 2024-07-15
 */
public interface IPapersService 
{
    /**
     * 查询论文
     * 
     * @param id 论文主键
     * @return 论文
     */
    public Papers selectPapersById(Long id);

    /**
     * 查询论文列表
     * 
     * @param papers 论文
     * @return 论文集合
     */
    public List<Papers> selectPapersList(Papers papers);

    /**
     * 新增论文
     * 
     * @param papers 论文
     * @return 结果
     */
    public int insertPapers(Papers papers);

    /**
     * 修改论文
     * 
     * @param papers 论文
     * @return 结果
     */
    public int updatePapers(Papers papers);

    /**
     * 批量删除论文
     * 
     * @param ids 需要删除的论文主键集合
     * @return 结果
     */
    public int deletePapersByIds(Long[] ids);

    /**
     * 删除论文信息
     * 
     * @param id 论文主键
     * @return 结果
     */
    public int deletePapersById(Long id);

    /**
     *
     * 导入论文
     * @param
     * @param
     * @param
     * @return
     */
    String importPapers(List<Papers> paperList, Boolean isUpdateSupport);




}
