package edu.sias.ims.adults.service.impl;

import edu.sias.ims.adults.domain.IntellectualProperty;
import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.adults.domain.TeacherHonors;
import edu.sias.ims.adults.mapper.TeacherHonorsMapper;
import edu.sias.ims.adults.service.ITeacherHonorsService;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师荣誉Service业务层处理
 * 
 * @author sb
 * @date 2024-07-15
 */
@Service
public class TeacherHonorsServiceImpl implements ITeacherHonorsService 
{
    @Autowired
    private TeacherHonorsMapper teacherHonorsMapper;

    private static final Logger log = LoggerFactory.getLogger(TeacherHonorsServiceImpl.class);

    /**
     * 查询教师荣誉
     * 
     * @param id 教师荣誉主键
     * @return 教师荣誉
     */
    @Override
    public TeacherHonors selectTeacherHonorsById(Long id)
    {
        return teacherHonorsMapper.selectTeacherHonorsById(id);
    }

    /**
     * 查询教师荣誉列表
     * 
     * @param teacherHonors 教师荣誉
     * @return 教师荣誉
     */
    @Override
    public List<TeacherHonors> selectTeacherHonorsList(TeacherHonors teacherHonors)
    {
        return teacherHonorsMapper.selectTeacherHonorsList(teacherHonors);
    }

    /**
     * 新增教师荣誉
     * 
     * @param teacherHonors 教师荣誉
     * @return 结果
     */
    @Override
    public int insertTeacherHonors(TeacherHonors teacherHonors)
    {
        teacherHonors.setCreateTime(DateUtils.getNowDate());
        return teacherHonorsMapper.insertTeacherHonors(teacherHonors);
    }

    /**
     * 修改教师荣誉
     * 
     * @param teacherHonors 教师荣誉
     * @return 结果
     */
    @Override
    public int updateTeacherHonors(TeacherHonors teacherHonors)
    {
        teacherHonors.setUpdateTime(DateUtils.getNowDate());
        return teacherHonorsMapper.updateTeacherHonors(teacherHonors);
    }

    /**
     * 批量删除教师荣誉
     * 
     * @param ids 需要删除的教师荣誉主键
     * @return 结果
     */
    @Override
    public int deleteTeacherHonorsByIds(Long[] ids)
    {
        return teacherHonorsMapper.deleteTeacherHonorsByIds(ids);
    }

    /**
     * 删除教师荣誉信息
     * 
     * @param id 教师荣誉主键
     * @return 结果
     */
    @Override
    public int deleteTeacherHonorsById(Long id)
    {
        return teacherHonorsMapper.deleteTeacherHonorsById(id);
    }

    @Override
    public String importTeacherHonors(List<TeacherHonors> userList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (TeacherHonors teacherHonors : userList) {

            try{
                TeacherHonors u = teacherHonorsMapper.selectTeacherHonorsById(teacherHonors.getId());
                if(StringUtils.isNull(u)){
                    try {
                        teacherHonorsMapper.insertTeacherHonors(teacherHonors);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、教师荣誉 " + teacherHonors.getAchievementName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、教师荣誉 " + teacherHonors.getAchievementName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        teacherHonorsMapper.updateTeacherHonors(teacherHonors);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、教师荣誉 " + teacherHonors.getAchievementName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、教师荣誉 " + teacherHonors.getAchievementName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、教师荣誉 " + teacherHonors.getAchievementName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、教师荣誉 " + teacherHonors.getAchievementName()+ " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
