package edu.sias.ims.adults.service;

import edu.sias.ims.adults.domain.CheckProjects;

import java.util.List;

/**
 * 鉴定项目Service接口
 * 
 * @author sb
 * @date 2024-07-16
 */
public interface ICheckProjectsService 
{
    /**
     * 查询鉴定项目
     * 
     * @param id 鉴定项目主键
     * @return 鉴定项目
     */
    public CheckProjects selectCheckProjectsById(Long id);

    /**
     * 查询鉴定项目列表
     * 
     * @param checkProjects 鉴定项目
     * @return 鉴定项目集合
     */
    public List<CheckProjects> selectCheckProjectsList(CheckProjects checkProjects);

    /**
     * 新增鉴定项目
     * 
     * @param checkProjects 鉴定项目
     * @return 结果
     */
    public int insertCheckProjects(CheckProjects checkProjects);

    /**
     * 修改鉴定项目
     * 
     * @param checkProjects 鉴定项目
     * @return 结果
     */
    public int updateCheckProjects(CheckProjects checkProjects);

    /**
     * 批量删除鉴定项目
     * 
     * @param ids 需要删除的鉴定项目主键集合
     * @return 结果
     */
    public int deleteCheckProjectsByIds(Long[] ids);

    /**
     * 删除鉴定项目信息
     * 
     * @param id 鉴定项目主键
     * @return 结果
     */
    public int deleteCheckProjectsById(Long id);

    String importCheckProjectsService(List<CheckProjects> userList, boolean updateSupport);
}
