package edu.sias.ims.adults.controller;

import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.adults.domain.TeacherHonors;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.adults.domain.WorksTextbooks;
import edu.sias.ims.adults.service.IWorksTextbooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 著作教材Controller
 * 
 * @author summer
 * @date 2024-07-15
 */
@RestController
@RequestMapping("/textbook/textbooks")
public class WorksTextbooksController extends BaseController
{
    @Autowired
    private IWorksTextbooksService worksTextbooksService;

    /**
     * 查询著作教材列表
     */
    @PreAuthorize("@ss.hasPermi('textbook:textbooks:list')")
    @GetMapping("/list")
    public TableDataInfo list(WorksTextbooks worksTextbooks)
    {
        startPage();
        List<WorksTextbooks> list = worksTextbooksService.selectWorksTextbooksList(worksTextbooks);
        return getDataTable(list);
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "著作教材导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('textbook:textbooks:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<WorksTextbooks> util = new ExcelUtil<WorksTextbooks>(WorksTextbooks.class);
        List<WorksTextbooks> userList = util.importExcel(file.getInputStream());
        String message = worksTextbooksService.importTeacherHonors(userList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<WorksTextbooks> util = new ExcelUtil<WorksTextbooks>(WorksTextbooks.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 导出著作教材列表
     */
    @PreAuthorize("@ss.hasPermi('textbook:textbooks:export')")
    @Log(title = "著作教材", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WorksTextbooks worksTextbooks)
    {
        List<WorksTextbooks> list = worksTextbooksService.selectWorksTextbooksList(worksTextbooks);
        ExcelUtil<WorksTextbooks> util = new ExcelUtil<WorksTextbooks>(WorksTextbooks.class);
        util.exportExcel(response, list, "著作教材数据");
    }

    /**
     * 获取著作教材详细信息
     */
    @PreAuthorize("@ss.hasPermi('textbook:textbooks:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(worksTextbooksService.selectWorksTextbooksById(id));
    }

    /**
     * 新增著作教材
     */
    @PreAuthorize("@ss.hasPermi('textbook:textbooks:add')")
    @Log(title = "著作教材", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WorksTextbooks worksTextbooks)
    {
        worksTextbooks.setAuthorEmployeeId(worksTextbooks.getTeacherId());
        worksTextbooks.setDepartmentId(getDeptId());
        return toAjax(worksTextbooksService.insertWorksTextbooks(worksTextbooks));
    }

    /**
     * 修改著作教材
     */
    @PreAuthorize("@ss.hasPermi('textbook:textbooks:edit')")
    @Log(title = "著作教材", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WorksTextbooks worksTextbooks)
    {
        worksTextbooks.setAuthorEmployeeId(worksTextbooks.getTeacherId());
        return toAjax(worksTextbooksService.updateWorksTextbooks(worksTextbooks));
    }

    /**
     * 删除著作教材
     */
    @PreAuthorize("@ss.hasPermi('textbook:textbooks:remove')")
    @Log(title = "著作教材", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(worksTextbooksService.deleteWorksTextbooksByIds(ids));
    }
}
