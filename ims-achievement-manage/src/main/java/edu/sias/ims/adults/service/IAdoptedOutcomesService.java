package edu.sias.ims.adults.service;

import edu.sias.ims.adults.domain.AdoptedOutcomes;
import edu.sias.ims.adults.domain.Papers;

import java.util.List;

/**
 * 成果采纳Service接口
 * 
 * @author sb
 * @date 2024-07-15
 */
public interface IAdoptedOutcomesService 
{
    /**
     * 查询成果采纳
     * 
     * @param id 成果采纳主键
     * @return 成果采纳
     */
    public AdoptedOutcomes selectAdoptedOutcomesById(Long id);

    /**
     * 查询成果采纳列表
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 成果采纳集合
     */
    public List<AdoptedOutcomes> selectAdoptedOutcomesList(AdoptedOutcomes adoptedOutcomes);

    /**
     * 新增成果采纳
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 结果
     */
    public int insertAdoptedOutcomes(AdoptedOutcomes adoptedOutcomes);

    /**
     * 修改成果采纳
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 结果
     */
    public int updateAdoptedOutcomes(AdoptedOutcomes adoptedOutcomes);

    /**
     * 批量删除成果采纳
     * 
     * @param ids 需要删除的成果采纳主键集合
     * @return 结果
     */
    public int deleteAdoptedOutcomesByIds(Long[] ids);

    /**
     * 删除成果采纳信息
     * 
     * @param id 成果采纳主键
     * @return 结果
     */
    public int deleteAdoptedOutcomesById(Long id);

    String importAdoptedOutcomes(List<AdoptedOutcomes> userList, boolean updateSupport);
}
