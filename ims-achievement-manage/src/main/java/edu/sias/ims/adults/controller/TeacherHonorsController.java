package edu.sias.ims.adults.controller;

import edu.sias.ims.adults.domain.AdoptedOutcomes;
import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.adults.domain.TeacherHonors;
import edu.sias.ims.adults.service.ITeacherHonorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师荣誉Controller
 * 
 * @author sb
 * @date 2024-07-15
 */
@RestController
@RequestMapping("/teacherHonors/teacherHonors")
public class TeacherHonorsController extends BaseController
{
    @Autowired
    private ITeacherHonorsService teacherHonorsService;

    /**
     * 查询教师荣誉列表
     */
    @PreAuthorize("@ss.hasPermi('teacherHonors:teacherHonors:list')")
    @GetMapping("/list")
    public TableDataInfo list(TeacherHonors teacherHonors)
    {
        startPage();
        List<TeacherHonors> list = teacherHonorsService.selectTeacherHonorsList(teacherHonors);
        return getDataTable(list);
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "教师荣誉导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('teacherHonors:teacherHonors:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<TeacherHonors> util = new ExcelUtil<TeacherHonors>(TeacherHonors.class);
        List<TeacherHonors> userList = util.importExcel(file.getInputStream());
        String message = teacherHonorsService.importTeacherHonors(userList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<TeacherHonors> util = new ExcelUtil<TeacherHonors>(TeacherHonors.class);
        util.importTemplateExcel(response, "用户数据");
    }


    /**
     * 导出教师荣誉列表
     */
    @PreAuthorize("@ss.hasPermi('teacherHonors:teacherHonors:export')")
    @Log(title = "教师荣誉", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TeacherHonors teacherHonors)
    {
        List<TeacherHonors> list = teacherHonorsService.selectTeacherHonorsList(teacherHonors);
        ExcelUtil<TeacherHonors> util = new ExcelUtil<TeacherHonors>(TeacherHonors.class);
        util.exportExcel(response, list, "教师荣誉数据");
    }

    /**
     * 获取教师荣誉详细信息
     */
    @PreAuthorize("@ss.hasPermi('teacherHonors:teacherHonors:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(teacherHonorsService.selectTeacherHonorsById(id));
    }

    /**
     * 新增教师荣誉
     */
    @PreAuthorize("@ss.hasPermi('teacherHonors:teacherHonors:add')")
    @Log(title = "教师荣誉", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TeacherHonors teacherHonors)
    {
        teacherHonors.setPrimaryRecipientId(teacherHonors.getTeacherId());
        teacherHonors.setDepartmentId(getDeptId());
        System.err.println(teacherHonors);
        return toAjax(teacherHonorsService.insertTeacherHonors(teacherHonors));
    }

    /**
     * 修改教师荣誉
     */
    @PreAuthorize("@ss.hasPermi('teacherHonors:teacherHonors:edit')")
    @Log(title = "教师荣誉", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TeacherHonors teacherHonors)
    {
        teacherHonors.setPrimaryRecipientId(teacherHonors.getTeacherId());
        return toAjax(teacherHonorsService.updateTeacherHonors(teacherHonors));
    }

    /**
     * 删除教师荣誉
     */
    @PreAuthorize("@ss.hasPermi('teacherHonors:teacherHonors:remove')")
    @Log(title = "教师荣誉", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(teacherHonorsService.deleteTeacherHonorsByIds(ids));
    }
}
