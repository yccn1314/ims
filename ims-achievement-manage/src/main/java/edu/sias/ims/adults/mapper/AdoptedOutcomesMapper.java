package edu.sias.ims.adults.mapper;

import edu.sias.ims.adults.domain.AdoptedOutcomes;

import java.util.List;

/**
 * 成果采纳Mapper接口
 * 
 * @author sb
 * @date 2024-07-15
 */
public interface AdoptedOutcomesMapper 
{
    /**
     * 查询成果采纳
     * 
     * @param id 成果采纳主键
     * @return 成果采纳
     */
    public AdoptedOutcomes selectAdoptedOutcomesById(Long id);

    /**
     * 查询成果采纳列表
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 成果采纳集合
     */
    public List<AdoptedOutcomes> selectAdoptedOutcomesList(AdoptedOutcomes adoptedOutcomes);

    /**
     * 新增成果采纳
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 结果
     */
    public int insertAdoptedOutcomes(AdoptedOutcomes adoptedOutcomes);

    /**
     * 修改成果采纳
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 结果
     */
    public int updateAdoptedOutcomes(AdoptedOutcomes adoptedOutcomes);

    /**
     * 删除成果采纳
     * 
     * @param id 成果采纳主键
     * @return 结果
     */
    public int deleteAdoptedOutcomesById(Long id);

    /**
     * 批量删除成果采纳
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAdoptedOutcomesByIds(Long[] ids);
}
