package edu.sias.ims.adults.mapper;

import edu.sias.ims.adults.domain.Papers;

import java.util.List;

/**
 * 论文Mapper接口
 * 
 * @author summer
 * @date 2024-07-15
 */
public interface PapersMapper 
{
    /**
     * 查询论文
     * 
     * @param id 论文主键
     * @return 论文
     */
    public Papers selectPapersById(Long id);

    /**
     * 查询论文列表
     * 
     * @param papers 论文
     * @return 论文集合
     */
    public List<Papers> selectPapersList(Papers papers);

    /**
     * 新增论文
     * 
     * @param papers 论文
     * @return 结果
     */
    public int insertPapers(Papers papers);

    /**
     * 修改论文
     * 
     * @param papers 论文
     * @return 结果
     */
    public int updatePapers(Papers papers);

    /**
     * 删除论文
     * 
     * @param id 论文主键
     * @return 结果
     */
    public int deletePapersById(Long id);

    /**
     * 批量删除论文
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePapersByIds(Long[] ids);
}
