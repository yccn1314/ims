package edu.sias.ims.adults.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 论文对象 papers
 * 
 * @author summer
 * @date 2024-07-15
 */
public class Papers extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 论文名称 */
    @Excel(name = "*论文名称")
    private String paperTitle;

    /** 支持基金或项目 */
    private String fundingOrProject;

    /** 作者工号 */
    @Excel(name = "*作者工号")
    private String authorEmployeeId;

    /** 作者名次 */
    @Excel(name = "作者名次")
    private String authorRank;

    /** 刊物名称 */
    @Excel(name = "*刊物名称")
    private String journalName;

    /** 论文级别(0-4对应A到E) */
    @Excel(name = "*论文级别(0-2对应普通刊物,北大核心刊物,南大核心刊物)")
    private String paperLevel;

    /** 统一刊号（CN） */
    @Excel(name = "统一刊号 以CN开头", readConverterExp = "C=N")
    private String cnNumber;

    /** 论文字数 */
    @Excel(name="论文字数")
    private String wordCount;

    /** 出版时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发表时间 yyyy-MM-dd", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publicationDate;

    /** 年 */
//    @Excel(name = "年")
//    private String year;

    /** 期 */
    @Excel(name = "期")
    private String issue;

    /** 论文检索网站 */
    @Excel(name="*论文检索网站")
    private String searchWebsite;

    /** 论文附件*（刊物封面、CN页、目录、正文、检索页） */
//    @Excel(name = "论文附件*", readConverterExp = "刊=物封面、CN页、目录、正文、检索页")
    private String attachment;

    /** 创建人 */
    private String createName;

    /** 修改人 */
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long departmentId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;
    @Excel(name = "*教师Id")
    private String teacherId;
    @Excel(name = "*教师姓名")
    @TableField(exist = false)
    private String name;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPaperTitle(String paperTitle) 
    {
        this.paperTitle = paperTitle;
    }

    public String getPaperTitle() 
    {
        return paperTitle;
    }
    public void setFundingOrProject(String fundingOrProject) 
    {
        this.fundingOrProject = fundingOrProject;
    }

    public String getFundingOrProject() 
    {
        return fundingOrProject;
    }
    public void setAuthorEmployeeId(String authorEmployeeId) 
    {
        this.authorEmployeeId = authorEmployeeId;
    }

    public String getAuthorEmployeeId() 
    {
        return authorEmployeeId;
    }
    public void setAuthorRank(String authorRank) 
    {
        this.authorRank = authorRank;
    }

    public String getAuthorRank() 
    {
        return authorRank;
    }
    public void setJournalName(String journalName) 
    {
        this.journalName = journalName;
    }

    public String getJournalName() 
    {
        return journalName;
    }
    public void setPaperLevel(String paperLevel) 
    {
        this.paperLevel = paperLevel;
    }

    public String getPaperLevel() 
    {
        return paperLevel;
    }
    public void setCnNumber(String cnNumber) 
    {
        this.cnNumber = cnNumber;
    }

    public String getCnNumber() 
    {
        return cnNumber;
    }
    public void setWordCount(String wordCount) 
    {
        this.wordCount = wordCount;
    }

    public String getWordCount() 
    {
        return wordCount;
    }
    public void setPublicationDate(Date publicationDate) 
    {
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() 
    {
        return publicationDate;
    }
//    public void setYear(String year)
//    {
//        this.year = year;
//    }
//
//    public String getYear()
//    {
//        return year;
//    }
    public void setIssue(String issue) 
    {
        this.issue = issue;
    }

    public String getIssue() 
    {
        return issue;
    }
    public void setSearchWebsite(String searchWebsite) 
    {
        this.searchWebsite = searchWebsite;
    }

    public String getSearchWebsite() 
    {
        return searchWebsite;
    }
    public void setAttachment(String attachment) 
    {
        this.attachment = attachment;
    }

    public String getAttachment() 
    {
        return attachment;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setDepartmentId(Long departmentId) 
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() 
    {
        return departmentId;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("paperTitle", getPaperTitle())
            .append("fundingOrProject", getFundingOrProject())
            .append("authorEmployeeId", getAuthorEmployeeId())
            .append("authorRank", getAuthorRank())
            .append("journalName", getJournalName())
            .append("paperLevel", getPaperLevel())
            .append("cnNumber", getCnNumber())
            .append("wordCount", getWordCount())
            .append("publicationDate", getPublicationDate())
//            .append("year", getYear())
            .append("issue", getIssue())
            .append("searchWebsite", getSearchWebsite())
            .append("attachment", getAttachment())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("departmentId", getDepartmentId())
            .append("remarks", getRemarks())
            .toString();
    }
}
