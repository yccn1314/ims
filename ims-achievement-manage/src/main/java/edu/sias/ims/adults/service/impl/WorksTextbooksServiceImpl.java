package edu.sias.ims.adults.service.impl;

import edu.sias.ims.adults.domain.TeacherHonors;
import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.adults.domain.WorksTextbooks;
import edu.sias.ims.adults.mapper.WorksTextbooksMapper;
import edu.sias.ims.adults.service.IWorksTextbooksService;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 著作教材Service业务层处理
 * 
 * @author summer
 * @date 2024-07-15
 */
@Service
public class WorksTextbooksServiceImpl implements IWorksTextbooksService 
{
    @Autowired
    private WorksTextbooksMapper worksTextbooksMapper;
    private static final Logger log = LoggerFactory.getLogger(WorksTextbooksServiceImpl.class);


    /**
     * 查询著作教材
     * 
     * @param id 著作教材主键
     * @return 著作教材
     */
    @Override
    public WorksTextbooks selectWorksTextbooksById(Long id)
    {
        return worksTextbooksMapper.selectWorksTextbooksById(id);
    }

    /**
     * 查询著作教材列表
     * 
     * @param worksTextbooks 著作教材
     * @return 著作教材
     */
    @Override
    public List<WorksTextbooks> selectWorksTextbooksList(WorksTextbooks worksTextbooks)
    {
        return worksTextbooksMapper.selectWorksTextbooksList(worksTextbooks);
    }

    /**
     * 新增著作教材
     * 
     * @param worksTextbooks 著作教材
     * @return 结果
     */
    @Override
    public int insertWorksTextbooks(WorksTextbooks worksTextbooks)
    {
        worksTextbooks.setCreateTime(DateUtils.getNowDate());
        return worksTextbooksMapper.insertWorksTextbooks(worksTextbooks);
    }

    /**
     * 修改著作教材
     * 
     * @param worksTextbooks 著作教材
     * @return 结果
     */
    @Override
    public int updateWorksTextbooks(WorksTextbooks worksTextbooks)
    {
        worksTextbooks.setUpdateTime(DateUtils.getNowDate());
        return worksTextbooksMapper.updateWorksTextbooks(worksTextbooks);
    }

    /**
     * 批量删除著作教材
     * 
     * @param ids 需要删除的著作教材主键
     * @return 结果
     */
    @Override
    public int deleteWorksTextbooksByIds(Long[] ids)
    {
        return worksTextbooksMapper.deleteWorksTextbooksByIds(ids);
    }

    /**
     * 删除著作教材信息
     * 
     * @param id 著作教材主键
     * @return 结果
     */
    @Override
    public int deleteWorksTextbooksById(Long id)
    {
        return worksTextbooksMapper.deleteWorksTextbooksById(id);
    }

    @Override
    public String importTeacherHonors(List<WorksTextbooks> userList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (WorksTextbooks worksTextbooks : userList) {

            try{
                WorksTextbooks u = worksTextbooksMapper.selectWorksTextbooksById(worksTextbooks.getId());
                if(StringUtils.isNull(u)){
                    try {
                        worksTextbooksMapper.insertWorksTextbooks(worksTextbooks);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、教师荣誉 " + worksTextbooks.getTitle() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、教师荣誉 " +  worksTextbooks.getTitle() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        worksTextbooksMapper.updateWorksTextbooks(worksTextbooks);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、教师荣誉 " +  worksTextbooks.getTitle() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、教师荣誉 " +  worksTextbooks.getTitle() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、教师荣誉 " +  worksTextbooks.getTitle() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、教师荣誉 " +  worksTextbooks.getTitle() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
