package edu.sias.ims.adults.controller;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.domain.entity.SysUser;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.adults.service.IPapersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 论文Controller
 * 
 * @author summer
 * @date 2024-07-15
 */
@RestController
@RequestMapping("/papers/papers")
public class PapersController extends BaseController
{
    @Autowired
    private IPapersService papersService;

    /**
     * 查询论文列表
     */
    @PreAuthorize("@ss.hasPermi('papers:papers:list')")
    @GetMapping("/list")
    public TableDataInfo list(Papers papers)
    {
        startPage();
        List<Papers> list = papersService.selectPapersList(papers);
        System.err.println(list);
        return getDataTable(list);
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "论文导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('papers:papers:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Papers> util = new ExcelUtil<Papers>(Papers.class);
        List<Papers> userList = util.importExcel(file.getInputStream());
        String message = papersService.importPapers(userList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<Papers> util = new ExcelUtil<Papers>(Papers.class);
        util.importTemplateExcel(response, "用户数据");
    }


    /**
     * 导出论文列表
     */
    @PreAuthorize("@ss.hasPermi('papers:papers:export')")
    @Log(title = "论文", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Papers papers)
    {
        List<Papers> list = papersService.selectPapersList(papers);
        ExcelUtil<Papers> util = new ExcelUtil<Papers>(Papers.class);
        util.exportExcel(response, list, "论文数据");
    }


    /**
     * 获取论文详细信息
     */
    @PreAuthorize("@ss.hasPermi('papers:papers:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(papersService.selectPapersById(id));
    }

    /**
     * 新增论文
     */
    @PreAuthorize("@ss.hasPermi('papers:papers:add')")
    @Log(title = "论文", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Papers papers)
    {
        papers.setAuthorEmployeeId(papers.getTeacherId());
        papers.setDepartmentId(getDeptId());
        return toAjax(papersService.insertPapers(papers));
    }

    /**
     * 修改论文
     */
    @PreAuthorize("@ss.hasPermi('papers:papers:edit')")
    @Log(title = "论文", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Papers papers)
    {
        papers.setAuthorEmployeeId(papers.getTeacherId());
        return toAjax(papersService.updatePapers(papers));
    }

    /**
     * 删除论文
     */
    @PreAuthorize("@ss.hasPermi('papers:papers:remove')")
    @Log(title = "论文", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(papersService.deletePapersByIds(ids));
    }
}
