package edu.sias.ims.adults.service.impl;

import edu.sias.ims.adults.domain.AdoptedOutcomes;
import edu.sias.ims.adults.domain.CheckProjects;
import edu.sias.ims.adults.mapper.CheckProjectsMapper;
import edu.sias.ims.adults.service.ICheckProjectsService;
import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 鉴定项目Service业务层处理
 * 
 * @author sb
 * @date 2024-07-16
 */
@Service
public class CheckProjectsServiceImpl implements ICheckProjectsService 
{
    @Autowired
    private CheckProjectsMapper checkProjectsMapper;

    private static final Logger log = LoggerFactory.getLogger(CheckProjectsServiceImpl.class);

    /**
     * 查询鉴定项目
     * 
     * @param id 鉴定项目主键
     * @return 鉴定项目
     */
    @Override
    public CheckProjects selectCheckProjectsById(Long id)
    {
        return checkProjectsMapper.selectCheckProjectsById(id);
    }

    /**
     * 查询鉴定项目列表
     * 
     * @param checkProjects 鉴定项目
     * @return 鉴定项目
     */
    @Override
    public List<CheckProjects> selectCheckProjectsList(CheckProjects checkProjects)
    {
        return checkProjectsMapper.selectCheckProjectsList(checkProjects);
    }

    /**
     * 新增鉴定项目
     * 
     * @param checkProjects 鉴定项目
     * @return 结果
     */
    @Override
    public int insertCheckProjects(CheckProjects checkProjects)
    {
        checkProjects.setCreateTime(DateUtils.getNowDate());
        checkProjects.setUpdateTime(DateUtils.getNowDate());
        return checkProjectsMapper.insertCheckProjects(checkProjects);
    }

    /**
     * 修改鉴定项目
     * 
     * @param checkProjects 鉴定项目
     * @return 结果
     */
    @Override
    public int updateCheckProjects(CheckProjects checkProjects)
    {
        checkProjects.setUpdateTime(DateUtils.getNowDate());
        return checkProjectsMapper.updateCheckProjects(checkProjects);
    }

    /**
     * 批量删除鉴定项目
     * 
     * @param ids 需要删除的鉴定项目主键
     * @return 结果
     */
    @Override
    public int deleteCheckProjectsByIds(Long[] ids)
    {
        return checkProjectsMapper.deleteCheckProjectsByIds(ids);
    }

    /**
     * 删除鉴定项目信息
     * 
     * @param id 鉴定项目主键
     * @return 结果
     */
    @Override
    public int deleteCheckProjectsById(Long id)
    {
        return checkProjectsMapper.deleteCheckProjectsById(id);
    }

    @Override
    public String importCheckProjectsService(List<CheckProjects> userList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (CheckProjects checkProjects : userList) {

            try{
                CheckProjects u = checkProjectsMapper.selectCheckProjectsById(checkProjects.getId());
                if(StringUtils.isNull(u)){
                    try {
                        checkProjectsMapper.insertCheckProjects(checkProjects);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、鉴定项目 " + checkProjects.getProjectName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、鉴定项目 " + checkProjects.getProjectName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        checkProjectsMapper.updateCheckProjects(checkProjects);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、鉴定项目 " + checkProjects.getProjectName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、鉴定项目 " + checkProjects.getProjectName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、鉴定项目 " + checkProjects.getProjectName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、鉴定项目 " + checkProjects.getProjectName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
