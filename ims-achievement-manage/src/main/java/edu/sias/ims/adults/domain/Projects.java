package edu.sias.ims.adults.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 项目对象 projects
 * 
 * @author ruoyi
 * @date 2024-07-15
 */
public class Projects extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 立项编号 */
    @Excel(name = "*立项编号")
    private String projectCode;

    /** 项目名称 */
    @Excel(name = "*项目名称")
    private String projectName;

    /** 项目级别（分国家级、省级、厅级、校级、企业5类） */
    @Excel(name = "项目级别")
    private String projectLevel;

    /** 项目来源（指项目发布单位） */
    @Excel(name = "*项目来源")
    private String projectSource;

    /** 主持人工号 */
    @Excel(name = "*主持人工号")
    private String principalInvestigatorId;

    /** 项目成员（限6名，含主持人） */
    @Excel(name = "项目成员")
    private String teamMembers;

    /** 立项时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "*立项时间 yyyy-MM-dd", width = 30, dateFormat = "yyyy-MM-dd")
    private Date initiationDate;

    /** 结项时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结项时间 yyyy-MM-dd", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completionDate;

    /** 附件*（申报材料、立项证书、结项材料、结项证书等） */
//    @Excel(name = "附件*", readConverterExp = "申=报材料、立项证书、结项材料、结项证书等")
    private String attachments;

    /** 创建人 */
    private String createName;

    /** 修改人 */
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long departmentId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;
    @Excel(name = "*教师Id 与主持人工号一致")
    private String teacherId;
    @Excel(name = "*教师姓名")
    @TableField(exist = false)
    private String name;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectCode(String projectCode) 
    {
        this.projectCode = projectCode;
    }

    public String getProjectCode() 
    {
        return projectCode;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setProjectLevel(String projectLevel) 
    {
        this.projectLevel = projectLevel;
    }

    public String getProjectLevel() 
    {
        return projectLevel;
    }
    public void setProjectSource(String projectSource) 
    {
        this.projectSource = projectSource;
    }

    public String getProjectSource() 
    {
        return projectSource;
    }
    public void setPrincipalInvestigatorId(String principalInvestigatorId) 
    {
        this.principalInvestigatorId = principalInvestigatorId;
    }

    public String getPrincipalInvestigatorId() 
    {
        return principalInvestigatorId;
    }
    public void setTeamMembers(String teamMembers) 
    {
        this.teamMembers = teamMembers;
    }

    public String getTeamMembers() 
    {
        return teamMembers;
    }
    public void setInitiationDate(Date initiationDate) 
    {
        this.initiationDate = initiationDate;
    }

    public Date getInitiationDate() 
    {
        return initiationDate;
    }
    public void setCompletionDate(Date completionDate) 
    {
        this.completionDate = completionDate;
    }

    public Date getCompletionDate() 
    {
        return completionDate;
    }
    public void setAttachments(String attachments) 
    {
        this.attachments = attachments;
    }

    public String getAttachments() 
    {
        return attachments;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setDepartmentId(Long departmentId) 
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() 
    {
        return departmentId;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectCode", getProjectCode())
            .append("projectName", getProjectName())
            .append("projectLevel", getProjectLevel())
            .append("projectSource", getProjectSource())
            .append("principalInvestigatorId", getPrincipalInvestigatorId())
            .append("teamMembers", getTeamMembers())
            .append("initiationDate", getInitiationDate())
            .append("completionDate", getCompletionDate())
            .append("attachments", getAttachments())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("departmentId", getDepartmentId())
            .append("remarks", getRemarks())
            .toString();
    }


}
