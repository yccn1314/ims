package edu.sias.ims.adults.service;

import edu.sias.ims.adults.domain.IntellectualProperty;

import java.util.List;

/**
 * 知识产权Service接口
 * 
 * @author sb
 * @date 2024-07-15
 */
public interface IIntellectualPropertyService 
{
    /**
     * 查询知识产权
     * 
     * @param id 知识产权主键
     * @return 知识产权
     */
    public IntellectualProperty selectIntellectualPropertyById(Long id);

    /**
     * 查询知识产权列表
     * 
     * @param intellectualProperty 知识产权
     * @return 知识产权集合
     */
    public List<IntellectualProperty> selectIntellectualPropertyList(IntellectualProperty intellectualProperty);

    /**
     * 新增知识产权
     * 
     * @param intellectualProperty 知识产权
     * @return 结果
     */
    public int insertIntellectualProperty(IntellectualProperty intellectualProperty);

    /**
     * 修改知识产权
     * 
     * @param intellectualProperty 知识产权
     * @return 结果
     */
    public int updateIntellectualProperty(IntellectualProperty intellectualProperty);

    /**
     * 批量删除知识产权
     * 
     * @param ids 需要删除的知识产权主键集合
     * @return 结果
     */
    public int deleteIntellectualPropertyByIds(Long[] ids);

    /**
     * 删除知识产权信息
     * 
     * @param id 知识产权主键
     * @return 结果
     */
    public int deleteIntellectualPropertyById(Long id);

    String importIntellectualProperty(List<IntellectualProperty> userList, boolean updateSupport);
}
