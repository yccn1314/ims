package edu.sias.ims.adults.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 鉴定项目对象 check_projects
 * 
 * @author sb
 * @date 2024-07-16
 */
public class CheckProjects extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 立项编号 */
    @Excel(name = "*立项编号")
    private String projectCode;

    /** 项目名称 */
    @Excel(name = "*项目名称")
    private String projectName;

    /** 项目级别（分国家级、省级、厅级、校级、企业5类） */
    @Excel(name = "项目级别 0~4代表国家级、省级、厅级、校级、企业", readConverterExp = "分国家级、省级、厅级、校级、企业5类")
    private String projectLevel;

    /** 项目来源（指项目发布单位） */
    @Excel(name = "项目来源", readConverterExp = "指项目发布单位")
    private String projectSource;

    /** 主持人工号 */
    @Excel(name = "*主持人工号")
    private String principalInvestigatorId;

    /** 鉴定级别 */
    @Excel(name = "鉴定级别 0~4代表国家级、省级、厅级、校级、企业")
    private String checkGrade;

    /** 鉴定时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "鉴定时间 yyyy-MM-dd", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkTime;

    /** 附件*（含申请材料、鉴定结果证书等） */
    private String attachments;

    /** 创建人 */
    private String creater;

    /** 修改人 */
    private String updater;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long partment;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;
    @Excel(name = "*教师Id 与主持人工号一致")
    private String teacherId;
    @Excel(name = "*教师姓名")
    @TableField(exist = false)
    private String name;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProjectCode(String projectCode) 
    {
        this.projectCode = projectCode;
    }

    public String getProjectCode() 
    {
        return projectCode;
    }
    public void setProjectName(String projectName) 
    {
        this.projectName = projectName;
    }

    public String getProjectName() 
    {
        return projectName;
    }
    public void setProjectLevel(String projectLevel) 
    {
        this.projectLevel = projectLevel;
    }

    public String getProjectLevel() 
    {
        return projectLevel;
    }
    public void setProjectSource(String projectSource) 
    {
        this.projectSource = projectSource;
    }

    public String getProjectSource() 
    {
        return projectSource;
    }
    public void setPrincipalInvestigatorId(String principalInvestigatorId) 
    {
        this.principalInvestigatorId = principalInvestigatorId;
    }

    public String getPrincipalInvestigatorId() 
    {
        return principalInvestigatorId;
    }
    public void setCheckGrade(String checkGrade) 
    {
        this.checkGrade = checkGrade;
    }

    public String getCheckGrade() 
    {
        return checkGrade;
    }
    public void setCheckTime(Date checkTime) 
    {
        this.checkTime = checkTime;
    }

    public Date getCheckTime() 
    {
        return checkTime;
    }
    public void setAttachments(String attachments) 
    {
        this.attachments = attachments;
    }

    public String getAttachments() 
    {
        return attachments;
    }
    public void setCreater(String creater) 
    {
        this.creater = creater;
    }

    public String getCreater() 
    {
        return creater;
    }
    public void setUpdater(String updater) 
    {
        this.updater = updater;
    }

    public String getUpdater() 
    {
        return updater;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setPartment(Long partment) 
    {
        this.partment = partment;
    }

    public Long getPartment() 
    {
        return partment;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("projectCode", getProjectCode())
            .append("projectName", getProjectName())
            .append("projectLevel", getProjectLevel())
            .append("projectSource", getProjectSource())
            .append("principalInvestigatorId", getPrincipalInvestigatorId())
            .append("checkGrade", getCheckGrade())
            .append("checkTime", getCheckTime())
            .append("attachments", getAttachments())
            .append("creater", getCreater())
            .append("createTime", getCreateTime())
            .append("updater", getUpdater())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("partment", getPartment())
            .append("remarks", getRemarks())
            .toString();
    }
}
