package edu.sias.ims.adults.service.impl;



import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.adults.mapper.PapersMapper;
import edu.sias.ims.adults.service.IPapersService;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Validator;
import java.util.List;

/**
 * 论文Service业务层处理
 * 
 * @author summer
 * @date 2024-07-15
 */
@Service
public class PapersServiceImpl implements IPapersService {

    private static final Logger log = LoggerFactory.getLogger(PapersServiceImpl.class);
    @Autowired
    private PapersMapper papersMapper;
    @Autowired
    protected Validator validator;

    /**
     * 查询论文
     *
     * @param id 论文主键
     * @return 论文
     */
    @Override
    public Papers selectPapersById(Long id) {
        return papersMapper.selectPapersById(id);
    }

    /**
     * 查询论文列表
     *
     * @param papers 论文
     * @return 论文
     */
    @Override
    public List<Papers> selectPapersList(Papers papers) {
        return papersMapper.selectPapersList(papers);
    }

    /**
     * 新增论文
     *
     * @param papers 论文
     * @return 结果
     */
    @Override
    public int insertPapers(Papers papers) {
        papers.setCreateTime(DateUtils.getNowDate());
        return papersMapper.insertPapers(papers);
    }

    /**
     * 修改论文
     *
     * @param papers 论文
     * @return 结果
     */
    @Override
    public int updatePapers(Papers papers) {
        papers.setUpdateTime(DateUtils.getNowDate());
        return papersMapper.updatePapers(papers);
    }

    /**
     * 批量删除论文
     *
     * @param ids 需要删除的论文主键
     * @return 结果
     */
    @Override
    public int deletePapersByIds(Long[] ids) {
        return papersMapper.deletePapersByIds(ids);
    }

    /**
     * 删除论文信息
     *
     * @param id 论文主键
     * @return 结果
     */
    @Override
    public int deletePapersById(Long id) {
        return papersMapper.deletePapersById(id);
    }

    /**
     * 导入信息
     * @param paperList
     * @param isUpdateSupport
     * @return
     */
    @Override
    public String importPapers(List<Papers> paperList, Boolean isUpdateSupport) {

        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(paperList) || paperList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (Papers paper : paperList) {

         try{
             Papers u = papersMapper.selectPapersById(paper.getId());
             if(StringUtils.isNull(u)){
                 try {
                     papersMapper.insertPapers(paper);
                     successNum++;
                     successMsg.append("<br/>" + successNum + "、论文 " + paper.getPaperTitle() + " 导入成功");
                 } catch (Exception e) {
                     failureNum++;
                     String msg = "<br/>" + failureNum + "、论文 " + paper.getPaperTitle() + " 导入失败：";
                     failureMsg.append(msg + e.getMessage());
                 }
             }
            else if (isUpdateSupport) {
                try {
                    papersMapper.updatePapers(paper);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、论文 " + paper.getPaperTitle() + " 导入成功");
                } catch (Exception e) {
                    failureNum++;
                    String msg = "<br/>" + failureNum + "、论文 " + paper.getPaperTitle() + " 导入失败：";

                }

            }else{
                 failureNum++;
                 failureMsg.append("<br/>" + failureNum + "、论文 " + paper.getPaperTitle() + " 已存在");
             }
         }catch (Exception e)
    {
        failureNum++;
        String msg = "<br/>" + failureNum + "、论文 " + paper.getPaperTitle() + " 导入失败：";
        failureMsg.append(msg + e.getMessage());
        log.error(msg, e);
    }
        }

            if (failureNum > 0) {
                failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                throw new ServiceException(failureMsg.toString());
            } else {
                successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
            }
            return successMsg.toString();
    }
}
