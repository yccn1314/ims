package edu.sias.ims.adults.controller;

import edu.sias.ims.adults.domain.AdoptedOutcomes;
import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.adults.domain.IntellectualProperty;
import edu.sias.ims.adults.service.IIntellectualPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 知识产权Controller
 * 
 * @author sb
 * @date 2024-07-15
 */
@RestController
@RequestMapping("/intellectualProperty/intellectualProperty")
public class IntellectualPropertyController extends BaseController
{
    @Autowired
    private IIntellectualPropertyService intellectualPropertyService;

    /**
     * 查询知识产权列表
     */
    @PreAuthorize("@ss.hasPermi('intellectualProperty:intellectualProperty:list')")
    @GetMapping("/list")
    public TableDataInfo list(IntellectualProperty intellectualProperty)
    {
        startPage();
        List<IntellectualProperty> list = intellectualPropertyService.selectIntellectualPropertyList(intellectualProperty);
        return getDataTable(list);
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "知识产权导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('intellectualProperty:intellectualProperty:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<IntellectualProperty> util = new ExcelUtil<IntellectualProperty>(IntellectualProperty.class);
        List<IntellectualProperty> userList = util.importExcel(file.getInputStream());
        String message = intellectualPropertyService.importIntellectualProperty(userList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<IntellectualProperty> util = new ExcelUtil<IntellectualProperty>(IntellectualProperty.class);
        util.importTemplateExcel(response, "用户数据");
    }


    /**
     * 导出知识产权列表
     */
    @PreAuthorize("@ss.hasPermi('intellectualProperty:intellectualProperty:export')")
    @Log(title = "知识产权", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, IntellectualProperty intellectualProperty)
    {
        List<IntellectualProperty> list = intellectualPropertyService.selectIntellectualPropertyList(intellectualProperty);
        ExcelUtil<IntellectualProperty> util = new ExcelUtil<IntellectualProperty>(IntellectualProperty.class);
        util.exportExcel(response, list, "知识产权数据");
    }

    /**
     * 获取知识产权详细信息
     */
    @PreAuthorize("@ss.hasPermi('intellectualProperty:intellectualProperty:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(intellectualPropertyService.selectIntellectualPropertyById(id));
    }

    /**
     * 新增知识产权
     */
    @PreAuthorize("@ss.hasPermi('intellectualProperty:intellectualProperty:add')")
    @Log(title = "知识产权", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IntellectualProperty intellectualProperty)
    {
        intellectualProperty.setPrincipalInvestigatorId(intellectualProperty.getTeacherId());
        intellectualProperty.setDepartmentId(getDeptId());
        return toAjax(intellectualPropertyService.insertIntellectualProperty(intellectualProperty));
    }

    /**
     * 修改知识产权
     */
    @PreAuthorize("@ss.hasPermi('intellectualProperty:intellectualProperty:edit')")
    @Log(title = "知识产权", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IntellectualProperty intellectualProperty)
    {
        intellectualProperty.setPrincipalInvestigatorId(intellectualProperty.getTeacherId());
        return toAjax(intellectualPropertyService.updateIntellectualProperty(intellectualProperty));
    }

    /**
     * 删除知识产权
     */
    @PreAuthorize("@ss.hasPermi('intellectualProperty:intellectualProperty:remove')")
    @Log(title = "知识产权", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(intellectualPropertyService.deleteIntellectualPropertyByIds(ids));
    }
}
