package edu.sias.ims.adults.service.impl;

import edu.sias.ims.adults.domain.AdoptedOutcomes;
import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.adults.mapper.AdoptedOutcomesMapper;
import edu.sias.ims.adults.service.IAdoptedOutcomesService;
import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 成果采纳Service业务层处理
 * 
 * @author sb
 * @date 2024-07-15
 */
@Service
public class AdoptedOutcomesServiceImpl implements IAdoptedOutcomesService 
{
    @Autowired
    private AdoptedOutcomesMapper adoptedOutcomesMapper;

    private static final Logger log = LoggerFactory.getLogger(AdoptedOutcomesServiceImpl.class);
    /**
     * 查询成果采纳
     * 
     * @param id 成果采纳主键
     * @return 成果采纳
     */
    @Override
    public AdoptedOutcomes selectAdoptedOutcomesById(Long id)
    {
        return adoptedOutcomesMapper.selectAdoptedOutcomesById(id);
    }

    /**
     * 查询成果采纳列表
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 成果采纳
     */
    @Override
    public List<AdoptedOutcomes> selectAdoptedOutcomesList(AdoptedOutcomes adoptedOutcomes)
    {
        return adoptedOutcomesMapper.selectAdoptedOutcomesList(adoptedOutcomes);
    }

    /**
     * 新增成果采纳
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 结果
     */
    @Override
    public int insertAdoptedOutcomes(AdoptedOutcomes adoptedOutcomes)
    {
        adoptedOutcomes.setCreateTime(DateUtils.getNowDate());
        return adoptedOutcomesMapper.insertAdoptedOutcomes(adoptedOutcomes);
    }

    /**
     * 修改成果采纳
     * 
     * @param adoptedOutcomes 成果采纳
     * @return 结果
     */
    @Override
    public int updateAdoptedOutcomes(AdoptedOutcomes adoptedOutcomes)
    {
        adoptedOutcomes.setUpdateTime(DateUtils.getNowDate());
        return adoptedOutcomesMapper.updateAdoptedOutcomes(adoptedOutcomes);
    }

    /**
     * 批量删除成果采纳
     * 
     * @param ids 需要删除的成果采纳主键
     * @return 结果
     */
    @Override
    public int deleteAdoptedOutcomesByIds(Long[] ids)
    {
        return adoptedOutcomesMapper.deleteAdoptedOutcomesByIds(ids);
    }

    /**
     * 删除成果采纳信息
     * 
     * @param id 成果采纳主键
     * @return 结果
     */
    @Override
    public int deleteAdoptedOutcomesById(Long id)
    {
        return adoptedOutcomesMapper.deleteAdoptedOutcomesById(id);
    }

    @Override
    public String importAdoptedOutcomes(List<AdoptedOutcomes> userList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (AdoptedOutcomes adoptedOutcomes : userList) {

            try{
                AdoptedOutcomes u = adoptedOutcomesMapper.selectAdoptedOutcomesById(adoptedOutcomes.getId());
                if(StringUtils.isNull(u)){
                    try {
                        adoptedOutcomesMapper.insertAdoptedOutcomes(adoptedOutcomes);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、成果采纳 " + adoptedOutcomes.getOutcomeName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、成果采纳 " + adoptedOutcomes.getOutcomeName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        adoptedOutcomesMapper.updateAdoptedOutcomes(adoptedOutcomes);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、成果采纳 " + adoptedOutcomes.getOutcomeName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、成果采纳 " + adoptedOutcomes.getOutcomeName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、成果采纳 " + adoptedOutcomes.getOutcomeName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、成果采纳 " + adoptedOutcomes.getOutcomeName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
