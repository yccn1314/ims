package edu.sias.ims.adults.service;

import edu.sias.ims.adults.domain.Projects;

import java.util.List;

/**
 * 项目Service接口
 * 
 * @author ruoyi
 * @date 2024-07-15
 */
public interface IProjectsService 
{
    /**
     * 查询项目
     * 
     * @param id 项目主键
     * @return 项目
     */
    public Projects selectProjectsById(Long id);

    /**
     * 查询项目列表
     * 
     * @param projects 项目
     * @return 项目集合
     */
    public List<Projects> selectProjectsList(Projects projects);

    /**
     * 新增项目
     * 
     * @param projects 项目
     * @return 结果
     */
    public int insertProjects(Projects projects);

    /**
     * 修改项目
     * 
     * @param projects 项目
     * @return 结果
     */
    public int updateProjects(Projects projects);

    /**
     * 批量删除项目
     * 
     * @param ids 需要删除的项目主键集合
     * @return 结果
     */
    public int deleteProjectsByIds(Long[] ids);

    /**
     * 删除项目信息
     * 
     * @param id 项目主键
     * @return 结果
     */
    public int deleteProjectsById(Long id);

    /**
     * 导入论文
     * @param projectsList
     * @param updateSupport
     * @return
     */
    String importProjects(List<Projects> projectsList, boolean updateSupport);
}
