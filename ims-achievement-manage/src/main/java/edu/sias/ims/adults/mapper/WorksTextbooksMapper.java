package edu.sias.ims.adults.mapper;

import edu.sias.ims.adults.domain.WorksTextbooks;

import java.util.List;

/**
 * 著作教材Mapper接口
 * 
 * @author summer
 * @date 2024-07-15
 */
public interface WorksTextbooksMapper 
{
    /**
     * 查询著作教材
     * 
     * @param id 著作教材主键
     * @return 著作教材
     */
    public WorksTextbooks selectWorksTextbooksById(Long id);

    /**
     * 查询著作教材列表
     * 
     * @param worksTextbooks 著作教材
     * @return 著作教材集合
     */
    public List<WorksTextbooks> selectWorksTextbooksList(WorksTextbooks worksTextbooks);

    /**
     * 新增著作教材
     * 
     * @param worksTextbooks 著作教材
     * @return 结果
     */
    public int insertWorksTextbooks(WorksTextbooks worksTextbooks);

    /**
     * 修改著作教材
     * 
     * @param worksTextbooks 著作教材
     * @return 结果
     */
    public int updateWorksTextbooks(WorksTextbooks worksTextbooks);

    /**
     * 删除著作教材
     * 
     * @param id 著作教材主键
     * @return 结果
     */
    public int deleteWorksTextbooksById(Long id);

    /**
     * 批量删除著作教材
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWorksTextbooksByIds(Long[] ids);
}
