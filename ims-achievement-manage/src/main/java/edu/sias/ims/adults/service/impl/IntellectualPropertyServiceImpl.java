package edu.sias.ims.adults.service.impl;

import edu.sias.ims.adults.domain.CheckProjects;
import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.adults.domain.IntellectualProperty;
import edu.sias.ims.adults.mapper.IntellectualPropertyMapper;
import edu.sias.ims.adults.service.IIntellectualPropertyService;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 知识产权Service业务层处理
 * 
 * @author sb
 * @date 2024-07-15
 */
@Service
public class IntellectualPropertyServiceImpl implements IIntellectualPropertyService 
{
    @Autowired
    private IntellectualPropertyMapper intellectualPropertyMapper;

    private static final Logger log = LoggerFactory.getLogger(IntellectualPropertyServiceImpl.class);

    /**
     * 查询知识产权
     * 
     * @param id 知识产权主键
     * @return 知识产权
     */
    @Override
    public IntellectualProperty selectIntellectualPropertyById(Long id)
    {
        return intellectualPropertyMapper.selectIntellectualPropertyById(id);
    }

    /**
     * 查询知识产权列表
     * 
     * @param intellectualProperty 知识产权
     * @return 知识产权
     */
    @Override
    public List<IntellectualProperty> selectIntellectualPropertyList(IntellectualProperty intellectualProperty)
    {
        return intellectualPropertyMapper.selectIntellectualPropertyList(intellectualProperty);
    }

    /**
     * 新增知识产权
     * 
     * @param intellectualProperty 知识产权
     * @return 结果
     */
    @Override
    public int insertIntellectualProperty(IntellectualProperty intellectualProperty)
    {
        intellectualProperty.setCreateTime(DateUtils.getNowDate());
        return intellectualPropertyMapper.insertIntellectualProperty(intellectualProperty);
    }

    /**
     * 修改知识产权
     * 
     * @param intellectualProperty 知识产权
     * @return 结果
     */
    @Override
    public int updateIntellectualProperty(IntellectualProperty intellectualProperty)
    {
        intellectualProperty.setUpdateTime(DateUtils.getNowDate());
        return intellectualPropertyMapper.updateIntellectualProperty(intellectualProperty);
    }

    /**
     * 批量删除知识产权
     * 
     * @param ids 需要删除的知识产权主键
     * @return 结果
     */
    @Override
    public int deleteIntellectualPropertyByIds(Long[] ids)
    {
        return intellectualPropertyMapper.deleteIntellectualPropertyByIds(ids);
    }

    /**
     * 删除知识产权信息
     * 
     * @param id 知识产权主键
     * @return 结果
     */
    @Override
    public int deleteIntellectualPropertyById(Long id)
    {
        return intellectualPropertyMapper.deleteIntellectualPropertyById(id);
    }

    @Override
    public String importIntellectualProperty(List<IntellectualProperty> userList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (IntellectualProperty intellectualProperty : userList) {

            try{
                IntellectualProperty u = intellectualPropertyMapper.selectIntellectualPropertyById(intellectualProperty.getId());
                if(StringUtils.isNull(u)){
                    try {
                        intellectualPropertyMapper.insertIntellectualProperty(intellectualProperty);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、知识产权 " + intellectualProperty.getAchievementName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、知识产权 " + intellectualProperty.getAchievementName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        intellectualPropertyMapper.updateIntellectualProperty(intellectualProperty);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、知识产权 " + intellectualProperty.getAchievementName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、知识产权 " + intellectualProperty.getAchievementName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、知识产权 " + intellectualProperty.getAchievementName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、知识产权 " + intellectualProperty.getAchievementName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
