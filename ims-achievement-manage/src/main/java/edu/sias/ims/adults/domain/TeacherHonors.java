package edu.sias.ims.adults.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 教师荣誉对象 teacher_honors
 * 
 * @author sb
 * @date 2024-07-15
 */
public class TeacherHonors extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 成果名称 */
    @Excel(name = "*成果名称")
    private String achievementName;

    /** 第一获奖人工号 */
    @Excel(name = "*第一获奖人工号")
    private String primaryRecipientId;

    /** 获奖成员 */
    @Excel(name = "获奖成员")
    private String recipients;

    /** 获奖类别（国家级、省级、厅级、校级、其他） */
    @Excel(name = "*获奖级别")
    private String awardCategory;

    /** 成果形式 */
    @Excel(name = "成果形式")
    private String achievementForm;

//    /** 奖励名称 */
//    @Excel(name = "奖励名称")
//    private String rewardName;

    /** 奖励等级 */
    @Excel(name = "奖励等级")
    private String rewardLevel;

    /** 授奖单位 */
    @Excel(name = "授奖单位")
    private String grantingOrganization;

    /** 证书附件* */
    private String certificateAttachment;

    /** 创建人 */
//    @Excel(name = "创建人")
    private String createName;

    /** 修改人 */
//    @Excel(name = "修改人")
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long departmentId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    @Excel(name = "*教师Id 与第一获奖人工号一致")
    private String teacherId;
    @Excel(name = "*教师姓名")
    @TableField(exist = false)
    private String name;
    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAchievementName(String achievementName) 
    {
        this.achievementName = achievementName;
    }

    public String getAchievementName() 
    {
        return achievementName;
    }
    public void setPrimaryRecipientId(String primaryRecipientId) 
    {
        this.primaryRecipientId = primaryRecipientId;
    }

    public String getPrimaryRecipientId() 
    {
        return primaryRecipientId;
    }
    public void setRecipients(String recipients) 
    {
        this.recipients = recipients;
    }

    public String getRecipients() 
    {
        return recipients;
    }
    public void setAwardCategory(String awardCategory) 
    {
        this.awardCategory = awardCategory;
    }

    public String getAwardCategory() 
    {
        return awardCategory;
    }
    public void setAchievementForm(String achievementForm) 
    {
        this.achievementForm = achievementForm;
    }

    public String getAchievementForm() 
    {
        return achievementForm;
    }
//    public void setRewardName(String rewardName)
//    {
//        this.rewardName = rewardName;
//    }
//
//    public String getRewardName()
//    {
//        return rewardName;
//    }
    public void setRewardLevel(String rewardLevel) 
    {
        this.rewardLevel = rewardLevel;
    }

    public String getRewardLevel() 
    {
        return rewardLevel;
    }
    public void setGrantingOrganization(String grantingOrganization) 
    {
        this.grantingOrganization = grantingOrganization;
    }

    public String getGrantingOrganization() 
    {
        return grantingOrganization;
    }
    public void setCertificateAttachment(String certificateAttachment) 
    {
        this.certificateAttachment = certificateAttachment;
    }

    public String getCertificateAttachment() 
    {
        return certificateAttachment;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setDepartmentId(Long departmentId) 
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() 
    {
        return departmentId;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("achievementName", getAchievementName())
            .append("primaryRecipientId", getPrimaryRecipientId())
            .append("recipients", getRecipients())
            .append("awardCategory", getAwardCategory())
            .append("achievementForm", getAchievementForm())
//            .append("rewardName", getRewardName())
            .append("rewardLevel", getRewardLevel())
            .append("grantingOrganization", getGrantingOrganization())
            .append("certificateAttachment", getCertificateAttachment())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("departmentId", getDepartmentId())
            .append("remarks", getRemarks())
            .toString();
    }
}
