package edu.sias.ims.adults.controller;

import edu.sias.ims.adults.domain.AdoptedOutcomes;
import edu.sias.ims.adults.domain.CheckProjects;
import edu.sias.ims.adults.domain.Papers;
import edu.sias.ims.adults.service.ICheckProjectsService;
import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 鉴定项目Controller
 * 
 * @author sb
 * @date 2024-07-16
 */
@RestController
@RequestMapping("/checkProjects/checkProjects")
public class CheckProjectsController extends BaseController
{
    @Autowired
    private ICheckProjectsService checkProjectsService;

    /**
     * 查询鉴定项目列表
     */
    @PreAuthorize("@ss.hasPermi('checkProjects:checkProjects:list')")
    @GetMapping("/list")
    public TableDataInfo list(CheckProjects checkProjects)
    {
        startPage();
        List<CheckProjects> list = checkProjectsService.selectCheckProjectsList(checkProjects);
        return getDataTable(list);
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "论文导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('checkProjects:checkProjects:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<CheckProjects> util = new ExcelUtil<CheckProjects>(CheckProjects.class);
        List<CheckProjects> userList = util.importExcel(file.getInputStream());
        String message = checkProjectsService.importCheckProjectsService(userList, updateSupport);
        return success(message);
    }

    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<CheckProjects> util = new ExcelUtil<CheckProjects>(CheckProjects.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 导出鉴定项目列表
     */
    @PreAuthorize("@ss.hasPermi('checkProjects:checkProjects:export')")
    @Log(title = "鉴定项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CheckProjects checkProjects)
    {
        List<CheckProjects> list = checkProjectsService.selectCheckProjectsList(checkProjects);
        ExcelUtil<CheckProjects> util = new ExcelUtil<CheckProjects>(CheckProjects.class);
        util.exportExcel(response, list, "鉴定项目数据");
    }

    /**
     * 获取鉴定项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('checkProjects:checkProjects:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(checkProjectsService.selectCheckProjectsById(id));
    }

    /**
     * 新增鉴定项目
     */
    @PreAuthorize("@ss.hasPermi('checkProjects:checkProjects:add')")
    @Log(title = "鉴定项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CheckProjects checkProjects)
    {
        checkProjects.setPrincipalInvestigatorId(checkProjects.getTeacherId());
        checkProjects.setPartment(getDeptId());
        return toAjax(checkProjectsService.insertCheckProjects(checkProjects));
    }

    /**
     * 修改鉴定项目
     */
    @PreAuthorize("@ss.hasPermi('checkProjects:checkProjects:edit')")
    @Log(title = "鉴定项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CheckProjects checkProjects)
    {
        checkProjects.setPrincipalInvestigatorId(checkProjects.getTeacherId());
        return toAjax(checkProjectsService.updateCheckProjects(checkProjects));
    }

    /**
     * 删除鉴定项目
     */
    @PreAuthorize("@ss.hasPermi('checkProjects:checkProjects:remove')")
    @Log(title = "鉴定项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(checkProjectsService.deleteCheckProjectsByIds(ids));
    }
}
