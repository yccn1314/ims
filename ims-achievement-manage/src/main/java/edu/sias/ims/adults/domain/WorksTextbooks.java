package edu.sias.ims.adults.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 著作教材对象 works_textbooks
 * 
 * @author summer
 * @date 2024-07-15
 */
public class WorksTextbooks extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 名称 */
    @Excel(name = "*名称")
    private String title;

    /** 类型（指著作或教材） */
    @Excel(name = "*类型")
    private String type;

    /** 作者工号 */
    @Excel(name = "*作者工号")
    private String authorEmployeeId;

    /** 作者名次（分主编、副主编、参编三类，或第一、第二等） */
    @Excel(name = "作者名次 0~2代表主编、副主编、参编")
    private String authorRole;

    /** 出版社 */
    @Excel(name = "*出版社")
    private String publisher;

    /** 出版日期 */
    private Date publicationDate;

    /** ISBN号 */
    @Excel(name="ISBN号")
    private String isbn;

    /** 全书总字数 */
    @Excel(name = "*全书总字数")
    private String totalWordCount;

    /** 本作者撰写字数 */
    @Excel(name="*本作者撰写字数")
    private String authorWordCount;

    /** 附件*（封面、CIP页、前言、目录） */
//    @Excel(name = "附件*", readConverterExp = "封=面、CIP页、前言、目录")
    private String attachment;

    /** 创建人 */
    private String createName;

    /** 修改人 */
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long departmentId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;
    @Excel(name = "*教师Id 与负责人工号一致")
    private String teacherId;
    @Excel(name = "*教师姓名")
    @TableField(exist = false)
    private String name;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setAuthorEmployeeId(String authorEmployeeId) 
    {
        this.authorEmployeeId = authorEmployeeId;
    }

    public String getAuthorEmployeeId() 
    {
        return authorEmployeeId;
    }
    public void setAuthorRole(String authorRole) 
    {
        this.authorRole = authorRole;
    }

    public String getAuthorRole() 
    {
        return authorRole;
    }
    public void setPublisher(String publisher) 
    {
        this.publisher = publisher;
    }

    public String getPublisher() 
    {
        return publisher;
    }
    public void setPublicationDate(Date publicationDate) 
    {
        this.publicationDate = publicationDate;
    }

    public Date getPublicationDate() 
    {
        return publicationDate;
    }
    public void setIsbn(String isbn) 
    {
        this.isbn = isbn;
    }

    public String getIsbn() 
    {
        return isbn;
    }
    public void setTotalWordCount(String totalWordCount) 
    {
        this.totalWordCount = totalWordCount;
    }

    public String getTotalWordCount() 
    {
        return totalWordCount;
    }
    public void setAuthorWordCount(String authorWordCount) 
    {
        this.authorWordCount = authorWordCount;
    }

    public String getAuthorWordCount() 
    {
        return authorWordCount;
    }
    public void setAttachment(String attachment) 
    {
        this.attachment = attachment;
    }

    public String getAttachment() 
    {
        return attachment;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setDepartmentId(Long departmentId) 
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() 
    {
        return departmentId;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("type", getType())
            .append("authorEmployeeId", getAuthorEmployeeId())
            .append("authorRole", getAuthorRole())
            .append("publisher", getPublisher())
            .append("publicationDate", getPublicationDate())
            .append("isbn", getIsbn())
            .append("totalWordCount", getTotalWordCount())
            .append("authorWordCount", getAuthorWordCount())
            .append("attachment", getAttachment())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("departmentId", getDepartmentId())
            .append("remarks", getRemarks())
            .toString();
    }
}
