package edu.sias.ims.adults.service.impl;

import edu.sias.ims.common.exception.ServiceException;
import edu.sias.ims.common.utils.DateUtils;
import edu.sias.ims.adults.domain.Projects;
import edu.sias.ims.adults.mapper.ProjectsMapper;
import edu.sias.ims.adults.service.IProjectsService;
import edu.sias.ims.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 项目Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-07-15
 */
@Service
public class ProjectsServiceImpl implements IProjectsService 
{
    private static final Logger log = LoggerFactory.getLogger(ProjectsServiceImpl.class);
    @Autowired
    private ProjectsMapper projectsMapper;

    /**
     * 查询项目
     * 
     * @param id 项目主键
     * @return 项目
     */
    @Override
    public Projects selectProjectsById(Long id)
    {
        return projectsMapper.selectProjectsById(id);
    }

    /**
     * 查询项目列表
     * 
     * @param projects 项目
     * @return 项目
     */
    @Override
    public List<Projects> selectProjectsList(Projects projects)
    {
        return projectsMapper.selectProjectsList(projects);
    }

    /**
     * 新增项目
     * 
     * @param projects 项目
     * @return 结果
     */
    @Override
    public int insertProjects(Projects projects)
    {
        projects.setCreateTime(DateUtils.getNowDate());
        return projectsMapper.insertProjects(projects);
    }

    /**
     * 修改项目
     * 
     * @param projects 项目
     * @return 结果
     */
    @Override
    public int updateProjects(Projects projects)
    {
        projects.setUpdateTime(DateUtils.getNowDate());
        return projectsMapper.updateProjects(projects);
    }

    /**
     * 批量删除项目
     * 
     * @param ids 需要删除的项目主键
     * @return 结果
     */
    @Override
    public int deleteProjectsByIds(Long[] ids)
    {
        return projectsMapper.deleteProjectsByIds(ids);
    }

    /**
     * 删除项目信息
     * 
     * @param id 项目主键
     * @return 结果
     */
    @Override
    public int deleteProjectsById(Long id)
    {
        return projectsMapper.deleteProjectsById(id);
    }

    @Override
    public String importProjects(List<Projects> projectsList, boolean updateSupport) {
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        if (StringUtils.isNull(projectsList) || projectsList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        for (Projects projects : projectsList) {

            try{
                Projects u = projectsMapper.selectProjectsById(projects.getId());
                if(StringUtils.isNull(u)){
                    try {
                        projectsMapper.insertProjects(projects);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、项目 " + projects.getProjectName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、项目 " + projects.getProjectName() + " 导入失败：";
                        failureMsg.append(msg + e.getMessage());
                    }
                }
                else if (updateSupport) {
                    try {
                        projectsMapper.updateProjects(projects);
                        successNum++;
                        successMsg.append("<br/>" + successNum + "、项目 " + projects.getProjectName() + " 导入成功");
                    } catch (Exception e) {
                        failureNum++;
                        String msg = "<br/>" + failureNum + "、项目 " + projects.getProjectName() + " 导入失败：";

                    }

                }else{
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、项目 " + projects.getProjectName() + " 已存在");
                }
            }catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、项目 " + projects.getProjectName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }

        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
