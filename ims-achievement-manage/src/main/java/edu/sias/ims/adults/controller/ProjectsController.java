package edu.sias.ims.adults.controller;

import edu.sias.ims.common.annotation.Log;
import edu.sias.ims.common.core.controller.BaseController;
import edu.sias.ims.common.core.domain.AjaxResult;
import edu.sias.ims.common.core.page.TableDataInfo;
import edu.sias.ims.common.enums.BusinessType;
import edu.sias.ims.common.utils.poi.ExcelUtil;
import edu.sias.ims.adults.domain.Projects;
import edu.sias.ims.adults.service.IProjectsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 项目Controller
 * 
 * @author ruoyi
 * @date 2024-07-15
 */
@RestController
@RequestMapping("/project/projects")
public class ProjectsController extends BaseController
{
    @Autowired
    private IProjectsService projectsService;

    /**
     * 查询项目列表
     */
    @PreAuthorize("@ss.hasPermi('project:projects:list')")
    @GetMapping("/list")
    public TableDataInfo list(Projects projects)
    {
        startPage();
        List<Projects> list = projectsService.selectProjectsList(projects);
        return getDataTable(list);
    }

    /**
     * 导出项目列表
     */
    @PreAuthorize("@ss.hasPermi('project:projects:export')")
    @Log(title = "项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Projects projects)
    {
        List<Projects> list = projectsService.selectProjectsList(projects);
        ExcelUtil<Projects> util = new ExcelUtil<Projects>(Projects.class);
        util.exportExcel(response, list, "项目数据");
    }

    /**
     * 获取项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('project:projects:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(projectsService.selectProjectsById(id));
    }

    /**
     * 新增项目
     */
    @PreAuthorize("@ss.hasPermi('project:projects:add')")
    @Log(title = "项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Projects projects)
    {
        projects.setPrincipalInvestigatorId(projects.getTeacherId());
        projects.setDepartmentId(getDeptId());
        return toAjax(projectsService.insertProjects(projects));
    }

    /**
     * 修改项目
     */
    @PreAuthorize("@ss.hasPermi('project:projects:edit')")
    @Log(title = "项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Projects projects)
    {
        projects.setPrincipalInvestigatorId(projects.getTeacherId());
        return toAjax(projectsService.updateProjects(projects));
    }


    /**
     * 模块
     * @param response
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<Projects> util = new ExcelUtil<Projects>(Projects.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 导入
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "项目导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('project:projects:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Projects> util = new ExcelUtil<Projects>(Projects.class);
        List<Projects> projectsList = util.importExcel(file.getInputStream());
        String message = projectsService.importProjects(projectsList, updateSupport);
        return success(message);
    }
    /**
     * 删除项目
     */
    @PreAuthorize("@ss.hasPermi('project:projects:remove')")
    @Log(title = "项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(projectsService.deleteProjectsByIds(ids));
    }
}
