package edu.sias.ims.adults.mapper;

import edu.sias.ims.adults.domain.Projects;

import java.util.List;

/**
 * 项目Mapper接口
 * 
 * @author ruoyi
 * @date 2024-07-15
 */
public interface ProjectsMapper 
{
    /**
     * 查询项目
     * 
     * @param id 项目主键
     * @return 项目
     */
    public Projects selectProjectsById(Long id);

    /**
     * 查询项目列表
     * 
     * @param projects 项目
     * @return 项目集合
     */
    public List<Projects> selectProjectsList(Projects projects);

    /**
     * 新增项目
     * 
     * @param projects 项目
     * @return 结果
     */
    public int insertProjects(Projects projects);

    /**
     * 修改项目
     * 
     * @param projects 项目
     * @return 结果
     */
    public int updateProjects(Projects projects);

    /**
     * 删除项目
     * 
     * @param id 项目主键
     * @return 结果
     */
    public int deleteProjectsById(Long id);

    /**
     * 批量删除项目
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProjectsByIds(Long[] ids);
}
