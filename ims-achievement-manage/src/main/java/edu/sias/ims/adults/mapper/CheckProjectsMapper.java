package edu.sias.ims.adults.mapper;

import edu.sias.ims.adults.domain.CheckProjects;

import java.util.List;

/**
 * 鉴定项目Mapper接口
 * 
 * @author sb
 * @date 2024-07-16
 */
public interface CheckProjectsMapper 
{
    /**
     * 查询鉴定项目
     * 
     * @param id 鉴定项目主键
     * @return 鉴定项目
     */
    public CheckProjects selectCheckProjectsById(Long id);

    /**
     * 查询鉴定项目列表
     * 
     * @param checkProjects 鉴定项目
     * @return 鉴定项目集合
     */
    public List<CheckProjects> selectCheckProjectsList(CheckProjects checkProjects);

    /**
     * 新增鉴定项目
     * 
     * @param checkProjects 鉴定项目
     * @return 结果
     */
    public int insertCheckProjects(CheckProjects checkProjects);

    /**
     * 修改鉴定项目
     * 
     * @param checkProjects 鉴定项目
     * @return 结果
     */
    public int updateCheckProjects(CheckProjects checkProjects);

    /**
     * 删除鉴定项目
     * 
     * @param id 鉴定项目主键
     * @return 结果
     */
    public int deleteCheckProjectsById(Long id);

    /**
     * 批量删除鉴定项目
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCheckProjectsByIds(Long[] ids);
}
