package edu.sias.ims.adults.mapper;

import edu.sias.ims.adults.domain.TeacherHonors;

import java.util.List;

/**
 * 教师荣誉Mapper接口
 * 
 * @author sb
 * @date 2024-07-15
 */
public interface TeacherHonorsMapper 
{
    /**
     * 查询教师荣誉
     * 
     * @param id 教师荣誉主键
     * @return 教师荣誉
     */
    public TeacherHonors selectTeacherHonorsById(Long id);

    /**
     * 查询教师荣誉列表
     * 
     * @param teacherHonors 教师荣誉
     * @return 教师荣誉集合
     */
    public List<TeacherHonors> selectTeacherHonorsList(TeacherHonors teacherHonors);

    /**
     * 新增教师荣誉
     * 
     * @param teacherHonors 教师荣誉
     * @return 结果
     */
    public int insertTeacherHonors(TeacherHonors teacherHonors);

    /**
     * 修改教师荣誉
     * 
     * @param teacherHonors 教师荣誉
     * @return 结果
     */
    public int updateTeacherHonors(TeacherHonors teacherHonors);

    /**
     * 删除教师荣誉
     * 
     * @param id 教师荣誉主键
     * @return 结果
     */
    public int deleteTeacherHonorsById(Long id);

    /**
     * 批量删除教师荣誉
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTeacherHonorsByIds(Long[] ids);
}
