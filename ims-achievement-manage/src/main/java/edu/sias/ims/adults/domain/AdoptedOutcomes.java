package edu.sias.ims.adults.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.sias.ims.common.annotation.Excel;
import edu.sias.ims.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 成果采纳对象 adopted_outcomes
 * 
 * @author sb
 * @date 2024-07-15
 */
public class AdoptedOutcomes extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 成果名称 */
    @Excel(name = "*成果名称")
    private String outcomeName;

    /** 负责人工号 */
    @Excel(name = "*负责人工号")
    private String principalInvestigatorId;

    /** 采纳单位 */
    @Excel(name = "采纳单位")
    private String adoptedBy;

    /** 采纳日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "采纳日期 yyyy-MM-dd", width = 30, dateFormat = "yyyy-MM-dd")
    private Date adoptionDate;

    /** 采纳证明* */
    private String proofAttachment;

    /** 创建人 */
//    @Excel(name = "创建人")
    private String createName;

    /** 修改人 */
//    @Excel(name = "修改人")
    private String updateName;

    /** 删除标记0未删除1已删除 */
    private Long deleteFlag;

    /** 部门id */
    private Long departmentId;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;
    @Excel(name = "*教师Id 与负责人工号一致")
    private String teacherId;
    @Excel(name = "*教师姓名")
    @TableField(exist = false)
    private String name;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOutcomeName(String outcomeName) 
    {
        this.outcomeName = outcomeName;
    }

    public String getOutcomeName() 
    {
        return outcomeName;
    }
    public void setPrincipalInvestigatorId(String principalInvestigatorId) 
    {
        this.principalInvestigatorId = principalInvestigatorId;
    }

    public String getPrincipalInvestigatorId() 
    {
        return principalInvestigatorId;
    }
    public void setAdoptedBy(String adoptedBy) 
    {
        this.adoptedBy = adoptedBy;
    }

    public String getAdoptedBy() 
    {
        return adoptedBy;
    }
    public void setAdoptionDate(Date adoptionDate) 
    {
        this.adoptionDate = adoptionDate;
    }

    public Date getAdoptionDate() 
    {
        return adoptionDate;
    }
    public void setProofAttachment(String proofAttachment) 
    {
        this.proofAttachment = proofAttachment;
    }

    public String getProofAttachment() 
    {
        return proofAttachment;
    }
    public void setCreateName(String createName) 
    {
        this.createName = createName;
    }

    public String getCreateName() 
    {
        return createName;
    }
    public void setUpdateName(String updateName) 
    {
        this.updateName = updateName;
    }

    public String getUpdateName() 
    {
        return updateName;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setDepartmentId(Long departmentId) 
    {
        this.departmentId = departmentId;
    }

    public Long getDepartmentId() 
    {
        return departmentId;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("outcomeName", getOutcomeName())
            .append("principalInvestigatorId", getPrincipalInvestigatorId())
            .append("adoptedBy", getAdoptedBy())
            .append("adoptionDate", getAdoptionDate())
            .append("proofAttachment", getProofAttachment())
            .append("createName", getCreateName())
            .append("createTime", getCreateTime())
            .append("updateName", getUpdateName())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("departmentId", getDepartmentId())
            .append("remarks", getRemarks())
            .toString();
    }
}
